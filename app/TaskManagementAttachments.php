<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class TaskManagementAttachments extends Eloquent
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = "task_management_attachments";
    protected $fillable = [
        'task_id', 'file_name', 'user_type', 'user_id'
    ];
}
