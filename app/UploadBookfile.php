<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class UploadBookfile extends Eloquent
{
    use SoftDeletes;
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "upload_bookfiles";
    protected $fillable = [
        'ext','file_name'
    ];
    
    public function getBookId(){
        return $this->belongsTo('App\UploadedBook', 'uploaded_book_id','_id');
    }
}
