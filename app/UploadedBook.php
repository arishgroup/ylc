<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class UploadedBook extends Eloquent
{
    use SoftDeletes;
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "uploaded_books";
    protected $fillable = [
        'name', 'description', 'location_city', 'location_state', 'year'
    ];
    
    public function getAttachments (){
        return $this->belongsToMany('App\UploadBookfile', '_id', 'uploaded_book_id');
    }
}
