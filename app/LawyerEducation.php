<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent; 
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class LawyerEducation extends Eloquent
{
    use SoftDeletes;
    protected $table = "lawyer_educations";
    protected $fillable = [ 'institute', 'degree', 'from_year', 'to_year',
                             'grade', 'hobbies', 'description'   
    ];
    protected $dates = ['deleted_at'];
    
    
    public function lawyer(){
        return $this->belongsTo('App\LawyerRegistration', 'lawyer_id', '_id');
    }
}
