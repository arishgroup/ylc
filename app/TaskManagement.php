<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;


class TaskManagement extends Eloquent
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = "task_management";
    protected $fillable = [
        'assignee_type', 'assignee_id', 'task_name','task_description','assigner_start_date','assigner_end_date','assignee_start_date',
        'assignee_end_date','priority','status','assigner_id','assigner_table','assignee_table'
    ];


    public function getTaskComments(){
        return $this->belongsToMany('App\TaskManagementComments', '_id', 'task_id');
    }

    public function getTaskAttachments(){
        return $this->belongsToMany('App\TaskManagementAttachments', '_id', 'task_id');
    }

    public function getTaskLawyer(){
        return $this->belongsTo('App\LawyerRegistration', 'assignee_id', '_id');
    }

    public function getTaskUser(){
        return $this->belongsTo('App\BackendUsers', 'assignee_id', '_id');
    }

    public function getTaskAdmin(){
        return $this->belongsTo('App\Admin', 'assigner_id', '_id');
    }
}
