<?php

namespace App;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Role extends Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "roles";
    protected $fillable = [
        'name', 'description'
    ];
}
