<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Lawyer extends Authenticatable
{
    use Notifiable;
    
    protected $table = "lawyers";
    
    protected $guard='lawyer';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname','lastname','email', 'role','designation','aos', 'serial_no', 'public_serial_no'
    ];
    
    public function aos(){
        return $this->hasMany(AreaOfSevice::class,'aos','_id');
    }
    
}
