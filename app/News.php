<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class News extends Eloquent
{
    use SoftDeletes;

    protected $fillable = [
        'title','description','link','broadcast','audience'
    ];


    public function aos()
    {
        return $this->belongsToMany('App\AreaOfSevice');
    }

    public function group()
    {
        return $this->belongsToMany('App\LawyerRegistration');
    }

}
