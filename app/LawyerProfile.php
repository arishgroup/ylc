<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class LawyerProfile extends Eloquent
{
    use SoftDeletes;
    protected $table = "lawyer_profiles";
    protected $fillable = [ 'first_name', 'last_name', 'mobile', 'nic',
        'email', 'aos_id', 'location','office_address','home_address','experiance','notes'
    ];
    protected $dates = ['deleted_at'];
    
    public function aos(){
        return $this->belongsTo('App\AreaOfSevice','aos_id','_id');
    }
    
    public function eduDetail(){
        return $this->hasMany('App\LawyersProfileEducation');
    }
}
