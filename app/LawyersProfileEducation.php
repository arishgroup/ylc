<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class LawyersProfileEducation extends Eloquent
{
    use SoftDeletes;
    protected $table = "lawyers_profile_educations";
    protected $fillable = [ 'institute', 'degree', 'from_year', 'to_year',
        'grade', 'hobbies', 'description'
    ];
    protected $dates = ['deleted_at'];
    
    
    public function lawyerProfile(){
        return $this->belongsTo('App\LawyerProfile', 'lawyer_profile_id', '_id');
    }
}
