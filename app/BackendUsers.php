<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class BackendUsers extends Authenticatable
{
    use Notifiable;
  
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "backend_users";
    protected $fillable = [ 'name','last_name',  'user_address', 'nic','mobile_no','serial_no','email','user_type','alocated_court','location'];
    
    public function get_location (){
        return $this->belongsTo('App\Location', 'location' , '_id');
    }
    
    public function get_court(){
        return $this->belongsTo('App\Court', 'alocated_court' , '_id');
    }
    
    public function getUserType (){
        return $this->belongsTo('App\UserType', 'user_type', '_id');
    }
}
