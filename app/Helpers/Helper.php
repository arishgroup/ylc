<?php 
namespace App\Helpers;
use Request;
use App\LogActivity as LogActivityModel;
use App\Helper;
use App\AreaOfSevice;


class Helper
{
    
    public static function categoryTree($parent_id = '0', $sub_mark = ''){
        
            $aos = AreaOfSevice::orderBy('_id')->where('parent_id',$parent_id)->get();
            
            foreach($aos as $value){
                $aos_sel[$value->_id] =  $sub_mark.$value->name;
                categoryTree($value->_id, $sub_mark.'---');
            }
            return $aos_sel;
    }
}

?>