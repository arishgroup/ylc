<?php

namespace App\Helpers;
use Request;
use App\LogActivity as LogActivityModel;
use App\Helper;
use Illuminate\Support\Carbon;


class LogActivity
{
    
    
    public static function addToLog($log_type, $subject, $content_model)
    {
        //return true;
        $helper = new Helper();
        $user_type = $helper->getUsertype();
        
        $log = [];
        $log['subject'] = $subject;
        $log['log_name'] = $log_type;
        $log['causer_id'] = $content_model->_id;
        $log['causer_type'] = 'App\\'.class_basename($content_model);
        $log['url'] = Request::fullUrl();
        $log['method'] = Request::method();
        $log['ip'] = Request::ip();
        $log['agent'] = Request::header('user-agent');
        $log['user_type'] = $user_type;
        if($user_type == 'user'){
            $log['user_id'] = auth()->guard('user')->user()->id;
        }
        if($user_type == 'lawyer'){
            $log['user_id'] = auth()->guard('lawyer')->user()->id;
        }
        
        LogActivityModel::create($log);
    }
    public static function addToLogtest($log_type, $subject, $content_model)
    {
        //dd(auth()->guard('user')->user()->id);
        $helper = new Helper();
        $user_type = $helper->getUsertype();
        
        $log = [];
        $log['subject'] = $subject;
        $log['log_name'] = $log_type;
        $log['causer_id'] = $content_model->_id;
        $log['causer_type'] = 'App\\'.class_basename($content_model);
        $log['url'] = Request::fullUrl();
        $log['method'] = Request::method();
        $log['ip'] = Request::ip();
        $log['agent'] = Request::header('user-agent');
        $log['user_type'] = $user_type;
        if($user_type == 'user'){
            $log['user_id'] = auth()->guard('user')->user()->id;
        }
        if($user_type == 'lawyer'){
            $log['user_id'] = auth()->guard('lawyer')->user()->id;
        }
        
        LogActivityModel::create($log);
    }
    
    public static function logActivityLists($id='0',$type='',$request)
    {
        $date = Carbon::now()->subDays(10);
        
        //$date = date('Y-m-d' ,strtotime(Carbon::now()->subMonth()));
        //$date = '2018-11-09';
        $dataLog = LogActivityModel::where('user_id','like',$id)
        ->where('user_type','like',$type);
        
        if($request->start_date){
            $year = date('Y',strtotime($request->start_date)); 
            $month=date('m',strtotime($request->start_date));
            $day=date('d',strtotime($request->start_date));
            
            $date = Carbon::createFromDate($year, $month, $day);
            $rs_date = $date;
            $dataLog->where('created_at', '>', $rs_date);
        } 
        if($request->end_date){
            $year = date('Y',strtotime($request->end_date));
            $month=date('m',strtotime($request->end_date));
            $day=date('d',strtotime($request->end_date));
            
            $date = Carbon::createFromDate($year, $month, $day);
            $rs_date = $date;
            $dataLog->where('created_at', '<', $rs_date);
        } 
        
        if($request->start_date == 'null'  && $request->end_date == 'null') {
            $dataLog->where('created_at', '>=', $date);
        }
        //$rs = $dataLog->toSql();
        $rs = $dataLog->latest()->get();
        //dd($dataLog);
        return $rs;
    }
    
    public static function logActivityListsAll()
    {
        
        return LogActivityModel::latest()->get();
    }
    
    
}
