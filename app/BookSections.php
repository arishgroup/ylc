<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class BookSections extends Eloquent
{
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "book_sections";
    protected $fillable = [
        'definition', 'section_no', 'content_read'
    ];
    
    public function bookSection(){
        return $this->belongsTo('App\Book', 'book_id', '_id')->withTrashed();
    }
    
    public function caseLaws()
    {
        return $this->belongsToMany('App\ContentLibrary');
    }
}
