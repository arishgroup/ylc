<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Court extends Eloquent
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "courts";
    protected $fillable = [
        'name', 'description'
    ];
}
