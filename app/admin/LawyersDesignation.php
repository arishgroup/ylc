<?php

namespace App\admin;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class LawyersDesignation extends Eloquent
{
    Protected $table = "lawyersDesgination";
    protected $fillable = [ 'name', 'description' ];
}
