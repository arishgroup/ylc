<?php

namespace App\admin;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class LawyerTeam extends Eloquent
{
    use SoftDeletes;
    //
    protected $dates = ['deleted_at'];
    
    protected $table = "lawyer_teams";
    protected $fillable = [
        'title', 'description', 'aos_id','team_head'
    ];
    
    public function getLawyers(){
         return $this->belongsToMany('App\LawyerRegistration');
    }
    public function getLawyer(){
        return $this->belongsTo('App\LawyerRegistration', 'team_head' , '_id');
    }
    public function getAOS(){
        return $this->belongsTo('App\AreaOfSevice', 'aos_id' , '_id');
    }
}
