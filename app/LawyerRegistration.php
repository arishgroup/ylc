<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Relations\HasMany;

class LawyerRegistration extends Authenticatable
{
    use Notifiable;
    protected $guard='lawyer';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','lastname','email', 'role','designation','area_of_service', 'serial_no', 'public_serial_no'
    ];
    
    public function aos(){
        return $this->belongsTo('App\AreaOfSevice','area_of_service', '_id');
    }
    
    public function getDesignation (){
        return $this->belongsTo('App\admin\LawyersDesignation', 'designation', '_id');
    }
    
    public function getLocation (){
        return $this->belongsTo('App\Location','location_id','_id');
    }

    public function getTeams (){
        return $this->belongsToMany('App\admin\LawyerTeam','_id','team_head');
    }

    public function getLog(){
        return $this->belongsToMany('App\LogActivity','_id','user_id');
    }

}
