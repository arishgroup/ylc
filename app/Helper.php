<?php 
namespace App;
use Auth;
class Helper {
	
    private $aos_sel = array ();
    private $tree_li = '';
    
    /**
     * Display the Area of services.
     *
     * @param  int  $parent_id
     * @return \Illuminate\Http\Response
     */
    function categoryTree($parent_id = '0', $sub_mark = ''){
        
        $aos = AreaOfSevice::orderBy('_id')->where('parent_id',$parent_id)->get();
        
        foreach($aos as $value){
            $this->aos_sel[$value->_id] =  $sub_mark.$value->name;
            $this->categoryTree($value->_id, $sub_mark.'---');
        }
        return $this->aos_sel;
    }
    
    function categoryTreeWithOutSubMark($parent_id = '0', $sub_mark = ''){
        
        $aos = AreaOfSevice::orderBy('_id')->where('parent_id',$parent_id)->get();
        
        foreach($aos as $value){
            $this->aos_sel[$value->_id] =  $sub_mark.$value->name;
            $this->categoryTreeWithOutSubMark($value->_id, $sub_mark);
        }
        return $this->aos_sel;
    }
    
    /**
     * Get Area of services for the Lawyers.
     *
     * @param  int  $parent_id
     * @return \Illuminate\Http\Response
     */
    
    function getLawyerAos($parent_id = '0'){
        
        $aos = AreaOfSevice::orderBy('_id')->where('parent_id',$parent_id)->get();
        //
        foreach($aos as $value){
            $this->aos_sel[] =  $value;
            $this->getLawyerAos($value->_id);
        }
        
        return $this->aos_sel;
    }
    
    function getLawyerAosByid($parent_id = '0', $law_id=0){
        $lawyer_id = $law_id;
        if($law_id == 0 ){
            $lawyer_id = Auth::user()->_id;
        }
        
        $aos = LawyerAos::orderBy('_id')
                            ->where('parent_id',$parent_id)
                            ->where('lawyer_id','like',$lawyer_id)
                            ->get();
        //dd($aos);
        foreach($aos as $value){
            $this->aos_sel[] =  $value;
            $this->getLawyerAosByid($value->_id, $lawyer_id);
        }
        
        return $this->aos_sel;
    }
    function getLawyerAosByidDD($parent_id = '0', $sub_mark = ''){
        
        $lawyer_id = Auth::user()->_id;
        $aos = LawyerAos::orderBy('_id')
                            ->where('parent_id',$parent_id)
                            ->where('lawyer_id','like',$lawyer_id)
                            ->get();
        
        foreach($aos as $value){
            $this->aos_sel[$value->_id] =  $sub_mark.$value->name;
            $this->getLawyerAosByidDD($value->_id, $sub_mark.'---');
        }
        return $this->aos_sel;
    }
    
    function getChild($parent_id = '0', $law_id=0){
        $aos = LawyerAos::orderBy('_id')
        ->where('parent_id',$parent_id)
        ->where('lawyer_id','like',$law_id)
        ->get();
        
        return $aos->count();
    }
    private $items = array();
    function getLawyerAosByidHTML($parent_id = '0', $law_id=0, $sub_mark = '<ul class="dl-menu"> <li>', $pid='0'){
        
        $lawyer_id = $law_id;
        if($law_id == 0 ){
            $lawyer_id = Auth::user()->_id;
        }
       
        $aos = LawyerAos::orderBy('_id')
        ->where('parent_id',$parent_id)
        ->where('lawyer_id','like',$lawyer_id)
        ->get();
        if($aos->count()){
            foreach($aos as $key => $value){
                $this->items[$value->_id] = array("parent_id" => $value->parent_id, "name" => $value->name, "hasChild" => $this->getChild($value->_id,$lawyer_id )); 
                $this->getLawyerAosByidHTML($value->_id,$lawyer_id, '<ul class="dl-submenu"><li>',$value->parent_id);
            }
        }
        
        
        return $this->items;
    }
    
    function getAOSChild($parent_id = '0'){
        $aos = AreaOfSevice::orderBy('_id')
        ->where('parent_id',$parent_id)
        ->get();
        return $aos->count();
    }
    
    function getAdminAosByidHTML($parent_id = '0', $sub_mark = '<ul class="dl-menu"> <li>'){

        $aos = AreaOfSevice::orderBy('_id')
        ->where('parent_id',$parent_id)
        ->get();
        if($aos->count()){
            foreach($aos as $key => $value){
                $this->items[$value->_id] = array("parent_id" => $value->parent_id, "name" => $value->name, "hasChild" => $this->getAOSChild($value->_id));
                $this->getAdminAosByidHTML($value->_id, '<ul class="dl-submenu"><li>');
            }
        }
        
        
        return $this->items;
    }
    
    function getUsertype(){
        $user_type = '';
        if(Auth::guard('admin')->check())
            $user_type = 'admin';
        
        if(Auth::guard('lawyer')->check())
            $user_type = 'lawyer';
        
        if(Auth::guard('user')->check())
            $user_type = 'user';
            
            return $user_type;
    }

    function getCommentsHtml($result){
        $comment = '<ul class="timeline">';
        if(!empty($result->getTaskComments[0]['comment'])) {

            foreach ($result->getTaskComments->sortByDesc('created_at') as $taskComment){
                if($taskComment->user_type =="admin") {
                    if ($taskComment->user_id) {
                        $user_type = $taskComment->getTaskAdmin->name;
                    }
                }
                elseif($taskComment->user_type =="lawyer") {
                    if ($taskComment->user_id) {
                        $user_type = $taskComment->getTaskLawyer->name;
                    }
                }
                elseif($taskComment->user_type =="user"){
                    if($taskComment->user_id) {
                        $user_type = $taskComment->getTaskUser->name;
                    }
                }
                $comment .= '<li>
                        <i class="fa fa-comments bg-yellow"></i>
                        <div class="timeline-item panel-default">
                            <span class="time"><i class="fa fa-clock-o"></i>'.$taskComment["created_at"]->diffForHumans().'</span>
                            <h5 class="panel-heading no-margin"><a href="#">'.$user_type.'
                                </a> commented on your post</h5>
                            <div class="timeline-body">'.
                    $taskComment->comment.'
                            </div>
                        </div>
                    </li>';
            }
        }
        else{
            $comment .='<li><i class="fa fa-comments bg-yellow"></i>
                            <div class="timeline-item">
                                <div class="panel-heading">
                                    No Comment Yet
                                </div>
                            </div>
                        </li>';
        }

        $comment .='<li> <i class="fa fa-clock-o bg-gray"></i></li></ul>';
        return $comment;
    }

    function getAttachmentsHtml($result){

        $div_loop=0;
        $attachment = '<div class="row box-header">';
        if(!empty($result->getTaskAttachments[0]['_id'])) {

            foreach ($result->getTaskAttachments as $taskAttachments){
                if($div_loop%4 == 0 && $div_loop!=0){
                    $attachment .="</div><div class='row box-header'>";
                }

                if($taskAttachments->file_extension == "docx" || $taskAttachments->file_extension == "xlsx" || $taskAttachments->file_extension == "pptx"){
                    $img_path = $path = asset("storage/taskmanagement/office.jpg");;
                    $path = asset("storage/taskmanagement/$taskAttachments->_id.$taskAttachments->file_extension");
                }
                else if($taskAttachments->file_extension == "pdf"){
                    $img_path = asset("storage/taskmanagement/pdf.jpg");
                    $path = asset("storage/taskmanagement/$taskAttachments->_id.$taskAttachments->file_extension");
                    $path = route('show.pdf', [encrypt($path)]);
                }
                else{
                    $img_path = $path =  asset("storage/taskmanagement/$taskAttachments->_id.$taskAttachments->file_extension");
                }
                $attachment .= "<div class='col-sm-3'>
                                    <img src='$img_path' alt='attachments' height='25' width='25'>
                                    <a target='_blank' href='$path' >$taskAttachments->file_name</a></div>";
                ++$div_loop;
            }

        }
        else{
            $attachment .="<div class='col-sm-11'>
                                <div class='panel'>
                                    <div class='panel-heading'>
                                        No File attached Yet
                                    </div>
                                </div>
                            </div>";
        }

        $attachment .="</div>";
        return $attachment;
    }

    function getPriority($id){
        if(empty($id))
        {
            return "";
        }
        else{
            $int = (int)$id;
            $list= Config::get('constants.priority');
            return $list[$int];
        }

    }

    function getFormattedDate($date_string, $date_format="Y-m-d"){
        if(empty($date_string)){
            $return_string ="";
        }
        else{
            $return_string =date($date_format, strtotime($date_string));
        }
            return $return_string;

    }
}