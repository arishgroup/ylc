<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class Book extends Eloquent
{
    use SoftDeletes;
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "books";
    protected $fillable = [
        'name', 'description', 'type'
    ];
    
    public function bookTypes(){
        return $this->belongsTo('App\BookType', 'type', '_id');
    }
    
    public function getTags (){
        return $this->belongsToMany('App\Tag', 'book_tag', 'tag_id', 'statute_id');
    }
}
