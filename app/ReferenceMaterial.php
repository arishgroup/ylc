<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class ReferenceMaterial extends Eloquent
{
    use SoftDeletes;
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "reference_materials";
    protected $fillable = [ 'area_of_service_id', 'user_id', 'title', 'detail_desciption', 'tags' ];
    
    public function getAreaOfService(){
        return $this->belongsTo('App\AreaOfSevice','area_of_service_id' ,'_id');
    }
}
