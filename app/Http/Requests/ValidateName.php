<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidateName extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|regex:/(^[a-zA-Z ()-\/]+$)+/'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Title is required.',
            'name.min' =>'Title must be at least 3 characters.',
            'name.regex' =>'Title may only contain letters.'
        ];
    }
}
