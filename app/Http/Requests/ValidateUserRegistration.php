<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidateUserRegistration extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        return [
            'name' => 'required|min:3|alpha',
            'lastname' => 'required|min:3|alpha',
            'nic' => 'required|digits:13',
            'mobile_no' => 'required|digits:11',
            'user_type' => 'required|not_in:0',
            'email' => 'required|email|unique:backend_users,'.$this->id,
            'password' => 'required_with:confirmed|same:confirmed',
            'nic_img' => 'mimes:jpeg,jpg,pdf',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'First Name is required.',
            'name.min' =>'First Name must be at least 3 characters.',
            'name.alpha' =>'First Name may only contain letters.',
            'lastname.required' => 'Last Name is required.',
            'lastname.min' =>'Last Name must be at least 3 characters.',
            'lastname.alpha' =>'Last Name may only contain letters.',
            'utype.required' => 'User Type is required'
        ];
    }
}
