<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidateJudgeProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|regex:/(^[a-zA-Z ]+$)+/',
            'image' => 'required|mimes:png,jpg,jpeg',
            'designation' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'First Name is required.',
            'name.min' =>'First Name must be at least 3 characters.',
            'name.regex' =>'First Name may only contain letters.',
        ];
    }
}
