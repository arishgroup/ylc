<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidateDepartment extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        return [
            'dept_name' => 'required|min:4|regex:/(^[a-zA-Z0-9 _-]+$)+/',
            'longitude' => 'required',
            'latitude' => 'required',
        ];
    }
    
    public function messages()
    {
        return [
            'dept_name.required' => 'Department name is a required field.',
            'dept_name.min' =>'Department name must be at least 4 characters.',
            'dept_name.regex' =>'Department name can only contain alphanumeric characters and -_ and spaces.',
            'longitude.required' => 'Longitude is a required field.',
            'longitude.regex' => 'Longitude was not a correct number.',
            'latitude.required' => 'Latitude is a required field.',
            'latitude.regex' => 'Latitude was not a correct number.',
        ];
    }
}
