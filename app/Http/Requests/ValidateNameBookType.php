<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidateNameBookType extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_book_type' => 'required|min:3|regex:/(^[a-zA-Z ]+$)+/'
        ];
    }

    public function messages()
    {
        return [
            'name_book_type.required' => 'Title is required.',
            'name_book_type.min' =>'Title must be at least 3 characters.',
            'name_book_type.regex' =>'Title may only contain letters.'
        ];
    }
}
