<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidateStatue extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => 'mimes:pdf',
            
            
        ];
        // 'book_title' => 'required|min:3|regex:/(^[a-zA-Z0-9 ,-()]+$)+/', 'type_id' => 'required|not_in:0',
    }

    public function messages()
    {
        return [
            'book_title.min' =>'Name must be at least 3 characters.',
            'book_title.regex' =>'Only Alphabets and spaces allow'
        ];
    }
}
