<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidateLawyerRegistration extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|regex:/(^[a-zA-Z. -]+$)+/',
            'lastname' => 'required|min:3|regex:/(^[a-zA-Z. -]+$)+/',
            'nic' => 'required|digits:13',
            'nic_img' => 'mimes:pdf',
            'serial_no' => 'required',
            'password' => 'required_with:confirmed|same:confirm_password',
            'bar_license_number' => 'required',
            'email' => 'required|email',
            'bar_license_number_image' => 'mimes:pdf',
            'high_court_license_image' => 'mimes:pdf',
            'sup_court_license_image' => 'mimes:pdf'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'First Name is required.',
            'name.min' =>'First Name must be at least 3 characters.',
            'name.regex' =>'First Name may only contain letters.',
            'lastname.required' => 'Last Name is required.',
            'lastname.min' =>'Last Name must be at least 3 characters.',
            'lastname.regex' =>'Last Name may only contain letters.',
        ];
    }
}
