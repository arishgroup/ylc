<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidateTaskManagementSystem extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'assignee_type' => 'required|not_in:0',
            'assignee_id' => 'required|not_in:0',
            'task_name' => 'required|regex:/(^[a-zA-Z0-9 ()-]+$)+/',
            'task_description' => 'required',
//            'assigner_start_date' => 'date',
//            'assigner_end_date' => 'date|after_or_equal:assigner_start_date'

        ];
    }

    public function messages()
    {
        return [

        ];
    }
}
