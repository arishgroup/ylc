<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidateTitle extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:3|regex:/(^[a-zA-Z ]+$)+/'
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Title is required.',
            'title.min' =>'Title must be at least 3 characters.',
            'title.regex' =>'Title may only contain letters.'
        ];
    }
}
