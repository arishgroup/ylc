<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidateData extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'area_of_service' => 'required|not_in:0',
            'title' => 'required|min:3',
            'court' => 'required|not_in:0',
            
            'lawyer_favor' => 'required|min:3',
            'lawyer_against' => 'required|min:3',
            'citation_name' => 'required|min:3',
            'location' => 'required|not_in:0',
            'category' => 'required|not_in:0',
            'statutes' => 'required|not_in:0',
            'statute_section' => 'required|not_in:0',
        ];
        //'other_judge' => 'required|not_in:0',
    }

    public function messages()
    {
        return [

        ];
    }
}
