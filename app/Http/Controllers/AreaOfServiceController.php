<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AreaOfSevice;
use Illuminate\Support\Facades\Session;
use \App\Helper;
use Auth;
use App\LawyerAos;
use App\LawyersAosContent;
use App\AosContent;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use App\LawyerRegistration;
use App\Helpers\LogActivity;

class AreaOfServiceController extends Controller
{
    protected $folderName = "admin.area_of_service.";
    protected $aos_sel = array ('0' => 'No Parent Node');
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin,lawyer');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $aos = AreaOfSevice::orderBy('_id')->where('is_del', '!=', 1)->paginate();
        return view($this->folderName.'aos',['aos'=>$aos]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tree()
    {
        //$aos = AreaOfSevice::orderBy('_id', 'asc')->where('is_del', '!=', 1)->get();
        $helper = new Helper();
        $aos_sel = $helper->getLawyerAos();
        $aos = $aos_sel;
        return view($this->folderName.'aosTree',['aos'=>$aos]);
    }
    
    public function lawyerTree(){
        $lawyer_aos = Auth::user()->area_of_service;
        // Get Root AOS
        $root_aos = AreaOfSevice::orderBy('_id')->where('_id',$lawyer_aos)->get();
        
        // get child AOS
        $aos = $root_aos;
        $helper = new Helper();
        $aos_sel = $helper->getLawyerAos($lawyer_aos);
        if(count($aos_sel) > 0) {
            foreach($aos_sel as $val){
                 $aos[] = $val;
            }
        }
        foreach ($aos as $val){
            $d[$val->_id] = $val->name;
        }
        
        return view('lawyers.area_of_service.aos_tree',['aos'=>$aos, 'tree_type'=>'standard']);
    }
    
    function personalLawyerTree(){
        $helper = new Helper();
        $aos_sel = $helper->getLawyerAosByid();
        $aos = $aos_sel;
        return view('lawyers.area_of_service.aos_tree',['aos'=>$aos, 'tree_type'=>'personal']);
    }
    function personalLawyerTreeFromAdmin($id){
        $helper = new Helper();
        $aos_sel = $helper->getLawyerAosByid('0',$id);
        $aos = $aos_sel;
        // Get lawyers detail
        $lawyerDetail = LawyerRegistration::find($id);
        //dd($lawyerDetail);
        $data = array ('lawyer'=>$lawyerDetail);
        return view('admin.area_of_service.lawyer_tree',['aos'=>$aos, 'tree_type'=>'personal', 'data' => $data]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function childNodePersonal($id)
    {
        $parent_data = LawyerAos::orderBy('_id')->where('_id', '=', $id)->get();
        return view ('lawyers.area_of_service.create',['aos'=>$id, 'parent'=>$parent_data[0]->name, 'type'=>'personal']);
    }
    public function childNode($id)
    {
        $parent_data = AreaOfSevice::orderBy('_id')->where('_id', '=', $id)->get();
        
        return view ($this->folderName.'aosCreate',['aos'=>$id, 'parent'=>$parent_data[0]->name]);
    }
    
    public function createPersonalTree(){
        $id = 0;
        $parent_data = "YLC";
        return view ('lawyers.area_of_service.create',['aos'=>$id, 'parent'=>$parent_data, 'type'=>'personal']);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
        $id = 0;
        $parent_data = "YLC";
        return view ($this->folderName.'aosCreate',['aos'=>$id, 'parent'=>$parent_data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //AreaOfSevice::create($request->all());
        //return response()->json(['message' => 'Area of service created Successfully.']);
        
        $data = $request->all();
        AreaOfSevice::create($data);
        
        Session::flash('message', 'Added Successfully.');
        return redirect()->route('aos.tree');
         
    }
    
    public function personalStore(Request $request){
        $data = new LawyerAos();
        $data->name         = addslashes($request->name);
        $data->description  = addslashes($request->description);
        $data->parent_id    = $request->parent_id;
        $data->lawyer_id    = $request->lawyer_id;
        $data->save();
        //$data = $request->all();
        //LawyerAos::create($data);
        
        
        LogActivity::addToLog('aos','New Personal Area of Service has been added.', $data);
        
        Session::flash('message', 'Added Successfully.');
        return redirect()->route('personal.aos.tree');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = AreaOfSevice::find($id);
        $helper = new Helper(); 
        $aos_sel = $helper->categoryTree();
        
        
        if($data->count() > 0){
            return view( $this->folderName.'aosUpdate', ['data' => $data, 'aos_sel'=>$aos_sel]);
        } else {
            return redirect()->route('personal.aos.tree');
        }
    }
    public function personalEdit($id)
    {
        $data = LawyerAos::find($id);
        $helper = new Helper();
        $aos_sel[0] = 'YLC';
        $aos_sel_val = $helper->getLawyerAosByidDD();
        foreach ($aos_sel_val as $key => $val){
            $aos_sel[$key] = $val;
        }
        
        if($data->count() > 0){
            return view( 'lawyers.area_of_service.update', ['data' => $data, 'aos_sel'=>$aos_sel, 'type'=>'personal']);
        } else {
            return redirect()->route('personal.aos.tree');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function personalUpdate(Request $request, $id){
        $aos = LawyerAos::where('_id', '=', $id)->first();
        
        $aos->update($request->all());
        
        LogActivity::addToLog('aos','Personal Area of Service has been updated.', $aos);
        
        Session::flash('message', 'Update Successfully.');
        return redirect()->route('personal.aos.tree');
    }
    
    public function update(Request $request, $id)
    {
        $aos = AreaOfSevice::where('_id', '=', $id)->first();
        
        $aos->update($request->all());
        Session::flash('message', 'Update Successfully.');
        return redirect()->route('aos.tree');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $del = AreaOfSevice::find($id);
        
        if($del->delete()){
            Session::flash('message', 'Deleted Successfully.');
            return redirect()->route('aos.tree');
        }
    }
    
    public function personalDestroy($id)
    {
        $del = LawyerAos::find($id);
        if($del->delete()){
            Session::flash('message', 'Deleted Successfully.');
            LogActivity::addToLog('aos','Personal Area of Service has been Deleted.', $del);
            return redirect()->route('personal.aos.tree');
        }
    }
    
    public function indexContent($aos_id, $type){
        
        if(Auth::guard('admin')->check()) { 
            $entity_name = "admin_id";
        } else {
            $entity_name = "lawyer_id";
        }
        $aosContent = LawyersAosContent::orderBy('_id')
                            ->where('aos_id','like',$aos_id)
                            ->where($entity_name,'=', Auth::user()->_id)
                            ->where('type','like',$type)
                            ->paginate();
        if($type =='standard')
            $getAOS = AreaOfSevice::find($aos_id);
        else 
            $getAOS = LawyerAos::find($aos_id);
       
        
            
        $data = array ('aos_content'=>$aosContent, 'type'=>$type, 'aos_id'=>$aos_id, 'aos_name' => $getAOS->name);
        return view ('lawyers.aos_notes.index', ['data' => $data]);
     }
     public function getAllNotesByHead($id, $lawyer_id){
         $content = AosContent::orderBy('created_at','DESC')->where('content_id','like',$id)->get();
       
         $AosContent = LawyersAosContent::find($id);
         
         $lawyers_detail =  $AosContent->lawyer;
         
         $p_aos = $AosContent;
         $content_id = $id;
         
         $data = array ('aos_content' => $content, 'lawyer'=>$lawyers_detail, 'aos'=>$p_aos, 'content_id'=>$content_id);
         return view ('admin.area_of_service.aos_notes', ['data' => $data]);
     }
    
    public function addStandardNotes($aos){
        // get AOS name
        $aos_name = AreaOfSevice::find($aos);
        
        $helper = new Helper();
        $aos_sel = $helper->getAdminAosByidHTML();
        $admin_id = Auth::user()->_id;
        //dd($lawyer_id);
        // Get Notes if tha Lawyer AOS
        $notes = array ();
        if(count($aos_sel)>0){
            foreach ($aos_sel as $key => $value){
                $notes[$key] = $this->getNotesByAdminIDS($admin_id, 'standard', $key);
            }
        }
        
        $record = array ('tree_view'=>$aos_sel, 'notes'=>$notes);
        
        $data = array ('type'=>'standard','aos'=>$aos,'name_aos'=>$aos_name->name);
        return view ('lawyers.aos_notes.create' , ['data'=>$data, 'record'=>$record]);
        
    }
    public function addPersonalNotes($aos){
        // get AOS name
        $aos_name = LawyerAos::find($aos);
        
        $helper = new Helper();
        $aos_sel = $helper->getLawyerAosByidHTML();
        $user_id = Auth::user()->_id;
        
        // Get Notes if tha Lawyer AOS
        $notes = array ();
        if(count($aos_sel)>0){
            foreach ($aos_sel as $key => $value){
                $notes[$key] = $this->getNotesByLawyerIDS($user_id, 'personal', $key);
            }
        }
        
        $record = array ('tree_view'=>$aos_sel, 'notes'=>$notes);
        $data = array ('type'=>'personal', 'aos'=>$aos, 'name_aos'=>$aos_name->name);
        return view ('lawyers.aos_notes.create', ['data'=>$data, 'record'=>$record]);
    }
    
    public function storeContent(Request $request){
        
        $content = new LawyersAosContent;
        $content->title     = $request->title;
        $content->lawyer_id = $request->lawyer_id;
        $content->admin_id  = $request->admin_id;
        $content->type      = $request->content_type;
        $content->aos_id    = $request->aos_id ;
        $content->save();
        
        $aos_content = new AosContent();
        $aos_content->heading       = $request->content_heading;
        $aos_content->description   = $request->article_ckeditor;
        $aos_content->content_id    = $content->_id;
        $aos_content->save();
        
        Session::flash('message', 'Added Successfully.');
        LogActivity::addToLog('aos_content','New Content has been added under an area of service.', $aos_content);
        return redirect()->route('aos.content.edit', [$content->_id]);
    }
    
    public function editContent($id){
        $data_content = LawyersAosContent::find($id);
        // Helper function to create tree
        $helper = new Helper();
        if(Auth::guard('admin')->check()) {
            $aos_sel = $helper->getAdminAosByidHTML();
        } else {
            $aos_sel = $helper->getLawyerAosByidHTML();
        }

        $user_id = Auth::user()->_id;
       
        // Get Notes if tha Lawyer AOS
        $notes = array ();
        if(count($aos_sel)>0){
            foreach ($aos_sel as $key => $value){
                if(Auth::guard('admin')->check()) {
                    $notes[$key] = $this->getNotesByAdminIDS($user_id, 'standard', $key);
                } else { 
                    $notes[$key] = $this->getNotesByLawyerIDS($user_id, 'personal', $key);
                }
                
            }
        }
        
        $record = array ('tree_view'=>$aos_sel, 'notes'=>$notes);
        return view ('lawyers.aos_notes.update', ['data'=>$data_content, 'record'=>$record]);
    }
    
    public function getNotesByLawyerIDS($id, $type, $aos){
        $aosContent = LawyersAosContent::orderBy('_id')
        ->where('lawyer_id','like', $id)
        ->where('type','like',$type)
        ->where('aos_id','like',$aos)->get();
        
         
        return $aosContent;
    }
    
    public function getNotesByAdminIDS($id, $type, $aos){
        $aosContent = LawyersAosContent::orderBy('_id')
        ->where('admin_id','like', $id)
        ->where('type','like',$type)
        ->where('aos_id','like',$aos)->get();
        
        
        return $aosContent;
    }
    public function updateContent(Request $request, $id){
        
        $aos_content = LawyersAosContent::where('_id', '=', $id)->first();
        $aos_content->title = $request->title;
        $aos_content->update();
        
        if($request->content_id =='' || $request->content_id == null){
            // insert data
            $aos_content = new AosContent();
            $aos_content->heading       = $request->content_heading;
            $aos_content->description   = $request->article_ckeditor;
            $aos_content->content_id    = $id;
            $aos_content->save();
            LogActivity::addToLog('aos_content','New Content has been added under an area of service.', $aos_content);
        } else {
            
            // update data
            $aos = AosContent::where('_id', '=', $request->content_id)->first();
            $aos->heading       = $request->content_heading;
            $aos->description   = $request->article_ckeditor;
            
            $aos->update($request->all());
            LogActivity::addToLog('aos_content','Content has been Updated under an area of service.', $aos);
        }
        
        Session::flash('message', 'update Successfully.');
        return redirect()->route('aos.content.edit', [$id]);
        
    }
    
    function get_content($id){
        $data_content = AosContent::find($id);
        $rt_content = array (
            'head'=> $data_content->heading,
            'descr'=> $data_content->description,
        );
        return Response::json( array('content'=> $rt_content), 200 );
    }
    
    function destroyContent($id){
        $del = AosContent::find($id);
        $c_id = $del->content_id;
        if($del->delete()){
            Session::flash('message', 'Deleted Successfully.');
            LogActivity::addToLog('aos_content','Content has been deleted under an area of service.', $del);
            return redirect()->route('aos.content.edit', [$c_id]);
        }
    }
    
    function destroyTitleContent($id){
        $del_n = LawyersAosContent::find($id);
        $c_id = $del_n->aos_id;
        $type = $del_n->type;
       
        if($del_n->delete()){
            Session::flash('message', 'Deleted Successfully.');
            LogActivity::addToLog('aos_content','Content Title has been deleted under an area of service.', $del_n);
            return redirect()->route('aos.content.index', [$c_id,$type]);
        }
    }
    
    function getContent(Request $request){
        $data_content = AosContent::where('content_id','like',$request->id)->get();
        
        $return_content = '';
        if($data_content->count()){
            foreach($data_content as $content){
                $return_content .= '<div><div><b>'.$content->heading.'</b></div><div>'.$content->description.'</div></div>';
            }
        }
        
        return Response::json( array('content'=> $return_content), 200 );
    }
    
    function getSingleContent(Request $request){
        $data_content = AosContent::find($request->id);
        $return_content = '<div><div><b>'.$data_content->heading.'</b></div><div>'.$data_content->description.'</div></div>';
        return Response::json( array('content'=> $return_content), 200 );
    }
    function getFullContent(Request $request){
        if(Auth::guard('admin')->check() && empty($request->lawyer)) {
            $entity_name = "admin_id";
        } else {
            $entity_name = "lawyer_id";
        }
        
        $lawyer_id = Auth::user()->_id;
        if($request->lawyer){
            $lawyer_id = $request->lawyer;
        }
        
        $data_content = LawyersAosContent::where($entity_name,'like',$lawyer_id)
                        ->where('aos_id','like',$request->id)
                        ->where('type','like',$request->type)
                        ->get();
                        $return_html = "";

        if($data_content->count()){
            foreach ($data_content as $content_head){
            $return_html .= '<div class="container-fluid">
                            <div class="row">
                              <div class="col-sm-12 row"><b>Title:</b>'.$content_head->title.'</div> ';
                if($content_head->getAosContent->count()) {
                    foreach ($content_head->getAosContent as $aos_content){
                        $return_html .= ' <div class="col-sm-12"><b>'.$aos_content->heading.'</b></div>
                              <div class="col-sm-12">'.$aos_content->description.'</div>';
                            
                    }
                }
                $return_html .= '  <div class="col-sm-12"></hr></div>
                </div>
                </div>'  ;
            }
         }
         //cho $return_html;
         return Response::json( array('content'=> $return_html), 200 );
    }
    
    function getAllNotesByLawyerId ( $id, $type, $aos){
            
            $aosContent = LawyersAosContent::orderBy('_id')
            ->where('lawyer_id','like', $id)
            ->where('type','like',$type)
            ->where('aos_id','like',$aos)
            ->paginate();
            
            // get lawyer_details
            $lawyers_detail = LawyerRegistration::find($id);
            
            $data = array ('aos_content'=>$aosContent, 'type'=>$type, 'lawyer'=>$lawyers_detail);
            return view ('lawyers.aos_notes.admin_view', ['data' => $data]);
        
    }
    
    function showContent($id){
        $content_detail = AosContent::find($id);
       
        $lawyerDetail = $content_detail->getAos->lawyer;
        
        $data = array ('lawyer'=>$lawyerDetail, 'content_detail' => $content_detail, 'aos'=>$content_detail->getAos, 'type'=>'personal');
        return view ('lawyers.aos_notes.view', ['data'=>$data]);
    }
}
