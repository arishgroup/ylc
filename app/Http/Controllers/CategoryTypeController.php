<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CategoryType;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\ValidateTitle;

class CategoryTypeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin,lawyer,user');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = CategoryType::orderBy('title', 'ASC')->paginate();
        return view('categoryType.index', ['cat_type'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categoryType.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ValidateTitle $request)
    {

        $request->validated();
        $data = $request->all();
        CategoryType::create($data);
        
        Session::flash('message', 'Added Successfully.');
        return redirect()->route('categoryType.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $get_data = CategoryType::find($id);
        return view('categoryType.update', ['view_Data' => $get_data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ValidateTitle $request, $id)
    {
        $request->validated();
        $ci = CategoryType::where('_id', '=', $id);
        
        $ci->update($request->all());
        Session::flash('message', 'Update Successfully.');
        return redirect()->route('categoryType.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $del_ci = CategoryType::find($id);
        if($del_ci->delete()){
            Session::flash('message', 'Deleted Successfully.');
            return redirect()->route('categoryType.index');
        }
    }
}
