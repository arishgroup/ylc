<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UploadBookTitle;
use Illuminate\Support\Facades\Config;
use App\UploadedBook;
use Illuminate\Support\Facades\Session;
use App\UploadBookfile;

class BookController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin,user');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search_ch = $request->ch;
        if($request->ch == "") {
            $search_ch = "a";
        }
        $data = UploadedBook::where('name','like', $search_ch.'%')->orderBy('name', 'ASC')->get();
        return view('books.index', ['book'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countryList = Config::get('constants.country');
        $book_type_array = array ();
        
        
        return view('books.create',['book_type' => $book_type_array, 'country'=>$countryList ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$request->validated();
       
        $data_post = new UploadedBook();
        $data_post->name        = ucwords(strtolower(trim($request->book_title)));
        $data_post->location_city        = $request->country_id;
        $data_post->state_id        = $request->state_id;
               
        $data_post->description = $request->description;
        $data_post->year = $request->book_year;
        $data_post->save();

        $files = $request->file('image');
        
        if($request->hasFile('image'))
        {
            $parent_id = $data_post->_id;
            foreach ($files as $file) {
                if($file) {
                // Get File extension
                $file_ext = $file->getClientOriginalExtension();
                
                //Display File Name
                $name = $file->getClientOriginalName();
                $data_post_file = new UploadBookfile();
                $data_post_file->ext               = $file_ext;
                $data_post_file->file_name         = $name;
                $data_post_file->uploaded_book_id  = $parent_id;
                $data_post_file->save();
                $id = $data_post_file->_id;
                $file_name = 'book_'.$id.'.'.$file_ext;
                if($file->storeAs('public/books/'.$parent_id, $file_name)){
                        
                    }
                }
            } 
        }
            
            Session::flash('message', 'Added Successfully.');
            return redirect()->route('book.index', ['ch'=>substr(trim($request->book_title), 0,1)]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $countryList = Config::get('constants.country');
        
        // get the row for update
        $data_row = UploadedBook::find($id);
        
        return view('books.update', [ 'row_data'=>$data_row, 'country'=>$countryList]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data_row = UploadedBook::find($request->_id);
        $data_row->name         = ucwords(strtolower(trim($request->book_title)));
        $data_row->description  = $request->description;
        $data_row->year         = $request->book_year;
        $data_row->country_id        = $request->country_id;
        $data_row->state_id        = $request->state_id;
        $data_row->save();
        
        $files = $request->file('image');
        
        if($request->hasFile('image'))
        {
            $parent_id = $data_row->_id;
            foreach ($files as $file) {
                if($file) {
                    // Get File extension
                    $file_ext = $file->getClientOriginalExtension();
                    
                    //Display File Name
                    $name = $file->getClientOriginalName();
                    $data_post_file = new UploadBookfile();
                    $data_post_file->ext               = $file_ext;
                    $data_post_file->file_name         = $name;
                    $data_post_file->uploaded_book_id  = $parent_id;
                    $data_post_file->save();
                    $id = $data_post_file->_id;
                    $file_name = 'book_'.$id.'.'.$file_ext;
                    if($file->storeAs('public/books/'.$parent_id, $file_name)){
                        
                    }
                }
            }
        }
        Session::flash('message', 'Updated Successfully.');
        return redirect()->route('book.edit', [$request->_id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyAttachment($id)
    {
        $del_book = UploadBookfile::find($id);
        $b_id = $del_book->getBookId->_id;
        
        if($del_book->delete()){
            Session::flash('message', 'Deleted Successfully.');
            return redirect()->route('book.edit', [$b_id]);
        }
    }
}
