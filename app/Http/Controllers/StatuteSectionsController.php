<?php

namespace App\Http\Controllers;

use App\BookSections;
use Illuminate\Http\Request;
use App\Book;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Response;
use App\Tag;
use App\Helper;
use Auth;
use App\Helpers\LogActivity;

class StatuteSectionsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin,lawyer,user');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        // get statute name
        $statute_name = Book::find($request->id);
        
        //Get Data from Database
        $book_section = BookSections::where('book_id','=',$request->id)->orderBy('_id','ASC')->get();
        return view('statute.sections.index',['book_sections' => $book_section, 'book_id'=>$request->id, 'statute'=>$statute_name->name]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $id)
    {
        // Get Book Title
        $book_title = Book::find($id);
        
      
        
        return view('statute.sections.create', ['book_title'=>$book_title[0]]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = Auth::user()->_id;
        $helper = new Helper();
        $user_type = $helper->getUsertype();
        //dd($request->input('content_ckeditor'));
        $data_request = new BookSections();
        $data_request->definition   = $request->section_title ;
        $data_request->content_read = $request->content_ckeditor ;
        $data_request->book_id      = $request->book_id ;
        $data_request->section_no   = $request->section_no ;
        $data_request->added_by        = $user_id;
        $data_request->user_type        = $user_type;
        $data_request->save();
        
        if($request->case_laws != '')
            $data_request->caseLaws()->sync($request->case_laws, true);
        
        LogActivity::addToLog('statute-section','Statute Section has been added.', $data_request);
        
        Session::flash('message', 'Added Successfully.');
        return redirect()->route('statute.sections.create', ['id'=>$request->book_id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BookSections  $bookSections
     * @return \Illuminate\Http\Response
     */
    public function show(BookSections $bookSections)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BookSections  $bookSections
     * @return \Illuminate\Http\Response
     */
    public function edit($bookSections)
    {
        $book_section = BookSections::find($bookSections);
        
        $selectedCaseLaw = array ();
        $selectedCaseLawId = array();
        foreach($book_section->caselaws as $caselaw){
            $selectedCaseLaw[$caselaw->_id] =$caselaw->citation_name;
            $selectedCaseLawId[] = $caselaw->_id ;
        }
        return view('statute.sections.update', ['book_section'=>$book_section, 'dd_caseLaw'=>$selectedCaseLaw, 'caseLaw_selected'=>$selectedCaseLawId]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BookSections  $bookSections
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user_id = Auth::user()->_id;
        
        $helper = new Helper();
        $user_type = $helper->getUsertype();
        
        $book_section = BookSections::find($request->book_section_id);
        //dd( $book_section );
        $book_section->definition = $request->section_title;
        $book_section->section_no = $request->section_no;
        $book_section->content_read = $request->content_ckeditor;
        $book_section->updates_by        = $user_id;
        $book_section->update_user_type        = $user_type;
        $book_section->update();
        
        if($request->case_laws != '')
            $book_section->caseLaws()->sync($request->case_laws, true);
        
        LogActivity::addToLog('statute-section','Statute Section has been updated.', $book_section);
        Session::flash('message', 'Update Successfully.');
        return redirect()->route('statute.sections.index', ['id'=>$book_section->book_id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BookSections  $bookSections
     * @return \Illuminate\Http\Response
     */
    public function destroy($bookSections)
    {
        $del_book = BookSections::find($bookSections);
        if($del_book->delete()){
            Session::flash('message', 'Deleted Successfully.');
            LogActivity::addToLog('statute-section','Statute Section has been deleted.', $del_book);
            return redirect()->route('statute.sections.index', ['id'=>$del_book->book_id]);
        }
    }
    
    public function get_content(Request $request){
        $book_title = BookSections::find($request->id);

        //dd($request->id);
        return Response::json( array('content'=> $book_title->content_read), 200 );
    }
    
    public function caselawWithSection($section_id){
        // fetchdata 
        $statuteSection = BookSections::find($section_id);
        $data = array ('ss'=>$statuteSection);
        return view('statute.sections.caselaws', ['data'=>$data]);
    }
}
