<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Department;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\Departments;
use App\Http\Requests\ValidateDepartment;

class DepartmentController extends Controller
{
    private $folder_view = "departments.";
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin,user');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $get_dept = Department::orderBy('name', 'ASC')->paginate();
        
        return view($this->folder_view.'index', [ 'dept_list'=> $get_dept]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->folder_view.'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ValidateDepartment $request)
    {
        $request->validated();
        $data = new Department();
        $data->name         = $request->dept_name;
        $data->description  = $request->description;
        $data->longitude    = $request->longitude;
        $data->latitude     = $request->latitude;
        $data->address      = $request->address;
        $data->save();
        
        Session::flash('message', 'Added Successfully.');
        return redirect()->route('department.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $get_data = Department::find($id);
        return view($this->folder_view.'update', ['data'=>$get_data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ValidateDepartment $request, $id)
    {
        $request->validated();
        
        $data = Department::find($id);
        $data->name         = $request->dept_name;
        $data->description  = $request->description;
        $data->longitude    = $request->longitude;
        $data->latitude     = $request->latitude;
        $data->address      = $request->address;
        
        $data->update();
        //dd('exit');
        Session::flash('message', 'Update Successfully.');
        return redirect()->route('department.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $del_dept = Department::find($id);
        if($del_dept->delete()){
            Session::flash('message', 'Deleted Successfully.');
            return redirect()->route('department.index');
        }
    }
}
