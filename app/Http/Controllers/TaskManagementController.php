<?php

namespace App\Http\Controllers;

use App\Helper;
use App\LawyerRegistration;
use App\TaskManagementAttachments;
use App\TaskManagementComments;
use App\BackendUsers;
use Illuminate\Http\Request;
use App\TaskManagement;
use Illuminate\Support\Facades\Session;
use Auth;
use Config;
use Carbon\Carbon;
use Illuminate\Support\Facades\Response;
use App\Http\Requests\ValidateTaskManagementSystem;

class TaskManagementController extends Controller
{
    private $folder_view = "taskManagement.";

    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $task_managements = TaskManagement::orderBy('created_at', 'DESC')->get();

        return view($this->folder_view.'index', compact('task_managements'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id="0")
    {
        $help = new Helper();
        $graph = array();
        $assigner_id  = Auth::user()->_id;
        $user_types = Config::get('constants.user_type.task_management');
        $priority = Config::get('constants.priority');
        $prev_6_month = Carbon::now()->subMonths(6)->format('Y-m-d');
        $last_6_month_tasks = TaskManagement::orderBy('assigner_end_date', 'asc')->where("assigner_end_date",">", $prev_6_month)->get();

        $cnt_m = 0;
        foreach ($last_6_month_tasks as $records){
            $data_array = $records->assigner_end_date;
            $date_numbers = date('m',strtotime($data_array));
            $month = $date_numbers;

            if($cnt_m != $month) {
                $cnt = 0;
                $cnt2 = 0;
                $cnt3 = 0;
                $cnt_m = $month;
                $graph[$month]['exceed'] = $cnt;
                $graph[$month]['ontime'] = $cnt2;
                $graph[$month]['pending'] = $cnt3;
            }
            

            if(empty($records->assignee_start_date)){
                $cnt3 =  $cnt3+1;
                $graph[$month]['pending'] = $cnt3;
            } else {
                if(strtotime($records->assigner_end_date) < strtotime($help->getFormattedDate($records->assignee_end_date)))
                {
                    $cnt =  $cnt+1;
                    $graph[$month]['exceed'] = $cnt;
                }
                else{
                    
                    $cnt2 =  $cnt2+1;
                    $graph[$month]['ontime'] = $cnt2;
                }
            }
        }
        if($id == "0"){
            return view ($this->folder_view.'create',['graph'=>$graph, 'user_types'=>$user_types, 'all_priorities'=> $priority, 'assigner_id'=> $assigner_id] );
        }
        else{
            $data_collection = TaskManagement::find($id);
//            dd($data_collection);
            return view ($this->folder_view.'create',['graph'=>$graph, 'data'=>$data_collection, 'user_types'=>$user_types, 'all_priorities'=> $priority, 'assigner_id'=> $assigner_id] );
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ValidateTaskManagementSystem $request)
    {

        $request->validated();

        $help = new Helper();
        $assigner_type = $help->getUsertype();

        $user_id  = Auth::user()->_id;
        $id = $request->current_record;
//        dd($request);

        if($id === NULL)
        {
            $save_data = new TaskManagement();
            $save_data->assignee_type = $request->assignee_type;
            $save_data->assignee_id = $request->assignee_id;
            $save_data->task_name = $request->task_name;
            $save_data->task_description = $request->task_description;
            $save_data->assigner_start_date = $request->assigner_start_date;
            $save_data->assigner_end_date = $request->assigner_end_date;
            $save_data->priority = $request->priority;
            $save_data->assigner_type = $assigner_type;
            $save_data->status = 0;
            $save_data->assigner_id = $user_id;

            $save_data->save();
            $id = $save_data->id;
            $send_id = "0";
            Session::flash('message', 'Record Added Successfully.');
        }
        else{

            $update_data = TaskManagement::find($id);
            $update_data->assignee_type = $request->assignee_type;
            $update_data->assignee_id = $request->assignee_id;
            $update_data->task_name = $request->task_name;
            $update_data->task_description = $request->task_description;
            $update_data->assigner_start_date = $request->assigner_start_date;
            $update_data->assigner_end_date = $request->assigner_end_date;
            $update_data->priority = $request->priority;
            $update_data->save();
            $send_id = $id;
            Session::flash('message', 'Record Updated Successfully.');
        }


        if(!empty($request->comment)) {

            $save_data2 = new TaskManagementComments();
            $save_data2->comment = $request->comment;
            $save_data2->task_id = $id;
            $save_data2->user_type = $assigner_type;
            $save_data2->user_id = $user_id;

            $save_data2->save();
        }

        if($request->hasFile('attachments'))
        {
            $files = $request->file('attachments');
            foreach ($files as $file) {
                $file_name = $file->getClientOriginalName();
                $file_ext = $file->getClientOriginalExtension();

                $save_data3 = new TaskManagementAttachments();
                $save_data3->file_name= $file_name;
                $save_data3->file_extension= $file_ext;
                $save_data3->task_id = $id;
                $save_data3->user_type = $assigner_type;
                $save_data3->user_id = $user_id;

                $save_data3->save();

                $store_file_name = $save_data3->id.'.'.$file_ext;

                $file->storeAs(
                    'public/taskmanagement/', $store_file_name
                );
            }
        }

        return redirect()->route($this->folder_view.'create', $send_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user_types = Config::get('constants.user_type.task_management');
        $data_collection = TaskManagement::find($id);

        return view($this->folder_view.'edit',['data'=>$data_collection, 'user_types'=>$user_types]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ValidateTaskManagementSystem $request, $id)
    {
        $request->validated();

        $user_id  = Auth::user()->_id;
        $task_managements = TaskManagement::find($id);
        $help = new Helper();
        $assigner_type = $help->getUsertype();

        $task_managements->assignee_type = $request->get('assignee_type');
        $task_managements->assignee_id = $request->get('assignee_id');
        $task_managements->task_name = $request->get('task_name');
        $task_managements->task_description = $request->get('task_description');
        $task_managements->assigner_start_date = $request->get('assigner_start_date');
        $task_managements->assigner_end_date = $request->get('assigner_end_date');
        $task_managements->priority = $request->get('priority');
        $task_managements->comments = $request->get('comment');
        $task_managements->save();

//        dd($task_managements->comments);

        if(!empty($task_managements->comments)){

            $save_data2 = new TaskManagementComments();
            $save_data2->comment = $task_managements->comments;
            $save_data2->task_id = $id;
            $save_data2->user_type = $assigner_type;
            $save_data2->user_id = $user_id;

            $save_data2->save();
        }

        if($request->hasFile('attachments'))
        {
            $files = $request->file('attachments');
            foreach ($files as $file) {
                $file_name = $file->getClientOriginalName();
                $file_ext = $file->getClientOriginalExtension();

                $save_data3 = new TaskManagementAttachments();
                $save_data3->file_name= $file_name;
                $save_data3->file_extension= $file_ext;
                $save_data3->task_id = $id;
                $save_data3->user_type = $assigner_type;
                $save_data3->user_id = $user_id;

                $save_data3->save();

                $store_file_name = $save_data3->id.'.'.$file_ext;

                $file->storeAs(
                    'public/taskmanagement/', $store_file_name
                );
            }
        }


        Session::flash('message', 'Data Updated.');
        return redirect()->route('taskManagement.edit', $id);
    }

    public function update_after_assignee_start(Request $request, $id)
    {
        $task_managements = TaskManagement::find($id);
        $user_id  = Auth::user()->_id;

        $task_managements->comments = $request->get('comment');


        if(!empty($task_managements->comments)){
            $help = new Helper();
            $assigner_type = $help->getUsertype();

            $save_data2 = new TaskManagementComments();
            $save_data2->comment = $task_managements->comments;
            $save_data2->task_id = $id;
            $save_data2->user_type = $assigner_type;
            $save_data2->user_id = $user_id;
//            dd($save_data2);
            $save_data2->save();
        }

        if($request->hasFile('attachments'))
        {
            $files = $request->file('attachments');
            foreach ($files as $file) {
                $file_name = $file->getClientOriginalName();
                $file_ext = $file->getClientOriginalExtension();

                $save_data3 = new TaskManagementAttachments();
                $save_data3->file_name= $file_name;
                $save_data3->file_extension= $file_ext;
                $save_data3->task_id = $id;
                $save_data3->user_type = $assigner_type;
                $save_data3->user_id = $user_id;

                $save_data3->save();

                $store_file_name = $save_data3->id.'.'.$file_ext;

                $file->storeAs(
                    'public/taskmanagement/', $store_file_name
                );
            }
        }


        Session::flash('message', 'Data Updated.');
        return redirect()->route('taskManagement.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task_managements = TaskManagement::find($id);
        $task_managements->delete();

        Session::flash('message', 'Task Deleted.');
        return redirect()->route('taskManagement.index');
    }

    public function getAssignee(Request $request){
        $term_request = explode("&", $request->id );
        $term = $term_request[0];
        if(isset($term_request[1]))
        $term_id = $term_request[1];

        if (empty($term)) {
            return Response::json([]);
        }

        $assignee_list = '<select id="assignee_id" name="assignee_id" ><option value="0">Select Assignee</option>';
        if($term  == 'lawyer') {
            $assignee_record = LawyerRegistration::orderBy('name','ASC')->get();

            foreach ($assignee_record as $assignee) {
                if(isset($term_id ) and ($term_id == $assignee->id))
                    $assignee_list .= '<option selected="selected" value="'.$assignee->id.'">'.$assignee->name.'</option>';
                else
                    $assignee_list .= '<option value="'.$assignee->id.'">'.$assignee->name.'</option>';
            }
        }

        if($term  == 'client'){

        }

        if($term  == 'user'){
            $assignee_record = BackendUsers::orderBy('name','ASC')->get();

            foreach ($assignee_record as $assignee) {
                if(isset($term_id ) and ($term_id == $assignee->id))
                    $assignee_list .= '<option selected="selected" value="'.$assignee->id.'">'.$assignee->name.'</option>';
                else
                    $assignee_list .= '<option value="'.$assignee->id.'">'.$assignee->name.'</option>';
            }
        }

        $assignee_list .= '</select>';
        return Response::json(array(
            'success' => true,
            'data'   => $assignee_list
        ));
    }
}
