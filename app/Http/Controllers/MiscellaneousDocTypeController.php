<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MiscellaneousDoctype;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\ValidateName;

class MiscellaneousDocTypeController extends Controller
{
    private $folder_view = 'admin.miscellenous.doc_type.';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin,lawyer,user');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $get_data = MiscellaneousDoctype::orderBy('name', 'ASC')->paginate();
        return view($this->folder_view.'index', ['doc_type_list' => $get_data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view( $this->folder_view.'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ValidateName $request)
    {
        $data = $request->all();
        MiscellaneousDoctype::create($data);
        
        Session::flash('message', 'Added Successfully.');
        return redirect()->route('miscellaneous.document.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $get_data = MiscellaneousDoctype::find($id);
        return view($this->folder_view.'edit', ['view_Data' => $get_data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ValidateName $request, $id)
    {
        $ci = MiscellaneousDoctype::where('_id', '=', $id);
        
        $ci->update($request->all());
        Session::flash('message', 'Update Successfully.');
        return redirect()->route('miscellaneous.document.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $del_ci = MiscellaneousDoctype::find($id);
        if($del_ci->delete()){
            Session::flash('message', 'Deleted Successfully.');
            return redirect()->route('miscellaneous.document.index');
        }
    }
}
