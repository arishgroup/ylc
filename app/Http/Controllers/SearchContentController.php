<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SearchContentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:lawyer', ['except' => ['logout']]);
    }
    
    public function index(){
        return view('lawyers.search.index');
    }
}
