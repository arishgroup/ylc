<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Court;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\ValidateName;

class CourtController extends Controller
{

    private $folder_view = "admin.court.";
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin,lawyer,user');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $get_court = Court::orderBy('_id', 'DESC')->paginate();
        return view($this->folder_view.'index', [ 'court_list'=> $get_court]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->folder_view.'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ValidateName $request)
    {
        $request->validated();
        $data = new Court();
        $data->name = $request->name;
        $data->save();
        
        Session::flash('message', 'Added Successfully.');
        return redirect()->route('court.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $req)
    {
        $get_data = Court::where('_id','=',$req->id)->get();
        return view($this->folder_view.'update', ['data'=>$get_data[0]]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ValidateName $request)
    {
        $request->validated();
        $get_data = Court::find($request->court_id);
        $get_data->name = $request->name;
        $get_data->update();
        //dd('exit');
        Session::flash('message', 'Update Successfully.');
        return redirect()->route('court.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $del_court = Court::find($id);
        if($del_court->delete()){
            Session::flash('message', 'Deleted Successfully.');
            return redirect()->route('court.index');
        }
    }
}
