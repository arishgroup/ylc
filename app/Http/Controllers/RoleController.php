<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\ValidateName;


class RoleController extends Controller
{
    private $folder_view = 'admin.roles.';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::orderBy('_id')->where('is_del', '!=', 1)->paginate();
        return view ($this->folder_view.'rolesListing', ['roles'=>$roles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ($this->folder_view.'rolesCreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ValidateName $request)
    {
        $request->validated();
        $data = $request->all();
        Role::create($data);
        
        Session::flash('message', 'Added Successfully.');
        return redirect()->route('role.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Role::find($id);
        if($data->count() > 0){
            return view( $this->folder_view.'rolesUpdate', ['data' => $data]);
        } else {
            return redirect()->route('role.index');
        }
      
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ValidateName $request,$id)
    {

        $request->validated();
        $role = Role::where('_id', '=', $id)->first();
        
        $role->update($request->all());
        Session::flash('message', 'Update Successfully.');
        return redirect()->route('role.index');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $role = Role::where('_id', '=', $id)->first();
        $role->is_del=1;
        $role->update();
        
        Session::flash('message', 'Record Deleted Successfully.');
        return redirect()->route('role.index');
    }
}
