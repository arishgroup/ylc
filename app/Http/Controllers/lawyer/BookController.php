<?php

namespace App\Http\Controllers\lawyer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\UploadedBook;
use App\UploadBookfile;

class BookController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:lawyer');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search_ch = $request->ch;
        if($request->ch == "") {
            $search_ch = "a";
        }
        $data = UploadedBook::where('name','like', $search_ch.'%')->orderBy('name', 'ASC')->get();
        return view('lawyers.books.index', ['book'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // get Book details
        $data_parent = UploadedBook::find($id);
        
        $data = UploadBookfile::where('uploaded_book_id','like', $id)->orderBy('name', 'ASC')->get();
        return view('lawyers.books.attachmentListing', ['book'=>$data, 'parent'=>$data_parent]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
