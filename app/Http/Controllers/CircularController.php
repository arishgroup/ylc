<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Circular;
use App\CircularInstitution;
use Illuminate\Support\Facades\Session;
use App\MiscellaneousDoctype;
use App\Tag;

class CircularController extends Controller
{
    private $folder_view = "admin.miscellenous.";
    //   
    public function __construct()
    {
        $this->middleware(['auth:lawyer,admin,user'], ['except' => ['logout']]);
    }
    
    public function index(){
        
        $get_data = Circular::orderBy('subject', 'ASC')->paginate();
        return view($this->folder_view.'index', ['circular_list' => $get_data]);
    }
    
    public function create(){
        $institute_array = array ();
        $institution_list = CircularInstitution::orderBy('name', 'ASC')->get();
        if($institution_list->count()){
            foreach ($institution_list as $value){
                $institute_array[$value->_id] = $value->name;
            }
        }
        
        $doc_type_array = array ();
        $doc_type_list = MiscellaneousDoctype::orderBy('name', 'ASC')->get();
        if($doc_type_list->count()){
            foreach ($doc_type_list as $value){
                $doc_type_array[$value->_id] = $value->name;
            }
        }
        return view( $this->folder_view.'create' , ['ins_list'=>$institute_array, 'doc_type_list'=> $doc_type_array]);
    }
    
    public function store(Request $request){
        $cir_data = new Circular();
        //$data = $request->all();
        //Circular::create($data);
        $cir_data->notification_no  = $request->notification_no;
        $cir_data->institution_id   = $request->institution_id;
        $cir_data->doc_type_id      = $request->doc_type_id;
        $cir_data->subject          = $request->subject;
        $cir_data->description      = $request->description;
        $cir_data->year             = $request->year;
        $cir_data->month            = $request->month;
        
        $cir_data->save();
        
        $cir_data->getTags()->sync($request->tags,false);
        
        $file = $request->file('image');
        $id = $cir_data->_id;
        if($file) {
            // Get File extension
            $file_ext = $file->getClientOriginalExtension();
            
            $file_name = 'doc_'.$id.'.'.$file_ext;
            
            if($request->file('image')->storeAs('public/miscellaneous', $file_name)){
                $cir_data->file_ext = $file_ext;
                $cir_data->update();
            }
        }
        Session::flash('message', 'Added Successfully.');
        return redirect()->route('miscellaneous.create');
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $institute_array = array ();
        $institution_list = CircularInstitution::orderBy('name', 'ASC')->get();
        if($institution_list->count()){
            foreach ($institution_list as $value){
                $institute_array[$value->_id] = $value->name;
            }
        }
        
        $doc_type_array = array ();
        $doc_type_list = MiscellaneousDoctype::orderBy('name', 'ASC')->get();
        if($doc_type_list->count()){
            foreach ($doc_type_list as $value){
                $doc_type_array[$value->_id] = $value->name;
            }
        }
        
        $get_data = Circular::find($id);
        
        $tags_id = array();
        // Get All the data for tags
        foreach($get_data->getTags as $tags){
            $tags_id[] = $tags->_id;
        }
        
        //get all_tags
        $all_tags = Tag::orderBy('title','ASC')->get();
        foreach ($all_tags as $db_tags){
            $sel_tags[$db_tags->_id] = $db_tags->title;
        }
        return view($this->folder_view.'edit', ['view_Data' => $get_data, 'ins_list'=>$institute_array , 'doc_type_list'=> $doc_type_array, 'tags_selected'=>$tags_id, 'allTags'=>$sel_tags]);
    }
    
    public function update(Request $request, $id)
    {
        
        $ci = Circular::find($id);

        $ci->notification_no  = $request->notification_no;
        $ci->institution_id   = $request->institution_id;
        $ci->doc_type_id      = $request->doc_type_id;
        $ci->subject          = $request->subject;
        $ci->description      = $request->description;
        $ci->year             = $request->year;
        $ci->month            = $request->month;
        $ci->save();
        
        $file = $request->file('image');
        $id = $ci->_id;
        if($file) {
            // Get File extension
            $file_ext = $file->getClientOriginalExtension();
            
            $file_name = 'doc_'.$id.'.'.$file_ext;
            
            if($request->file('image')->storeAs('public/miscellaneous', $file_name)){
                $ci->file_ext = $file_ext;
                $ci->update();
            }
        }
        $ci->getTags()->sync($request->tags,true);
        Session::flash('message', 'Update Successfully.');
        return redirect()->route('miscellaneous.index');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $del_ci = Circular::find($id);
        if($del_ci->delete()){
            Session::flash('message', 'Deleted Successfully.');
            return redirect()->route('miscellaneous.index');
        }
    }
}
