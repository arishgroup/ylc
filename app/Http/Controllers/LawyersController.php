<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\admin\LawyersDesignation;
use Illuminate\Support\Facades\Session;
use App\Lawyer;
use App\Helper;
use App\LawyerRegistration;
use App\Location;
use App\LawyerProfile;
use App\LawyersProfileEducation;
use App\AreaOfSevice;
use Illuminate\Support\Facades\Storage;
use App\Helpers\LogActivity;
use Yajra\DataTables\DataTables;

class LawyersController extends Controller
{
    private $folder_view = "lawyers.";
    private $cnt = 1;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:lawyer,admin,user', ['except' => ['logout','showSignupForm', 'signup','setPassword','updatePassword']]);
        
    }
    
    public function lawyersProfileIndex(Request $request){
        return view ($this->folder_view.'profile_index');
    }
    function datatable(Request $request){
        // $all_aos_id = implode(',',$aos_all_id);
        $aos_id = "0";
        $aos_details = "";
        if($request->aos){
            
            $aos_id = $request->aos;
            
            // Get All the Lawyers from the current AOS to the all Child notes
            $helper = new Helper();
            $aos = $helper->categoryTree($aos_id);
            $aos_all_id[] = $aos_id;
            // get all AOS ID
            foreach ($aos as $key => $value){
                $aos_all_id[] = $key;
            }
            $lawyers = LawyerRegistration::orderBy('_id')->whereIn('area_of_service',  $aos_all_id)->get();//->toSql();//
            // dd($lawyers);
        } else {
            $lawyers = LawyerRegistration::orderBy('name')->get();
        }
      
        return Datatables::of($lawyers)
        ->addColumn('action', function ($lawyers) {
            return '<a href="'. route('lawyers.log', [$lawyers->_id,'user']) .'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-eye-open"></i> Log</a>
                    <a href="'. route('lawyer.personal.tree', [$lawyers->_id, 'personal']) .'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-tree-conifer"></i> AOS</a>
                    <a href="'. route('lawyers.edit', $lawyers->_id) .'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                    <a href="'. route('lawyers.destroy', $lawyers->_id) .'" class="btn btn-xs btn-danger confirmation "><i class="glyphicon glyphicon-trash"></i> Delete</a>   ';
        })
        ->editColumn('name', function ($lawyers) {
            return $lawyers->name.' '.$lawyers->lastname;
        })
        ->editColumn('area_of_service', function ($lawyers) {
            return ($lawyers->area_of_service)?$lawyers->aos->name:' N/A ';
        })
        ->editColumn('_id', function ($lawyers) {
            return $this->cnt++;
        })
        ->make(true);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       
        $aos_id = "0";
        $aos_details = "";
        if($request->aos_id){
            $aos_id = $request->aos_id;
            // Get current AOS Name and details
            $aos_details = AreaOfSevice::find($request->aos_id);
        }
        
        return view ($this->folder_view.'index', [ 'aos'=>$aos_id, 'aos_detail'=>$aos_details]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        
        // get area of service
        $helper = new Helper();
        $aos = $helper->categoryTree();
        
        
        // Get Roles
        $pass_role = array();
        $roles = Role::orderBy('_id')->where('is_del', '!=', 1)->get();
        foreach ($roles as $role){
            $pass_role[$role->_id] = $role->name;
        }
        
        // Get Designation
        $pass_designation = array();
        $designations = LawyersDesignation::orderBy('_id')->where('is_del', '!=', 1)->get();
        foreach ($designations as $designation){
            $pass_designation[$designation->_id] = $designation->name;
        }
        return view($this->folder_view.'create',['role'=>$pass_role, 'aos_sel'=>$aos, 'designation'=>$pass_designation, 'aos_id'=>$request->aos_id]);
    }

    public function lawyersProfileCreate(Request $request){
        // get area of service
        $helper = new Helper();
        $aos = $helper->categoryTree();
        
        
        
        
        $location = array();
        $loc_data = Location::orderBy('title')->get();
        if($loc_data->count()){
            foreach($loc_data as $loc){
                $location[$loc->_id] = $loc->title;
            }
        }
        
        $data = array ('loc' => $location);
        
        return view($this->folder_view.'profile_create',['aos_sel'=>$aos, 'aos_id'=>$request->aos_id, 'data'=>$data]);
        
        
    }
    
    public function lawyersProfilestore(Request $request){
        
        
        // Validation is required
        $save_data = new LawyerProfile();
        $save_data->first_name          = $request->first_name;
        $save_data->last_name           = $request->last_name;
        $save_data->nic                 = $request->nic;
        $save_data->mobile              = $request->mobile;
        $save_data->email               = $request->email;
        $save_data->aos_id              = $request->aos_id;
        $save_data->location            = $request->location;
        $save_data->office_address      = $request->office_address;
        $save_data->home_address        = $request->home_address;
        $save_data->experiance          = $request->experiance;
        $save_data->notes               = $request->notes;
        $save_data->save();
        $lawyer_profile_id = $save_data->_id;
        // Save NIC
        $file = $request->file('nic_img');
        if($file) {
            $id = $lawyer_profile_id;
            $file_ext = $file->getClientOriginalExtension();
            
            //Display File Name
            //$name = $file->getClientOriginalName();
            $file_name = 'nic_'.$id.'.'.$file_ext;
            
            if($file->storeAs('public/lawyers_profile/', $file_name)){
                $save_data->nic_ext = $file_ext;
                $save_data->update();
            }
        }
        $profile_file = $request->file('profile_img');
        if($profile_file) {
            $id = $lawyer_profile_id;
            $file_ext = $profile_file->getClientOriginalExtension();
            
            //Display File Name
            //$name = $file->getClientOriginalName();
            $file_name = 'profile_'.$id.'.'.$file_ext;
            
            if($profile_file->storeAs('public/lawyers_profile/', $file_name)){
                $save_data->profile_img_ext = $file_ext;
                $save_data->update();
            }
        }
        
        // Education Details
        foreach ($request->collage as $key => $edu_data ){
         
            $save_edu_data = new LawyersProfileEducation();
            $save_edu_data->institute           = $edu_data ;
            $save_edu_data->degree              = $request->degree[$key] ;
            $save_edu_data->from_year           = $request->from_year[$key] ;
            $save_edu_data->to_year             = $request->to_year[$key] ;
            $save_edu_data->grade               = $request->grade[$key] ;
            $save_edu_data->hobbies             = $request->hobby[$key] ;
            $save_edu_data->description         = $request->edu_desc[$key] ;
            $save_edu_data->lawyer_profile_id   = $lawyer_profile_id;
            $save_edu_data->save();
            // save edu-degree
            
            $degree_file = $request->file('degree_img')[$key];
            if($degree_file) {
                $id = $save_edu_data->_id;
                $file_ext = $degree_file->getClientOriginalExtension();
                
                //Display File Name
                //$name = $file->getClientOriginalName();
                $file_name = 'degree_'.$id.'.'.$file_ext;
                
                if($degree_file->storeAs('public/lawyers_profile/degree/', $file_name)){
                    $save_edu_data->degree_ext = $file_ext;
                    $save_edu_data->update();
                }
            }
        }
        
        Session::flash('message', 'Added Successfully.');
        return redirect()->route('lawyers.profile.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        
       //dd($data);exit;
        Lawyer::create($data);
        
        Session::flash('message', 'Added Successfully.');
        return redirect()->route('lawyers.register');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    public function lawyersProfileEdit($id){
        $LawyerData = LawyerProfile::find($id);
        // get area of service
        $helper = new Helper();
        $aos = $helper->categoryTree();
        
        
        
        
        $location = array();
        $loc_data = Location::orderBy('title')->get();
        if($loc_data->count()){
            foreach($loc_data as $loc){
                $location[$loc->_id] = $loc->title;
            }
        }
        
        $data = array ('loc' => $location, 'lawyer'=>$LawyerData);

        return view( $this->folder_view.'profile_edit', ['data' => $data,'aos_sel'=>$aos]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    
    public function lawyersProfileUpdate(Request $request, $id)
    {
      //  dd($request->file('degree_img'));
        $data_row = LawyerProfile::find($id);
        $lawyer_profile_id = $id;
        $data_row->first_name          = $request->first_name;
        $data_row->last_name           = $request->last_name;
        $data_row->nic                 = $request->nic;
        $data_row->mobile              = $request->mobile;
        $data_row->email               = $request->email;
        $data_row->aos_id              = $request->aos_id;
        $data_row->location            = $request->location;
        $data_row->office_address      = $request->office_address;
        $data_row->home_address        = $request->home_address;
        $data_row->experiance          = $request->experiance;
        $data_row->notes               = $request->notes;
        $data_row->save();
        
        // Save NIC
        $file = $request->file('nic_img');
        if($file) {
            $id = $lawyer_profile_id;
            $file_ext = $file->getClientOriginalExtension();
            
            //Display File Name
            //$name = $file->getClientOriginalName();
            $file_name = 'nic_'.$id.'.'.$file_ext;
            
            if($file->storeAs('public/lawyers_profile/', $file_name)){
                $data_row->nic_ext = $file_ext;
                $data_row->update();
            }
        }
        $profile_file = $request->file('profile_img');
        if($profile_file) {
            $id = $lawyer_profile_id;
            $file_ext = $profile_file->getClientOriginalExtension();
            
            //Display File Name
            //$name = $file->getClientOriginalName();
            $file_name = 'profile_'.$id.'.'.$file_ext;
            
            if($profile_file->storeAs('public/lawyers_profile/', $file_name)){
                $data_row->profile_img_ext = $file_ext;
                $data_row->update();
            }
        }
        
        // Update Edu Record
        // Delete and update record
        // Keep old_id
        
        LawyersProfileEducation::where('lawyer_profile_id','like',$lawyer_profile_id)->delete();
        foreach ($request->collage as $key => $edu_data ){
            
            $save_edu_data = new LawyersProfileEducation();
            $save_edu_data->institute           = $edu_data ;
            $save_edu_data->degree              = $request->degree[$key] ;
            $save_edu_data->from_year           = $request->from_year[$key] ;
            $save_edu_data->to_year             = $request->to_year[$key] ;
            $save_edu_data->grade               = $request->grade[$key] ;
            $save_edu_data->hobbies             = $request->hobby[$key] ;
            $save_edu_data->description         = $request->edu_desc[$key] ;
            $save_edu_data->lawyer_profile_id   = $lawyer_profile_id;
            $save_edu_data->save();
            
            if( $request->file('degree_img') ) {
         //       dd('here');
                $degree_file = $request->file('degree_img')[$key];
                
                if($degree_file) {
                    $id = $save_edu_data->_id;
                    $file_ext = $degree_file->getClientOriginalExtension();
                    
                    //Display File Name
                    //$name = $file->getClientOriginalName();
                    $file_name = 'degree_'.$id.'.'.$file_ext;
                    
                    if($degree_file->storeAs('public/lawyers_profile/degree/', $file_name)){
                        $save_edu_data->degree_ext = $file_ext;
                        $save_edu_data->update();
                    }
                }
            } else {
                if($request->file_ext[$key] != "") {
                    Storage::move(('public/lawyers_profile/degree/degree_'.$request->edu_id[$key].'.'.$request->file_ext[$key]), ('public/lawyers_profile/degree/degree_'.$save_edu_data->_id.'.'.$request->file_ext[$key]));
                    $save_edu_data->degree_ext = $request->file_ext[$key];
                    $save_edu_data->update();
                }
            }
        }
       // dd('ss');
        Session::flash('message', 'Updated Successfully.');
        return redirect()->route('lawyer_profile.index');
    }
    
    
    public function lawyersProfileDestroy( $id )
    {
        $del = LawyerProfile::find($id);
        if($del->delete()){
            Session::flash('message', 'Deleted Successfully.');
            return redirect()->route('lawyer_profile.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    function dashboard(){
        return redirect()->route('personal.aos.tree');
    }
    function showSignupForm(){
        return view('authLawyer.lawyer-signup');
    }
    
    function signup(Request $request){
        $getData = LawyerRegistration::where('serial_no', 'like', $request->email)->get();
        if($getData->count()){
            if($getData[0]->password == "" ) {
                Session::flash('message', 'Please set you password.');
                return redirect()->route('lawyer.setPassword', ['id'=>$request->email]);
            }   
        }      
    }
    
    function setPassword(Request $id){
        return view ('authLawyer.lawyer-password',['email' => $id->id]);
    }
    
    public function updatePassword(Request $request){
        
        $lawyer = LawyerRegistration::where('serial_no', '=', $request->email)->first();
        //dd($lawyer);
        $lawyer->password = bcrypt($request->password);
        $lawyer->update();
        Session::flash('message', 'Login with the new password.');
        return redirect()->route('lawyer.login');
    }
    
    public function activityLog(Request $request, $type){
        $user = LawyerRegistration::find($request->id);
        $logs = LogActivity::logActivityLists($request->id, 'lawyer');
        $data = array ('logs'=>$logs, 'userData'=>$user);
        return view('admin.lawyers.logActivity',compact('data'));
    }
    
}
