<?php

namespace App\Http\Controllers\AuthLawyer;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Helpers\LogActivity;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

  
    public function __construct()
    {
        //this->middleware('guest')->except('logout');
        $this->middleware('guest:lawyer', ['except' => ['logout','userLogout']]);    
    }
    
    public function userLogout(Request $request)
    {
       
        Auth::guard('lawyer')->logout();
        return redirect()->route('lawyer.login');
    }
    
    public function showLoginForm(){
        return view('authLawyer.login');
    }
    
    public function login(Request $request){
        // Validate the Data First
        $this->validate($request, [
            'email'=> 'required',
            'password' => 'required|min:6'
        ]);
       
        // Attempt to user logged in
        if(Auth::guard('lawyer')->attempt(['serial_no'=>$request->email, 'password'=>$request->password], $request->remember)){
            $data_post = Auth::guard('lawyer')->user();
            LogActivity::addToLog('login','User has been logged in successfull.', $data_post);
            return redirect()->intended(route('lawyers.dashboard'));
        }
        // if unsuccess
        return redirect()->back()->withInput($request->only('email', 'remember'));
    } 
}
