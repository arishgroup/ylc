<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Court;
use App\Category;
use App\Book;
use App\ContentLibrary;
use Illuminate\Support\Facades\Session;
use App\AreaOfSevice;
use App\Helper;
use App\BookType;
use Illuminate\Support\Facades\Storage;
use App\Location;
use App\CategoryType;
use App\Http\Middleware\TrimStrings;
use App\Tag;
use App\ContentTags;
use App\JudgesProfile;
use App\ContentJudge;
use App\Http\Requests\ValidateData;
use Illuminate\Support\Facades\Config;

class DataLibraryController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:lawyer,admin,user', ['except' => ['logout']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $content_lib = ContentLibrary::orderBy('title','ASC')->paginate();
        
        return view('lawyers.dataLib_index', ['content_lib'=>$content_lib]);   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $court = Court::orderBy('_id')->get();
        $sel_court = array();
        foreach($court as $value){
            $sel_court[$value->_id] =  $value->name;
        }
        
        $cat = Category::orderBy('_id')->get();
        $sel_cat = array();
        foreach($cat as $value){
            $sel_cat[$value->_id] =  $value->name;
        }
        
        $statutes = Book::orderBy('_id')->get();
        $sel_statute = array ();
        foreach($statutes as $value){
            $sel_statute[$value->_id] =  addslashes($value->name);
        }
        
        
        
        $location = Location::orderBy('title', 'ASC')->get();
        $sel_location = array();
        foreach($location as $value){
            $sel_location[$value->_id] =  $value->title;
        }
        
        $cat_type = CategoryType::orderBy('title', 'ASC')->get();
        $sel_cat_type = array ();
        foreach($cat_type as $value){
            $sel_cat_type[$value->_id] =  $value->title;
        }
        
 
        // get all tags 
        $tags = Tag::orderBy('title', 'ASC')->get();
        $tag = array();
        foreach($tags as $value){
            $tag[] =  $value->title;
        }
        
        $helper = new Helper();
        $aos_sel = $helper->categoryTree();
        $aoss = $aos_sel;
        
        $countryList = Config::get('constants.country');
        return view('lawyers.dataLib',['court'=>$sel_court, 'category'=>$sel_cat, 'statutes'=>$sel_statute, 
            'aos'=>$aoss, 'location' => $sel_location, 'category_type'=>$sel_cat_type, 'tags'=>$tag, 'country'=>$countryList ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ValidateData $request)
    {

        $request->validated();
        $content_upload = new ContentLibrary;
        $content_upload->title      = $request->title ;
        $content_upload->court      = $request->court ;
        $content_upload->applent    = $request->appellant ;
        $content_upload->petitioner = $request->petitioner ;
        $content_upload->defandent = $request->defandent ;
        $content_upload->respondent     = $request->respondent ;
        $content_upload->page_no    = $request->page_no ;
        $content_upload->reference  = $request->referance ;
        $content_upload->lawyer_favor    = $request->lawyer_favor ;
        $content_upload->lawyer_against    = $request->lawyer_against ;
        $content_upload->location      = $request->location ;
        $content_upload->rule           = $request->rule ;
        $content_upload->aos            = $request->area_of_service;
        $content_upload->citation_name  = $request->citation_name ;
        $content_upload->journel        = $request->journel;
        $content_upload->category       = $request->category ;
        $content_upload->category_type  = $request->category_type ;
        $content_upload->jurnel_type    = $request->jurnel_type ;
        $content_upload->description    = $request->article_ckeditor;
        $content_upload->publish_year   = $request->publish_year;
        $content_upload->publish_month  = $request->publish_month;
        $content_upload->statute_Section = $request->section_id;
        $content_upload->head_notes = $request->head_notes;
        $content_upload->short_desc = $request->short_desc;
        $content_upload->country_id        = $request->country_id;
        $content_upload->state_id        = $request->state_id;
        
        $content_upload->save();
       // dd($content_upload->_id);
        
        if($request->tags != '')
            $content_upload->getTags()->sync($request->tags,false);
        
         if($request->statute_section != '') {
             $statute_section = array();
             foreach($request->statute_section as $val){
                 if($val != '' && $val != null)
                     $statute_section[] = $val;
             }
             $content_upload->statuteSections()->sync($statute_section,false);
        }

        if($request->judge_name == ""){
            $judges_id = array ();
        }
        $judges_id = $request->judge_name;
        if($request->other_judge != ""){
            
            $judge_names = explode(',',$request->other_judge);
            foreach ($judge_names as $values){
                $judges = new JudgesProfile();
                $judges->name = $values;
                $judges->save();
                if($judges_id == "") {
                    $judges_id[] = $judges->_id;
                } else {
                    array_push($judges_id,$judges->_id);
                }
            }
        }

        // So added judges
        if($request->judge_name != '')
            $content_upload->getJudges()->sync($judges_id,true);
        
        $file = $request->file('image');
        $id = $content_upload->_id;
        if($file) {
            //Display File Name
            $name = $file->getClientOriginalName();
            
            /*
             * Commented for future to ge the file details
             *
             //Display File Extension
             echo 'File Extension: '.$file->getClientOriginalExtension();
             echo '<br>';
             
             //Display File Real Path
             echo 'File Real Path: '.$file->getRealPath();
             echo '<br>';
             
             //Display File Size
             echo 'File Size: '.$file->getSize();
             echo '<br>';
             
             //Display File Mime Type
             echo 'File Mime Type: '.$file->getMimeType();
             */
            // Get File extension
            $file_ext = $file->getClientOriginalExtension();
            
            //Display File Name
            $name = $file->getClientOriginalName();
            $file_name = 'data_entry_'.$id.'.'.$file_ext;
            
            if($request->file('image')->storeAs('public/data_entry', $file_name)){
                $content_upload->file_ext = $file_ext;
                $content_upload->update();
            }
        }
       // dd($request);
        Session::flash('message', 'Added Successfully.');
        return redirect()->route('lawyers.datalib');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = ContentLibrary::find($id);
        
        $tags_id = array();
        // Get All the data for tags
        foreach($data->getTags as $tags){
            $tags_id[] = $tags->_id;
        }
        
        $judge_id = array();
        // Get All the data for tags
        foreach($data->getJudges as $judges){
            $judge_id[] = $judges->_id;
        }
        
        // get all judges 
        $all_judges = JudgesProfile::orderBy('name','ASC')->get();
        foreach ($all_judges as $db_judges){
            $sel_judge[$db_judges->_id] = $db_judges->name;
        }
        
        $court = Court::orderBy('_id')->get();
        foreach($court as $value){
            $this->sel_court[$value->_id] =  $value->name;
        }
        
        $cat = Category::orderBy('_id')->get();
        foreach($cat as $value){
            $this->sel_cat[$value->_id] =  $value->name;
        }
        
        $statutes = Book::orderBy('_id')->get();
        foreach($statutes as $value){
            $this->sel_statute[$value->_id] =  $value->name;
        }
        
        
        $location = Location::orderBy('title', 'ASC')->get();
        foreach($location as $value){
            $this->sel_location[$value->_id] =  $value->title;
        }
        
        $cat_type = CategoryType::orderBy('title', 'ASC')->get();
        foreach($cat_type as $value){
            $this->sel_cat_type[$value->_id] =  $value->title;
        }
        //get all_tags
        $all_tags = Tag::orderBy('title','ASC')->get();
        foreach ($all_tags as $db_tags){
            $sel_tags[$db_tags->_id] = $db_tags->title;
        }
        
        $helper = new Helper();
        $aos_sel = $helper->categoryTree();
        $aoss = $aos_sel;
        
        $countryList = Config::get('constants.country');
        
        return view('lawyers.dataLibUpdate',['court'=>$this->sel_court, 'category'=>$this->sel_cat, 'statutes'=>$this->sel_statute,
            'aos'=>$aoss, 'location' => $this->sel_location,'category_type'=>$this->sel_cat_type, 'content'=>$data, 'tags_selected'=>$tags_id, 'allTags'=>$sel_tags, 
            'sel_judge'=>$judge_id, 'alljudges'=> $sel_judge, 'country'=>$countryList]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ValidateData $request)
    {

        $request->validated();
        $id = $request->_id;
        $file = $request->file('image');
        $data_row = ContentLibrary::find( $id );
        if($file) {
            // Get File extension
            $file_ext = $file->getClientOriginalExtension();
            
            //Display File Name
            $name = $file->getClientOriginalName();
            $file_name = 'data_entry_'.$id.'.'.$file_ext;

            if($request->file('image')->storeAs('public/data_entry', $file_name)){
                $data_row->file_ext = $file_ext;
            } 
        }   
        
        if($request->statute_section != '') {
            $statute_section = array();
            foreach($request->statute_section as $val){
                if($val != '' && $val != null)
                    $statute_section[] = $val;
            }
            $data_row->statuteSections()->sync($statute_section,false);
        }
        
        //dd($data_row);
        $data_row->title        = $request->title;
        $data_row->court        = $request->court;
        $data_row->applent      = $request->appellant;
        $data_row->verses       = $request->verses;
        $data_row->page_no      = $request->page_no;
        $data_row->reference    = $request->referance;
        
        $data_row->lawyer_name  = $request->lawyer_name;
        $data_row->rule         = $request->rule;
        $data_row->location      = $request->location ;
        $data_row->aos          = $request->area_of_service;
        $data_row->citation_name  = $request->citation_name;
        $data_row->journel        = $request->journel;
        $data_row->category       = $request->category;
        $data_row->jurnel_type    = $request->jurnel_type;
        $data_row->publish_month  = $request->publish_month;
        $data_row->publish_year   = $request->publish_year;
        
        $data_row->description    = $request->article_ckeditor;
        $data_row->statute_Section = $request->section_id;
        $data_row->head_notes      = $request->head_notes; 
        $data_row->short_desc      = $request->short_desc;
        $data_row->country_id        = $request->country_id;
        $data_row->state_id        = $request->state_id;
        
        $data_row->update();
        
        if($request->tags != '')
            $data_row->getTags()->sync($request->tags,true);
        
            $judges_id = $request->judge_name;
        if($request->other_judge != ""){
            $judge_names = explode(',',$request->other_judge);
            foreach ($judge_names as $values){
                $judges = new JudgesProfile();
                $judges->name = $values;
                $judges->save();
                
                array_push($judges_id,$judges->_id);
            }
        }
        // So added judges
            if($request->judge_name != '')
                $data_row->getJudges()->sync($judges_id,true);
            
        Session::flash('message', 'Updated Successfully.');
        return redirect()->route('lawyers.datalib.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $del_book = ContentLibrary::find($id);
        if($del_book->delete()){
            Session::flash('message', 'Deleted Successfully.');
            return redirect()->route('lawyers.datalib.index');
        }
    }
    
    function getDownload ($file_id){
        $content_data = ContentLibrary::find(decrypt($file_id));
        $get_ext = explode('.',$content_data->file_ext);
        return Storage::download("content/".decrypt($file_id).'.'.$get_ext[1], $content_data->file_ext );
    }
}
