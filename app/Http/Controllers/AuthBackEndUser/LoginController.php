<?php

namespace App\Http\Controllers\AuthBackEndUser;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Helpers\LogActivity;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //this->middleware('guest')->except('logout');
        $this->middleware('guest:user', ['except' => ['logout','userLogout']]);    
    }
    
    public function userLogout(Request $request)
    {
        
        Auth::guard('user')->logout();
        return redirect()->route('user.login');
    }
    
    public function showLoginForm(){
        
        return view('authBackendUser.login');
    }
    
    public function login(Request $request){
        // Validate the Data First
        $this->validate($request, [
            'email'=> 'required|email',
            'password' => 'required|min:6'
        ]);
        
        // Attempt to user logged in
        if(Auth::guard('user')->attempt(['email'=>$request->email, 'password'=>$request->password], $request->remember)){
            $data_post = Auth::guard('user')->user();
            LogActivity::addToLog('login','User has been logged in successfull.', $data_post);
            return redirect()->intended(route('user.dashboard'));
        }
        // if unsuccess
        return redirect()->back()->withInput($request->only('email', 'remember'));
    } 
}
