<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tag;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\ValidateTitle;

class TagController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin,lawyer,user');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Tag::orderBy('title', 'ASC')->paginate();
        return view('tags.index', ['tags'=>$data]);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tags.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ValidateTitle $request)
    {
        $request->validated();
        $data = $request->all();
        Tag::create($data);
        
        Session::flash('message', 'Added Successfully.');
        return redirect()->route('tags.create');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $get_data = Tag::find($id);
        return view('tags.update', ['view_Data' => $get_data]);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ValidateTitle $request, $id)
    {
        $request->validated();
        $ci = Tag::where('_id', '=', $id);
        
        $ci->update($request->all());
        Session::flash('message', 'Update Successfully.');
        return redirect()->route('tags.index');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $del_ci = Tag::find($id);
        if($del_ci->delete()){
            Session::flash('message', 'Deleted Successfully.');
            return redirect()->route('tags.index');
        }
    }
    
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function find(Request $request)
    {
        $term = trim($request->q);
        
        if (empty($term)) {
            return Response::json([]);
        }
        
        $tags = Tag::where('title','like',"%$term%")->limit(5)->get();
        //return $tags;
        $formatted_tags = [];
        
        foreach ($tags as $tag) {
            $formatted_tags[] = ['id' => $tag->id, 'text' => $tag->title];
        }
        return response()->json(array($formatted_tags), 200);
        
    }
}
