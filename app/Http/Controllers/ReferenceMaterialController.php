<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ReferenceMaterial;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\AreaOfSevice;

class ReferenceMaterialController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin,lawyer');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $user_id = Auth::id();
        $get_data = ReferenceMaterial::orderBy('title', 'ASC')->where('user_id','=',$user_id)->where('area_of_service_id','=',$id)->paginate();
        return view('reference.index', ['view_data'=>$get_data, 'aos'=>$id]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($aos_id)
    {
        $get_aos = AreaOfSevice::find($aos_id);
        return view('reference.create', ['aos'=>$get_aos]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $aos_id)
    {
        $data = $request->all();
        ReferenceMaterial::create($data);
        
        Session::flash('message', 'Added Successfully.');
        return redirect()->route('reference_note.create', $aos_id);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $get_data = ReferenceMaterial::find($id);
        //dd($get_data);
        return view('reference.update', ['view_data'=>$get_data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ci = ReferenceMaterial::where('_id', '=', $id);
        $aos = $request->area_of_service_id;
        $ci->update($request->all());
        Session::flash('message', 'Update Successfully.');
        return redirect()->route( 'reference.material' ,$aos);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $del_ci = ReferenceMaterial::find($id);
        $aos = $del_ci->area_of_service_id;
        if($del_ci->delete()){
            Session::flash('message', 'Deleted Successfully.');
            return redirect()->route('reference.material',$aos);
        }
    }
}
