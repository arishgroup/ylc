<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CircularInstitution;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\ValidateName;

class CircularInstitutionController extends Controller
{
    private $folder_view = "admin.miscellenous.institute.";
    //
    public function __construct()
    {
        $this->middleware('auth:admin,lawyer,user');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $get_data = CircularInstitution::orderBy('name')->paginate();
        
        return view( $this->folder_view.'index', ['institute_list'=>$get_data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view( $this->folder_view.'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ValidateName $request)
    {
        $request->validated();
        $data = $request->all();
        CircularInstitution::create($data);
        
        Session::flash('message', 'Added Successfully.');
        return redirect()->route('circular.institute.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $get_data = CircularInstitution::find($id);
        return view($this->folder_view.'edit', ['view_Data' => $get_data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ValidateName $request, $id)
    {

        $request->validated();
        $ci = CircularInstitution::where('_id', '=', $id);
       
        $ci->name = $request->name;
        $ci->update($request->all());
        Session::flash('message', 'Update Successfully.');
        return redirect()->route('circular.institute.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $del_ci = CircularInstitution::find($id);
        if($del_ci->delete()){
            Session::flash('message', 'Deleted Successfully.');
            return redirect()->route('circular.institute.index');
        }
    }
}
