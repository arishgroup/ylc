<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JudgesProfile;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Response;
use App\Http\Requests\ValidateJudgeProfile;

class JudgesProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin,user');
    }
    
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = JudgesProfile::orderBy('name', 'ASC')->paginate();
        return view('judgeProfile.index', ['jp'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('judgeProfile.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ValidateJudgeProfile $request)
    {
        $request->validated();
        
        
        $jProfile = new JudgesProfile();
        $jProfile->name = $request->name;
        $jProfile->designation = $request->designation;
        $jProfile->dob = $request->dob;
        $jProfile->description = $request->article_ckeditor;
        $jProfile->save();
        //dd($jProfile->_id);
        
        $file = $request->file('image');
        if($file) {
            //Display File Name
            $name = $file->getClientOriginalName();
            $file_ext = $file->getClientOriginalExtension();
            
            //Display File Name
            $name = $file->getClientOriginalName();
            $file_name = $jProfile->_id.'.'.$file_ext;
            
            if($file->storeAs('public/judges_profile/', $file_name)){
                $jProfile->file_ext = $name;
            } 
        }
        $jProfile->update();
        Session::flash('message', 'Added Successfully.');
        return redirect()->route('judge_profile.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $view_data = JudgesProfile::find($id);
        return view('judgeProfile.update', ['view_data'=>$view_data]);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ValidateJudgeProfile $request, $id)
    {
        $request->validated();
        $jProfile = JudgesProfile::find( $id );
        $jProfile->name = $request->name;
        $jProfile->designation = $request->designation;
        $jProfile->dob = $request->dob;
        $jProfile->description = $request->article_ckeditor;
        $file = $request->file('image');
        if($file) {
            //Display File Name
            $name = $file->getClientOriginalName();
            $file_ext = $file->getClientOriginalExtension();
            
            //Display File Name
            $name = $file->getClientOriginalName();
            $file_name = $jProfile->_id.'.'.$file_ext;
            
            if($file->storeAs('public/judges_profile/', $file_name)){
                $jProfile->file_ext = $name;
            }
        }
        $jProfile->update();
        
        Session::flash('message', 'Updated Successfully.');
        return redirect()->route('judge_profile.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $del_jp = JudgesProfile::find($id);
        if($del_jp->delete()){
            Session::flash('message', 'Deleted Successfully.');
            return redirect()->route('judge_profile.index');
        }
    }
    
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function find(Request $request)
    {
        $term = trim($request->q);
        
        if (empty($term)) {
            return Response::json([]);
        }
        
        $tags = JudgesProfile::where('name','like',"%$term%")->limit(5)->get();
        //return $tags;
        $formatted_tags = [];
        
        foreach ($tags as $tag) {
            $formatted_tags[] = ['id' => $tag->id, 'text' => $tag->name];
        }
        return response()->json(array($formatted_tags), 200);
        
    }
}
