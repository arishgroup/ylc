<?php

namespace App\Http\Controllers;

use App\Helper;
use Illuminate\Http\Request;
use App\LawyerRegistration;
use App\BackendUsers;
use Illuminate\Support\Facades\Response;
use App\TaskManagement;
use App\TaskManagementComments;
use App\TaskManagementAttachments;
use Yajra\Datatables\Datatables;
use Config;
use App\Http\Controllers\Controller;

use Auth;

class AjaxTaskManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function getAssignee(Request $request){
        $term_request = explode("&", $request->id );
        $term = $term_request[0];
        if(isset($term_request[1]))
            $term_id = $term_request[1];

        if (empty($term)) {
            return Response::json([]);
        }

        $assignee_list = '<select id="assignee_id" name="assignee_id" ><option value="">Select Assignee</option>';
        if($term  == 'lawyer') {
            $assignee_record = LawyerRegistration::orderBy('name','ASC')->get();

            foreach ($assignee_record as $assignee) {
                if(isset($term_id ) and ($term_id == $assignee->id))
                    $assignee_list .= '<option selected="selected" value="'.$assignee->id.'">'.$assignee->name.'</option>';
                else
                    $assignee_list .= '<option value="'.$assignee->id.'">'.$assignee->name.' '.$assignee->lastname.'('.$assignee->serial_no.')</option>';
            }
        }

        if($term  == 'client'){

        }

        if($term  == 'user'){
            $assignee_record = BackendUsers::orderBy('name','ASC')->get();

            foreach ($assignee_record as $assignee) {
                if(isset($term_id ) and ($term_id == $assignee->id))
                    $assignee_list .= '<option selected="selected" value="'.$assignee->id.'">'.$assignee->name.'</option>';
                else
                    $assignee_list .= '<option value="'.$assignee->id.'">'.$assignee->name.'</option>';
            }
        }

        $assignee_list .= '</select>';
        return Response::json(array(
            'success' => true,
            'data'   => $assignee_list
        ));
    }

    public function assignerTasksStatus($assigner_id)
    {

        $total = TaskManagement::where('assigner_id', '=', "$assigner_id")->count();
        $pending = TaskManagement::where('assigner_id','=', "$assigner_id")->where('status','=',0)->count();
        $in_progress = TaskManagement::where('assigner_id','=', $assigner_id)->where('status','=',1)->count();
        $complete = TaskManagement::where('assigner_id','=', $assigner_id)->where('status','=',2)->count();
        $assigner_status= array('total'=>$total, 'pending'=>$pending, 'in_progress'=>$in_progress,'complete'=>$complete);

        return Response::json(array(
            'success' => true,
            'data'   => $assigner_status
        ));
    }

    public function assigneeTasksStatusCount($assigner_type){

        $total_count = $pending_count = $in_progress_count = $complete_count = 0;
        $user_id =auth()->guard($assigner_type)->user()->id;
        $records = TaskManagement::where("assignee_id", $user_id)
            ->where("assignee_type", $assigner_type)
            ->get();

        $total_count = $records->count();

        $pending_count = $records->filter(function ($records){
            return $records->status == 0;
        })->count();

        $in_progress_count = $records->filter(function ($records){
            return $records->status == 1;
        })->count();

        $complete_count = $records->filter(function ($records){
            return $records->status == 2;
        })->count();

        $fullResult = array('total'=>$total_count, 'pending'=>$pending_count, 'in_progress'=>$in_progress_count,'complete'=>$complete_count);

        return Response::json(array(
            'success' => true,
            'data'   => $fullResult
        ));
    }




    public function assigneeTasksStatus(Request $request)

    {
//        dd($request);

        $fullResult = array ();
        $assigner_id  = $request->assigner_id;
       // $assigner_id  = '5b980986a67e121fd4007c23';

        $assignee_type  = $request->assignee_type;
        $assignee_id  = $request->assignee_id;
        $task_name = $request->task_name;
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $priority = $request->priority;

        $help = new Helper();
        $getUserType = $help->getUsertype();

        $total = TaskManagement::orderBy('_id', 'DESC')
                    ->where('assigner_id','like',$assigner_id)
                    ->where('assigner_type','like',$getUserType);

        $pending = TaskManagement::orderBy('_id', 'DESC')
            ->where('assigner_id','like',$assigner_id)
            ->where('assigner_type','like',$getUserType)
            ->where('status','=',0);

        $in_progress = TaskManagement::orderBy('_id', 'DESC')
            ->where('assigner_id','like',$assigner_id)
            ->where('assigner_type','like',$getUserType)
            ->where('status','=',1);
        $complete = TaskManagement::orderBy('_id', 'DESC')
            ->where('assigner_id','like',$assigner_id)
            ->where('assigner_type','like',$getUserType)
            ->where('status','=',2);

        if(!empty($assignee_type)){
            $total->where('assignee_type', 'like', $assignee_type);
            $pending->where('assignee_type', 'like', $assignee_type);
            $in_progress->where('assignee_type', 'like', $assignee_type);
            $complete->where('assignee_type', 'like', $assignee_type);
        }

        if(!empty($assignee_id)){
            $total->where('assignee_id', 'like', $assignee_id);
            $pending->where('assignee_id', 'like', $assignee_id);
            $in_progress->where('assignee_id', 'like', $assignee_id);
            $complete->where('assignee_id', 'like', $assignee_id);
        }

        if(!empty($priority)){
            $total->where('priority', 'like', $priority);
            $pending->where('priority', 'like', $priority);
            $in_progress->where('priority', 'like', $priority);
            $complete->where('priority', 'like', $priority);
        }

        if(!empty($task_name)){
            $task_name="%$task_name%";
            $total->where('task_name', 'like', $task_name);
            $pending->where('task_name', 'like', $task_name);
            $in_progress->where('task_name', 'like', $task_name);
            $complete->where('task_name', 'like', $task_name);
        }

        if(!empty($start_date) && !empty($end_date)){
            //dd('ss');
            //$start_date = '2018-11-29';
            $total->where('assigner_start_date', '>=', $start_date);
            $total->where('assigner_end_date', '<=', $end_date);


            $pending->where('assigner_start_date', '>=', $start_date);
            $pending->where('assigner_end_date', '<=', $end_date);

            $in_progress->where('assigner_start_date', '>=', $start_date);
            $in_progress->where('assigner_end_date', '<=', $end_date);

            $complete->where('assigner_start_date', '>=', $start_date);
            $complete->where('assigner_end_date', '<=', $end_date);
        } else {

            if(!empty($start_date)) {
                $total->where('assigner_start_date', '=', $start_date);
                $pending->where('assigner_start_date', '=', $start_date);
                $in_progress->where('assigner_start_date', '=', $start_date);
                $complete->where('assigner_start_date', '=', $start_date);
            }

            if(!empty($end_date)){

                $total->where('assigner_end_date', '=', $end_date);
                $pending->where('assigner_end_date', '=', $end_date);
                $in_progress->where('assigner_end_date', '=', $end_date);
                $complete->where('assigner_end_date', '=', $end_date);
            }
        }


        $total_count = $total->count();
        $pending_count =  $pending->count();
        $in_progress_count =  $in_progress->count();
        $complete_count = $complete->count();

        $fullResult = array('total'=>$total_count, 'pending'=>$pending_count, 'in_progress'=>$in_progress_count,'complete'=>$complete_count);

        return Response::json(array(
            'success' => true,
            'data'   => $fullResult
        ));

    }

    public function getDataTables(Request $request)
    {
        $input = $request->id;

        $input_value = explode('&',$input);
        $assignee_type = $input_value[0];
        $assignee_id = $input_value[1];
        $task_name = $input_value[2];
        $task_type = $input_value[3];

//        dd($task_type);

        $tasks = TaskManagement::orderBy('_id','asc');
        if($assignee_type !=""){
            $tasks->where('assignee_type', $assignee_type);

        }
        if($assignee_id !=""){
            $tasks->where('assignee_id', $assignee_id);

        }
        if($task_name !=""){
            $tasks->where('task_name','like', '%'.$task_name.'%');

        }
        if($task_type !=""){
            $tasks->where('status','=', (int)$task_type );
        }
        //dd($tasks);
        $tasks = $tasks->get();

        return Datatables::of($tasks)
            ->addColumn('action', function ($tasks) {
                return '<a href="'. route('taskManagement.create', $tasks->_id) .'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                   <a href="'. route('taskManagement.destroy', $tasks->_id) .'" class="btn btn-xs btn-danger confirmation "><i class="glyphicon glyphicon-trash"></i> Delete</a>   ';
            })
            ->addColumn('assignee_name', function ($tasks) {
                return empty($tasks->getTaskLawyer) ? $tasks->getTaskUser->name : $tasks->getTaskLawyer->name;
            })
            ->addColumn('priority', function ($tasks) {
                $list = Config::get('constants.priority');
                return ($list[$tasks->priority] == "Select Priority" ) ? 'N/A' : $list[$tasks->priority];
            })
            ->make(true);
    }

    public function getAssigneeDataTables(Request $request)
    {
        $input = $request->id;

        $input_value = explode('&',$input);
        $assignee_type = $input_value[0];
        $task_type = $input_value[1];

        $user_id =auth()->guard($assignee_type)->user()->id;
        $tasks = TaskManagement::orderBy('_id','asc');
        $tasks->where('assignee_id', $user_id);

        if($task_type!="")
        {
            $tasks->where('status','=', (int)$task_type );
        }

        $tasks = $tasks->get();
        return Datatables::of($tasks)
            ->addColumn('assignee_name', function ($tasks) {
                return empty($tasks->getTaskLawyer) ? $tasks->getTaskUser->name : $tasks->getTaskLawyer->name;
            })
            ->addColumn('priority', function ($tasks) {
                $list = Config::get('constants.priority');
                return ($list[$tasks->priority] == "Select Priority" ) ? 'N/A' : $list[$tasks->priority];
            })
            ->addColumn('status', function ($tasks) {
                $list = Config::get('constants.status');
                return ($list[$tasks->status] == "Select Status" ) ? 'N/A' : $list[$tasks->status];
            })
            ->addColumn('action', function ($tasks) {
                if($tasks->assignee_type == "lawyer"){
                    return ' <a class="btn btn-xs btn-info" data-toggle="tooltip" data-placement="top" title="Detail" href="'. route('lawyers.taskManagement.edit', $tasks->id) .'"><i class="fa fa-book"></i></a> ';
                }
                if($tasks->assignee_type == "user"){
                    return ' <a class="btn btn-xs btn-info" data-toggle="tooltip" data-placement="top" title="Detail" href="'. route('users.taskmanagement.edit', $tasks->id) .'"><i class="fa fa-book"></i></a> ';
                }


            })
            ->make(true);

    }


    public function dataTablesNavigation(Request $request)
    {
        $result=array();
        $merge_id = $request->id;
        $value = explode('&',$merge_id );
        $action  = $value[0];
        $id = $value[1];
        $help = new Helper();
        if($action == 'first'){
            $result = TaskManagement::orderBy('_id','asc')->first();
        }
        if($action == 'last'){
            $result = TaskManagement::orderBy('_id','desc')->first();
        }
        if($action == 'prev'){
            $result = TaskManagement::where('_id', '<', $id)->orderBy('_id','desc')->first();
        }
        if($action == 'next'){
            $result = TaskManagement::where('_id', '>', $id)->orderBy('_id','asc')->first();
        }
        $data[] = $result;
        $data[] = $help->getCommentsHtml($result);
        $data[] = $help->getAttachmentsHtml($result);

        return Response::json(array(
            'success' => true,
            'data'   => $data
        ));
    }

    public function assigneeDataTablesNavigation(Request $request)
    {

        $result=array();
        $merge_id = $request->id;
        $value = explode('&',$merge_id );
        $action  = $value[0];
        $id = $value[1];
        $assignee_type = $value[2];
        $user_id =auth()->guard($assignee_type)->user()->id;
        $records = TaskManagement::where('assignee_type',$assignee_type)->where('assignee_id',$user_id);
        $help = new Helper();
        if($action == 'first'){
            $result = $records->orderBy('_id','asc')->first();
        }
        if($action == 'last'){
            $result = $records->orderBy('_id','desc')->first();
        }
        if($action == 'prev'){
            $result = $records->where('_id', '<', $id)->orderBy('_id','desc')->first();
        }
        if($action == 'next'){
            $result = $records->where('_id', '>', $id)->orderBy('_id','asc')->first();
        }
        $data[] = $result;
        $data[] = $help->getCommentsHtml($result);
        $data[] = $help->getAttachmentsHtml($result);

        return Response::json(array(
            'success' => true,
            'data'   => $data
        ));
    }

    public function startTask(Request $request){

        $task_managements = TaskManagement::find($request->id);
        $task_managements->assignee_start_date = date('Y-m-d H:i:s');
        $task_managements->status = 1;
        $task_managements->save();
        return Response::json(array(
            'success' => true,
            'data'   => 'Task is Started'
        ));
    }

    public function endTask(Request $request){

        $task_managements = TaskManagement::find($request->id);
        $task_managements->assignee_end_date = date('Y-m-d H:i:s');
        $task_managements->status = 2;
        $task_managements->save();
        return Response::json(array(
            'success' => true,
            'data'   => 'Task is Ended'
        ));
    }

    public function assignee_update_tasks(Request $request)
    {
        $id = $request->current_record;
        $task_managements = TaskManagement::find($id);
        $assigner_type = "user";
        $redirect = "users.taskmanagement.edit";

        if(auth()->guard('lawyer')->check()){
            $assigner_type = "lawyer";
            $redirect = "lawyers.taskManagement.edit";
        }
        $user_id =auth()->guard($assigner_type)->user()->id;

        $task_managements->comments = $request->get('comment');
        if(!empty($task_managements->comments)){

            $save_data2 = new TaskManagementComments();
            $save_data2->comment = $task_managements->comments;
            $save_data2->task_id = $id;
            $save_data2->user_type = $assigner_type;
            $save_data2->user_id = $user_id;

            $save_data2->save();
        }

        if($request->hasFile('attachments'))
        {
            $files = $request->file('attachments');
            foreach ($files as $file) {
                $file_name = $file->getClientOriginalName();
                $file_ext = $file->getClientOriginalExtension();

                $save_data3 = new TaskManagementAttachments();
                $save_data3->file_name= $file_name;
                $save_data3->file_extension= $file_ext;
                $save_data3->task_id = $id;
                $save_data3->user_type = $assigner_type;
                $save_data3->user_id = $user_id;

                $save_data3->save();

                $store_file_name = $save_data3->id.'.'.$file_ext;

                $file->storeAs(
                    'public/taskmanagement/', $store_file_name
                );
            }
        }

        return redirect()->route($redirect, $id);
    }

}
