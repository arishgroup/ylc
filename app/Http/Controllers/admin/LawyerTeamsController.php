<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\admin\LawyerTeam;
use Yajra\Datatables\Datatables;
use App\Admin;
use App\LawyerRegistration;
use App\Helper;
use Illuminate\Support\Facades\Session;

class LawyerTeamsController extends Controller
{
    private $view_folder = 'admin.lawyers.teams';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->view_folder.'.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // GET AOS
        $aos = new Helper();
        $aos = $aos->categoryTreeWithOutSubMark();
        
        $data = array('aos'=>$aos, 'aos_id'=>$request->aos_id);
        
        return view($this->view_folder.'.create',['data'=>$data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validations required
       // dd($request);
        if( $request->input('_id')){
            $team_id = $request->input('_id');
            $team = LawyerTeam::find($team_id);
            $msg = "Record update successfully";
        } else {
        // login implementation
            $team = new LawyerTeam();
        }
        $team->title = $request->input('title');
        $team->description = $request->input('description');
        $team->aos_id = $request->input('aos_id');
        $team->team_head = $request->input('head_lawyer');
       
        $team->save();
        
        if(!empty($request->input('head_lawyer')))
            $team->getLawyers()->sync($request->input('PairedSelectBox'),true);
        
        Session::flash('message', 'Added Successfully.');
        return redirect()->route('team.create', [$request->input('aos_id')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function fetchRecord(){
        //dd(Datatables::of(LawyerTeam::query())->make(true));
        $teams = LawyerTeam::all();
        return Datatables::of($teams)
        ->editColumn('teamhead', function ($teams) {
            $lawyer_name= '';
            if(!empty($teams->getLawyer->name)){
                $lawyer_name= $teams->getLawyer->name.' '.$teams->getLawyer->lastname;
            }
            
            return $lawyer_name;
            })
        ->editColumn('member_count', function ($teams) {
                $member_count= '0';
                if(!empty($teams->lawyer_registration_ids)){
                    $member_count= count($teams->lawyer_registration_ids);
                }
                
                return $member_count;
            })
        ->editColumn('aos', function ($teams) {
            $aos= '';
            if(!empty($teams->getAOS->name)){
                $aos= $teams->getAOS->name;
            }
            
            return $aos;
        })->make(true);
    }
}
