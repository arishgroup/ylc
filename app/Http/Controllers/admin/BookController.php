<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\UploadedBook;
use App\UploadBookfile;
use Illuminate\Support\Facades\Config;

class BookController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search_ch = $request->ch;
        if($request->ch == "") {
            $search_ch = "";
        }
        $data = UploadedBook::where('name','like', $search_ch.'%')->orderBy('name', 'ASC')->get();
        return view('admin.books.index', ['book'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // get Book details
        $data_parent = UploadedBook::find($id);
        
        $data = UploadBookfile::where('uploaded_book_id','like', $id)->orderBy('name', 'ASC')->get();
        return view('admin.books.attachmentListing', ['book'=>$data, 'parent'=>$data_parent]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $countryList = Config::get('constants.country');
        
        // get the row for update
        $data_row = UploadedBook::find($id);
        
        return view('admin.books.update', [ 'row_data'=>$data_row, 'country'=>$countryList]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data_row = UploadedBook::find($request->_id);
        $data_row->name         = ucwords(strtolower(trim($request->book_title)));
        $data_row->description  = $request->description;
        $data_row->year         = $request->book_year;
        $data_row->country_id        = $request->country_id;
        $data_row->state_id        = $request->state_id;
        $data_row->save();
        
        $files = $request->file('image');
        
        if($request->hasFile('image'))
        {
            $parent_id = $data_row->_id;
            foreach ($files as $file) {
                if($file) {
                    // Get File extension
                    $file_ext = $file->getClientOriginalExtension();
                    
                    //Display File Name
                    $name = $file->getClientOriginalName();
                    $data_post_file = new UploadBookfile();
                    $data_post_file->ext               = $file_ext;
                    $data_post_file->file_name         = $name;
                    $data_post_file->uploaded_book_id  = $parent_id;
                    $data_post_file->save();
                    $id = $data_post_file->_id;
                    $file_name = 'book_'.$id.'.'.$file_ext;
                    if($file->storeAs('public/books/'.$parent_id, $file_name)){
                        
                    }
                }
            }
        }
        Session::flash('message', 'Updated Successfully.');
        return redirect()->route('admin.book.edit', [$request->_id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
