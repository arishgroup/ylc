<?php

namespace App\Http\Controllers;

use App\StatuteType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\ValidateNameBookType;

class StatuteTypeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin,lawyer,user');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bookTypes = StatuteType::orderBy('_id')->where('is_del', '!=', 1)->paginate();
        
        return view('statute.type.index', ['bookType'=>$bookTypes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('statute.type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ValidateNameBookType $request)
    {
        $request->validated();
        $data_post = new StatuteType();
        $data_post->name=$request->name_book_type;
        $data_post->save();
        Session::flash('message', 'Added Successfully.');
        return redirect()->route('statute.type.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StatuteType  $bookType
     * @return \Illuminate\Http\Response
     */
    public function show(BookType $bookType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StatuteType  $bookType
     * @return \Illuminate\Http\Response
     */
    public function edit($bookType)
    {
        $data = StatuteType::find($bookType);
        return view('statute.type.update',['book_type'=>$data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StatuteType  $bookType
     * @return \Illuminate\Http\Response
     */
    public function update(ValidateNameBookType $request)
    {
        $request->validated();
        $data = StatuteType::where('_id', '=', $request->_id)->first();
        $data->name=$request->name_book_type;
        $data->update();
        Session::flash('message', 'Update Successfully.');
        return redirect()->route('statute.type.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StatuteType  $bookType
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $del_book = StatuteType::find($id);
        if($del_book->delete()){
            Session::flash('message', 'Deleted Successfully.');
            return redirect()->route('statute.type.index');
        }
    }
}
