<?php

namespace App\Http\Controllers;


use App\BookSections;
use App\LawyerRegistration;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\ContentLibrary;
use Illuminate\Support\Facades\Config;
use App\Helper;
use App\admin\LawyerTeam;
use Yajra\DataTables\DataTables;

class AjaxController extends Controller
{
    
    function getSections($id,$select_val){
        $make_drop_down = "<select name='section_id' id='section_id' class='selections'><option value='0'>Select Section</option><option value='-1'>No Section Found</option></select>";
        $section_data = BookSections::where('book_id','=',$id)->get();
        if($section_data) {
            $make_drop_down = "<select name='section_id[]' id='section_id' class='selections form-control'><option value='0'>Select Section</option>";
            foreach($section_data as $val){
                $selected = '';
                if($select_val == $val->_id){
                    $selected = 'selected="selected"';
                }
                
                $make_drop_down .= "<option value='".$val->_id."' ".$selected.">".$val->section_no."</option>";
            }
            $make_drop_down .= '</select>';
        } 
        
        return Response::json(array(
            'success' => true,
            'data'   => $make_drop_down
        )); 
    }
    
    function getCaseLaws(Request $request){
        $term = trim($request->q);
        
        if (empty($term)) {
            return Response::json([]);
        }
        
        $tags = ContentLibrary::where('citation_name','like',"%$term%")->limit(5)->get();
        //return $tags;
        $formatted_tags = [];
        
        foreach ($tags as $tag) {
            $formatted_tags[] = ['id' => $tag->id, 'text' => $tag->citation_name];
        }
        return response()->json(array($formatted_tags), 200);
    }
    
    function getCaseLawContent(Request $request){
        $book_title = ContentLibrary::find($request->id);
        $contentToShow = $book_title->head_notes;
        if($request->content == 0){
            $contentToShow = $book_title->description;
        }
        
        return Response::json( array('content'=> $contentToShow), 200 );
    }
    
    public function getCountryState($country_code){
        
        if($country_code == 'us'){
        
            $return_array = '<option value="">Select State</option>';
            $state_list = Config::get('constants.US_STATE');
            foreach ($state_list as $key => $value){
                $return_array .= '<option value="'.$key.'">'.$value.'</option>';
            }
        }

        if($country_code == 'pk') {
            $return_array = '<select id="state_id" name="state_id" >';
            $return_array .= '<option value="">Select Province</option>';
            $state_list = Config::get('constants.PAK_PROVINCE');
            foreach ($state_list as $key => $value){
                $return_array .= '<option value="'.$key.'">'.$value.'</option>';
            }
        }
        $return_array .= '</select>';
        
        
        return Response::json(array(
            'success' => true,
            'data'   => $return_array
        )); 
       
    }
    
    public function getCity($state_code){
        
        $list = Config::get('constants');
        
        $return_array = "";
        if(array_key_exists($state_code, $list)) {
            $return_array = '<select id="city_id" name="city_id" >';
            $return_array .= '<option value="">Select City</option>';
            foreach($list[$state_code] as $key => $value){
                $return_array .= '<option value="'.$key.'">'.$value.'</option>';
            }
            $return_array .= '</select>';
        }
        
        return Response::json(array(
            'success' => true,
            'data'   => $return_array
        ));
        
    }

    public function getLawyers(Request $request)
    {
        $group_list = '<select id="group_id" name="group"><option value="0" disabled>Select Lawyer Name</option>';
        $group_record = LawyerRegistration::orderBy('name', 'ASC')->get();
        foreach ($group_record as $group) {
            if (isset($term_id) and ($term_id == $group->id))
                $group_list .= '<option selected="selected" value="' . $group->id . '">' . $group->name . '</option>';
            else
                $group_list .= '<option value="' . $group->id . '">' . $group->name . '</option>';
        }
        $group_list .= '</select>';
        return Response::json(array(
            'success' => true,
            'data'   => $group_list
        ));

    }

    public function getClients(Request $request)
    {
        $group_list = '<select id="group_id" name="group"><option value="0" disabled>Select Lawyer Name</option>';
        $group_list .= '<option value="">No Record Found</option>';
        $group_list .= '</select>';
        return Response::json(array(
            'success' => true,
            'data'   => $group_list
        ));

    }
    
    public function get_lawyers(Request $request){
        $aos_all_id = array();
        $aos_child = new Helper();
        $aos = $aos_child->categoryTreeWithOutSubMark($request->id);
        $aos_all_id[] = $request->id;
        // get all AOS ID
        foreach ($aos as $key => $value){
            $aos_all_id[] = $key;
        }
        
        // get all active lawyers
        $lawyers = LawyerRegistration::where(function ($query) {
                                            $query->where('role','like', '5be40c2be9cfa65c0c003583')
                                            ->orWhere('role', 'like', '5be44f6ae9cfa65c0c00367b');})
                                            ->whereIn('area_of_service',  $aos_all_id)
        ->orderBy('name','ASC')->get();
        $group_list = '<select id="team_head" name="team_head" onchange="refreshDropdown(this.value)"><option value="0">Select Team Head</option>';
        if($lawyers->count()) {
            foreach ($lawyers as $lawyer) {
                $group_list .= '<option value="'.$lawyer->_id.'">'.$lawyer->name.' '.$lawyer->lastname.'</option>';
            }
        } else {
            $group_list .= '<option value="">No Record Found</option>';
        }
        $group_list .= '</select>';
        return Response::json(array(
            'success' => true,
            'data'   => $group_list
        ));
        
    }
    
    public function getAllTeams(Request $request){
        
        //dd(Datatables::of(LawyerTeam::query())->make(true));
        $teams = LawyerTeam::all();
        
        return Datatables::of($teams)
        ->editColumn('teamhead', function ($teams) {
            $lawyer_name= '';
            if(!empty($teams->getLawyer->name)){
                $lawyer_name= $teams->getLawyer->name.' '.$teams->getLawyer->lastname;
            }
            
            return $lawyer_name;
        })
        ->editColumn('member_count', function ($teams) {
            $member_count= '0';
            if(!empty($teams->lawyer_registration_ids)){
                $member_count= count($teams->lawyer_registration_ids);
            }
            
            return $member_count;
        })
        ->editColumn('aos', function ($teams) {
            $aos= '';
            if(!empty($teams->getAOS->name)){
                $aos= $teams->getAOS->name;
            }
            
            return $aos;
        })
        ->editColumn('totalcases', function ($teams) {
            $aos= '0';
            return $aos;
        })
        ->make(true);
        
        
    }
    function top_navigation (Request $request){
        $model =  $request->post('table');
        
        if($request->post('nav') == 'last') {
            $data = $model::orderBy('_id','desc')->first();
        }
        return Response::json(array(
            'success' => true,
            'data'   => $data
        ));
        
    }

}
