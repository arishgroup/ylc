<?php

namespace App\Http\Controllers;

use App\UserType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\ValidateTitle;
use Yajra\DataTables\DataTables;

class UserTypeController extends Controller
{
    private $cnt = 1;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin,lawyer');
        $menu = 'active menu-open';
        
        
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $data = UserType::orderBy('title', 'ASC')->paginate(15);
        return view('admin.userType.index', ['userType'=>$data]);
    }

    function datatable(Request $request){
        $user_type = UserType::orderBy('_id')->get();
        //dd($lawyers);
        return Datatables::of($user_type)
        ->addColumn('action', function ($user_type) {
            return '<a href="'. route('utype.edit', [$user_type->_id]) .'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
        })
        ->editColumn('_id', function ($user_type) {
            return $this->cnt++;
        })
        ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.userType.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ValidateTitle $request)
    {
        $request->validated();

        $data = $request->all();
        UserType::create($data);
        
        Session::flash('message', 'Added Successfully.');
        return redirect()->route('utype.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserType  $userType
     * @return \Illuminate\Http\Response
     */
    public function show(UserType $userType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserType  $userType
     * @return \Illuminate\Http\Response
     */
    public function edit($userType)
    {
        $get_data = UserType::find($userType);
        return view('admin.userType.update', ['view_Data' => $get_data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserType  $userType
     * @return \Illuminate\Http\Response
     */
    public function update(ValidateTitle $request, $id)
    {
        $request->validated();

        $ci = UserType::where('_id', '=', $id);
        
        $ci->update($request->all());
        Session::flash('message', 'Update Successfully.');
        return redirect()->route('utype.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserType  $userType
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $del_ci = UserType::find($id);
        if($del_ci->delete()){
            Session::flash('message', 'Deleted Successfully.');
            return redirect()->route('utype.index');
        }
    }
}
