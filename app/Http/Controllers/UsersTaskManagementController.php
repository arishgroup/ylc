<?php

namespace App\Http\Controllers;

use App\Helper;
use Carbon\Carbon;
use App\TaskManagement;
use App\TaskManagementComments;
use App\TaskManagementAttachments;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class UsersTaskManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $folder_view = "user.taskManagement.";

    public function __construct()
    {
        $this->middleware('auth:admin,lawyer,user', ['except' => ['logout']]);
    }

    public function index()
    {
        return view($this->folder_view.'index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $help = new Helper();
        $user_id = Auth::user()->_id;
        $prev_6_month = Carbon::now()->subMonths(6)->format('Y-m-d');
        $last_6_month_tasks = TaskManagement::orderBy('assigner_end_date', 'asc')
            ->where("assignee_id","=", $user_id)
            ->where("assigner_end_date",">", $prev_6_month)
            ->get();

        $cnt_m = 0;
        foreach ($last_6_month_tasks as $records){
            $data_array = $records->assigner_end_date;
            $date_numbers = date('m',strtotime($data_array));
            $month = $date_numbers;

            if($cnt_m != $month) {
                $cnt = 0;
                $cnt2 = 0;
                $cnt3 = 0;
                $cnt_m = $month;
                $graph[$month]['exceed'] = $cnt;
                $graph[$month]['ontime'] = $cnt2;
                $graph[$month]['pending'] = $cnt3;
            }
            if(strtotime($records->assigner_end_date) < strtotime($help->getFormattedDate($records->assignee_end_date)))
            {
                $cnt =  $cnt+1;
                $graph[$month]['exceed'] = $cnt;
            }
            else{

                $cnt2 =  $cnt2+1;
                $graph[$month]['ontime'] = $cnt2;
            }

            if(empty($records->assignee_start_date)){
                $cnt3 =  $cnt3+1;
                $graph[$month]['pending'] = $cnt3;
            }
        }

        $data_collection = TaskManagement::find($id);
        return view($this->folder_view.'edit',['graph'=>$graph,'data'=>$data_collection]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        dd('here');
        $task_managements = TaskManagement::find($id);
        $assigner_type = "user";

        if(Auth::guard('admin')->check())
            $assigner_type = "admin";

        if(Auth::guard('lawyer')->check())
            $assigner_type = "lawyer";

        if(Auth::guard('web')->check())
            $assigner_type = "client";

        $user_id = Auth::user()->_id;

        $task_managements->comments = $request->get('comment');
        if(!empty($task_managements->comments)){

            $save_data2 = new TaskManagementComments();
            $save_data2->comment = $task_managements->comments;
            $save_data2->task_id = $id;
            $save_data2->user_type = $assigner_type;
            $save_data2->user_id = $user_id;

            $save_data2->save();
        }

        if($request->hasFile('attachments'))
        {
            $files = $request->file('attachments');
            foreach ($files as $file) {
                $file_name = $file->getClientOriginalName();
                $file_ext = $file->getClientOriginalExtension();

                $save_data3 = new TaskManagementAttachments();
                $save_data3->file_name= $file_name;
                $save_data3->file_extension= $file_ext;
                $save_data3->task_id = $id;
                $save_data3->user_type = $assigner_type;
                $save_data3->user_id = $user_id;

                $save_data3->save();

                $store_file_name = $save_data3->id.'.'.$file_ext;

                $file->storeAs(
                    'public/taskmanagement/', $store_file_name
                );
            }
        }


//        Session::flash('message', 'Data Updated.');
        return redirect()->route('lawyers.taskManagement.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function startTask(Request $request){

        $task_managements = TaskManagement::find($request->id);
        $task_managements->assignee_start_date = date('Y-m-d H:i:s');
        $task_managements->status = 1;
        $task_managements->save();
//        return $request->id;

        return Response::json(array(
            'success' => true,
            'data'   => 'Task is Started'
        ));


    }

    public function endTask(Request $request){

        $task_managements = TaskManagement::find($request->id);
        $task_managements->assignee_end_date = date('Y-m-d H:i:s');
        $task_managements->status = 2;
        $task_managements->save();
//        return $request->id;

        return Response::json(array(
            'success' => true,
            'data'   => 'Task is Ended'
        ));


    }
}
