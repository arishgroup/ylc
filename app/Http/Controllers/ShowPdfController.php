<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
class ShowPdfController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin,lawyer,user', ['except' => ['logout']]);
    }
    public function showPdf ($enc_path){
        if(Auth::guard('lawyer')->check()){
            return view('lawyers.show_pdf', ['file_path' => $enc_path]);
        }
        if(Auth::guard('admin')->check()){
            return view('admin.show_pdf', ['file_path' => $enc_path]);
        }
        if(Auth::guard('user')->check()){
            return view('show_pdf', ['file_path' => $enc_path]);
        }
    }
    
    public function showWord ($enc_path){
        return view('show_word', ['file_path' => $enc_path]);
    }
}
