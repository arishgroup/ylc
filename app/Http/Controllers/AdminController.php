<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;
use Illuminate\Support\Facades\Session;
use App\Location;
use Illuminate\Support\Facades\Storage;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.dashboard');
    }
    
    public function edit($id){
        // Get location 
        $location = array();
        $loc_data = Location::orderBy('title')->get();
        if($loc_data->count()){
            foreach($loc_data as $loc){
                $location[$loc->_id] = $loc->title;
            }
        }
        $data = array ('loc' => $location);
        return view('admin.profile', ['data'=>$data]);
    }
    
    public function update (Request $request, $id){ 
        
        $data_row = Admin::find($id);
        
        $file = $request->file('profile_img');
        if($file) {
            
            $file_ext = $file->getClientOriginalExtension();
            //Display File Name
            //$name = $file->getClientOriginalName();
            $file_name = 'img_'.$id.'.'.$file_ext;
            // delete file before upload
            if(Storage::disk('public')->exists('/admin/profile/'.$file_name)){
                Storage::delete('public/admin/profile/'.$file_name);
            }
            
            if($file->storeAs('public/admin/profile/', $file_name)){
                $data_row->profile_img = $file_ext;
            }
            
        }
        
        $data_row->name         = ucwords(strtolower(trim($request->name)));
        $data_row->lastname     = $request->lastName;
        $data_row->job_title    = $request->jobTitle;
        $data_row->location     = $request->location;
        $data_row->address      = $request->address;
        $data_row->experiance   = $request->experiance;
        $data_row->notes        = $request->notes;
        if($request->password){
            $data_row->password        = bcrypt($request->password);
        }
        
        
        $data_row->save();
            
            Session::flash('message', 'Updated Successfully.');
            return redirect()->route('admin.profile', [$id]);
    }
}
