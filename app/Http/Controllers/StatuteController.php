<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BookType;
use App\Book;
use Illuminate\Support\Facades\Session;
use App\Tag;
use Barryvdh\DomPDF\PDF;
use App\Http\Requests\ValidateStatue;
use Illuminate\Support\Facades\Config;
use App\Helper;

use Auth;
use App\Helpers\LogActivity;

class StatuteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin,lawyer,user');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search_ch = $request->ch;
        if($request->ch == "") {
            $search_ch = "a";
        }
        $data = Book::where('name','like', $search_ch.'%')->orderBy('name', 'ASC')->get();
        return view('statute.index', ['book'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countryList = Config::get('constants.country');
        
        $book_type_array = array();
        $book_type = BookType::orderBy('_id')->get();
        if($book_type->count()){
            foreach ($book_type as $value){
                $book_type_array[$value->_id] = $value->name;
          }  
        }
        
        return view('statute.create',['book_type' => $book_type_array, 'country'=>$countryList ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ValidateStatue $request)
    {
        $user_id = Auth::user()->_id;
        $helper = new Helper();
        $user_type = $helper->getUsertype();
        
        $request->validated();
        $data_post = new Book();
        $data_post->name        = ucwords(strtolower(trim($request->book_title)));
        $data_post->type        = $request->type_id;
        $data_post->country_id        = $request->country_id;
        $data_post->state_id        = $request->state_id;
       
        $data_post->added_by        = $user_id;
        $data_post->user_type        = $user_type;
        
        if($data_post->country_id == 'pk'){
            $data_post->city_id        = $request->city_id;
        } else {
            $data_post->city_id        = '';
        }
        
        $data_post->description = $request->description;
        $data_post->year = $request->book_year;
        $data_post->save();
        // End the tags in the tabele
        if($request->tags)
        $data_post->getTags()->sync($request->tags,false);
        
        $file = $request->file('image');
        $id = $data_post->_id;
        if($file) {
            // Get File extension
            $file_ext = $file->getClientOriginalExtension();
            
            //Display File Name
            $name = $file->getClientOriginalName();
            $file_name = 'statute_'.$id.'.'.$file_ext;
            
            if($request->file('image')->storeAs('public/statute', $file_name)){
                $data_post->file_ext = $file_ext;
                $data_post->update();
            }
        }
        
        LogActivity::addToLog('statute','New Statute has been added.', $data_post);
        Session::flash('message', 'Added Successfully.');
        return redirect()->route('statute.create');
       // return redirect()->route('statute.index', ['ch'=>substr(trim($request->book_title), 0,1)]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $id)
    {
        $countryList = Config::get('constants.country');
        
        $book_type_array = array();
        $book_type = BookType::where('is_del','!=','1')->get();
        if($book_type->count()){
            foreach ($book_type as $value){
                $book_type_array[$value->_id] = $value->name;
            }
        }
        // get the row for update
        $data_row = Book::find($id->id);
        
        $tags_id = array();
        // Get All the data for tags 
        foreach($data_row->getTags as $tags){
            $tags_id[] = $tags->_id;
        }        

        //get all_tags 
        $sel_tags = array();
        $all_tags = Tag::orderBy('title','ASC')->get();
        foreach ($all_tags as $db_tags){
            $sel_tags[$db_tags->_id] = $db_tags->title;
        }
        //dd($all_tags);
        return view('statute.update', [ 'row_data'=>$data_row, 'book_type'=>$book_type_array, 'tags_selected'=>$tags_id, 'allTags'=>$sel_tags, 'country'=>$countryList]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ValidateStatue $request)
    {
        
        $request->validated();
        
        $user_id = Auth::user()->_id;
        
        $helper = new Helper();
        $user_type = $helper->getUsertype();
        
        
        $data_row = Book::find($request->_id);
        
        $file = $request->file('image');
        $id = $request->_id;
        if($file) {
            // Get File extension
            $file_ext = $file->getClientOriginalExtension();
            
            $file_name = 'statute_'.$id.'.'.$file_ext;
            
            if($request->file('image')->storeAs('public/statute', $file_name)){
                $data_row->file_ext = $file_ext;
            }
        }  
        
        $data_row->name         = ucwords(strtolower(trim($request->book_title)));
        $data_row->description  = $request->description;
        $data_row->type         = $request->type_id;
        $data_row->year         = $request->book_year;
        $data_row->country_id        = $request->country_id;
        $data_row->state_id        = $request->state_id;
        $data_row->updates_by        = $user_id;
        $data_row->update_user_type        = $user_type;
        if($data_row->country_id == 'pk'){
            $data_row->city_id        = $request->city_id;
        } else {
            $data_row->city_id        = '';
        }
        $data_row->save();
        //dd($request->tags);
        
        if($request->tags)
        $data_row->getTags()->sync($request->tags,true);
        
        LogActivity::addToLog('statute','Statute has been updated.', $data_row);
        
        Session::flash('message', 'Updated Successfully.');
        return redirect()->route('statute.index', ['ch'=>substr(trim($request->book_title), 0,1)]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $del_book = Book::find($id);
        if($del_book->delete()){
            Session::flash('message', 'Deleted Successfully.');
            LogActivity::addToLog('statute','Statute has been deleted.', $del_book);
            return redirect()->route('statute.index');
        }
    }
}
