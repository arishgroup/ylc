<?php

namespace App\Http\Controllers;

use App\LawyerRegistration;
use Illuminate\Http\Request;
use App\News;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Controller;
use App\Helper;

class NewsController extends Controller
{
    private $folder_view = "news.";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $news = News::orderBy('updated_at', 'DESC')->get();
        return view ($this->folder_view.'index', ['data' => $news]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $helper = new Helper();
        $aos_sel = $helper->categoryTree();

        return view ($this->folder_view.'create',['aos' =>$aos_sel]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->group);
        $file_name = "";


        $news_data = new News();
        $news_data->title           = $request->title ;
        $news_data->description     = $request->description ;
        $news_data->link            = $request->link ;

        $news_data->alert           = $request->alert ;
        $news_data->broadcast       = $request->broadcast ;
        $news_data->type        = $request->type ;
        $news_data->audience        = $request->audience ;

        if ($request->hasFile('attachment')) {
            $file = $request->file('attachment');
            $file_name = $file->getClientOriginalName();
            $file_ext = $file->getClientOriginalExtension();
            $news_data->attachment      = $file_ext;
        }

        $news_data->save();

        if ($request->hasFile('attachment')) {
            $store_file_name = $news_data->_id.'.'.$file_ext;
            $file->storeAs(
                'public/news/', $store_file_name
            );
        }

        if ($request->aos)
            $news_data->aos()->sync($request->aos,false);

        if ($request->group)
            $news_data->group()->sync($request->group,false);

        Session::flash('message', 'Added Successfully.');
        return redirect()->route('news.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $helper = new Helper();
        $aos_sel = $helper->categoryTree();

        $data_collection = News::find($id);
        return view ($this->folder_view.'edit',['data'=> $data_collection, 'aos'=>$aos_sel]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $news_managements = News::find($id);

        $news_managements->title = $request->get('title');
        $news_managements->description = $request->get('description');
        $news_managements->link = $request->get('link');
        $news_managements->alert = $request->get('alert');
        $news_managements->broadcast = $request->get('broadcast');
        $news_managements->type = $request->get('type');
        $news_managements->audience = $request->get('audience');
        $news_managements->attachment      = $request->get('attachment');
//        $news_managements->group = $request->get('group');
        $aos_array = array();
        if ($request->type == 'aos') {
            $helper = new Helper();

            foreach ($request->aos as $req_aos){
                $aos_sel = $helper->categoryTree($req_aos);
                foreach($aos_sel as $key=>$value){
                    $aos_array[] = $key;
                }
            }
        }

        if ($request->aos) {
            $news_managements->aos()->sync(array_unique($aos_array), true);
        }
        if ($request->group) {
            $news_managements->group()->sync($request->group, true);
        }
        if ($request->type == 'group') {
            $news_managements->aos()->sync(array(), true);
        }
        else{
            $news_managements->group()->sync(array(), true);
        }


        $news_managements->save();

        Session::flash('message', 'Updated Successfully.');
        return redirect()->route('news.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $del = News::find($id);
        if($del->delete()){
            Session::flash('message', 'Deleted Successfully.');
            return redirect()->route('news.index');
        }
    }
}
