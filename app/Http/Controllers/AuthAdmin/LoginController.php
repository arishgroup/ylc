<?php

namespace App\Http\Controllers\AuthAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

  
    public function __construct()
    {
        //this->middleware('guest')->except('logout');
        $this->middleware('guest:admin', ['except' => ['logout','userLogout']]);    
    }
    
    public function userLogout(Request $request)
    {
       
        Auth::guard('admin')->logout();
        return redirect()->route('admin.login');
    }
    
    public function showLoginForm(){
        return view('authAdmin.login');
    }
    
    public function login(Request $request){
        // Validate the Data First
        $this->validate($request, [
            'email'=> 'required|email',
            'password' => 'required|min:6'
        ]);
       
        // Attempt to user logged in
        if(Auth::guard('admin')->attempt(['email'=>$request->email, 'password'=>$request->password], $request->remember)){
            // If success then logic
            return redirect()->intended(route('admin.dashboard'));
        }
        // if unsuccess
        return redirect()->back()->withInput($request->only('email', 'remember'));
    } 
}
