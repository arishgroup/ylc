<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\admin\LawyersDesignation;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\ValidateName;
use Yajra\DataTables\DataTables;


class LawyersDesignationController extends Controller
{
    private $folder_view = 'admin.lawyers.';
    private $cnt = 1;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ($this->folder_view.'designation');
    }
    
    function datatable(Request $request){
        $lawyer_designation = LawyersDesignation::orderBy('_id')->get();
        return Datatables::of($lawyer_designation)
        ->editColumn('_id', function ($lawyer_designation) {
            return $this->cnt++;
        })
        ->addColumn('action', function ($users) {
            return '<a href="'. route('lawyer.designation.edit', $users->_id) .'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
        })
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ($this->folder_view.'designationCreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ValidateName $request)
    {
        $request->validated();

        $data = $request->all();
        LawyersDesignation::create($data);
        
        Session::flash('message', 'Added Successfully.');
        return redirect()->route('lawyers.designation.index');
        //return redirect()->getUrlGenerator()->previous();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = LawyersDesignation::find($id);
        if($data->count() > 0){
            return view( $this->folder_view.'designationUpdate', ['data' => $data]);
        } else {
            return redirect()->route('lawyers.designation.index');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ValidateName $request, $id)
    {
        $request->validated();
        $role = LawyersDesignation::where('_id', '=', $id)->first();
        
        $role->update($request->all());
        Session::flash('message', 'Update Successfully.');
        return redirect()->route('lawyers.designation.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = LawyersDesignation::where('_id', '=', $id)->first();
        $role->is_del=1;
        $role->update();
        
        Session::flash('message', 'Record Deleted Successfully.');
        return redirect()->route('lawyers.designation.index');
    }
}
