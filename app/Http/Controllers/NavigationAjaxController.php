<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\admin\LawyerTeam;
use Illuminate\Support\Facades\DB;


class NavigationAjaxController extends Controller
{
   
    function top_navigation (Request $request){
        $next = "0";
        $prev = '0';
        $model =  'App\admin\\'.$request->post('table');
        //$model =  'App\admin\LawyerTeam';
        $id = $request->post('table_id');
        
        if($request->post('nav') == 'last' || $id == 0) {   
            $data = $model::orderBy('_id','desc')->first();
            $prev = '1';
        }
        
        if($request->post('nav') == 'first') {
            $data = $model::orderBy('_id','asc')->first();
            $next = "1";
        }
        
        if($request->post('nav') == 'next') {
            $rs = $model::orderBy('_id','asc')->where('_id','>',$id)->limit(2)->get();
            $data = $rs[0];
            
            if(count($rs) == 2 ){$next = '1';}
            $prev = '1';
        }
        
        if($request->post('nav') == 'prev' && $id != 0) {
            $rs = $model::orderBy('_id','desc')->where('_id','<',$id)->limit(2)->get();
            $data = $rs[0];
            
            if(count($rs) == 2 ){$prev = '1';}
            $next = "1";
            
        }
        $data['next'] = $next;
        $data['prev'] = $prev;
        
        return Response::json(array(
            'success' => true,
            'data'   => $data
        ));
    }

}
