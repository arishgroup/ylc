<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Location;
use App\Court;
use App\UserType;
use App\BackendUsers;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\ValidateUserRegistration;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use App\Helpers\LogActivity;
use Yajra\DataTables\DataTables;

class UserRegistrationController extends Controller
{
    private $cnt = 1;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('admin.userRegistration.index', ['data'=>$request->type]);
    }
    
    function datatable(Request $request){
        
        $users = BackendUsers::where('user_type','like',$request->type_id)->orderBy('_id')->get();
        //dd($lawyers);
        return Datatables::of($users)
        ->addColumn('action', function ($users) {
            return '<a href="'. route('user_registration.log', [$users->_id, 'user']) .'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-eye-open"></i> Log</a>
                    <a href="'. route('user_registration.edit', $users->_id) .'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                    <a href="'. route('user_registration.destroy', $users->_id) .'" class="btn btn-xs btn-danger confirmation "><i class="glyphicon glyphicon-trash"></i> Delete</a>   ';
        })
        ->editColumn('_id', function ($users) {
            return $this->cnt++;
        })
        ->editColumn('name', function ($users) {
            return $users->name.' '.$users->last_name;
        })
        ->editColumn('location', function ($users) {
            return ($users->location)?$users->get_location->title:'N/A';
        })
        ->editColumn('alocated_court', function ($users) {
            return ($users->alocated_court)?$users->get_court->name:'N/A';
        })
        ->editColumn('user_type', function ($users) {
            return ($users->user_type)?$users->getUserType->title:'N/A';
        })
        
        ->make(true);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // Get location
        $sel_location = array();
        $location = Location::orderBy('title', 'ASC')->get();
        foreach($location as $value){
            $sel_location[$value->_id] =  $value->title;
        }
        
        $sel_court = array ();
        $court = Court::orderBy('_id')->get();
        foreach($court as $value){
            $sel_court[$value->_id] =  $value->name;
        }
        
        //Get user Type
        $utype = UserType::orderBy('_id')->get();
        $sel_utype = array ();
        foreach($utype as $value){
            $sel_utype[$value->_id] =  $value->title;
        }
        
        // get serial number
        $sr_number = BackendUsers::orderBy('_id', 'desc')->first();
        if($sr_number != null){
            $sr_no = $sr_number->serial_no + 1;
        } else {
            $sr_no  = "655661";
        }
        
        $data = array ('sr_no'=>$sr_no,'user_type'=>$request->type);
        
        return view('admin.userRegistration.create',['location'=>$sel_location, 'court'=>$sel_court, 'utype'=>$sel_utype, 'data'=>$data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ValidateUserRegistration $request)
    {

        $request->validated();

        $save_content = new BackendUsers();
        $save_content->name             = $request->name ;
        $save_content->last_name        = $request->lastname ;
        $save_content->user_address     = $request->user_address ;
        $save_content->nic              = $request->nic ;
        $save_content->mobile_no        = $request->mobile_no ;
        $save_content->serial_no        = $request->serial_no ;
        $save_content->email            = $request->email ;
        $save_content->user_type        = $request->user_type ;
        $save_content->alocated_court   = $request->court ;
        $save_content->location         = $request->location ;
        $save_content->password         = bcrypt($request->password);
        
        $save_content->save();
        $last_inserted_id =  $save_content->_id;
        // Upload image of NIC
        $file = $request->file('nic_img');
        if($file) {
            $id = $last_inserted_id;
            $file_ext = $file->getClientOriginalExtension();
            
            //Display File Name
            $file_name = 'nic_'.$id.'.'.$file_ext;
            
            if($file->storeAs('public/backenduser/', $file_name)){
                $save_content->nic_img = $file_ext;
                $save_content->update();
            }
            
        }
        
        $file = $request->file('profile_img');
        if($file) {
            $id = $last_inserted_id;
            $file_ext = $file->getClientOriginalExtension();
            
            //Display File Name
            $file_name = 'profile_'.$id.'.'.$file_ext;
            
            if($file->storeAs('public/backenduser/', $file_name)){
                $save_content->profile_img = $file_ext;
                $save_content->update();
            }
            
        }
        
        Session::flash('message', 'Added Successfully.');
        return redirect()->route('user_registration.create',[$request->user_type]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data_content = BackendUsers::find($id);
        
        $utype = "";
        if($data_content->user_type ){$utype = $data_content->getUserType->title; }
        
        $courtName = "";
        if($data_content->alocated_court  ){$courtName = $data_content->get_court->name; }
        
        $location = "";
        if($data_content->location ){$location = $data_content->get_location->title; }
        
        $data_content = '<div class="container-fluid">
                            <div class="row">
                                <div class="col-sm-1"><b>First Name</b></div>
                                <div class="col-sm-3">'.$data_content->name .'</div>
                                <div class="col-sm-1"><b>Last Name</b></div>
                                <div class="col-sm-3">'.$data_content->last_name .'</div>
                            </div>
<div class="row">
                                <div class="col-sm-1"><b>NIC</b></div>
                                <div class="col-sm-3">'.$data_content->nic .'</div>
                                
                            </div>
<div class="row">
                                <div class="col-sm-1"><b>Mobile Number</b></div>
                                <div class="col-sm-3">'.$data_content->mobile_no .'</div>
                                <div class="col-sm-1"><b>Email</b></div>
                                <div class="col-sm-3">'.$data_content->email .'</div>
                            </div>
<div class="row">
                                <div class="col-sm-1"><b>User Type</b></div>
                                <div class="col-sm-3">'.$utype.'</div>
                                <div class="col-sm-1"><b>Alocted Court</b></div>
                                <div class="col-sm-3">'.$courtName.'</div>
                            </div>
<div class="row">
                                <div class="col-sm-1"><b>Location</b></div>
                                <div class="col-sm-3">'.$location .'</div>
                                <div class="col-sm-1"><b>Serial Number</b></div>
                                <div class="col-sm-3">'.$data_content->serial_no  .'</div>
                            </div>
<div class="row">
                                <div class="col-sm-1"><b>Address</b></div>
                                <div class="col-sm-3">'.$data_content->user_address  .'</div>
                                
                            </div>
                         </div>';
        return Response::json( array('content'=> $data_content), 200 );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->sel_location = array ();
        $this->sel_court = array();
        // Get location
        $location = Location::orderBy('title', 'ASC')->get();
        foreach($location as $value){
            $this->sel_location[$value->_id] =  $value->title;
        }
        
        $court = Court::orderBy('_id')->get();
        foreach($court as $value){
            $this->sel_court[$value->_id] =  $value->name;
        }
        //Get user Type
        $utype = UserType::orderBy('_id')->get();
        foreach($utype as $value){
            $this->sel_utype[$value->_id] =  $value->title;
        }
        $user_data = BackendUsers::find($id);
        $data = array ('user' => $user_data);
        
        return view('admin.userRegistration.update',['location'=>$this->sel_location, 'court'=>$this->sel_court , 'data'=>$data, 'utype'=>$this->sel_utype ]);
    }
    public function logActivity(Request $request, $type){
        $user = BackendUsers::find($request->id);
        $logs = LogActivity::logActivityLists($request->id, 'user', $request);
        $data = array ('logs'=>$logs, 'userData'=>$user,'rq_data' =>$request);
        return view('admin.userRegistration.logActivity',compact('data'));
    }
    
    public function searchLog (Request $request){
        $user = BackendUsers::find($request->id);
        $logs = LogActivity::logActivityLists($request->id, $request->type, $request);
        $data = array ('logs'=>$logs, 'userData'=>$user, 'rq_data' =>$request);
        return view('admin.userRegistration.logActivity',compact('data'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ValidateUserRegistration $request, $id)
    {
        $request->validated();
        $user = BackendUsers::find($id);
       
        $file = $request->file('nic_img');
        if($file) {
            $file_ext = $file->getClientOriginalExtension();
            
            //Display File Name
            $file_name = 'nic_'.$id.'.'.$file_ext;
            
            if(Storage::disk('public')->exists('/backenduser/'.$file_name)){
                Storage::delete('public/backenduser/'.$file_name);
            }
            
            if($file->storeAs('public/backenduser/', $file_name)){
                $user->nic_img = $file_ext;
                $user->update();
            }
            
        }
        
        $file = $request->file('profile_img');
        if($file) {
            $file_ext = $file->getClientOriginalExtension();
            
            //Display File Name
            $file_name = 'profile_'.$id.'.'.$file_ext;
            
            if(Storage::disk('public')->exists('/backenduser/'.$file_name)){
                Storage::delete('public/backenduser/'.$file_name);
            }
            
            if($file->storeAs('public/backenduser/', $file_name)){
                $user->profile_img = $file_ext;
                $user->update();
            }
            
        }
        //dd($request);
        $user->update($request->all());
        //dd($user);
        Session::flash('message', 'Update Successfully.');
        return redirect()->route('user_registration.index', [$request->user_type]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
 
}
