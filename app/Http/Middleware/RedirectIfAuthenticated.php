<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        switch ($guard){
            case 'user':
                if(Auth::guard($guard)->check()){
                    return redirect()->route('user.dashboard');
                }
                
                Break;
            case 'admin':
                if(Auth::guard($guard)->check()){
                   return redirect()->route('admin.dashboard');  
                }
                
                Break;
            case 'lawyer':
                if(Auth::guard($guard)->check()){
                    return redirect()->route('lawyers.dashboard');
                }
                
                Break;
            default:
                if (Auth::guard($guard)->check()) {
                    return redirect('/home');
                } 
                Break;
        }
        
        /* if (Auth::guard($guard)->check()) {
            return redirect('/home');
        } */

        return $next($request);
    }
}
