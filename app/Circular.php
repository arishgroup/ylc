<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use Jenssegers\Mongodb\Relations\BelongsTo;

class Circular extends Eloquent
{
    use SoftDeletes;
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "circulars";
    protected $fillable = [
        'notification_no', 'institution_id', 'subject', 'description', 'year','month','doc_type_id'
    ];
    
    public function getInstitute(){
        return $this->belongsTo('App\CircularInstitution', 'institution_id', '_id');
    }
    
    public function getDocType(){
        return $this->belongsTo('App\MiscellaneousDoctype', 'doc_type_id', '_id');
    }
    
    public function getTags (){
        return $this->belongsToMany('App\Tag');
    }
}
