<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class LawyerAos extends Eloquent
{
    use SoftDeletes;
    protected $table = "lawyer_aos";
    protected $fillable = [ 'name', 'description', 'parent_id', 'lawyer_id' ];
    protected $dates = ['deleted_at'];
    
    
    public function lawyer(){
        return $this->belongsTo('App\LawyerRegistration', 'lawyer_id', '_id');
    }
    
   
}
