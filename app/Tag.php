<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class Tag extends Eloquent
{
    use SoftDeletes;
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "tags";
    protected $fillable = [ 'title' ];
    
    public function getStatute(){
        return $this->belongsToMany('App\Book', 'book_tag', 'statute_id', 'tag_id');
    }
    
    public function getMiscDoc(){
        return $this->belongsToMany('App\MiscellaneousDoctype');
    }
}
