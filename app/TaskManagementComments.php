<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class TaskManagementComments extends Eloquent
{
    //
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = "task_management_comments";
    protected $fillable = [
        'task_id', 'comments'
    ];

    public function getTaskAdmin(){
        return $this->belongsTo('App\Admin', 'user_id', '_id');
    }

    public function getTaskLawyer(){
        return $this->belongsTo('App\LawyerRegistration', 'user_id', '_id');
    }

    public function getTaskUser(){
        return $this->belongsTo('App\BackendUsers', 'user_id', '_id');
    }


}
