<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class BookType extends Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "book_types";
    protected $fillable = [
        'name', 'description'
    ];
}
