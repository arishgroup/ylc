<?php

namespace App;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class AreaOfSevice extends Eloquent
{
    use SoftDeletes;
    protected $table = "area_of_service";
    protected $fillable = [ 'name', 'description', 'parent_id' ];
    protected $dates = ['deleted_at'];
    
    
    public function lawyers(){
        return $this->belongsTo(lawyer::class);
    }
    
}
