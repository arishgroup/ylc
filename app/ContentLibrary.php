<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class ContentLibrary extends Eloquent
{
    use SoftDeletes;
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "content_libraries";
    protected $fillable = [
        'file_ext'
    ];
    
    public function area_of_service(){
        return $this->belongsTo('App\AreaOfSevice', 'aos', '_id');
    }
    
    public function getTags()
    {
        return $this->belongsToMany('App\Tag');
    }
    public function getJudges()
    {
        return $this->belongsToMany('App\JudgesProfile');
    }
    
    public function statuteSections()
    {
        return $this->belongsToMany('App\BookSections');
    }
}
