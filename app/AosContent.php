<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class AosContent extends Eloquent
{
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "aos_contents";
    protected $fillable = [
        'heading', 'description'
    ];
    
    public function getAos(){
        return $this->belongsTo('App\LawyersAosContent', 'content_id', '_id');
    }
    
    public function lawyer(){
        return $this->belongsTo('App\LawyersAosContent', 'content_id', '_id');
    }
    
}
