<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class LawyersAosContent extends Eloquent
{
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "lawyers_aos_contents";
    protected $fillable = [
        'title'
    ];
    
    public function aos_standard (){
        return $this->belongsTo('App\AreaOfSevice', 'aos_id' ,'_id');
    }
    public function aos_personal (){
        return $this->belongsTo('App\LawyerAos', 'aos_id' ,'_id');
    }
    public function getContent(){
        return $this->belongsToMany('App\AosContent', '_id' ,'content_id');
    }
    
    public function getAosContent(){
        return $this->belongsToMany('App\AosContent','_id','content_id');
    }
    
    public function lawyer(){
        return $this->belongsTo('App\LawyerRegistration', 'lawyer_id' ,'_id');
    }
}
