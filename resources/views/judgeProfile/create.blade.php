@extends(Auth::guard('admin')->check() ? 'layouts.admin' : 'layouts.backendUser')

@section('content')
<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>{{ __('Judges Profile(s)') }}</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('judge_profile.index')}}">Judges Profile(s)</a></li>
        <li class="active">Add</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info col-md-6">
            <div class="box-header with-border">
              @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
				{{--@if ($errors->any())--}}
    {{--<div class="alert alert-danger">--}}
        {{--<ul>--}}
            {{--@foreach ($errors->all() as $error)--}}
                {{--<li>{{ $error }}</li>--}}
            {{--@endforeach--}}
        {{--</ul>--}}
    {{--</div>--}}
{{--@endif--}}
            </div>
            <!-- /.box-header -->
            <!-- form start -->
             {!! Form::open(['route' => 'judge_profile.store', 'method' => 'post', 'class'=>'form-horizontal','files'=>'true']) !!}
              {{ csrf_field() }}
              <div class="box-body">
              <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">{{ __('Name:') }}<span class="label_red"> *</span></label>
                   <div class="col-sm-5">
                    {!! FORM::text('name' , '' ,['class'=>'form-control', 'placeholder'=>'Name', 'data-validation'=>"custom" ,'data-validation-regexp'=>'^([a-zA-Z\s]+)$' ]) !!}
                   @if ($errors->has('name'))
                       <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                      </span>
                   @endif
                  </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">{{ __('Profile Picture:') }}<span class="label_red"> *</span></label>
                   <div class="col-sm-5">
                    <input type='file' name='image' id='image' data-validation="required extension" validate_extension='png,jpg,jpeg' data-validation-allowing="png,jpg,jpeg" />
                   @if ($errors->has('image'))
                       <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('image') }}</strong>
                  </span>
                   @endif
                   </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">{{ __('Designation:') }}<span class="label_red"> *</span></label>
                   <div class="col-sm-5">
                    {!! FORM::text('designation' , '' ,['class'=>'form-control', 'placeholder'=>'Designation', 'data-validation'=>"custom" ,'data-validation-regexp'=>'^([a-zA-Z\s]+)$']) !!}
                       @if ($errors->has('designation'))
                           <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('designation') }}</strong>
                          </span>
                       @endif
                  </div>
                </div>
                 <div class="form-group">
                  {!! FORM::label('DOB' ,'Date of Birth',['class'=>'col-sm-2 control-label']) !!}
                   <div class="col-sm-5">
                    {!! FORM::text('dob' , '' ,['class'=>'form-control', 'placeholder'=>'DOB']) !!}
                  </div>
                </div>
                <div class="form-group">
                  {!! FORM::label('name' ,'Description',['class'=>'col-sm-2 control-label']) !!}
                   <div class="col-sm-5">
                   {!! FORM::textarea('article_ckeditor' ,'',['class'=>'form-control', 'rows' => 3, 'cols' => 40]) !!}
                  </div>
                </div>
               </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="{{route('judge_profile.index')}}"><button type="button" class="btn btn-default">Back</button></a>
                {!! Form::submit('Submit', ['class'=>'btn btn-info pull-right']) !!}
              </div>
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <!-- /.box-footer -->
   
            {!! Form::close() !!}
          </div>
         
        </div>
        <!--/.col (right) -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
	<script>
    	CKEDITOR.replace( 'article_ckeditor' );
	</script>
@endsection 
