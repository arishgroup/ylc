@extends(Auth::guard('admin')->check() ? 'layouts.admin' : 'layouts.backendUser')
@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Tasks
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#"><i class="fa fa-tasks"></i> Task Management System</a></li>
                <li class="active"><a href="#"> <i class="fa fa-circle-o"></i>My Tasks</a></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-12">
                    <!-- small box -->
                    <!-- TO DO List -->
                    <div class="box box-primary">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="box-body">
                                <table id="user_table" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Task</th>
                                        <th>Description</th>
                                        <th>Deadline</th>
                                        <th>Priority</th>
                                        <th>Status</th>
                                        <th>Detail</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script>
        $(function() {
            var assignee_type = "user";
            var task_type = "";

            $('#user_table').DataTable({
                destroy: true,
                processing: true,
                serverSide: true,
                "order": [[ 3, "desc" ]],
                "pageLength": 25,
                ajax: '/getAssigneeDataTables/'+assignee_type+"&"+task_type,
                columns: [
                    { data: 'task_name', name: 'task_name' },
                    { data: 'task_description', name: 'task_description' },
                    { data: 'assigner_end_date', name: 'assigner_end_date' },
                    { data: 'priority', name: 'priority' },
                    { data: 'status', name: 'status' },
                    { data: 'action', name: 'action' }
                ]
            });
        });
    </script>
@endsection
