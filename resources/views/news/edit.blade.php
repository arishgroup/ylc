@extends('layouts.admin')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Update News
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">News Management System</li>
                <li class="active">Update News</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- right column -->
                    <div class="col-md-12">
                        <!-- Horizontal Form -->
                        <div class="box box-info col-md-6">
                            <div class="box-header with-border">
                                @if (Session::has('message'))
                                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                                @endif
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            {!! Form::open(['route' => array('news.update', $data->_id), 'method' => 'post', 'class'=>'form-horizontal', 'files'=>'true']) !!}
                            {{ csrf_field() }}
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="name" class="col-sm-4 control-label">{{ __('Title:') }}<span class="label_red"> *</span></label>
                                    <div class="col-sm-4">
                                        <input id="title" type="text" value="{!! $data->title !!}" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" placeholder='Title' name="title">
                                        @if ($errors->has('title'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('title') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-4 control-label">{{ __('Description:') }}<span class="label_red"> *</span></label>
                                    <div class="col-sm-4">
                                        {!! FORM::textarea('description' , $data->description ,['class'=>'form-control', 'rows' => 3, 'cols' => 40]) !!}
                                        @if ($errors->has('description'))
                                            <span class="invalid-feedback" role="alert">
                                             <strong>{{ $errors->first('description') }}</strong>
                                          </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-4 control-label">{{ __('Link:') }}</label>
                                    <div class="col-sm-4">
                                        {!! FORM::textarea('link' ,$data->link,['class'=>'form-control', 'rows' => 3, 'cols' => 40]) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-4 control-label">{{ __('Attachments:') }}</label>
                                    <div class="col-sm-4">
                                        <input type="file" name="attachment" multiple>
                                        @if($data->attachment)
                                            @if($data->attachment == "pdf")
                                                <a  href="{!! route('show.pdf', [encrypt('/storage/news/'.$data->_id.'.'.$data->attachment)]) !!}" target="_blank" >View Uploaded File</a>
                                            @else
                                                <a target="_blank" href='{{ asset("storage/news/$data->_id.$data->attachment") }}' >View Uploaded File</a>
                                            @endif
                                        @endif
                                        @if ($errors->has('attachments'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('attachments') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-4 control-label">{{ __('Alert:') }}</label>
                                    <div class="col-sm-4">
                                        <input type="checkbox" value="1" name="alert" id="alert" @if($data->alert == 1) checked  @endif>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-4 control-label">{{ __('Broadcast:') }}</label>
                                    <div class="col-sm-4">
                                        <input type="checkbox" value="1" name="broadcast" id="broadcast" @if($data->broadcast == 1) checked  @endif onchange="hideSection()">
                                    </div>
                                </div>
                                <div id="noBroadcast">
                                    <div class="form-group">
                                        <label for="name" class="col-sm-4 control-label">{{ __('Audience:') }}</label>
                                        <div class="col-sm-4">
                                            <label class="radio-inline"><input type="radio" name="audience" id="lawyer_id" value="lawyer" @if($data->audience == 'lawyer') checked  @endif onclick="selectType()">Lawyer</label>
                                            <label class="radio-inline"><input type="radio" name="audience" id="client_id" value="client" @if($data->audience == 'client') checked  @endif onclick="selectType()">Client</label>
                                            {{--<label class="radio-inline"><input type="radio" name="audience">Both</label>--}}
                                            @if ($errors->has('audience'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('audience') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="col-sm-4 control-label">{{ __('Target Type:') }}</label>
                                        <div class="col-sm-4">
                                            <select id="type" name="type" class='form-control' onchange="selectType()">
                                                <option value="" selected="selected">Target Type</option>
                                                <option value="group">Group</option>
                                                <option value="aos">Area of Service</option>
                                            </select>
                                            @if ($errors->has('type'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('type') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div id="groupSection" class="form-group">
                                        <label for="name" class="col-sm-4 control-label">{{ __('Group:') }}</label>
                                        <div class="col-sm-4">
                                            {!! Form::select('group[]', [],'',['multiple'=>'multiple','class'=>'form-control', 'placeholder'=>'group', 'id'=>'select_group'] ) !!}
                                            @if ($errors->has('group'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('group') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div id="aosSection" class="form-group">
                                        <label for="name" class="col-sm-4 control-label">{{ __('Area of Service:') }}</label>
                                        <div class="col-sm-4">
                                            {!! Form::select('aos[]', $aos,$data->area_of_sevice_ids,['multiple'=>'multiple','class'=>'form-control', 'placeholder'=>'Area of Service'] ) !!}
                                            @if ($errors->has('aos'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('aos') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <a href="{{route('news.index')}}"><button type="button" class="btn btn-default">Back</button></a>
                                {!! Form::submit('Submit', ['class'=>'btn btn-info pull-right']) !!}
                            </div>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <!-- /.box-footer -->
                            {!! Form::close() !!}
                        </div>

                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
    </div>
    <!-- /.content-wrapper -->
    <script>
        function hideSection() {
            var broadcast = document.getElementById("broadcast");
            var noBroadcast = document.getElementById("noBroadcast");
            if (broadcast.checked == true){
                noBroadcast.style.display = "none";
            } else {
                noBroadcast.style.display = "block";
            }
        }

        function selectType(){
            var type = document.getElementById("type").value;
            var groupSection = document.getElementById("groupSection");
            var aosSection = document.getElementById("aosSection");
            if (type == 'group'){
                groupSection.style.display = "block";
                aosSection.style.display = "none";

                if (document.getElementById('lawyer_id').checked) {
                    $.ajax({url: "/getLawyers",async: false, success: function(result){
                            var row_id = 'select_group';
                            document.getElementById(row_id).innerHTML = result.data;
                        }});
                    // alert("lawyer button is checked");
                }
                else if (document.getElementById('client_id').checked) {
                    $.ajax({url: "/getClients",async: false, success: function(result){
                            var row_id = 'select_group';
                            document.getElementById(row_id).innerHTML = result.data;
                        }});
                    // alert("client button is checked");
                }
                else {
                    alert("no button is checked");
                }
            }
            else if(type == 'aos') {
                groupSection.style.display = "none";
                aosSection.style.display = "block";
            }
            else{
                groupSection.style.display = "none";
                aosSection.style.display = "none";
            }
        }

        hideSection();
        selectType();


        function setSelectedValue(selectObj, valueToSet) {
            for (var i = 0; i < selectObj.options.length; i++) {

                if (selectObj.options[i].value == valueToSet) {
                    selectObj.options[i].selected = true;
                    return;
                }
            }
        }

        <?php if($data->type ){ ?>
        var objSelect_n = document.getElementById("type");
        setSelectedValue(objSelect_n, "<?php echo $data->type;?>");
        selectType();
              <?php }?>

        <?php if($data->type  == "group"){ ?>
             var objSelect_n = document.getElementById("select_group");
          <?php  foreach ($data->lawyer_registration_ids as $value){?>

            setSelectedValue(objSelect_n, "<?php echo $value;?>");
        <?php }
        }?>
    </script>
@endsection
