@extends('layouts.admin')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                News
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">News Management System</li>
                <li class="active">News</li>
            </ol>
        </section>
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-md-12">
                    <div class="box">
                        <div class="box-header" style="float: right">
                            <a href="{{route('news.create')}}">Add News</a>
                        </div>
                        <div>
                            @if (Session::has('message'))
                                <div class="alert alert-success">{{ Session::get('message') }}</div>
                            @endif
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Sr.</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 0 ;?>
                                @if($data->count())
                                    @foreach($data as $news)
                                        <tr>
                                            <td>{!! $i = $i+1 !!}</td>
                                            <td>{!! $news->title !!} @if($news->alert == 1) <small class="pull-right label label-danger"><i class="fa fa-clock-o"></i> Alert </small> @endif</td>
                                            <td>{!! $news->description !!}</td>
                                            <td>
                                                <a href="{{ route('news.edit', [$news->_id]) }}"><i class="fa fa-edit"></i></a> | <a class="confirmation" href="{{ route('news.destroy', $news->_id) }}"><i class="fa fa-trash"></i></a></td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5">No Record Found!</td>
                                    </tr>
                                @endif

                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- /.content-wrapper -->
@endsection
