@extends('layouts.admin')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Create News
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">News Management System</li>
                <li class="active">Create News</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- right column -->
                    <div class="col-md-12">
                        <!-- Horizontal Form -->
                        <div class="box box-info col-md-6">
                            <div class="box-header with-border">
                                @if (Session::has('message'))
                                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                                @endif
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            {!! Form::open(['route' => 'news.store', 'method' => 'post', 'class'=>'form-horizontal', 'files'=>'true']) !!}
                            {{ csrf_field() }}
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="name" class="col-sm-4 control-label">{{ __('Title:') }}<span class="label_red"> *</span></label>
                                    <div class="col-sm-4">
                                        <input id="title" type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" placeholder='Title' name="title">
                                        @if ($errors->has('title'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('title') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-4 control-label">{{ __('Description:') }}<span class="label_red"> *</span></label>
                                    <div class="col-sm-4">
                                        {!! FORM::textarea('description' ,'',['class'=>'form-control', 'rows' => 3, 'cols' => 40]) !!}
                                        @if ($errors->has('description'))
                                          <span class="invalid-feedback" role="alert">
                                             <strong>{{ $errors->first('description') }}</strong>
                                          </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-4 control-label">{{ __('Link:') }}</label>
                                    <div class="col-sm-4">
                                        {!! FORM::textarea('link' ,'',['class'=>'form-control', 'rows' => 3, 'cols' => 40]) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-4 control-label">{{ __('Attachments:') }}</label>
                                    <div class="col-sm-4">
                                        <input type="file" name="attachment">
                                        @if ($errors->has('attachment'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('attachment') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-4 control-label">{{ __('Alert:') }}</label>
                                    <div class="col-sm-4">
                                        <input type="checkbox" value="1" name="alert" id="alert">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-sm-4 control-label">{{ __('Broadcast:') }}</label>
                                    <div class="col-sm-4">
                                        <input type="checkbox" value="1" name="broadcast" id="broadcast" onchange="hideSection()">
                                    </div>
                                </div>
                                <div id="noBroadcast">
                                    <div class="form-group">
                                        <label for="name" class="col-sm-4 control-label">{{ __('Audience:') }}</label>
                                        <div class="col-sm-4">
                                            <label class="radio-inline"><input type="radio" id="lawyer_id" name="audience" value="lawyer" onclick="selectType()" >Lawyer</label>
                                            <label class="radio-inline"><input type="radio" id="client_id" name="audience" value="client" onclick="selectType()" >Client</label>
                                            {{--<label class="radio-inline"><input type="radio" name="audience">Both</label>--}}
                                            @if ($errors->has('audience'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('audience') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="col-sm-4 control-label">{{ __('Target Type:') }}</label>
                                        <div class="col-sm-4">
                                            <select id="type" name="type" class='form-control' onchange="selectType()">
                                                <option value="" selected="selected">Target Type</option>
                                                <option value="group">Group</option>
                                                <option value="aos">Area of Service</option>
                                            </select>
                                            @if ($errors->has('type'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('type') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div id="groupSection" class="form-group">
                                        <label for="name" class="col-sm-4 control-label">{{ __('Group:') }}</label>
                                        <div class="col-sm-4">
                                            {!! Form::select('group[]', [],'',['multiple'=>'multiple','class'=>'form-control', 'id'=>'select_group'] ) !!}
                                            @if ($errors->has('group'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('group') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div id="aosSection" class="form-group">
                                        <label for="name" class="col-sm-4 control-label">{{ __('Area of Service:') }}</label>
                                        <div class="col-sm-4">
                                            {!! Form::select('aos[]', $aos,'',['multiple'=>'multiple','class'=>'form-control'] ) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <a href="{{route('news.index')}}"><button type="button" class="btn btn-default">Back</button></a>
                                {!! Form::submit('Submit', ['class'=>'btn btn-info pull-right']) !!}
                            </div>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <!-- /.box-footer -->
                            {!! Form::close() !!}
                        </div>

                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
    </div>
    <!-- /.content-wrapper -->
    <script>
        function hideSection() {
            var broadcast = document.getElementById("broadcast");
            var noBroadcast = document.getElementById("noBroadcast");
            if (broadcast.checked == true){
                noBroadcast.style.display = "none";
            } else {
                noBroadcast.style.display = "block";
            }
        }

        function selectType(){
            var type = document.getElementById("type").value;
            var groupSection = document.getElementById("groupSection");
            var aosSection = document.getElementById("aosSection");
            if (type == 'group'){
                groupSection.style.display = "block";
                aosSection.style.display = "none";

                if (document.getElementById('lawyer_id').checked) {
                    $.ajax({url: "/getLawyers", success: function(result){
                        var row_id = 'select_group';
                        document.getElementById(row_id).innerHTML = result.data;
                    }});
                    // alert("lawyer button is checked");
                }
                else if (document.getElementById('client_id').checked) {
                    $.ajax({url: "/getClients", success: function(result){
                            var row_id = 'select_group';
                            document.getElementById(row_id).innerHTML = result.data;
                        }});
                    // alert("client button is checked");
                }
                else {
                    alert("no button is checked");
                }
            }
            else if(type == 'aos') {
                groupSection.style.display = "none";
                aosSection.style.display = "block";
            }
            else{
                groupSection.style.display = "none";
                aosSection.style.display = "none";
            }
        }

        selectType();

    </script>
@endsection
