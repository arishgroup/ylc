<?php
$month = '';
$exceed= '';
$ontime = '';
$pending='';

for ($i = 5; $i >= 0; $i--) {
    $index = date('m', strtotime("-$i month"));
    if(key_exists($index, $graph)){
        $exceed .=  '"'. $graph[$index]['exceed'].'",';
        $ontime .=  '"'. $graph[$index]['ontime'].'",';
        $pending .=  '"'. $graph[$index]['pending'].'",';
    }
    else{
        $exceed.=  '"0",';
        $ontime.=  '"0",';
        $pending.= '"0",';

    }

    $month .=  '"'.date('F', strtotime("-$i month")).'",';

}
?>
<script>
$(function () {
    new Chart(document.getElementById("barChart"), {
        type: 'bar',
        data: {
            labels: [<?php echo substr($month,0,strlen($month)-1); ?>],
            datasets: [
                {
                label: "Exceeds",
                backgroundColor: "#C0392B",
                data: [<?php echo substr($exceed,0,strlen($exceed)-1); ?>]
                }, {
                label: "On Time",
                backgroundColor: "#52BE80",
                data: [<?php echo substr($ontime,0,strlen($ontime)-1); ?>]
                }, {
                label: "Pending",
                backgroundColor: "#F5B041",
                data: [<?php echo substr($pending,0,strlen($ontime)-1); ?>]
                }
            ]
        },
        options: {
        title: {
            display: true,
            text: 'Tasks Status'
            }
        }
    });
})
</script>