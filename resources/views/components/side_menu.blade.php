<!-- 		 -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/component.css') }}" />
		
		<script  src="{{ asset('js/modernizr.custom.js') }}" ></script>
<style>
input{ font-size: 1em; }
ol.tree
{
    padding: 0 0 0 20px;
    width: 320px;
}
li 
{
    position: relative; 
    margin-left: -5px;
    list-style: none;
}
li input
{
    position: absolute;
    left: 0;
    margin-left: 0;
    opacity: 0;
    z-index: 2;
    cursor: pointer;
    height: 1em;
    width: 1em;
    top: 0;
}
li input + ol
{
    background: url(img/arrow-down.png) 45px -3px no-repeat;
    margin: -0.938em 0 0 -44px;
    height: 1em;
}
li input + ol > li { display: none; margin-left: -14px !important; padding-left: 1px; }
li label
{
    cursor: pointer;
    display: block;
    padding-left: 24px;
}
 
li input:checked + ol
{
    background: url(img/arrow-up.png) 45px 4px no-repeat;
    margin: -1.25em 0 0 -44px;
    padding: 1.563em 0 0 80px;
    height: auto;
}
li input:checked + ol > li { display: block; margin: 0 0 0.125em;}
li input:checked + ol > li:last-child { margin: 0 0 0.063em; }
</style>
<div id="dl-menu" class="dl-menuwrapper">

<?php if(count($record['tree_view']) > 0){
    generateTreeView($record, 0);
}

?>
</div>



<?php

function generateTreeView($items, $currentParent, $currLevel = 0, $prevLevel = -1) {
    
    
    foreach ($items['tree_view'] as $itemId => $item) {
        
        if ($currentParent == $item['parent_id']) {
            
            if($prevLevel == -1){
                if ($currLevel > $prevLevel){
                    echo "<ul class='dl-menu dl-menuopen'><li> ";
                }
            } else {
                if ($currLevel > $prevLevel){
                    echo "<ul class='dl-submenu'>";
                    if(count($items['notes'][$item['parent_id']]) >0){
                        foreach( $items['notes'][$item['parent_id']] as $key=>$val ){
                            echo "<li><a href='".route('aos.content.edit',  $val->_id)."'>$val->title</a></li>";
                        }
                    }else {
                        echo "<li><a href='javascript:void(0)'>No Notes Found</a></li>";
                    }
                    echo "<li>";
                }
            }
            
            if ($currLevel == $prevLevel){
                echo " <li>";
            }
            
            $menuLevel = $item['parent_id'];
            if($item['hasChild'] > 0){
                $menuLevel = $itemId;
            }
            
            echo '<a href="#">'.$item['name'].'</a>';
            if($item['hasChild'] == 0){
                if(count($items['notes'][$itemId]) >0){
                    echo "<ul class='dl-submenu'>";
                    foreach( $items['notes'][$itemId] as $key=>$val ){
                        echo "<li><a href='".route('aos.content.edit',  $val->_id)."'>$val->title</a></li>";
                    }
                    echo "</ul>";
                }else {
                    echo "<ul class='dl-submenu'><li><a href='javascript:void(0)'>No Notes Found</a></li></ul>";
                }
            }
            
            
            if ($currLevel > $prevLevel) {
                $prevLevel = $currLevel;
            }
            
            $currLevel++;
            
            generateTreeView ($items, $itemId, $currLevel, $prevLevel);
            $currLevel--;
        }
    }
    
    
       
    if ($currLevel == $prevLevel) { echo "</li>";
    echo "</ul>";
    }
    
} ?>


    

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<script src="{{ asset('js/jquery.dlmenu.js') }}"></script>
<script>
$(function() {
    $( '#dl-menu' ).dlmenu();
});
    </script>