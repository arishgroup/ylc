@if(Auth::guard('web')->check())
<p class="text-success">You are logged in as <strong>User</strong>!</p>
@else 
	<p class="text-danger">You are logged out in as <strong>User</strong>!</p>
@endif

@if (Auth::guard('admin')->check())
<p class="text-success">You are logged in as <strong>ADMIN</strong>!</p>
@else 
<p class="text-danger">You are logged out as <strong>ADMIN</strong>!</p>
@endif

@if(Auth::guard('lawyer')->check())
<p class="text-success">You are logged in as <strong>Lawyer</strong>!</p>
@else 
<p class="text-danger">You are logged out as <strong>Lawyer</strong>!</p>
@endif


@if(Auth::guard('user')->check())
<p class="text-success">You are logged in as <strong>User</strong>!</p>
@else 
<p class="text-danger">You are logged out as <strong>User</strong>!</p>
@endif
