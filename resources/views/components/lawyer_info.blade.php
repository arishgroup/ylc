 <div class="col-xs-12">
        <div class="box">
        	<div class="box-body">
 <div class="container-fluid" style="padding-bottom:20px;float:left; padding-top:20px; width:100%">
      <div class="row" style='padding-bottom:10px'>
      	<div class="col-md-4"><b>FullName: </b>{!! $record->name.' '.$record->lastname !!}</div>
      	<div class="col-md-4"><b>Public Serial Number: </b>{!! $record->public_serial_no !!}</div>
      	<div class="col-md-4"><b>Serial Number: </b>{!! $record->serial_no !!}</div>
      </div>
      <div class="row" style='padding-bottom:10px'>
      	<div class="col-md-4"><b>Email: </b>{!! $record->email !!}</div>
      	<div class="col-md-4"><b>Lawyer Designation: </b>@if($record->designation){!! $record->getDesignation->name !!}@endif</div>
      	<div class="col-md-4"><b>NIC: </b>{!! $record->nic !!}</div>
      	
      </div>
</div>
</div></div></div>