<div class="clearfix my-5">
    <div class="pull-left btn-group">
        <button type="button" id="last" onclick="getNavigation('last')" class="btn btn-default">Last </button>
        <button type="button" id="prev" onclick="getNavigation('prev')" class="btn btn-default">Previous </button>
    </div>
    <div class="pull-right btn-group">
        <button type="button" id='next' onclick="getNavigation('next')" class="btn btn-default">Next</button>
        <button type="button" id="first" onclick="getNavigation('first')" class="btn btn-default">First</button>
    </div>
</div>

<script>
function getNavigation(nav){
var table_id = '0';
	if($('#_id').val() != ''){
		table_id = $('#_id').val()
	}
	
	$.ajax({
				url: "{!! route('top.navigation') !!}",
				method:'post',
				data: {_token: '{!! csrf_token() !!}', nav:nav, table:'{!! $table_model !!}', table_id:table_id },
				success: function(result){
					show_result(result);

					document.getElementById("prev").disabled = false;
					if(result.data.prev == '0'){
						document.getElementById("prev").disabled = true;
						
					}
					document.getElementById("next").disabled = false;
					if(result.data.next == '0'){
						document.getElementById("next").disabled = true;
						
					}
	 }});
	 if (nav == 'first'){
		 document.getElementById("prev").disabled = true;
		 document.getElementById("next").disabled = false;
			}
	 if (nav == 'last'){
		 document.getElementById("prev").disabled = false;
		 document.getElementById("next").disabled = true;
			}
}



$(document).ready( function(){
	if($('#_id').val() == ''){
		document.getElementById("next").disabled = true;
		} 
	 
});
</script>
            
            
            
          