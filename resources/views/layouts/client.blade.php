<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">
		
		<title>Lawyers</title>
		
		<!-- Loading third party fonts -->
		<link href="{{ asset('fonts/template/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
		<link href="{{ asset('fonts/template/novecento-font/novecento-font.css') }}" rel="stylesheet" >

		<!-- Loading main css file -->
		<link href="{{ asset('css/template/style.css') }}" rel="stylesheet" >
		
		<!--[if lt IE 9]>
		<script src="js/ie-support/html5.js"></script>
		<script src="js/ie-support/respond.js"></script>
		<![endif]-->

	</head>


	<body>
		
		<div id="site-content">
			
			<header class="site-header">
				<div class="container">
					<a href="index.html" id="branding">
						<img src="images/logo.png" alt="Company Name" class="logo">
						<div class="branding-copy">
							<h1 class="site-title">YLC</h1>
							<small class="site-description">You Legal Connection</small>
						</div>
					</a>

					<nav class="main-navigation">
						<button type="button" class="menu-toggle"><i class="fa fa-bars"></i></button>
						<ul class="menu">
							<li class="menu-item current-menu-item"><a href="index.html">Home</a></li>
							<li class="menu-item"><a href="about.html">About Us</a></li>
							<li class="menu-item"><a href="experience.html">Experience</a></li>
							<li class="menu-item"><a href="service.html">Service</a></li>
							<li class="menu-item"><a href="contact.html">Contact</a></li>
							<li class="menu-item"><a href="{{ route('login') }}">Login</a></li>
							<li class="menu-item"><a href="{{ route('getRegistertype') }}">Signup</a></li>
						</ul>
					</nav>
					<nav class="mobile-navigation"></nav>
				</div>
			</header> <!-- .site-header -->

			 @yield('content')

			<footer class="site-footer">
				<div class="container">
					<div class="subscribe-form">
						<form action="#">
							<label for="#">
								<span>Do you want to get news?</span>
								<span>Join our news letter</span>
							</label>
							<div class="control">
								<input type="text" placeholder="Enter your email to subscribe...">
								<button type="submit"><img src="images/icon-envelope.png" alt=""></button>
														</form>
							</div>
					</div>
					<div class="social-links">
						<a href="#"><i class="fa fa-facebook"></i></a>
						<a href="#"><i class="fa fa-twitter"></i></a>
						<a href="#"><i class="fa fa-google-plus"></i></a>
						<a href="#"><i class="fa fa-pinterest"></i></a>
					</div>
					<div class="copy">
						<p>Copyright 2014 Company name. Designed by Themeezy. All rights reserved.</p>
					</div>
				</div>
			</footer> <!-- .site-footer -->

		</div> <!-- #site-content -->

		

		<script src="{{ asset('js/template/jquery-1.11.1.min.js') }}"></script>
		<script src="{{ asset('js/template/plugins.js') }}"></script>
		<script src="{{ asset('js/template/app.js') }}"></script>
		<script src="{{ asset('js/pair-select.js') }}"></script>
		<script src="{{ asset('js/pair-select.min.js') }}"></script>
		
		
		@stack('scripts')
	</body>

</html>