<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>YLC | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('bower_components/Ionicons/css/ionicons.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('dist/css/AdminLTE.min.css')}}">
  
  <link href="{{ asset('css/custum_app.css') }}" rel="stylesheet">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ asset('dist/css/skins/_all-skins.min.css')}}">
  <!-- Morris chart -->
  <link rel="stylesheet" href="{{ asset('bower_components/morris.js/morris.css')}}">
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{ asset('bower_components/jvectormap/jquery-jvectormap.css')}}">
  <!-- Date Picker -->
  <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
<!-- jQuery 3 -->
<script src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}" ></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('bower_components/jquery-ui/jquery-ui.min.js') }}" ></script>

  <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="//cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>

  <link rel = "stylesheet"
        type = "text/css"
        href = "//cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" />
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>Y</b>LC</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>YLC</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
     
          <!-- Notifications: style can be found in dropdown.less -->
          
          <!-- Tasks: style can be found in dropdown.less -->
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
               @if(Auth::user()->profile_img) 
              	<img src="/storage/backenduser/profile_{!! Auth::user()->_id!!}.{!! Auth::user()->profile_img !!}" class="user-image" alt="User Image">
              @else
                <img src="{{ asset('dist/img/icon-user-default-462x400.png') }}" class="user-image" alt="User Image">
              @endif
              
              <span class="hidden-xs">{{ Auth::user()->name }}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
              @if(Auth::user()->profile_img) 
              	<img src="/storage/backenduser/profile_{!! Auth::user()->_id!!}.{!! Auth::user()->profile_img !!}" class="img-circle" alt="User Imagea">
              @else
                <img src="{{ asset('dist/img/icon-user-default-462x400.png') }}" class="img-circle" alt="User Images">
              @endif

                <p>
                  {{ Auth::user()->name.' '.Auth::user()->last_name }}
                 
                </p>
              </li>
              <!-- Menu Body -->
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a class="dropdown-item btn btn-default btn-flat" href="{{ route('logout') }}" 
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('user.logout') }}" method="POST" style="display: none;">
                                             <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        </form>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->

        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
        	  @if(Auth::user()->profile_img) 
              	<img src="/storage/backenduser/profile_{!! Auth::user()->_id!!}.{!! Auth::user()->profile_img !!}" class="img-circle" alt="User Image">
              @else
	          	<img src="{{ asset('dist/img/icon-user-default-462x400.png') }}" class="img-circle" alt="User Image">
              @endif
        </div>
        <div class="pull-left info">
          <p>{{ Auth::user()->name }}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
      <li class="treeview">
          <a href="#">
            <i class="fa fa-user-circle"></i> <span>{{ __('Profile(s)') }}</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
          	<li><a href="{{ route('judge_profile.index') }}"><i class="fa fa-circle-o"></i> Judge(s)</a></li>
          	<li><a href="{{ route('lawyer_profile.index') }}"><i class="fa fa-circle-o"></i> Lawyers(s)</a></li>
          </ul>
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa fa-gg"></i> <span>{{ __('Content Management System(s)') }}</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
          	<li><a href="{{ route('lawyers.datalib.index') }}"><i class="fa fa-circle-o"></i> Data Entry(s)</a></li>
          	<li><a href="{{ route('book.index') }}"><i class="fa fa-circle-o"></i> Book(s)</a></li>
            <li><a href="{{ route('statute.index') }}"><i class="fa fa-circle-o"></i> Statute(s)</a></li>
            <li><a href="{{ route('statute.type.index') }}"><i class="fa fa-circle-o"></i> Statute(s) Type</a></li>
            <li><a href="{{ route('tags.index') }}"><i class="fa fa-circle-o"></i> Tag(s)</a></li>
          	<li><a href="{{ route('miscellaneous.index') }}"><i class="fa fa-circle-o"></i> Miscellaneous Document(s)</a></li>
            <li><a href="{{ route('miscellaneous.document.index') }}"><i class="fa fa-circle-o"></i> Document Type(s)</a></li>
            <li><a href="{{ route('circular.institute.index') }}"><i class="fa fa-circle-o"></i> Document Institution(s)</a></li>
            <li><a href="{{ route('court.index') }}"><i class="fa fa-circle-o"></i> Court(s)</a></li>
            <li><a href="{{ route('location.index') }}"><i class="fa fa-circle-o"></i> Location(s)</a></li>
            <li><a href="{{ route('department.index') }}"><i class="fa fa-circle-o"></i> Department(s)</a></li>
            <li><a href="{{ route('categoryType.index') }}"><i class="fa fa-circle-o"></i> Category Type(s)</a></li>
          </ul>
        </li>

        <li>
          <a href="{{ route('users.taskmanagement.index') }}">
            <i class="fa fa-tasks"></i> <span>{{ __('Task Management System') }}</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
 
	@yield('content')
	
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; <?php echo date('Y')?>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<div class=""></div>
<!-- ./wrapper -->


<script src="{{ asset('bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- Morris.js charts -->

<script src="{{ asset('bower_components/raphael/raphael.min.js') }}"></script>
{{--<script src="{{ asset('bower_components/morris.js/morris.min.js') }}"></script>--}}
{{--<script src="{{ asset('bower_components/morris.js/morris.js') }}"></script>--}}
<!-- Sparkline -->
<script src="{{ asset('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('bower_components/jquery-knob/dist/jquery.knob.min.js') }}"></script>
<!-- ChartJS -->
<script src="{{ asset('bower_components/chart.js/barChart.js') }}"></script>
<!-- daterangepicker -->
<script src="{{ asset('bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<!-- datepicker -->
<script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<!-- Slimscroll -->
<script src="{{ asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('bower_components/fastclick/lib/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
{{--<script src="{{ asset('dist/js/pages/dashboard.js') }}"></script>--}}
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('dist/js/demo.js') }}"></script>
<script src="{{ asset('js/custom.js') }}"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
  <script>
  $.validate({
    lang: 'en',
    modules : 'file'
  });
</script>

</body>
</html>
