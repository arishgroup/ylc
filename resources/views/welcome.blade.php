@extends('layouts.client')

@section('content')
<main class="main-content">
				<div class="hero">
					<div class="container">
						<div class="row">
							<div class="col-md-8">
								<figure><img src="dummy/lawyers.jpg" alt=""></figure>
							</div>
							<div class="col-md-4">
								<div class="hero-content">
									<h1 class="hero-title">Header Goes Here</h1>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi accusamus aut rerum sed maxime, magnam neque sapiente quidem</p>
									<a href="#" class="button">Read more</a>
								</div>
							</div>
						</div>
					</div>
				</div> <!-- .hero-slider -->
				
				<div class="fullwidth-block" data-bg-color="#111113">
					<div class="container">
						<div class="row">
							<div class="col-md-8">
								<h2>Welcome to our website</h2>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto labore alias nobis quam suscipit aut nostrum neque quibusdam eligendi explicabo, animi, eius beatae. Deleniti at, nam fuga accusamus non reprehenderit.  amet, consectetur adipisicing elit. Architecto labore alias nobis quam suscipit aut nostrum neque quibusdam eligendi explicabo, animi, eius beatae. Deleniti at, nam fuga accusamus non reprehenderit.</p>
							</div>
							<div class="col-md-4">
								<div class="latest-news">
									<h3>Latest News</h3>
									<ul>
										<li>
											<h3 class="entry-title"><a href="#">Enim ad minim veniam quis nostrud</a></h3>
											<small class="date">07/04/2014</small>
										</li>
										<li>
											<h3 class="entry-title"><a href="#">Duis aute irure dolor in reprehenderit</a></h3>
											<small class="date">07/04/2014</small>
										</li>
									</ul>
								</div> <!-- .latest-news -->
							</div>
						</div> <!-- .row -->
					</div> <!-- .container -->
				</div> <!-- .fullwidth-block -->

				<div class="fullwidth-block">
					<div class="container">
						<div class="row feature-list-section">
							<div class="col-md-4">
								<div class="feature">
									<header>
										<img src="images/icon-1.png" class="feature-icon">
										<div class="feature-title-copy">
											<h2 class="feature-title">Criminal Things</h2>
											<small class="feature-subtitle">Lorem ipsum dolor sit</small>
										</div>
									</header>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias doloremque, quis, eaque minus harum modi eius veritatis consequuntur expedita impedit ad, facilis. Asperiores assumenda aperiam atque, accusamus cupiditate vero sit!</p>
									<a href="#" class="more-link">Read More</a>
								</div>
							</div>
							<div class="col-md-4">
								<div class="feature">
									<header>
										<img src="images/icon-2.png" class="feature-icon">
										<div class="feature-title-copy">
											<h2 class="feature-title">Estate Planning</h2>
											<small class="feature-subtitle">Lorem ipsum dolor sit</small>
										</div>
									</header>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias doloremque, quis, eaque minus harum modi eius veritatis consequuntur expedita impedit ad, facilis. Asperiores assumenda aperiam atque, accusamus cupiditate vero sit!</p>
									<a href="#" class="more-link">Read More</a>
								</div>
							</div>
							<div class="col-md-4">
								<div class="feature">
									<header>
										<img src="images/icon-3.png" class="feature-icon">
										<div class="feature-title-copy">
											<h2 class="feature-title">Employment Law</h2>
											<small class="feature-subtitle">Lorem ipsum dolor sit</small>
										</div>
									</header>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias doloremque, quis, eaque minus harum modi eius veritatis consequuntur expedita impedit ad, facilis. Asperiores assumenda aperiam atque, accusamus cupiditate vero sit!</p>
									<a href="#" class="more-link">Read More</a>
								</div>
							</div>
						</div>
						
						<div class="quote-section">
							<div class="quote-slider">
								<ul class="slides">
									<li>
										<figure class="quote-avatar"><img src="dummy/avatar.png"></figure>
										<blockquote>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum reiciendis eveniet suscipit, totam doloribus iure quasi quos, quidem quam labore pariatur nesciunt rem unde odio a ex vel ullam, quis!</p>
											<footer>
												<cite>Jason Howard</cite>
												<span>(CEO, Books author)</span>
											</footer>
										</blockquote>
									</li>
									<li>
										<figure class="quote-avatar"><img src="dummy/avatar.png"></figure>
										<blockquote>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum reiciendis eveniet suscipit, totam doloribus iure quasi quos, quidem quam labore pariatur nesciunt rem unde odio a ex vel ullam, quis!</p>
											<footer>
												<cite>Jason Howard</cite>
												<span>(CEO, Books author)</span>
											</footer>
										</blockquote>
									</li>
									<li>
										<figure class="quote-avatar"><img src="dummy/avatar.png"></figure>
										<blockquote>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum reiciendis eveniet suscipit, totam doloribus iure quasi quos, quidem quam labore pariatur nesciunt rem unde odio a ex vel ullam, quis!</p>
											<footer>
												<cite>Jason Howard</cite>
												<span>(CEO, Books author)</span>
											</footer>
										</blockquote>
									</li>
									<li>
										<figure class="quote-avatar"><img src="dummy/avatar.png"></figure>
										<blockquote>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum reiciendis eveniet suscipit, totam doloribus iure quasi quos, quidem quam labore pariatur nesciunt rem unde odio a ex vel ullam, quis!</p>
											<footer>
												<cite>Jason Howard</cite>
												<span>(CEO, Books author)</span>
											</footer>
										</blockquote>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>

			</main> <!-- .main-content -->
@stop
@push('scripts')

@endpush