@extends(Auth::guard('admin')->check() ? 'layouts.admin' : 'layouts.backendUser')


@section('content')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>{{ __('Book(s)') }}</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('aos.index')}}">Book(s)</a></li>
        <li class="active">Add</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info col-md-6">
            <div class="box-header with-border">
              @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::open(['route' => 'book.store', 'method' => 'post', 'class'=>'form-horizontal','files'=>'true']) !!}
              {{ csrf_field() }}
              <div class="box-body">
              <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">{{ __('File upload:') }}</label>
                                   <div class="col-sm-5">
                 <input type='file' name='image[]' id='image' data-validation="extension" validate_extension='pdf,docx' data-validation-allowing="pdf, docx" multiple />
                   @if ($errors->has('image'))
                       <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('image') }}</strong>
                      </span>
                   @endif
                  </div>
                </div>
              <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">{{ __('Title:') }}<span class="label_red"> *</span></label>
                   <div class="col-sm-5">
                    {!! FORM::text('book_title' , '' ,['class'=>'form-control', 'placeholder'=>'Title', 'data-validation'=>'length alphanumeric', 'data-validation-length'=>'min4',  'data-validation-allowing'=>'()-_ ' ]) !!}
                       @if ($errors->has('book_title'))
                           <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('book_title') }}</strong>
                      </span>
                       @endif
                   </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">{{ __('Location:') }}</label>
                    <div class="col-sm-5">
                    	<div class='row'>
                    		<div class="col-sm-3">{!! Form::select('country_id', $country, '',['placeholder' => 'Select Country', 'onChange'=>'GetCountryChange(this.value)']) !!}</div>
                    		<div class="col-sm-5" id='state_div' style='display:none'>{!! Form::select('state_id', array(), '',['placeholder' => 'Select State', 'id'=>'country_state']) !!}</div>
                    		<div class="col-sm-4" id='city_div' style='display:none'>{!! Form::select('city_id', array(), '',['placeholder' => 'Select City', 'id'=>'city_dd']) !!}</div>
                    	</div>
                    </div>
                </div>
                <div class="form-group">
                  {!! FORM::label('name' ,'Year',['class'=>'col-sm-2 control-label']) !!}
               <div class="col-sm-5">
                	 {!! FORM::text('book_year' , '' ,['class'=>'form-control', 'placeholder'=>'Year']) !!}
                  </div>
                </div>
               
                <div class="form-group">
                  {!! FORM::label('name' ,'Description',['class'=>'col-sm-2 control-label']) !!}
                   <div class="col-sm-5">
                   {!! FORM::textarea('description' ,'',['class'=>'form-control', 'rows' => 3, 'cols' => 40]) !!}
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="{{route('book.index')}}"><button type="button" class="btn btn-default">Back</button></a>
                {!! Form::submit('Submit', ['class'=>'btn btn-info pull-right']) !!}
              </div>
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <!-- /.box-footer -->
   
            {!! Form::close() !!}
          </div>
         
        </div>
        <!--/.col (right) -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script>
        $('#tags').select2({
            placeholder: "Choose tags...",
            minimumInputLength: 2,
            ajax: {
                url: '/tags/find',
                dataType: 'json',
                data: function (params) {
                    return {
                        q: $.trim(params.term)
                    };
                },
                processResults: function (data) {
                    return {
                        results: data[0]
                    };
                },
                cache: true
            }
        });

    function GetCountryChange(country_code){
        		
				$.ajax({url: "/get_state/"+country_code, success: function(result){
					console.log(result);
					document.getElementById('country_state').innerHTML = result.data;
					document.getElementById('state_div').style.display = 'block';
					//document.getElementById('city_div').style.display = 'none';
			    }});
			} 
    function GetstateChange(state_code){
		
		$.ajax({url: "/get_city/"+state_code, success: function(result){
			document.getElementById('city_dd').innerHTML = result.data;
			if(result.data != "")
				document.getElementById('city_div').style.display = 'block';
	    }});
	} 
</script>
@endsection 
