@extends('layouts.client')
                <style>
/* Float four columns side by side */
.column {
  float: left !important;
  width: 33% !important;;
  padding: 0 10px !important;
}

/* Remove extra left and right margins, due to padding */
.row {margin: 0 -5px;}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}



/* Style the counter cards */
.card-test {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2) !important;
  padding: 16px !important;
  background-color: #f1f1f1 !important;
}
</style>


@section('content')

 <main class="main-content">
				
				<div class="fullwidth-block content">
<div class="container">
<h2 class="entry-title">Registration Type</h2>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                

                   <form id='row_name' action="{{ route('registertype') }}" method="post">
                   {{ csrf_field() }}
                <div class="card-body">
                   <div class="row">
                      <div class="column">
                        <div class="card card-test">
                          <h3><input type="radio" name="register_type" id="register_type" value="personal" /> Personal</h3>
                          <p>Some text</p>
                          <p>Some text</p>
                        </div>
                      </div>
                    
                      <div class="column ">
                        <div class="card card-test">
                          <h3><input type="radio" name="register_type" id="register_type" value="family" /> Family</h3>
                          <p>Some text</p>
                          <p>Some text</p>
                        </div>
                      </div>
                      
                      <div class="column">
                        <div class="card card-test">
                          <h3><input type="radio" name="register_type" id="register_type" value="company" /> Company</h3>
                          <p>Some text</p>
                          <p>Some text</p>
                        </div>
                      </div>
                    </div> 
                </div>
                <div class="col-md-12 mt-2" style="text-align:center; padding-top:10px">
                	<input type="submit" value="Start Registration" class="btn btn-info mb-2" />
                </div>
                      </form>
            </div>
        </div>
    </div>
</div>
</div>
        </main>

@endsection    
