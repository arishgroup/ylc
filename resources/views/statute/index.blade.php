    
@extends(Auth::guard('admin')->check() ? 'layouts.admin' : 'layouts.backendUser')

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>{{ __('Statute(s)') }}</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Statute(s)</a></li>
        <li class="active">Listing</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">


          <div class="box">
            <div class="box-header" style="float: right">
              <a href="{{route('statute.create')}}"><button id="new_booktype" class="btn btn-primary">Add Statute(s)</button></a>
            </div>
            <div>
             @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <div style="float:right"><input id="myInput" type="text" placeholder="Search.."></div>
            <div id="alphabut" class="btn-group btn-group-sm alphabetical_order_btns">
               <?php foreach(range('A','Z') as $v){?>
                     <a href="{{ route('statute.index', ['ch'=>$v]) }}"><button class="btn btn-default char"><?php echo $v;?></button></a>
                <?php } ?>
                    </div>
              <table  class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Sr.</th>
                  <th>Name</th>
                  <th>Type</th>
                  <th>Tags</th>
                  <th>Updated Date</th>
                  <th>Action</th>
                  
                </tr>
                </thead>
                <tbody id="tableGrid">
                <?php $i=0;?>
              @if($book->count())
               	@foreach($book as $val)
               	<tr>
                  <td>{!! $i = $i+1 !!}</td>
                  <td>{!! $val->name !!}</td>
                  <td>@if($val->type){!! $val->bookTypes->name !!}@endif</td>
                  <td>@foreach($val->getTags as $tags) <label class="label label-info">{!! $tags->title !!}</label> @endforeach</td>
                  <td>{!! $val->updated_at !!}</td>
                  <td><a href="{{ route('statute.sections.index', ['id'=>$val->_id]) }}">Section(s)</a> | <a href="{{ route('statute.edit', ['id'=>$val->_id]) }}">Update</a> | <a class="confirmation" href="{{ route('statute.destroy', $val->_id) }}">Delete</a></td>
                </tr>
                 @endforeach
                @else
                <tr id="no_record">
                  <td colspan="6">No Record Found!</td>
                </tr>
                @endif   
			  </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection 
