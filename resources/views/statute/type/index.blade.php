@extends(Auth::guard('admin')->check() ? 'layouts.admin' : 'layouts.backendUser')



@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>{{ __('Statute(s) Type') }}</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Statute(s) Type</a></li>
        <li class="active">Add</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box">
            <div class="box-header" style="float: right">
              <a href="{{route('statute.type.create')}}"><button id="new_booktype" class="btn btn-primary">Add Statute Type(s)</button></a>
            </div>
            <div>
             @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="data_grid" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="5%">Sr.</th>
                  <th width="65%">Name</th>
                  <th width="15%">Updated Date</th>
                  <th width="15%">Action</th>
                </tr>
                </thead>
                <tbody>
             	<?php $i = 0 ;?>

		@if($bookType->count())
                @foreach($bookType as $bt)
                <tr>
                  <td>{!! $i = $i+1 !!}</td>
                  <td>{!! $bt->name !!}</td>
                  <td>{!! $bt->updated_at !!}</td>
                  <td><a href="{{ route('statute.type.edit', $bt->_id) }}"><i class="fa fa-edit"></i></a> | <a class="confirmation" href="{{ route('statute.type.destroy', $bt->_id) }}"><i class="fa fa-trash"></i></a></td>
                </tr>
                 @endforeach
                @else
                <tr id="no_record">
                  <td colspan="4">No Record Found!</td>
                </tr>
                @endif   
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection 
