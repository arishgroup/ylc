@extends(Auth::guard('admin')->check() ? 'layouts.admin' : 'layouts.backendUser')


@section('content')

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>{{ __('Statute(s)') }}</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Statute(s)</a></li>
        <li class="active">Listing</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- The Modal -->
<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
    <p>Some text in the Modal..</p>
  </div>

</div>

          <div class="box">
          
            <div class="box-header" style="float: right">
              <a href="{{route('statute.index', ['ch'=>substr($statute, 0,1)])}}"><button id="new_booktype" class="btn btn-primary">Back to Statute(s)</button></a>
              <a href="{{route('statute.sections.create', ['id' => $book_id])}}"><button id="new_booktype" class="btn btn-primary">Add Statutes Section(s)</button></a>
            </div>
            <div>
             @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
                
            </div>
            <!-- /.box-header -->
            <div class="box-body">
             <div style="float:right"><input id="myInput" type="text" placeholder="Search.."></div>
              <div style="width:100%; float:left; padding-bottom:10px"><b>Statute: {!! $statute !!}</b></div>
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="50px"></th>
                  <th>Content Title</th>
                  <th>Section</th>
                  <th>Act/Ordinace</th>
                  <th width="100px"></th>
                  <th>Updated Date</th>
                  <th>Action</th>
                  
                </tr>
                </thead>
                <tbody id="tableGrid">
                <?php $i=0;?>
              	@if($book_sections->count())
               	@foreach($book_sections as $val)
               	<tr>
                  <td><a href="javascript:void(0)" onClick="showPopup('{!! $val->_id !!}')">Read</a></td>
                  <td>{!! $val->definition !!}</td>
                  <td>{!! $val->section_no !!}</td>
                  <td>{!! $val->bookSection->name !!}</td>
                  <td>@if($val->caseLaws->count() > 0)  <a href="{{ route('caselaws', $val->_id) }}">Case Laws</a>@endif</td>
                  <td>{!! $val->updated_at !!}</td>
                  <td><a href="{{ route('statute.sections.edit', $val->_id) }}"><i class="fa fa-edit"></i></a> | <a class="confirmation" href="{{ route('statute.sections.destroy', $val->_id) }}"><i class="fa fa-trash"></i></a></td>
                </tr>
                 @endforeach
                @else
                <tr id="no_record">
                  <td colspan="7">No Record Found!</td>
                </tr>
                @endif   
             
			  </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- Modal -->



<script type="text/javascript">
    
    var modal = document.getElementById('myModal');
   
    var span = document.getElementsByClassName("close")[0];
    // When the user clicks the button, open the modal 
    function showPopup(id){
      
    	var section_id = id;
    	$(".modal-content").html('');
        $.get( "/admin/statutes/get_content?id=" + section_id+"&t=<?php echo time();?>", function( data ) {
           //console.log(data);
        	$(".modal-content").html(data.content);
        });
    	
        modal.style.display = "block";
    }
    
    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }
    
    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>
@endsection 
