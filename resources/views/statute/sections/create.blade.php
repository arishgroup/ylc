@extends(Auth::guard('admin')->check() ? 'layouts.admin' : 'layouts.backendUser')


@section('content')
<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>{{ __('Book(s) Section') }}</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('statute.index')}}">Book(s)</a></li>
        <li class="active">Add</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info col-md-6">
            <div class="box-header with-border">
              @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::open(['route' => ('statute.sections.store'), 'method' => 'post', 'class'=>'form-horizontal']) !!}
              {{ csrf_field() }}
              <div class="box-body">
       
              <div class="form-group">
                  {!! FORM::label('name' ,'Book Title',['class'=>'col-sm-2 control-label']) !!}
                   <div class="col-sm-10">
                    {!! FORM::label('name' , $book_title->name ,['class'=>'col-sm-10 control-label', 'style'=>'padding-left: 0px; text-align: left;']) !!}
                  </div>
                </div>
              
              <div class="form-group">
                  {!! FORM::label('name' ,'Text of Section',['class'=>'col-sm-2 control-label']) !!}
                   <div class="col-sm-7">
                    {!! FORM::text('section_title' , '' ,['class'=>'form-control', 'placeholder'=>'Text of Section']) !!}
                  </div>
                </div>
                <div class="form-group">
                  {!! FORM::label('name' ,'Section Number',['class'=>'col-sm-2 control-label']) !!}
                 <div class="col-sm-7">
                 	{!! FORM::text('section_no' , '' ,['class'=>'form-control', 'placeholder'=>'Section no']) !!}
                  </div>
                </div>
                <div class="form-group">
                {!! FORM::label('name' ,'Case Laws',['class'=>'col-sm-2 control-label']) !!}
                  <div class="col-sm-7">
                 	<select id="case_laws" name="case_laws[]" class="form-control" multiple></select>
                 	
                  </div> 
                  </div>
                <div class="form-group">
                  {!! FORM::label('name' ,'Content',['class'=>'col-sm-2 control-label']) !!}
               <div class="col-sm-7">
                	  {!! FORM::textarea('content_ckeditor' ,'',['class'=>'form-control', 'rows' => 3, 'cols' => 40]) !!}
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="{{route('statute.sections.index', ['id'=>$book_title->_id])}}"><button type="button" class="btn btn-default">Back</button></a>
                {!! Form::submit('Submit', ['class'=>'btn btn-info pull-right']) !!}
              </div>
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <!-- /.box-footer -->
   				{!! FORM::hidden('book_id' ,$book_title->_id) !!}
            {!! Form::close() !!}
          </div>
         
        </div>
        <!--/.col (right) -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<script>
	CKEDITOR.replace( 'content_ckeditor',
     {
        toolbar : 'master'
    });

</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script>
        $('#case_laws').select2({
            placeholder: "Choose tags...",
            minimumInputLength: 2,
            ajax: {
                url: '/getCaseLaws/find',
                dataType: 'json',
                data: function (params) {
                    return {
                        q: $.trim(params.term)
                    };
                },
                processResults: function (data) {
                    return {
                        results: data[0]
                    };
                },
                cache: true
            }
        });
    </script>
@endsection 
