@extends('layouts.admin')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tasks
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#"><i class="fa fa-tasks"></i> Task Management System</a></li>
        <li class="active"><a href="#"> <i class="fa fa-circle-o"></i>Tasks</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-12">
          <!-- small box -->
          <!-- TO DO List -->
          <div class="box box-primary">
            <div style="float:right; margin:05px"><a href="{{ route('taskManagement.create') }}"><button id="new_booktype" class="btn btn-primary"><i class="fa fa-plus"></i> Assign Task</button></a></div>
            <div>
             @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              
              <div class="box-body">
                <table id="assignee_table" class="table table-striped table-bordered" style="width:100%">
                  <thead>
                  <tr>
                    <th>Assignee</th>
                    <th>Task</th>
                    <th>Description</th>
                    <th>Created</th>
                    <th>Deadline</th>
                    <th>Priority</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                </table>
              </div>
            </div>
            <div class="box-footer clearfix no-border">
              <button type="button" onclick="location.href = '{{ route('taskManagement.create') }}';" class="btn btn-default pull-right"><i class="fa fa-plus"></i> Add item</button>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- /.content-wrapper -->
  <script>
      $('#assignee_table').on('click', 'a.editor_edit', function (e) {
          e.preventDefault();

          editor.edit( $(this).closest('tr'), {
              title: 'Edit record',
              buttons: 'Update'
          } );
      } );

      // Delete a record
      $('#assignee_table').on('click', 'a.editor_remove', function (e) {
          e.preventDefault();

          editor.remove( $(this).closest('tr'), {
              title: 'Delete record',
              message: 'Are you sure you wish to remove this record?',
              buttons: 'Delete'
          } );
      } );


      $(function() {
          $('#assignee_table').DataTable({
              destroy: true,
              processing: true,
              serverSide: true,
              "order": [[ 3, "desc" ]],
              "pageLength": 25,
              ajax: '/getDataTables/&&&',
              columns: [
                  { data: 'assignee_name', name: 'assignee_name' },
                  { data: 'task_name', name: 'task_name' },
                  { data: 'task_description', name: 'task_description' },
                  { data: 'created_at', name: 'created_at' },
                  { data: 'assigner_end_date', name: 'assigner_end_date' },
                  { data: 'priority', name: 'priority' },
                  { data: 'action', name: 'action', orderable: false, searchable: false}
              ]
          });
      });
  </script>

@endsection
  