@extends('layouts.admin')

@section('content')
    <!-- Content Wrapper. Contains page content -->

    <!-- Content Header (Page header) -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>{{ __('Update Task') }}</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#"><i class="fa fa-tasks"></i> Task Management System</a></li>
                <li class="active"><a href="#"> <i class="fa fa-circle-o"></i>Update Task</a></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- right column -->
                    <div class="col-md-12">
                        <!-- Horizontal Form -->
                        <div class="box box-info col-md-6">
                            <div class="box-header with-border">
                                @if (Session::has('message'))
                                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                                @endif
                            </div>
                            <!-- /.box-header -->
                                <!-- form start -->
                            @if(!empty($data->assignee_start_date))
                                {!! Form::open(['route' => array('taskManagement.update_after_assignee_start', $data->id), 'method' => 'post', 'class'=>'form-horizontal','files'=>'true']) !!}
                                {{ csrf_field() }}
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="name" class="col-sm-4 control-label">{{ __('Assignee Type:') }}<span class="label_red"> *</span></label>
                                        <div class="col-sm-5 control-label text-left">
                                            {{ucfirst($data->assignee_type)}}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="col-sm-4 control-label">{{ __('Assignee Name:') }}<span class="label_red"> *</span></label>
                                        <div class="col-sm-5 control-label text-left">
                                            @if($data->assignee_type == 'lawyer') {{$data->getTaskLawyer->name}} @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="col-sm-4 control-label">{{ __('Task Name:') }}<span class="label_red"> *</span></label>
                                        <div class="col-sm-5 control-label text-left">
                                            {{$data->task_name}}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="col-sm-4 control-label">{{ __('Description:') }}<span class="label_red"> *</span></label>
                                        <div class="col-sm-5 control-label text-left">
                                            {{$data->task_description}}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="col-sm-4 control-label">{{ __('Assigner Start Date:') }}<span class="label_red"> *</span></label>
                                        <div class="col-sm-5 control-label text-left">
                                            {{$data->assigner_start_date}}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="col-sm-4 control-label">{{ __('Assigner End Date:') }}<span class="label_red"> *</span></label>
                                        <div class="col-sm-5 control-label text-left">
                                            {{$data->assigner_end_date}}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="col-sm-4 control-label">{{ __('Priority:') }}</label>
                                        <div class="col-sm-5 control-label text-left">
                                            @if($data->priority =='0')
                                                <small class="label label-info"><i class="fa fa-clock-o"></i> Minor </small>
                                            @elseif($data->priority =='1')
                                                <small class="label label-primary"><i class="fa fa-clock-o"></i> Low </small>
                                            @elseif($data->priority =='2')
                                                <small class="label label-success"><i class="fa fa-clock-o"></i> Normal </small>
                                            @elseif($data->priority =='3')
                                                <small class="label label-warning"><i class="fa fa-clock-o"></i> High </small>
                                            @elseif($data->priority =='4')
                                                <small class="label label-danger"><i class="fa fa-clock-o"></i> Urgent </small>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! FORM::label('name' ,'Comment',['class'=>'col-sm-4 control-label']) !!}
                                        <div class="col-sm-5">
                                            {!! FORM::textarea('comment' ,'',['class'=>'form-control', 'rows' => 3, 'cols' => 40]) !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! FORM::label('name' ,'Attachments',['class'=>'col-sm-4 control-label']) !!}
                                        <div class="col-sm-5">
                                            <input type="file" name="attachments[]" multiple />
                                        </div>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <a href="{{route('taskManagement.index')}}"><button type="button" class="btn btn-default">Back</button></a>
                                    @if(empty($data->assignee_end_date)) {!! Form::submit('Submit', ['class'=>'btn btn-info pull-right']) !!} @endif
                                </div>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <!-- /.box-footer -->

                                {!! Form::close() !!}
                                @else
                                {!! Form::open(['route' => array('taskManagement.update', $data->id), 'method' => 'post', 'class'=>'form-horizontal','files'=>'true']) !!}
                                {{ csrf_field() }}
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="name" class="col-sm-4 control-label">{{ __('Assignee Type:') }}<span class="label_red"> *</span></label>
                                        <div class="col-sm-5">
                                            {!! Form::select('assignee_type', $user_types,$data->assignee_type,['class'=>'form-control', 'placeholder'=>'Select Assignee Type', 'onChange'=>'getAssignee(this.value)'] ) !!}
                                            @if ($errors->has('assignee_type'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('assignee_type') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="col-sm-4 control-label">{{ __('Assignee Name:') }}<span class="label_red"> *</span></label>
                                        <div class="col-sm-5">
                                            {!! Form::select('assignee_id', array(),$data->assignee_id,['class'=>'form-control', 'placeholder'=>'Select Assignee', 'id'=>'select_assignee'] ) !!}
                                            @if ($errors->has('assignee_id'))
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('assignee_id') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="col-sm-4 control-label">{{ __('Task Name:') }}<span class="label_red"> *</span></label>
                                        <div class="col-sm-5">
                                            {!! FORM::text('task_name' , $data->task_name  ,['class'=>'form-control', 'placeholder'=>'Task Name']) !!}
                                            @if ($errors->has('task_name'))
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('task_name') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="col-sm-4 control-label">{{ __('Description:') }}<span class="label_red"> *</span></label>
                                        <div class="col-sm-5">
                                            {!! FORM::textarea('task_description' ,$data->task_description,['class'=>'form-control', 'rows' => 3, 'cols' => 40]) !!}
                                            @if ($errors->has('task_description'))
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('task_description') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! FORM::label('name' ,'Start Date',['class'=>'col-sm-4 control-label']) !!}
                                        <div class="col-sm-5">
                                            {{--{!! Form::date('deadline', \Carbon\Carbon::now()->format('D/M/Y'), ['class' => 'form-control']) !!}--}}
                                            <input type="date" class ="form-control" name="assigner_start_date" value="{{ old('assigner_start_date',$data->assigner_start_date)}}">
                                    </div>
                                    </div>
                                    <div class="form-group">
                                        {!! FORM::label('name' ,'End Date',['class'=>'col-sm-4 control-label']) !!}
                                        <div class="col-sm-5">
                                            {{--{!! Form::date('deadline', \Carbon\Carbon::now()->format('D/M/Y'), ['class' => 'form-control']) !!}--}}
                                            <input type="date" class ="form-control" name="assigner_end_date" value="{{ old('assigner_end_date', $data->assigner_end_date) }}">
                                            @if ($errors->has('assigner_end_date'))
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('assigner_end_date') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="col-sm-4 control-label">{{ __('Priority:') }}</label>
                                        <div class="col-sm-5">
                                            <select name='priority' class='form-control'>
                                                <option {{ old('role_id', $data->priority) == '0' ? 'selected' : '' }} value='0'>Minor</option>
                                                <option {{ old('role_id', $data->priority) == '1' ? 'selected' : '' }} value='1'>Low</option>
                                                <option {{ old('role_id', $data->priority) == '2' ? 'selected' : '' }} value='2'>Normal</option>
                                                <option {{ old('role_id', $data->priority) == '3' ? 'selected' : '' }} value='3'>High</option>
                                                <option {{ old('role_id', $data->priority) == '4' ? 'selected' : '' }} value='4'>Urgent</option>
                                            </select>
                                            @if ($errors->has('priority'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('priority') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! FORM::label('name' ,'Comment',['class'=>'col-sm-4 control-label']) !!}
                                        <div class="col-sm-5">
                                            {!! FORM::textarea('comment' ,'',['class'=>'form-control', 'rows' => 3, 'cols' => 40]) !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! FORM::label('name' ,'Attachments',['class'=>'col-sm-4 control-label']) !!}
                                        <div class="col-sm-5">
                                            <input type="file" name="attachments[]" multiple />
                                        </div>
                                    </div>
                                </div>

                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <a href="{{route('taskManagement.index')}}"><button type="button" class="btn btn-default">Back</button></a>
                                    @if(empty($data->assignee_end_date)) {!! Form::submit('Submit', ['class'=>'btn btn-info pull-right']) !!} @endif
                                </div>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <!-- /.box-footer -->

                                {!! Form::close() !!}
                            @endif

                            <div class="box-footer">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h3>Attachments</h3>
                                    </div><!-- /col-sm-12 -->
                                </div><!-- /row -->
                                @if(!empty($data->getTaskAttachments[0]['_id']))
                                <div class="row">
                                    @foreach($data->getTaskAttachments as $taskAttachments)
                                        <div class="col-sm-3">
                                            @if($taskAttachments->file_extension == "pdf")
                                                <a  href="{!! route('show.pdf', [encrypt('/storage/taskmanagement/'.$taskAttachments->_id.'.'.$taskAttachments->file_extension)]) !!}" target="_blank" >{{$taskAttachments->file_name}}</a>
                                            @else
                                                <a target="_blank" href='{{ asset("storage/taskmanagement/$taskAttachments->_id.$taskAttachments->file_extension") }}' >{{$taskAttachments->file_name}}</a>
                                            @endif
                                        </div><!-- /col-sm-1 -->
                                    @endforeach
                                </div><!-- /row -->
                                @else
                                <div class="row">
                                    <div class="col-sm-11">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                No File attached Yet
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                            <div class="box-footer">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h3>Comments</h3>
                                    </div><!-- /col-sm-12 -->
                                </div><!-- /row -->
                                @if(!empty($data->getTaskComments[0]['comment']))
                                    @foreach($data->getTaskComments as $taskComment)
                                        <div class="row">
                                            <div class="col-sm-1">
                                                <div class="thumbnail">
                                                    <img class="img-responsive user-photo" src="https://ssl.gstatic.com/accounts/ui/avatar_2x.png">
                                                </div><!-- /thumbnail -->
                                            </div><!-- /col-sm-1 -->

                                            <div class="col-sm-11">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        @if($taskComment->user_type =='admin')
                                                            <strong>@if($taskComment->user_id) {!! $taskComment->getTaskAdmin->name  !!} @endif </strong>
                                                        @elseif($taskComment->user_type =='lawyer')
                                                            <strong>@if($taskComment->user_id) {!! $taskComment->getTaskLawyer->name  !!} @endif </strong>
                                                        @elseif($taskComment->user_type =='user')
                                                            <strong>@if($taskComment->user_id) {!! $taskComment->getTaskUser->name  !!} @endif </strong>
                                                        @endif <span class="text-muted"> commented on {{ $taskComment['created_at'] }}</span>
                                                    </div>
                                                    <div class="panel-body">
                                                        {{ $taskComment['comment'] }}
                                                    </div><!-- /panel-body -->
                                                </div><!-- /panel panel-default -->
                                            </div><!-- /col-sm-5 -->
                                        </div><!-- /row -->
                                    @endforeach
                                @else
                                    <div class="row">
                                        <div class="col-sm-11">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    No Comments Yet
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    @if(empty($data->assignee_start_date))
    <script>
        function getAssignee(type,id){
            $.ajax({url: "/taskManagement/getAssignee/"+type+'&'+id, success: function(result){
                    var row_id = 'select_assignee';
                    document.getElementById(row_id).innerHTML = result.data;
                }});
        }

        getAssignee('{{$data->assignee_type}}','{{$data->assignee_id}}' );
    </script>
    @endif

@endsection
