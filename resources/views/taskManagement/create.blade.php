@extends('layouts.admin')

@section('content')
    <?php
    $assignee_type= "";
    $assignee_id="";
    $task_name="";
    $task_description="";
    $assigner_start_date="";
    $assigner_end_date="";
    $priority="";
    $current_record="";
    if(isset($data))
    {
        $assignee_type = $data->assignee_type;
        $assignee_id = $data->assignee_id;
        $task_name = $data->task_name;
        $task_description = $data->task_description;
        $assigner_start_date = $data->assigner_start_date;
        $assigner_end_date = $data->assigner_end_date;
        $priority = $data->priority;
        $current_record = $data->_id;
    }    ?>
        <!-- Content Header (Page header) -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>{{ __('Assign Task') }}</h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li><a href="#"><i class="fa fa-tasks"></i> Task Management System</a></li>
                    <li class="active"><a href="#"> <i class="fa fa-circle-o"></i>Assign Task</a></li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-aqua">
                            <div class="inner">
                                <h3><b id="total">0</b></h3>
                                <h4>Total Tasks</h4>
                            </div>
                            <div class="icon">
                                <i class="ion-briefcase"></i>
                            </div>
                            <a data-toggle="modal" data-target="#modal-popup" href="#" onclick="getAllDataTables('-1');" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-yellow">
                            <div class="inner">
                                <h3><b id="pending">0</b></h3>
                                <h4>Pending Tasks</h4>
                            </div>
                            <div class="icon">
                                <i class="ion-arrow-expand"></i>
                            </div>
                            <a data-toggle="modal" data-target="#modal-popup" href="#" onclick="getAllDataTables('0');" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-red">
                            <div class="inner">
                                <h3><b id="in_progress">0</b></h3>

                                <h4>In Progress Tasks</h4>
                            </div>
                            <div class="icon">
                                <i class="ion-close-circled"></i>
                            </div>
                            <a data-toggle="modal" data-target="#modal-popup" href="#" onclick="getAllDataTables('1');" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-green">
                            <div class="inner">
                                <h3><b id="complete">0</b></h3>
                                <h4>Completed Tasks</h4>
                            </div>
                            <div class="icon">
                                <i class="ion-checkmark-circled"></i>
                            </div>
                            <a data-toggle="modal" data-target="#modal-popup" href="#" onclick="getAllDataTables('2');" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                </div>
                <div class="row">
                    <section class="col-lg-6">

                        <!-- right column -->
                        <div class="box box-info">
                            <div class="margin">
                                <div class="box-header">
                                    @if (Session::has('message'))
                                        <div class="alert alert-success">{{ Session::get('message') }}</div>
                                    @endif
                                </div>

                                <!-- /.box-header -->
                                <!-- form start -->
                                <div class="clearfix my-5">

                                    <div class="pull-left btn-group">
                                        <button type="button" id="last" onclick="getNavigation('last')" class="btn btn-default">Last </button>
                                        <button type="button" id="prev" onclick="getNavigation('prev')" class="btn btn-default">Previous </button>
                                    </div>
                                    <div class="pull-right btn-group">
                                        <button type="button" id='next' onclick="getNavigation('next')" class="btn btn-default">Next</button>
                                        <button type="button" id="first" onclick="getNavigation('first')" class="btn btn-default">First</button>
                                    </div>
                                </div>
                                <div class="box-header"></div>


                                {!! Form::open(['route' => 'taskManagement.store', 'method' => 'post','id'=>'taskForm' , 'class'=>'form-horizontal','files'=>'true']) !!}
                                {{ csrf_field() }}

                                <div class="form-group row">
                                    <div class="col-xs-6">
                                        <h4>{{ __('Assignee Type:') }}<span class="label_red"> *</span></h4>

                                        {!! Form::select('assignee_type',  $user_types,$assignee_type,['class'=>'form-control', 'id'=>'assignee_type', 'placeholder'=>'Select Assignee Type', 'onChange'=>'getAssignee(this.value); getResult();'] ) !!}
                                        @if ($errors->has('assignee_type'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('assignee_type') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-xs-6">
                                        <h4>{{ __('Assignee Name:') }}<span class="label_red"> *</span></h4>
                                        <div class="input-group">
                                            {!! Form::select('assignee_id', array(),$assignee_id,['class'=>'form-control', 'id'=>'assignee_id', 'placeholder'=>'Select Assignee' , 'onClick'=>'getResult();'] ) !!}
                                            <span class="input-group-btn">
                                              <button type="button" data-toggle="modal" data-target="#modal-popup" onclick="getAllDataTables('assignee_name')" class="btn btn-primary btn-flat"><i class="fa fa-fw fa-search"></i></button>
                                            </span>
                                        </div>

                                        @if ($errors->has('assignee_id'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('assignee_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-xs-12">
                                        <h4>{{ __('Task Name:') }}<span class="label_red"> *</span></h4>
                                        <div class="input-group">
                                            {!! FORM::text('task_name' , $task_name ,['class'=>'form-control', 'id'=>'task_name','placeholder'=>'Task Name','onkeyup'=>'getResult()']) !!}
                                            <span class="input-group-btn">
                                              <button type="button" data-toggle="modal" data-target="#modal-popup" onclick="getAllDataTables('task_name')" class="btn btn-primary btn-flat"><i class="fa fa-fw fa-search"></i></button>
                                            </span>
                                        </div>
                                        @if ($errors->has('task_name'))
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('task_name') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-xs-12">
                                        <h4>{{ __('Description:') }}<span class="label_red"> *</span></h4>
                                        <div>
                                            {!! FORM::textarea('task_description' ,$task_description,['class'=>'form-control', 'id'=>'task_description' ,'rows' => 3, 'cols' => 40]) !!}
                                            @if ($errors->has('task_description'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('task_description') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-xs-6">
                                        <h4>{{ __('Start Date:') }}</h4>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            {{--{!! Form::date('deadline', \Carbon\Carbon::now()->format('D/M/Y'), ['class' => 'form-control']) !!}--}}
                                            <input type="text" class ="form-control" id="start_date" name="assigner_start_date" onclick = 'getResult()' value="{{ old('assigner_start_date',$assigner_start_date)}}">
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <h4>{{ __('End Date:') }}</h4>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            {{--{!! Form::date('deadline', \Carbon\Carbon::now()->format('D/M/Y'), ['class' => 'form-control']) !!}--}}
                                            <input type="text" class ="form-control" id="end_date" name="assigner_end_date" onkeyup = 'getResult()' value="{{ old('assigner_end_date', $assigner_end_date) }}">
                                            @if ($errors->has('assigner_end_date'))
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('assigner_end_date') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-xs-6">
                                        <h4>{{ __('Priority:') }}</h4>
                                        <div>
                                            {!! Form::select('priority',  $all_priorities,$priority,['class'=>'form-control', 'id'=>'priority', 'onkeyup'=>'getResult();'] ) !!}
                                            @if ($errors->has('priority'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('priority') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <h4>{{ __('Attachments:') }}</h4>
                                        <div>
                                            <input type="file" name="attachments[]" multiple />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-xs-12">
                                        <h4>{{ __('Comment:') }}</h4>
                                        <div>
                                            {!! FORM::textarea('comment' ,'',['class'=>'form-control', 'rows' => 3, 'cols' => 40]) !!}
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <div>
                                    <a href="{{route('taskManagement.index')}}"><button type="button" class="btn btn-default">Back</button></a>
                                    <input class="btn btn-primary" type="button" onclick="clearForm()" value="New Task">
                                    {!! Form::submit('Submit', ['class'=>'btn btn-primary pull-right']) !!}
                                </div>
                                <div class="box-header"></div>
                                <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" id="assigner_id" name="assigner_id" value="{{ Auth::user()->_id }}">
                                <input type="hidden" id="current_record" name="current_record" value="{!!  $current_record !!}">
                                <!-- /.box-footer -->

                                {!! Form::close() !!}
                            </div>
                        </div>
                        <!--/.col (right) -->
                        <div class="box box-solid bottom-part">
                            <div class="box-header with-border">
                                <i class="fa fa-envelope"></i>
                                <h3 class="box-title">Attachments</h3>
                            </div>
                            <div id="attachments" class="box-body">
                                <div class="row box-header">
                                    <?php $div_loop=0; ?>
                                    @if(!empty($data->getTaskAttachments[0]['_id']))
                                        @foreach($data->getTaskAttachments as $taskAttachments)
                                            <?php
                                            if($div_loop%4 == 0 && $div_loop!=0){ ?>
                                </div><div class='row box-header'> <?php
                                    }?>
                                    <div class="col-sm-3">
                                        @if($taskAttachments->file_extension == "docx" || $taskAttachments->file_extension == "xlsx" || $taskAttachments->file_extension == "pptx")
                                            <img src='{{ asset("storage/taskmanagement/office.jpg") }}' alt="attachments" height="25" width="25">
                                            <a target="_blank" href='{{ asset("storage/taskmanagement/$taskAttachments->_id.$taskAttachments->file_extension") }}' >{{$taskAttachments->file_name}}</a>
                                        @elseif($taskAttachments->file_extension == "pdf")
                                            <img src='{{ asset("storage/taskmanagement/pdf.jpg") }}' alt="attachments" height="25" width="25">
                                            <a  href="{!! route('show.pdf', [encrypt('/storage/taskmanagement/'.$taskAttachments->_id.'.'.$taskAttachments->file_extension)]) !!}" target="_blank" >{{$taskAttachments->file_name}}</a>
                                        @else
                                            <img src='{{ asset("storage/taskmanagement/$taskAttachments->_id.$taskAttachments->file_extension") }}' alt="attachments" height="25" width="25">
                                            <a target="_blank" href='{{ asset("storage/taskmanagement/$taskAttachments->_id.$taskAttachments->file_extension") }}' >{{$taskAttachments->file_name}}</a>
                                        @endif
                                    </div><?php
                                    ++$div_loop; ?>
                                    @endforeach
                                    @else
                                        <div class="col-sm-11">
                                            <div class="panel">
                                                <div class="panel-heading">
                                                    No File attached Yet
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="box box-solid bottom-part">
                            <div class="box-header with-border">
                                <i class="fa fa-comments"></i>
                                <h3 class="box-title">Comments</h3>
                            </div>
                            <section class="box-body">
                                <div class="row">
                                    <div id="comments" class="col-md-12">
                                        <ul class="timeline">
                                            @if(!empty($data->getTaskComments[0]['comment']))
                                                @foreach($data->getTaskComments->sortByDesc('created_at') as $taskComment)
                                                    <li>
                                                        <i class="fa fa-comments bg-yellow"></i>
                                                        <div class="timeline-item panel-default">
                                                            <span class="time"><i class="fa fa-clock-o"></i> {{ $taskComment['created_at']->diffForHumans() }}</span>
                                                            <h5 class="panel-heading no-margin"><a href="#">
                                                                    @if($taskComment->user_type =='admin')
                                                                        @if($taskComment->user_id) {!! $taskComment->getTaskAdmin->name  !!} @endif
                                                                    @elseif($taskComment->user_type =='lawyer')
                                                                        @if($taskComment->user_id) {!! $taskComment->getTaskLawyer->name  !!} @endif
                                                                    @elseif($taskComment->user_type =='user')
                                                                        @if($taskComment->user_id) {!! $taskComment->getTaskUser->name  !!} @endif
                                                                    @endif
                                                                </a> commented on your post</h5>
                                                            <div class="timeline-body">
                                                                {{ $taskComment['comment'] }}
                                                            </div>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            @else
                                                <li>
                                                    <i class="fa fa-comments bg-yellow"></i>
                                                    <div class="timeline-item">
                                                        <div class="panel-heading">
                                                            No Comment Yet
                                                        </div>
                                                    </div>
                                                </li>
                                            @endif
                                            <li>
                                                <i class="fa fa-clock-o bg-gray"></i>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </section>
                    <section class="col-xs-6">
                        <div class="box box-warning">
                            <div class="box-header">
                                <h3 class="box-title">Tasks</h3>

                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                </div>

                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <table id="assignee_table" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Task</th>
                                        <th>Assign To</th>
                                        <th>Designation</th>
                                        <th>Created</th>
                                        <th>Deadline</th>
                                        <th>Priority</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>

                        <div class="box box-success">
                            <div class="box-header with-border">
                                <h3 class="box-title">Bar Chart</h3>

                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="chart">
                                    <canvas id="barChart" style="height:230px"></canvas>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </section>
                    <!-- /.col -->
                </div>

            </section>
            <!-- /.content -->

        </div>

    <div class="modal fade" id="modal-popup">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Tasks</h4>
                </div>
                <div class="modal-body">
                    <div class="box">
                        <div class="box-body">
                            <table id="model_table" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Task</th>
                                    <th>Assign To</th>
                                    <th>Designation</th>
                                    <th>Created at</th>
                                    <th>Deadline</th>
                                    <th>Priority</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@stop
@push('scripts')
    <script>
        function getResult(){
            CSRF_TOKEN = document.getElementById('_token').value;
            var assignee_type = "";
            var assignee_id = "";
            var task_name = "";
            var start_date = "";
            var end_date = "";
            var task_priority = "";

            if(document.getElementById('assignee_type')){
                assignee_type = document.getElementById('assignee_type').value;
            }
            if(document.getElementById('assignee_id')){
                assignee_id = document.getElementById('assignee_id').value;
            }
            if(document.getElementById('task_name')){
                task_name = document.getElementById('task_name').value;
            }
            if(document.getElementById('start_date')){
                start_date = document.getElementById('start_date').value;
            }
            if(document.getElementById('end_date')){
                end_date = document.getElementById('end_date').value;
            }
            if(document.getElementById('priority')){
                task_priority = document.getElementById('priority').value;
            }
            var assigner_id = document.getElementById('assigner_id').value;
            $.ajax({
                type: 'POST',
                url: "/assigneeTasksStatus/",
                data:{
                    _token: CSRF_TOKEN,
                    'assignee_type': assignee_type,
                    'assignee_id': assignee_id,
                    'task_name': task_name,
                    'start_date': start_date,
                    'end_date': end_date,
                    'priority': task_priority,
                    'assigner_id': assigner_id,
                },
                success: function(result){
                    document.getElementById('total').innerHTML = result.data['total'];
                    document.getElementById('pending').innerHTML = result.data['pending'];
                    document.getElementById('in_progress').innerHTML = result.data['in_progress'];
                    document.getElementById('complete').innerHTML = result.data['complete'];
                }});
        }
    function getAssignee(type,id){

        $.ajax({url: "/getAssignee/"+type+"&"+id, async: false,success: function(result){
            var row_id = 'assignee_id';
            document.getElementById(row_id).innerHTML = result.data;
        }});
    }
    getAssignee('{!! $assignee_type !!}','{!! $assignee_id !!}');

        function getAssignerStatus(id){
        $.ajax({url: "/assignerTasksStatus/"+id, success: function(result){
            document.getElementById('total').innerHTML = result.data['total'];
            document.getElementById('pending').innerHTML = result.data['pending'];
            document.getElementById('in_progress').innerHTML = result.data['in_progress'];
            document.getElementById('complete').innerHTML = result.data['complete'];
        }});
    }

    getAssignerStatus("{{ $assigner_id }}");

        $(document).ready(function() {

            $('#start_date').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true
            });

            $('#end_date').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true
            });
        $('#start_date').datepicker()
            .on('changeDate', function(e) {
                getResult();
            });
            $('#end_date').datepicker()
                .on('changeDate', function(e) {
                    getResult();
                });
        });


        function getAllDataTables(name){
            var table = "";
            var assignee_type = "";
            var assignee_name = "";
            var task_name = "";
            var task_type = "";
            if(name == 'all'){
                table = '#assignee_table';
            }
            if(name == 'assignee_name'){
                table = '#model_table';
                assignee_type = document.getElementById('assignee_type').value;
                assignee_name = document.getElementById('assignee_id').value;
            }
            if(name == 'task_name'){
                table = '#model_table';
                assignee_name = document.getElementById('assignee_id').value;
                task_name = document.getElementById('task_name').value;
            }
            if(name == "-1"){
                table = '#model_table';
            }
            if(name == "0" || name == "1" || name == "2"){
                table = '#model_table';
                task_type = name;
            }
            $(table).DataTable({
                destroy: true,
                processing: true,
                serverSide: true,
                "order": [[ 3, "desc" ]],
                ajax: '/getDataTables/'+assignee_type+'&'+assignee_name+'&'+task_name+'&'+task_type,
                columns: [
                    { data: 'task_name', name: 'task_name' },
                    { data: 'assignee_name', name: 'assignee_name' },
                    { data: 'assignee_type', name: 'assignee_type' },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'assigner_end_date', name: 'assigner_end_date' },
                    { data: 'priority', name: 'priority'}
                ]
            });
        }
        getAllDataTables("all");

        function getNavigation(nav){
            var id=0;
            if(nav == 'next' || nav == 'prev'){
              id = document.getElementById("current_record").value;
            }
            var total_val  = nav+'&'+id;
            $.ajax({url: "/dataTablesNavigation/"+total_val,  success: function(result){

                if(result.data[0]){
                    document.getElementById("current_record").value = result.data[0]._id;
                    // console.log(result.data);
                    getAssignee(result.data[0].assignee_type);
                    var objSelect_n = document.getElementById("assignee_type");
                    setSelectedValue(objSelect_n, result.data[0].assignee_type);

                    var objSelect = document.getElementById("assignee_id");
                    setSelectedValue(objSelect, result.data[0].assignee_id );

                    document.getElementById('task_name').value = result.data[0].task_name;
                    document.getElementById('task_description').value = result.data[0].task_description;

                    var objSelect_p = document.getElementById("priority");
                    setSelectedValue(objSelect_p, result.data[0].priority );

                    document.getElementById('start_date').value = result.data[0].assigner_start_date;
                    document.getElementById('end_date').value = result.data[0].assigner_end_date;

                    if(result.data[1]){
                        document.getElementById('comments').innerHTML = result.data[1];
                    }
                    if(result.data[2]){
                        document.getElementById('attachments').innerHTML = result.data[2];
                    }
                }
                else{
                    alert("No record Found");
                }


            }});
        }

        function setSelectedValue(selectObj, valueToSet) {
            for (var i = 0; i < selectObj.options.length; i++) {

                if (selectObj.options[i].value == valueToSet) {
                    selectObj.options[i].selected = true;
                    return;
                }
            }
        }

        function clearForm() {
            getAssignerStatus("{{ $assigner_id }}");
            document.getElementById('assignee_type').value= "";
            document.getElementById('assignee_id').value= "";
            document.getElementById('task_name').value= "";
            document.getElementById('task_description').value= "";
            document.getElementById('start_date').value= "";
            document.getElementById('end_date').value= "";
            document.getElementById('priority').value= "";
            document.getElementById('current_record').value= "";
            $('#comments .timeline').remove();
            $('#attachments .row').remove();
        }
    </script>

    @component('components.graph', ['graph'=>$graph])
    @endcomponent
@endpush
