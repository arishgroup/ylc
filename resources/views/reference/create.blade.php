@extends('layouts.admin')


@section('content')
<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>{{ __('Book(s)') }}</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Reference Note(s)</a></li>
        <li class="active">Add</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info col-md-6">
            <div class="box-header with-border">
              @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::open(['route' => array('reference_note.store', $aos->_id), 'method' => 'put', 'class'=>'form-horizontal']) !!}
              {{ csrf_field() }}
              <div class="box-body">
              <div class="form-group">
                  {!! FORM::label('name' ,'Area of Service',['class'=>'col-sm-2 control-label']) !!}
                   <div class="col-sm-5">
                    {!! FORM::label('aos' , $aos->name ,['class'=>'col-sm-7']) !!}
                  </div>
                </div>
           
              <div class="form-group">
                  {!! FORM::label('name' ,'Title',['class'=>'col-sm-2 control-label']) !!}
                   <div class="col-sm-5">
                    {!! FORM::text('title' , '' ,['class'=>'form-control', 'placeholder'=>'Title']) !!}
                  </div>
                </div>
           
                <div class="form-group">
                  {!! FORM::label('name' ,'Description',['class'=>'col-sm-2 control-label']) !!}
                   <div class="col-sm-5">
                   {!! FORM::textarea('detail_desciption' ,'',['class'=>'form-control', 'rows' => 3, 'cols' => 40]) !!}
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="{{route('reference.material',$aos )}}"><button type="button" class="btn btn-default">Back</button></a>
                {!! Form::submit('Submit', ['class'=>'btn btn-info pull-right']) !!}
              </div>
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="hidden" name="user_id" value="{{ Auth::id() }}">
              <input type="hidden" name="area_of_service_id" value="{{ $aos }}">
              <!-- /.box-footer -->
   
            {!! Form::close() !!}
          </div>
         
        </div>
        <!--/.col (right) -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
	<script>
    	CKEDITOR.replace( 'detail_desciption' );
	</script>
@endsection 
