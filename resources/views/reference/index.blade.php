@extends('layouts.admin')


@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>{{ __('Reference Note(s)') }}</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Reference Note(s)</a></li>
        <li class="active">Listing</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box">
         
            <div class="box-header" style="float: right">
              <a href="{{route('aos.tree')}}"><button id="new_booktype" class="btn btn-primary">Back To Area of Service(s)</button></a> <a href="{{route('reference_note.create', $aos)}}"><button id="new_booktype" class="btn btn-primary">Add Notes</button></a>
            </div>
            <div>
             @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <div style="float:right"><input id="myInput" type="text" placeholder="Search.."></div>
           
              <table  class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Sr.</th>
                  <th>Title</th>
                  <th>Area of Service</th>
                  <th>Updated Date</th>
                  <th>Action</th>
                  
                </tr>
                </thead>
                <tbody id="tableGrid">
                <?php $i=0;?>
              @if($view_data->count())
               	@foreach($view_data as $val)
               	<tr>
                  <td>{!! $i = $i+1 !!}</td>
                  <td>{!! $val->title !!}</td>
                  <td>{!! $val->getAreaOfService->name !!}</td>
                  <td>{!! $val->updated_at !!}</td>
                  <td><a href="{{ route('book.sections.index', ['id'=>$val->_id]) }}"><i class="fa fa-cubes"></i></a> | <a href="{{ route('reference_note.edit', ['id'=>$val->_id]) }}"><i class="fa fa-edit"></i></a> | <a class="confirmation" href="{{ route('reference_note.destroy', $val->_id) }}"><i class="fa fa-trash"></i></a></td>
                </tr>
                 @endforeach
                @else
                <tr id="no_record">
                  <td colspan="5">No Record Found!</td>
                </tr>
                @endif   
			  </table>
            </div>
               <div style="text-align:right; padding-right: 10px;">{{ $view_data->links() }}</div> 
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection 
