@extends(Auth::guard('admin')->check() ? 'layouts.admin' : 'layouts.backendUser')

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>{{ __('Location(s)') }}</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('location.index')}}">Location(s)</a></li>
        <li class="active">Update</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info col-md-6">
            <div class="box-header with-border">
              @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::open(['route' => array('location.update',$view_Data->_id), 'method' => 'put', 'class'=>'form-horizontal']) !!}
              {{ csrf_field() }}
              <div class="box-body">
              <div class="form-group">
                <label for="name" class="col-sm-2 control-label">{{ __('Title:') }}<span class="label_red"> *</span></label>
                   <div class="col-sm-5">
                    {!! FORM::text('title' , $view_Data->title ,['class'=>'form-control', 'placeholder'=>'Title']) !!}
                     @if ($errors->has('title'))
                       <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('title') }}</strong>
                     </span>
                     @endif
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="{{route('location.index')}}"><button type="button" class="btn btn-default">Back</button></a>
                {!! Form::submit('Submit', ['class'=>'btn btn-info pull-right']) !!}
              </div>
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              
              <!-- /.box-footer -->
   
            {!! Form::close() !!}
          </div>
         
        </div>
        <!--/.col (right) -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection 
