@extends(Auth::guard('admin')->check() ? 'layouts.admin' : 'layouts.backendUser')


@section('content')
	<script src="http://demo.itsolutionstuff.com/plugin/jquery.js"></script>
<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>{{ __('Data Entry') }}</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('aos.index')}}">>Data Entry</a></li>
        <li class="active">Update</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info col-md-6">
            <div class="box-header with-border">
              @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::open(['route' => array ('datalib.update',$content->_id ), 'method' => 'put', 'class'=>'form-horizontal','files'=>'true']) !!}
              {{ csrf_field() }}
              <div class="box-body">
            
                <div class="form-group">
                  {!! FORM::label('name' ,'File upload',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-3">
                 {!! Form::file('image');!!}
                  @if($content->file_ext != "") 
                  <a  href="{!! route('show.pdf', [encrypt('/storage/data_entry/data_entry_'.$content->_id.'.'.$content->file_ext)]) !!}" target="_blank" >View File</a>
                  @endif
                  </div>
                  {!! FORM::label('name' ,'Area of Service',['class'=>'col-sm-2 control-label']) !!}
                  <div class="col-sm-3">
                 {!! Form::select('area_of_service', $aos,$content->aos,['class'=>'form-control', 'placeholder'=>'Select Area of Service'] ) !!}
                  </div>
                 </div>
                <div class="form-group">
                  {!! FORM::label('name' ,'Title',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-3">
                 {!! FORM::text('title' ,$content->title,['class'=>'form-control', 'placeholder'=>'Title']) !!}
                  </div>
                  {!! FORM::label('name' ,'Court',['class'=>'col-sm-2 control-label']) !!}
                  <div class="col-sm-3">
                 	{!! Form::select('court', $court, $content->court,['class'=>'form-control', 'placeholder'=>'Select Court'] ) !!}
                  </div>
                </div>
                <div class="form-group">
                  {!! FORM::label('name' ,'Petitioner',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-3">
                 {!! FORM::text('petitioner' ,$content->petitioner,['class'=>'form-control', 'placeholder'=>'Petitioner']) !!}
                  </div>
                  {!! FORM::label('name' ,'Respondents',['class'=>'col-sm-2 control-label']) !!}
                  <div class="col-sm-3">
                 {!! FORM::text('respondent' ,$content->respondent,['class'=>'form-control', 'placeholder'=>'Respondents']) !!}
                  </div>
                </div>
                <div class="form-group">
                  {!! FORM::label('name' ,'Appellant',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-3">
                 {!! FORM::text('appellant' ,$content->applent,['class'=>'form-control', 'placeholder'=>'Appellant']) !!}
                  </div>
                  {!! FORM::label('name' ,'Defandent',['class'=>'col-sm-2 control-label']) !!}
                  <div class="col-sm-3">
                 {!! FORM::text('defandent' ,$content->defandent,['class'=>'form-control', 'placeholder'=>'Defandent']) !!}
                  </div>
                </div>
                <div class="form-group">
                  {!! FORM::label('name' ,'Page No.',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-3">
                 {!! FORM::text('page_no' ,$content->page_no,['class'=>'form-control', 'placeholder'=>'Page No.']) !!}
                  </div>
                  {!! FORM::label('name' ,'Referance',['class'=>'col-sm-2 control-label']) !!}
                  <div class="col-sm-3">
                 {!! FORM::text('referance' ,$content->reference,['class'=>'form-control', 'placeholder'=>'Referance']) !!}
                  </div>
                </div>
                <div class="form-group">
                  {!! FORM::label('name' ,'Judge Name',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-3">
                 {!! Form::select('judge_name[]', $alljudges, $sel_judge,['class' => 'form-control', 'multiple', 'id'=>'judge_name']) !!}
                 <br>
                 {!! FORM::text('other_judge' ,'',['class'=>'form-control', 'placeholder'=>'Other Judges']) !!}
                  </div>
                   {!! FORM::label('name' ,'Tags',['class'=>'col-sm-2 control-label']) !!}
                  <div class="col-sm-3">
                  {!! Form::select('tags[]', $allTags, $tags_selected,['class' => 'form-control', 'multiple', 'id'=>'tags']) !!}
                 	
                 	
                  </div> 
                  
                </div>
                <div class="form-group">
                  {!! FORM::label('name' ,'Lawyer In-Favor',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-3">
                 {!! FORM::text('lawyer_favor' ,$content->lawyer_favor,['class'=>'form-control', 'placeholder'=>'Lawyer In-Favor']) !!}
                  </div>
  
                  
                  {!! FORM::label('name' ,'Lawyer Against',['class'=>'col-sm-2 control-label']) !!}
                  <div class="col-sm-3">
                 {!! FORM::text('lawyer_against' ,$content->lawyer_against,['class'=>'form-control', 'placeholder'=>'Lawyer Name']) !!}
                  </div>
		       </div>
                 <div class="form-group">
                  {!! FORM::label('name' ,'Citation Name',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-3">
                 {!! FORM::text('citation_name' ,$content->citation_name,['class'=>'form-control', 'placeholder'=>'Citation Name']) !!}
                  </div>
                  {!! FORM::label('name' ,'City',['class'=>'col-sm-2 control-label']) !!}
                  <div class="col-sm-3">
                 	{!! Form::select('location', $location,$content->location,['class'=>'form-control', 'placeholder'=>'Select Location'] ) !!}
                  </div>
               </div>
                <div class="form-group" style="display: none">
                    <label for="name" class="col-sm-2 control-label">{{ __('Location:') }}</label>
                    <div class="col-sm-3">
                    	{!! Form::select('country_id', $country, '',['placeholder' => 'Select Country','class'=>'form-control', 'onChange'=>'GetCountryChange(this.value)', 'id'=>'country_id']) !!}
                    </div>
                    <div class="col-sm-2"></div>
                    <div class="col-sm-5"><div  id='state_div' style='display:none'>{!! Form::select('state_id', array(), '',['placeholder' => 'Select State', 'id'=>'country_state','class'=>'form-control']) !!}
                    </div></div>
                </div>
                   <div class="form-group">
                  {!! FORM::label('name' ,'Category',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-3">
                 {!! Form::select('category', $category,$content->category,['class'=>'form-control', 'placeholder'=>'Select Category'] ) !!}
                  </div>
                  
                  {!! FORM::label('name' ,'Category Type',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-3">
                 {!! Form::select('category_type', $category_type,$content->category_type,['class'=>'form-control', 'placeholder'=>'Select Category'] ) !!}
                  </div>
                </div>
                <div class="form-group">
                  {!! FORM::label('name' ,'Publish Month',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-3">
                 {!! FORM::text('publish_month' ,$content->publish_month,['class'=>'form-control', 'placeholder'=>'Publish Month']) !!}
                  </div>
                  {!! FORM::label('name' ,'Publish Year',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-3">
                 {!! FORM::text('publish_year' ,$content->publish_year,['class'=>'form-control', 'placeholder'=>'Pubish Year']) !!}
                  </div>
                </div>
                @if($content->statuteSections->count())
                <?php $i = 0; ?>
                <div class="new_row" >
                  @foreach($content->statuteSections as $statute)
                  	
                  	<div class="form-group row_<?php echo $i;?>" > 
                  <?php if($i == 0){ ?> 
                  		{!! FORM::label('name' ,'Statute(s)',['class'=>'col-sm-2 control-label']) !!}
                  	<?php } else { ?>  
                  		<label for="name" class="col-sm-2 control-label"></label>
                  	<?php }  ?>
                  
                  
                    <div class="col-sm-3">
                {!! Form::select('statutes', $statutes,$statute->bookSection->_id,['class'=>'form-control selections', 'placeholder'=>'Select Statute', 'onChange'=>'GetSectionChange(this.value, 0, $i)' ] ) !!}
                  </div>
                  {!! FORM::label('name' ,'Statute Sections',['class'=>'col-sm-2 control-label']) !!}
                  <div class="col-sm-3">
                 	{!! Form::select('statute_section[]', array(),'',['class'=>'form-control selections', 'placeholder'=>'Select Section', 'id'=>'select_div_'.$i] ) !!}
                  </div>
                 	<?php if($i == 0){ ?> 
                  		<input type="button" class="add-row" value="Add Row">
                  	<?php } else { ?>  
                  		<input type="button" class="remove-row" value="Remove" onClick="remove_row('<?php echo $i;?>')">
                  	<?php } $i = $i + 1; ?>
                </div>
                  @endforeach()
                </div>
                @else 
                <div class="form-group">
                  {!! FORM::label('name' ,'Statute(s)',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-3">
                {!! Form::select('statutes', $statutes,'',['class'=>'form-control selections', 'placeholder'=>'Select Statute', 'onChange'=>'GetSectionChange(this, 0, 0)' ] ) !!}
                  </div>
                  {!! FORM::label('name' ,'Statute Sections',['class'=>'col-sm-2 control-label']) !!}
                  <div class="col-sm-3">
                 	{!! Form::select('statute_section[]', array(),'',['class'=>'form-control selections', 'placeholder'=>'Select Section', 'id'=>'select_div_0'] ) !!}
                  </div>
                  <input type="button" class="add-row" value="Add Row">
                </div>
                @endif
             
        		<div class="form-group">
                  {!! FORM::label('name' ,'Short Description',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-8">
                    {!! FORM::textarea('short_desc' ,$content->short_desc,['class'=>'form-control', 'rows' => 3, 'cols' => 40]) !!}
					 </div>
                 </div>
                <div class="form-group">
                  {!! FORM::label('name' ,'Head Notes',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-8">
                    {!! FORM::textarea('head_notes' ,$content->head_notes,['class'=>'form-control', 'rows' => 3, 'cols' => 40]) !!}
					 </div>
                 </div>	
                <div class="form-group">
                  {!! FORM::label('name' ,'Description',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-8">
                    {!! FORM::textarea('article_ckeditor' ,$content->description,['class'=>'form-control', 'rows' => 3, 'cols' => 40]) !!}
					 </div>
                 </div>
                
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="{{route('lawyers.datalib.index')}}"><button type="button" class="btn btn-default">Back</button></a>
                {!! Form::submit('Submit', ['class'=>'btn btn-info pull-right']) !!}
              </div>
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="hidden" name="_id" value="{{ $content->_id }}">
              <input type="hidden" value="{!! $content->statuteSections->count() !!}" name="row_count" id="row_count" />
              <!-- /.box-footer -->
         
            {!! Form::close() !!}
          </div>
         
        </div>
        <!--/.col (right) -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
	<script>
    	CKEDITOR.replace( 'article_ckeditor' );
    	CKEDITOR.replace( 'head_notes' );
    	CKEDITOR.replace( 'short_desc' );
	</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script>
        $('#tags').select2({
            placeholder: "Choose tags...",
            minimumInputLength: 2,
            ajax: {
                url: '/tags/find',
                dataType: 'json',
                data: function (params) {
                    return {
                        q: $.trim(params.term)
                    };
                },
                processResults: function (data) {
                    console.log(data);
                    return {
                        results: data[0]
                    };
                },
                cache: true
            }
        });
    </script>
    <script>
        $('#judge_name').select2({
            placeholder: "Choose Judge...",
            minimumInputLength: 2,
            ajax: {
                url: '/judge/find',
                dataType: 'json',
                data: function (params) {
                    return {
                        q: $.trim(params.term)
                    };
                },
                processResults: function (data) {
                    console.log(data);
                    return {
                        results: data[0]
                    };
                },
                cache: true
            }
        });

        
    </script>
    <script type="text/javascript">
			$(".selections").select2();

			function GetSectionChange(value, new_call, cell_id){
				$.ajax({url: "/getSections/"+value+"/"+new_call, success: function(result){
					var row_id = 'select_div_'+cell_id;
					document.getElementById(row_id).innerHTML = result.data;
			    }});
			}
			$(document).ready(function(){
		        $(".add-row").click(function(){
			        var row_count = $('#row_count').val();
			        var markup = '<div class="form-group row_'+row_count+'">'+
				        '<label for="name" class="col-sm-2 control-label"></label>'+
                    '<div class="col-sm-3">'+
	                 '<select class="form-control selections" onchange="GetSectionChange(this.value, 0, '+row_count+')" name="statutes[]" tabindex="-1" aria-hidden="true">'+
	                 	'<option value="">Select Statute</option>'+
					<?php foreach($statutes as $key => $statute){?>
	                 	'<option value="<?php echo $key;?>"><?php echo $statute?></option>'+
					<?php } ?>
	                 	'</select>'+
	                  '</div>'+
	                  '<label for="name" class="col-sm-2 control-label">Statute Sections</label>'+
	                    '<div class="col-sm-3">'+
	                 		'<select class="form-control selections" id="select_div_'+row_count+'" name="statute_section[]" tabindex="-1" aria-hidden="true">'+
	                 		'<option selected="selected" value="">Select Section</option></select>'+
	                  '</div>'+
	                  '<input type="button" class="remove-row" value="Remove" onClick="remove_row('+row_count+')">'+
		  	          '</div>';
		            $(".new_row").append(markup);
		            $('#row_count').val(parseInt(row_count)+1);
		            $(".selections").select2();
		        });
			});

			function remove_row(row_id){
				$('.row_'+row_id).remove();
			}
    <?php if($content->statuteSections->count()) {
             $i = 0;
            foreach($content->statuteSections as $statute) { ?>
                GetSectionChange('<?php echo $statute->bookSection->_id?>', '<?php echo $statute->_id;?>', '<?php echo $i;?>')
           <?php $i = $i+1; }
        } ?>

        function GetCountryChange(country_code){
			$.ajax({url: "/get_state/"+country_code,  async: false,success: function(result){
				console.log(result);
				document.getElementById('country_state').innerHTML = result.data;
				document.getElementById('state_div').style.display = 'inline-block';

		    }});
		} 
 
<?php if($content->country_id){ ?>
var objSelect = document.getElementById("country_id");
setSelectedValue(objSelect, "<?php echo $content->country_id;?>");
GetCountryChange('<?php echo $content->country_id;?>');
<?php }?>

<?php if($content->state_id){ ?>
var objSelect_n = document.getElementById("country_state");
setSelectedValue(objSelect_n, "<?php echo $content->state_id;?>");
//GetstateChange('<?php //echo $row_data->state_id;?>');

<?php }?>
function setSelectedValue(selectObj, valueToSet) {
    for (var i = 0; i < selectObj.options.length; i++) {
        
        if (selectObj.options[i].value == valueToSet) {
            selectObj.options[i].selected = true;
            return;
        }
    }
}
		</script>
@endsection 
