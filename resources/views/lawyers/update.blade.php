@extends(Auth::guard('admin')->check() ? 'layouts.admin' : 'layouts.lawyer')


@section('content')
<style>
.dropbtn {
    background-color: #3498DB;
    color: white;
    padding: 16px;
    font-size: 16px;
    border: none;
    cursor: pointer;
}

.dropbtn:hover, .dropbtn:focus {
    background-color: #2980B9;
}

.dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f1f1f1;
    min-width: 160px;
    overflow: auto;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

.dropdown-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
}

.dropdown a:hover {background-color: #ddd;}

.show {display: block;}
</style>
<link rel="stylesheet" href="{{ asset('css/simple-scrollbar.css?123')}}">
				<link rel="stylesheet" href="{{ asset('css/Treant.css')}}">
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>{{ __('Profile') }}</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Staff</a></li>
        <li><a href="{{route('lawyers.index')}}">Lawyer(s)</a></li>
        <li class="active">update</li> 
      </ol>
    </section>

    <!-- Main content -->
      <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
            @if($data['data_collection']->profile_img != "") 
              <img class="profile-user-img img-responsive img-circle" src="/storage/lawyer/profile/img_{!! $data['data_collection']->_id!!}.{!! $data['data_collection']->profile_img !!}" alt="User profile picture">
			@else 
			  <img class="profile-user-img img-responsive img-circle" src="/storage/icon-user-default-462x400.png" alt="User profile picture">
			  @endif
              <h3 class="profile-username text-center">{{ ucfirst($data['data_collection']->name)." ".ucfirst($data['data_collection']->lastname) }}</h3>

              <p class="text-muted text-center">@if($data['data_collection']->designation){{ $data['data_collection']->getDesignation->name }}@endif</p>
               @if(Auth::guard('admin')->check())<a href="{{route('lawyers.index')}}" class="btn btn-primary btn-block"><b>Back</b></a>@endif
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">About Me</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <strong><i class="fa fa-envelope-square margin-r-5"></i> Email</strong>

              <p class="text-muted">
                {{ $data['data_collection']->email }}
              </p>

              <hr>

              <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>

              <p class="text-muted">@if($data['data_collection']->location_id){{ $data['data_collection']->getLocation->title}}@else-@endif, Pakistan</p>

              <hr>
              
              <strong><i class="fa fa-file-text-o margin-r-5"></i> Address</strong>

              <p class="text-muted">{{ $data['data_collection']->address }}</p>
@if(Auth::guard('admin')->check())
              <hr>

              <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>

              <p>{{ $data['data_collection']->notes }}</p>
              @endif
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
            
              <li class="active"><a href="#settings" data-toggle="tab">Settings</a></li>
              <!--  <li><a href="#education" data-toggle="tab">Education Detail</a></li>  -->
           @if(Auth::guard('admin')->check()) 
          	  <li><a href="#activity" data-toggle="tab">Activity</a></li>
              <li><a href="#paos" data-toggle="tab" onclick="setTimeout(show_tree, 1000)">Personal Notes</a></li>
              <li><a href="#task" data-toggle="tab" >Task(s)</a></li>
          @endif    
            </ul>
            
            <div class="tab-content">
            <div class="tab-pane" id="task">
  				<div class="box-body">
       				<table id="assignee_table" class="table table-striped table-bordered" style="width:100%">
                  <thead>
                  <tr>
                    <th>Task</th>
                    <th>Assignee</th>
                    <th>Description</th>
                    <th>Due Date</th>
                    <th>Created</th>
                    <th>Priority</th>
                
                  </tr>
                  </thead>
                </table>
	            </div>
			</div>
              <div class="tab-pane" id="activity">
     			<ul class="timeline">
				@if($data['logs']->count())
					@php $c_date = ""; @endphp
					@foreach($data['logs'] as $key => $log)
                    <!-- timeline time label -->
    					@php $db_create_date = date('d-M-y',strtotime($log->created_at)) @endphp
    					@if($c_date != $db_create_date)
    						@php $c_date = $db_create_date; @endphp
        					<li class="time-label">
                                <span class="bg-red">
                                    {!! $db_create_date !!}
                                </span>
        					</li>
        				@endif
                    <!-- /.timeline-label -->
                        <!-- timeline item -->
                        <li>
            			    <i class="fa fa fa-circle-o bg-blue"></i>
                            <div class="timeline-item">
                                <span class="time"><i class="fa fa-clock-o"></i> {!! date('g:i:s A',strtotime($log->created_at)) !!}</span>
                    
                                <!-- <h3 class="timeline-header"><a href="#">Support Team</a> ...</h3>  -->
                    
                                <div class="timeline-body">
                                    {{ $log->subject }}
                                </div>
                        		@if(false && $log->log_name!= 'login' )
                                    <div class="timeline-footer">
                                        <a class="btn btn-primary btn-xs">view</a>
                                    </div>
                                @endif
        					</div>
    					</li>
    				@endforeach
               	@endif
                    <!-- END timeline item -->
              </ul>
              </div>
  			<div class="tab-pane" id="paos">
  				<div class="box-body">
       				<div class="chart" id="OrganiseChart1" style="height: 650px;"></div>
	            </div>
			</div>
			<div class="tab-pane" id="education">
			{!! Form::open(['route' => array('lawyers.update', $data['data_collection']->_id ), 'method' => 'post', 'class'=>'form-horizontal','files'=>'true']) !!}
              			{{ csrf_field() }}
  		
              			
			@if($data['edu']->count())  
                <?php $cnt  = 0;?>
                	@foreach($data['edu'] as $education)
                	 <div class='row_<?php echo $cnt;?>'>
                	 <fieldset>
                  <legend>Educational Detail:</legend>
                		<div class="form-group">
                  <label for="name" class="col-sm-2 control-label">{{ __('School/Collage:') }}<span class="label_red"> *</span></label>
                  <div class="col-sm-3">
                 	<input id="collage<?php echo $cnt;?>" type="text" class="form-control{{ $errors->has('collage') ? ' is-invalid' : '' }}" placeholder='Institute Name' name="collage[]" value="{!! $education->institute !!}" autofocus>
                 @if ($errors->has('collage'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('collage') }}</strong>
                    </span>
                @endif
                  </div>
                     <label for="name" class="col-sm-2 control-label">{{ __('Degree:') }}<span class="label_red"> *</span></label>
                  <div class="col-sm-3">
                 	<input id="degree<?php echo $cnt;?>" type="text" class="form-control{{ $errors->has('degree') ? ' is-invalid' : '' }}" placeholder='Degree Name' name="degree[]" value="{!! $education->degree !!}" autofocus>
                 @if ($errors->has('degree'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('degree') }}</strong>
                    </span>
                @endif
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">{{ __('From Year:') }}<span class="label_red"> *</span></label>
                  <div class="col-sm-3">
                 	<select name='from_year[]' class='form-control'>
                 		<option value='0'>Select From Year</option>
                 		<?php for($i=date('Y'); $i >=1900 ; $i-- ){
                 		    ?>
                 			<option <?php if( $education->from_year == $i){ echo 'selected="selected"';} else { echo ''; } ?>  value='<?php echo $i;?>'><?php echo $i;?></option>
                 		<?php } ?>
                 	</select>
                 @if ($errors->has('collage'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('collage') }}</strong>
                    </span>
                @endif
                  </div>
                     <label for="name" class="col-sm-2 control-label">{{ __('To Year:') }}<span class="label_red"> *</span></label>
                  <div class="col-sm-3">
					<select name='to_year[]' class='form-control'>
                 		<option value='0'>Select To Year</option>
                 		<?php for($i=date('Y')+7; $i >=1900 ; $i-- ){?>
                 			<option <?php if( $education->to_year == $i){ echo 'selected="selected"';} else { echo ''; } ?> value='<?php echo $i;?>'><?php echo $i;?></option>
                 		<?php } ?>
                 	</select>
                 @if ($errors->has('degree'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('degree') }}</strong>
                    </span>
                @endif
                  </div>
                </div>
                    <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">{{ __('Grade:') }}<span class="label_red"> *</span></label>
                  <div class="col-sm-3">
                 	<input id="grade<?php echo $cnt;?>" type="text" class="form-control{{ $errors->has('grade') ? ' is-invalid' : '' }}" placeholder='Grades' name="grade[]" value="{!! $education->grade !!}" autofocus>
                 @if ($errors->has('grade'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('grade') }}</strong>
                    </span>
                @endif
                  </div>
                     <label for="name" class="col-sm-2 control-label">{{ __('Hobbies:') }}<span class="label_red"> *</span></label>
                  <div class="col-sm-3">
                 	<input id="hobby<?php echo $cnt;?>" type="text" class="form-control{{ $errors->has('hobby') ? ' is-invalid' : '' }}" placeholder='Degree Name' name="hobby[]" value="{!! $education->hobbies !!}" autofocus>
                 @if ($errors->has('hobby'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('hobby') }}</strong>
                    </span>
                @endif
                  </div>
                </div>
                <div class="form-group">
                  
                     <label for="name" class="col-sm-2 control-label">{{ __('Description:') }}</label>
                  <div class="col-sm-3">
					<textarea name="edu_desc[]" id="edu_desc<?php echo $cnt;?>" style="width: 100%" cols="10" >{!! $education->description !!}</textarea>
                 @if ($errors->has('edu_desc'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('edu_desc') }}</strong>
                    </span>
                @endif
                  </div>
                   <div class="col-sm-2">
                   </div>
                  <div class="col-sm-3">
                  	<?php if($cnt == 0){ ?>
					<input type="button" class="add-row pull-right" value="Add More">
					<?php } else { ?>
					<input type="button" class="remove-row pull-right" value="Remove" onClick="remove_row('<?php echo $cnt;?>')">
					<?php }  ;?>
                  </div>
                </div>
                <?php if($cnt == 0){ ?>
					<div class="new_row" >
                  
                  
                </div>
					<?php }
					$cnt = $cnt+1; ?>
                
                </fieldset></div>
                	@endforeach
                @else
                  <fieldset>
                  <legend>Educational Detail:</legend>
                  <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">{{ __('School/Collage:') }}<span class="label_red"> *</span></label>
                  <div class="col-sm-3">
                 	<input id="collage" type="text" class="form-control{{ $errors->has('collage') ? ' is-invalid' : '' }}" placeholder='Institute Name' name="collage[]" value="{{ old('collage') }}" autofocus>
                 @if ($errors->has('collage'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('collage') }}</strong>
                    </span>
                @endif
                  </div>
                     <label for="name" class="col-sm-2 control-label">{{ __('Degree:') }}<span class="label_red"> *</span></label>
                  <div class="col-sm-3">
                 	<input id="degree" type="text" class="form-control{{ $errors->has('degree') ? ' is-invalid' : '' }}" placeholder='Degree Name' name="degree[]" value="{{ old('degree') }}" autofocus>
                 @if ($errors->has('degree'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('degree') }}</strong>
                    </span>
                @endif
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">{{ __('From Year:') }}<span class="label_red"> *</span></label>
                  <div class="col-sm-3">
                 	<select name='from_year[]' class='form-control'>
                 		<option value='0'>Select From Year</option>
                 		<?php for($i=date('Y'); $i >=1900 ; $i-- ){?>
                 			<option value='<?php echo $i;?>'><?php echo $i;?></option>
                 		<?php } ?>
                 	</select>
                 @if ($errors->has('collage'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('collage') }}</strong>
                    </span>
                @endif
                  </div>
                     <label for="name" class="col-sm-2 control-label">{{ __('To Year:') }}<span class="label_red"> *</span></label>
                  <div class="col-sm-3">
					<select name='to_year[]' class='form-control'>
                 		<option value='0'>Select To Year</option>
                 		<?php for($i=date('Y')+7; $i >=1900 ; $i-- ){?>
                 			<option value='<?php echo $i;?>'><?php echo $i;?></option>
                 		<?php } ?>
                 	</select>
                 @if ($errors->has('degree'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('degree') }}</strong>
                    </span>
                @endif
                  </div>
                </div>
                    <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">{{ __('Grade:') }}<span class="label_red"> *</span></label>
                  <div class="col-sm-3">
                 	<input id="grade" type="text" class="form-control{{ $errors->has('grade') ? ' is-invalid' : '' }}" placeholder='Grades' name="grade[]" value="{{ old('grade') }}" autofocus>
                 @if ($errors->has('grade'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('grade') }}</strong>
                    </span>
                @endif
                  </div>
                     <label for="name" class="col-sm-2 control-label">{{ __('Hobbies:') }}<span class="label_red"> *</span></label>
                  <div class="col-sm-3">
                 	<input id="hobby" type="text" class="form-control{{ $errors->has('hobby') ? ' is-invalid' : '' }}" placeholder='Degree Name' name="hobby[]" value="{{ old('hobby') }}" autofocus>
                 @if ($errors->has('hobby'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('hobby') }}</strong>
                    </span>
                @endif
                  </div>
                </div>
                <div class="form-group">
                  
                     <label for="name" class="col-sm-2 control-label">{{ __('Description:') }}</label>
                  <div class="col-sm-3">
					<textarea name="edu_desc[]" id="edu_desc" style="width: 100%" cols="10" ></textarea>
                 @if ($errors->has('edu_desc'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('edu_desc') }}</strong>
                    </span>
                @endif
                  </div>
                   <div class="col-sm-2">
                   </div>
                  <div class="col-sm-3">
					<input type="button" class="add-row pull-right" value="Add More">
                  </div>
                </div>
                <div class="new_row" >
                  
                  
                </div>
                 </fieldset>
                 
                @endif
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox"> I agree to the <a href="#">terms and conditions</a>
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      {!! Form::submit('Submit', ['class'=>'btn btn-danger']) !!}
                    </div>
                  </div>
                  <input type="hidden" value="@if($data['edu']->count()){!! $data['edu']->count() !!}@else{!! '1' !!}@endif" name="row_count" id="row_count" />
                  <input type="hidden" value="edu" name="type" id="types" />
                       {!! Form::close() !!}
			</div>
              
              
              <div class="active tab-pane" id="settings">
                {!! Form::open(['route' => array('lawyers.update', $data['data_collection']->_id ), 'method' => 'post', 'class'=>'form-horizontal','files'=>'true']) !!}
              			{{ csrf_field() }}
			
      			@php 
    				$readonly = "readonly";
    				$disable = 'disabled';
				@endphp 			
				@if(Auth::guard('admin')->check())
				 @php
      				$readonly = "";
      				$disable = '';
      				@endphp
      			@endif
      			@if(Auth::guard('admin')->check()) 
                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Profile Picture</label>

                    <div class="col-sm-10">
                      {!! Form::file('profile_img');!!}
                    </div>
                  </div>
                  @endif
                  <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">{{ __('NIC:') }}<span class="label_red"> *</span></label>
                	<div class="col-sm-3">
                     {!! FORM::text('nic' ,$data['data_collection']->nic,['class'=>'form-control', 'placeholder'=>'NIC', $readonly]) !!}
                        @if ($errors->has('nic'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('nic') }}</strong>
                            </span>
                        @endif
                  	</div>
                    <label for="name" class="col-sm-2 control-label">{{ __('NIC Image:') }}</label>
                	<div class="col-sm-3">
                	@if(Auth::guard('admin')->check()) 
                     {!! Form::file('nic_img');!!}
                     @endif
                      @if ($errors->has('nic_img'))
                          <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('nic_img') }}</strong>
                            </span>
                      @endif
                    @if($data['data_collection']->nic_ext != "") 
                  		<a  href="{!! route('show.pdf', [encrypt('/storage/lawyers/nic_'.$data['data_collection']->_id.'.'.$data['data_collection']->nic_ext)]) !!}" target="_blank" >View File</a>
                  	@endif
                  	</div>
                </div>
                  <div class="form-group">
                   <label for="name" class="col-sm-2 control-label">{{ __('First Name:') }}<span class="label_red"> *</span></label>
                    <div class="col-sm-3">
                 {!! FORM::text('name' ,$data['data_collection']->name,['class'=>'form-control', 'placeholder'=>'Lawyer First Name', $readonly]) !!}
                 @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                        @endif
                  </div>
                  <label for="name" class="col-sm-2 control-label">{{ __('Last Name:') }}<span class="label_red"> *</span></label>
                  <div class="col-sm-3">
                 {!! FORM::text('lastname' ,$data['data_collection']->lastname,['class'=>'form-control', 'placeholder'=>'Lawyer Last Name', $readonly]) !!}
                  @if ($errors->has('lastname'))
                          <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('lastname') }}</strong>
                    </span>
                      @endif
                  </div>
                </div>
                 <div class="form-group">
                	<label for="name" class="col-sm-2 control-label">{{ __('Serial Number:') }}<span class="label_red"> *</span></label>
                	<div class="col-sm-3">
                 {!! FORM::text('serial_no' ,$data['data_collection']->serial_no,['class'=>'form-control', 'placeholder'=>'Lawyer Serial Number', 'readonly']) !!}
                 @if ($errors->has('serial_no'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('serial_no') }}</strong>
                            </span>
                        @endif
                  </div>
                  @if(Auth::guard('admin')->check()) 
                  {!! FORM::label('name' ,'Public Serial Number',['class'=>'col-sm-2 control-label']) !!}
                	<div class="col-sm-3">
                	<?php if ($readonly != ""){
                		$data['data_collection']->public_serial_no = '***************';
                	}?>
                
                 {!! FORM::text('public_serial_no' ,$data['data_collection']->public_serial_no,['class'=>'form-control', 'placeholder'=>'Public Serial Number','readonly']) !!}
                  </div>
                  @endif
                </div>
                <div class="form-group">
                	<label for="name" class="col-sm-2 control-label">{{ __('Password:') }}<span class="label_red"> *</span></label>
                	<div class="col-sm-3">
                <input type="password" class="form-control" name="password" id="inputPassword" placeholder="Password">
                @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                  </div>
                  <label for="inputEmail" class="col-sm-2 control-label">Confirm Password</label>
                	<div class="col-sm-3">
                 <input type="password" class="form-control" name="confirm_password" id="inputcPassword" placeholder="Confirm Password">
                   @if ($errors->has('confirm_password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('confirm_password') }}</strong>
                            </span>
                        @endif
                  </div>
                </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-2 control-label">Email</label>

                    <div class="col-sm-3">
                      <input type="email" class="form-control" name="email" id="inputEmail" placeholder="Email" value="{!! $data['data_collection']->email!!}">
                    	@if ($errors->has('email'))
                           <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                           </span>
                       @endif
                    </div>
                    <label for="inputEmail" class="col-sm-2 control-label">Location</label>

                    <div class="col-sm-3">
                      {!! Form::select('location', $data['loc'], $data['data_collection']->location_id,['class'=>'form-control', 'placeholder'=>'Select Location', $disable] ) !!}
                    </div>
                  </div>
                   <div class="form-group">
                	{!! FORM::label('name' ,'Bar License',['class'=>'col-sm-2 control-label']) !!}
                	<div class="col-sm-3">
                     {!! FORM::text('bar_license_number' ,$data['data_collection']->bar_license_number,['class'=>'form-control', 'placeholder'=>'Lawyer Bar License', $readonly]) !!}
                    </div>
                      {!! FORM::label('name' ,'High Court License',['class'=>'col-sm-2 control-label']) !!}
                	<div class="col-sm-3">
                     {!! FORM::text('high_court_license_number' ,$data['data_collection']->high_court_license_number,['class'=>'form-control', 'placeholder'=>'Lawyer High Court License' , $readonly]) !!}
                  	</div>
                </div>
                 <div class="form-group">
                     <div class="col-sm-2"></div>
                  <div class="col-sm-3">
                  @if(Auth::guard('admin')->check()) 
                 	{!! Form::file('bar_license_number_image');!!}
             	  @endif
                  @if($data['data_collection']->bar_license_file != "") 
                  		<a  href="{!! route('show.pdf', [encrypt('/storage/lawyers/bar_license_'.$data['data_collection']->_id.'.'.$data['data_collection']->bar_license_file)]) !!}" target="_blank" >View File</a>
                  	@endif
                  </div>
                  <div class="col-sm-2"></div>
                    <div class="col-sm-3">
                    @if(Auth::guard('admin')->check()) 
                 		{!! Form::file('high_court_license_image');!!}
                 	@endif
                  @if($data['data_collection']->high_court_file != "") 
                  		<a  href="{!! route('show.pdf', [encrypt('/storage/lawyers/high_court_'.$data['data_collection']->_id.'.'.$data['data_collection']->high_court_file)]) !!}" target="_blank" >View File</a>
                  	@endif
               
                  </div>
               
                 </div><div class="form-group">
                 {!! FORM::label('name' ,'Supreme Court License',['class'=>'col-sm-2 control-label']) !!}
                	<div class="col-sm-3">
                     {!! FORM::text('sup_court_license_number' ,$data['data_collection']->sup_court_license_number,['class'=>'form-control', 'placeholder'=>'Lawyer Supreme Court License', $readonly]) !!}
                  	</div>
                  {!! FORM::label('name' ,'Address',['class'=>'col-sm-2 control-label']) !!}
                   <div class="col-sm-3">
                   {!! Form::textarea('inputAddress', $data['data_collection']->address, ['class'=>'form-control', $readonly,  'rows' => 2]) !!}
                  </div>
                 
                </div>
                <div class="form-group">
                 <div class="col-sm-2"></div>
                    <div class="col-sm-3">
                    @if(Auth::guard('admin')->check()) 
                 {!! Form::file('sup_court_license_image');!!}
                 @endif
                   @if($data['data_collection']->supreme_court_file != "") 
                  		<a  href="{!! route('show.pdf', [encrypt('/storage/lawyers/supreme_court_'.$data['data_collection']->_id.'.'.$data['data_collection']->supreme_court_file)]) !!}" target="_blank" >View File</a>
                  	@endif
               
                  </div>
               <div class="col-sm-2">
                 </div>
                  <div class="col-sm-3">
                 
                  </div>
                 </div>
                <div class="form-group">
                 {!! FORM::label('name' ,'Mobile',['class'=>'col-sm-2 control-label']) !!}
                  <div class="col-sm-3">
                 {!! FORM::text('mobile' ,$data['data_collection']->mobile,['class'=>'form-control', 'placeholder'=>'Lawyer Mobile', $readonly]) !!}
                  </div>
                  {!! FORM::label('name' ,'Designation',['class'=>'col-sm-2 control-label']) !!}
                   <div class="col-sm-3">
                    {!! Form::select('designation', $designation,$data['data_collection']->designation,['class'=>'form-control', 'placeholder'=>'Lawyer(s) Designation', $disable] ) !!}
                  </div>
                </div>
       
              <div class="form-group">
              {!! FORM::label('name' ,'Area of Service',['class'=>'col-sm-2 control-label']) !!}
                   <div class="col-sm-3">
                        {!! Form::select('area_of_service', $aos_sel, $data['data_collection']->area_of_service,['class'=>'form-control', 'placeholder'=>'Area of service', $disable]) !!}
                  </div>
                  {!! FORM::label('name' ,'Lawyers Role',['class'=>'col-sm-2 control-label']) !!}
                   <div class="col-sm-3">
                    {!! Form::select('role', $role,$data['data_collection']->role,['class'=>'form-control', 'placeholder'=>'Lawyer(s) Role', $disable] ) !!}
                  </div>
                </div>
                 @if(Auth::guard('admin')->check()) 
                  <div class="form-group">
                    <label for="inputExperience"  class="col-sm-2 control-label">Experience</label>

                    <div class="col-sm-3">
                     {!! Form::textarea('experiance', $data['data_collection']->experiance, ['class'=>'form-control', $readonly,  'rows' => 2]) !!}
                   
                    </div>
			
                    <label for="inputSkills" class="col-sm-2 control-label">Notes</label>
			        <div class="col-sm-3">
                      {!! Form::textarea('notes', $data['data_collection']->notes, ['class'=>'form-control', $readonly,  'rows' => 2]) !!}
                    </div>
                  </div>
                    @endif
                   
                  <div class="form-group" style="display:none">
                    <div class="col-sm-offset-2 col-sm-10">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox"> I agree to the <a href="#">terms and conditions</a>
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      {!! Form::submit('Submit', ['class'=>'btn btn-danger']) !!}
                    </div>
                  </div>
                  
                  <input type="hidden" value="info" name="type" id="type" />
                       {!! Form::close() !!}
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<script type="text/javascript">
$(document).ready(function(){
    $(".add-row").click(function(){
        var row_count = $('#row_count').val();
        var markup = '<div class="row_'+row_count+'"><fieldset>'+
            			'<legend>Educational Detail:</legend>'+
        '<div class="form-group">'+
        '<label for="name" class="col-sm-2 control-label">School/Collage:</label>'+
        '<div class="col-sm-3">'+
       	'<input id="collage" type="text" class="form-control" placeholder="Institute Name" name="collage[]" autofocus>'+
        '</div>'+
         '  <label for="name" class="col-sm-2 control-label">Degree:</label>'+
        '<div class="col-sm-3">'+
       	'<input id="degree" type="text" class="form-control" placeholder="Degree Name" name="degree[]" autofocus>'+
       ' </div>'+
      '</div>'+
      	'<div class="form-group">'+
        '<label for="name" class="col-sm-2 control-label">From Year:</label>'+
        '<div class="col-sm-3">'+
       	'<select name="from_year[]" class="form-control">'+
       		'<option value="0">Select From Year</option>'+
       		<?php for($i=date('Y'); $i >=1900 ; $i-- ){?>
       			'<option value="<?php echo $i;?>"><?php echo $i;?></option>'+
       		<?php } ?>
       	'</select>'+
        '</div>'+
           '<label for="name" class="col-sm-2 control-label">To Year:</label>'+
        	'<div class="col-sm-3">'+
			'<select name="to_year[]" class="form-control">'+
       		'<option value="0">Select To Year</option>'+
       		<?php for($i=date('Y')+7; $i >=1900 ; $i-- ){?>
       			'<option value="<?php echo $i;?>"><?php echo $i;?></option>'+
       		<?php } ?>
       	'</select>'+
        '</div>'+
      '</div>'+
      '<div class="form-group">'+
      '<label for="name" class="col-sm-2 control-label">Grade:</label>'+
      '<div class="col-sm-3">'+
     	'<input id="grade" type="text" class="form-control" placeholder="Grades" name="grade[]" autofocus>'+
      '</div>'+
        '<label for="name" class="col-sm-2 control-label">Hobbies:</label>'+
      '<div class="col-sm-3">'+
     	'<input id="hobby" type="text" class="form-control" placeholder="Degree Name" name="hobby[]"  autofocus>'+
     	'</div>'+
   		'</div>'+
   		'<div class="form-group">'+
        '<label for="name" class="col-sm-2 control-label">Description:</label>'+
     '<div class="col-sm-3">'+
		'<textarea name="edu_desc[]" id="edu_desc" style="width: 100%" cols="10" ></textarea>'+
'     </div>'+
  '    <div class="col-sm-2">'+
    '  </div>'+
   '  <div class="col-sm-3">'+
		'<input type="button" class="remove-row pull-right" value="Remove" onClick="remove_row('+row_count+')">'
    ' </div>'+
  ' </div>'+
	          '</fieldset><div>';
        $(".new_row").append(markup);
        $('#row_count').val(parseInt(row_count)+1);
        $(".selections").select2();
    });
});

function remove_row(row_id){
	$('.row_'+row_id).remove();
}
</script>
<script src="{{ asset('js/raphael.js') }}"></script>
<script src="{{ asset('js/Treant.js') }}"></script>
<script>

function show_tree(){
	
	new Treant( ALTERNATIVE );
}
var config = {
        container: "#OrganiseChart1",
        rootOrientation:  'WEST', // NORTH || EAST || WEST || SOUTH
        scrollbar: "fancy",
        // levelSeparation: 30,
        siblingSeparation:   20,
        subTeeSeparation:    60,
        
        connectors: {
            type: 'step'
        },
        node: {
            HTMLclass: 'nodeExample1'
        }
    },
    
    root_node = {
        text: {
            name: "YLC",
        },
        HTMLclass : 'dropdown-toggle',
        HTMLid: "ceo",
    	innerHTML : '<a href="#" onclick="show_div(\'0\')">YLC</a><div id="0" class="dropdown-content">'+
          '</div>'
    },
    <?php if(count($aos)> 0 ){
        foreach($aos as $service) { ?>
    node_<?php echo $service->_id;?> = {
		parent: <?php if($service->parent_id == '0'){ echo "root_node";}else{echo "node_".$service->parent_id;};?>,
            text: {
                name: "<?php echo $service->name;?>",
            },
            HTMLclass : 'dropdown-toggle <?php echo $service->_id;?>',
            HTMLid: "node_<?php echo $service->_id;?>",
            innerHTML : '<a href="javascript:void(0)" onclick="show_div(\'<?php echo $service->_id;?>\')"><?php echo $service->name;?></a><div id="<?php echo $service->_id;?>" class="dropdown-content">'+
            ' <?php echo link_to_route("aos.content.lawyer", "View Note(s)",[$service->lawyer_id, 'personal', $service->_id], array('target'=>'_blank')) ?>'+ 
            '</div>'
        },
        <?php }
        } ?> 
    

    ALTERNATIVE = [
        config,
        root_node,
        ];
    <?php if(count($aos)> 0){
        foreach($aos as $service) { ?>
        	ALTERNATIVE.push(node_<?php echo $service->_id;?>);	
        <?php }
    } ?>




var  old_div = "";
function show_div(class_name){
	console.log(class_name);
	document.getElementById(class_name).classList.toggle("show");	

	if(old_div != ""){
		document.getElementById(old_div).classList.toggle("show");
		
		}
	old_div = class_name;
}

$(function() {
    $('#assignee_table').DataTable({
        destroy: true,
        processing: true,
        serverSide: true,
        "order": [[ 3, "desc" ]],
        "pageLength": 25,
        ajax: '/getDataTables/'+'lawyer'+'&'+'{!! $data["data_collection"]->_id !!}'+'&&',
        columns: [
            { data: 'task_name', name: 'task_name' },
            { data: 'assignee_name', name: 'assignee_name' },
            { data: 'task_description', name: 'task_description' },
            { data: 'assigner_end_date', name: 'assigner_end_date' },
            { data: 'created_at', name: 'created_at' },
            { data: 'priority', name: 'priority' }
        ]
    });
});
</script>
@endsection 
