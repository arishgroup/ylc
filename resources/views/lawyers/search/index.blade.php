@extends('layouts.lawyer')


@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>{{ __('Content Library') }}</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Lawyer(s)</a></li>
        <li class="active">View</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box">
            <div class="box-header" style="float: right">
              <a href="{{route('lawyers.datalib')}}"><button id="new_booktype" class="btn btn-primary">Add Content</button></a>
            </div>
            <div>
             @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12" id="searchDiv" style="display: block;">
                            <div class="search_main_div">
                                <div class="">
                                    <label class="search_label" id="searchLabel">Case Law Search</label>
                                    <a href="javascript:void(0)" class="glyphicon glyphicon-question-sign search_help" data-placement="left" rel="tooltip" title="" data-original-title="Use quotation marks to get an exact match of your search query, for example, if looking for case law with the phrase &quot;leave to defend&quot;, write the phrase &quot;leave to defend&quot; enclosed in quotation marks &quot;&quot;. Entering &quot;Court Name&quot; is optional. Use &quot;Advance Search&quot; for more precise results."></a>
                                </div>
                                <div class="input-group quad-input col-md-12 col-lg-12 col-sm-12 col-xs-12">

                                    <input type="text" id="bookSearch" placeholder="Enter Keyword" class="form-control" style="width: 47%;">
                                    <input type="text" id="yearSearch" placeholder="Enter Year YYYY" value="2018" class="form-control" maxlength="4" style="display: none; width: 33%;">
                                    <input type="text" id="courtSearch" placeholder="Enter Court" class="form-control" style="width: 33%;">
                                    <input type="text" id="codeOrPageSearch" placeholder="Enter Code or Page#" class="form-control" style="display: none; width: 33%;">
                                    <select id="caseLawYear" class="form-control" style="width: 20%;">
                                        <option value="5">Last 5 Year</option>
                                        <option value="10">Last 10 Year</option>
                                        <option value="15">Last 15 Year</option>
                                        <option value="20">Last 20 Year</option>
                                        <option value="200">All</option>
                                    </select>

                                    <span class="input-group-btn" id="btn_hide_on_Citation">
                                        <button class="btn btn-success caseLaw" id="" type="button">Search</button>
                                    </span>

                                </div>
                                <div class="input-group  col-md-12 col-lg-12 col-sm-12 col-xs-12" id="Citation_Advance_Search_div" style="display: none">
                                    <input type="text" id="judgeSearch" placeholder="Enter judge name" class="form-control advanced_search_inputs">
                                    <input type="text" id="lawyerSearch" placeholder="Enter lawyer name" class="form-control advanced_search_inputs">
                                    <input type="text" id="partySearch" placeholder="Enter Appellant/Opponent name" class="form-control advanced_search_inputs">
                                    <span class="input-group-btn">
                                        <button class="btn btn-success caseLaw" id="" type="button">Search</button>
                                    </span>
                                </div>
                                <div class="row" id="caseLAwErrorNotifier" style="display:none">
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <span class="text-center text-danger" style="display:block;font-weight:bold;padding-top:10px;">Invalid search, please refine your search</span>
                                    </div>

                                </div>
                                <div class="row" id="courtwiseYearErrorNotifier" style="display:none">
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <span class="text-center text-danger" style="display:block;font-weight:bold;padding-top:10px;">Invalid search, please enter valid year</span>
                                    </div>

                                </div>
                            </div>
                            

                            <div class="citationSearchDiv">
                                <div class="citation_search_main_content_div">
                                    

                                    <div class="col-lg-12 col-sm-12 col-xs-12 no-padding" id="index_search_content">
                                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no_padding">
                                            <label class="advance_search_label"> Index Search </label>
                                            <a href="javascript:void(0)" class="glyphicon glyphicon-question-sign search_help" data-placement="left" rel="tooltip" title="" data-original-title="Use quotation marks to get an exact match of your search query, for example, if looking for case law with the phrase &quot;leave to defend&quot;, write the phrase &quot;leave to defend&quot; enclosed in quotation marks &quot;&quot;. Entering &quot;Court Name&quot; is optional. Use &quot;Advance Search&quot; for more precise results."></a>
                                        </div>
                                        <div class="col-md-11 col-lg-11 col-sm-11 col-xs-12 no_padding">
                                            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 no_padding">
                                                <select id="Index_Category_Search_dropdown" class="form-control dropdown_css xs_input_margin">
                                                    <option value="CLC">CLC</option>
                                                    <option value="CLCN">CLCN</option>
                                                    <option value="CLD">CLD</option>
                                                    <option value="GBLR">GBLR</option>
                                                    <option value="MLD">MLD</option>
                                                    <option value="PLD">PLD</option>
                                                    <option value="PCRLJ">PCRLJ</option>
                                                    <option value="PCRLJN">PCRLJN</option>
                                                    <option value="PLC">PLC</option>
                                                    <option value="PLC(CS)">PLC(CS)</option>
                                                    <option value="PLC(CS)N">PLCN</option>
													<option value="PTD">PTD</option>
                                                    <option value="SCMR">SCMR</option>
                                                    <option value="YLR">YLR</option>
                                                    <option value="YLRN">YLRN</option>

                                                </select>
                                            </div>
                                            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-6 no_padding">
                                                <input type="text" id="Index_Year_Search_input" placeholder="Enter Year (required)" value="2018" class="form-control xs_input_margin" maxlength="4">
                                            </div>
                                            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-6 no_padding">
                                                <input type="text" id="Index_Court_Search_input" placeholder="Enter Court" class="form-control xs_input_margin">
                                            </div>
                                        </div>

                                        <div class="col-md-1 col-lg-1 col-sm-1 col-xs-12 no_padding xs_center_align_col">
                                            <span class="input-group-btn xs_input_group">
                                                <button class="btn btn-success Index_Search_btn" id="" type="button">Search</button>
                                            </span>
                                        </div>
                                        <div class="row" id="indexYearErrorNotifier" style="display:none">
                                            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                                <span class="text-center text-danger" style="display:block;font-weight:bold;padding-top:10px;">Invalid search, please enter valid year</span>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-padding" id="citation_search_content">
                                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no_padding">
                                            <label class="advance_search_label"> Citation Search </label>
                                            
                                        </div>
                                        <div class="col-md-11 col-lg-11 col-sm-11 col-xs-12 no_padding">
                                            <div class="col-md-3 col-lg-3 col-sm-3 col-xs-6 no_padding">
                                                <select id="Citation_Category_Search_dropdown" class="form-control dropdown_css xs_input_margin">
                                                    <option value="CLC">CLC</option>
                                                    <option value="CLCN">CLCN</option>
                                                    <option value="CLD">CLD</option>
                                                    <option value="GBLR">GBLR</option>
                                                    <option value="MLD">MLD</option>
                                                    <option value="PLD">PLD</option>
                                                    <option value="PCRLJ">PCRLJ</option>
                                                    <option value="PCRLJN">PCRLJN</option>
                                                    <option value="PLC">PLC</option>
                                                    <option value="PLC(CS)">PLC(CS)</option>
                                                   	<option value="PLC(CS)N">PLCN</option> 
													<option value="PTD">PTD</option>
                                                    <option value="SCMR">SCMR</option>
                                                    <option value="YLR">YLR</option>
                                                    <option value="YLRN">YLRN</option>
												

                                                </select>
                                            </div>
                                            <div class="col-md-3 col-lg-3 col-sm-3 col-xs-6 no_padding">
                                                <input type="text" id="Citation_Year_Search_input" placeholder="Enter Year (required)" value="2018" class="form-control xs_input_margin" maxlength="4">
                                            </div>
                                            <div class="col-md-3 col-lg-3 col-sm-3 col-xs-6 no_padding">
                                                <input type="text" id="Citation_Court_Search_input" placeholder="Enter Court" class="form-control xs_input_margin">
                                            </div>
                                            <div class="col-md-3 col-lg-3 col-sm-3 col-xs-6 no_padding">
                                                <input type="text" id="Citation_Code_Or_Page_Search_input" placeholder="Enter Code or Page#" class="form-control xs_input_margin">
                                            </div>

                                            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 no_padding">
                                                <input type="text" id="Citation_Judge_Search_input" placeholder="Enter judge name" class="form-control xs_input_margin">
                                            </div>
                                            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 no_padding">
                                                <input type="text" id="Citation_Lawyer_Search_input" placeholder="Enter lawyer name" class="form-control xs_input_margin">
                                            </div>
                                            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 no_padding">
                                                <input type="text" id="Citation_Party_Search_input" placeholder="Enter Appellant/Opponent name" class="form-control xs_input_margin">
                                            </div>
                                        </div>

                                        <div class="col-md-1 col-lg-1 col-sm-1 col-xs-12 no_padding xs_center_align_col">
                                            <span class="input-group-btn citation_search_btn_top_margin xs_input_group">
                                                <button class="btn btn-success Citation_Search_btn" id="" type="button">Search</button>
                                            </span>
                                        </div>
                                        <div class="row" id="citationYearErrorNotifier" style="display:none">
                                            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                                <span class="text-center text-danger" style="display:block;font-weight:bold;padding-top:10px;">Invalid search, please enter valid year</span>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="advanceSearchDiv">
                                <div class="advance_search_main_content_div">
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no_padding">
                                        <label class="advance_search_label"> Advance Search </label>
                                        <a href="javascript:void(0)" class="glyphicon glyphicon-question-sign search_help" data-placement="left" rel="tooltip" title="" data-original-title="Use quotation marks to get an exact match of your search query, for example, if looking for case law with the phrase &quot;leave to defend&quot;, write the phrase &quot;leave to defend&quot; enclosed in quotation marks &quot;&quot;. Entering &quot;Court Name&quot; is optional. Use &quot;Advance Search&quot; for more precise results."></a>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no_padding">
                                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-6 no_padding">
                                            <input type="text" id="Advance_Court_Name_Search_input" placeholder="Enter Court Name" class="form-control xs_input_margin">
                                        </div>
                                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-6 no_padding">
                                            <input type="text" id="Advance_Judge_Search_input" placeholder="Enter Judge Name" class="form-control xs_input_margin">
                                        </div>
                                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-6 no_padding">
                                            <input type="text" id="Advance_Lawyer_Search_input" placeholder="Enter Lawyer Name" class="form-control xs_input_margin">
                                        </div>
                                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-6 no_padding">
                                            <input type="text" id="Advance_Party_Search_input" placeholder="Enter Appellant/Opponent" class="form-control xs_input_margin">
                                        </div>
                                        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 no_padding">
                                            <input type="text" id="Advance_Keyword_Search_input" placeholder="Enter Keyword" class="form-control xs_input_margin">
                                        </div>
                                        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 no_padding">
                                            <input type="text" id="Advance_Rule_Search_input" placeholder="Enter Rule" class="form-control xs_input_margin">
                                        </div>
                                        
                                        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 no_padding">
                                            <input type="text" id="Advance_Act_Search_input" placeholder="Enter Act/Ordinance" class="form-control xs_input_margin">
                                        </div>
                                        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 no_padding">
                                            <input type="text" id="Advance_Section_Name_Search_input" placeholder="Enter Section" class="form-control xs_input_margin">
                                        </div>
                                        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 no_padding">
                                            <input type="text" id="Advance_Act_Two_Search_input" placeholder="Enter another Act/Ordinance" class="form-control xs_input_margin">
                                        </div>
                                        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 no_padding">
                                            <input type="text" id="Advance_Section_Name_Two_Search_input" placeholder="Enter another Section" class="form-control xs_input_margin">
                                        </div>


                                    </div>

                                    <div class="row" id="advanceErrorNotifier" style="display:none">
                                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                            <span class="text-center text-danger" style="display:block;font-weight:bold;padding-top:10px;">Invalid search, please refine your search</span>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                            <span class="advancedSearchNoteSpan">* Please enter at least one-search criteria, and you may add as many for more precise results.</span>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no_padding align_center">
                                        <span class="input-group-btn Advanced_Search_Btn_Input_Group">
                                            <button class="btn btn-success Advance_Search_btn" id="" type="button">Search</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <br>
                        </div>
            
            </div>
        </div>
    </div>
    </div>
    </section>
    </div>
@endsection 