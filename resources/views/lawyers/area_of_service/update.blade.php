@extends(Auth::guard('admin')->check() ? 'layouts.admin' : 'layouts.lawyer')


@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>{{ __('Area of Service(s)') }}</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('aos.index')}}">Area of Service(s)</a></li>
        <li class="active">Update</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info col-md-6">
            <div class="box-header with-border">
              @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::open(['route' => array('personal.aos.update', $data['_id']), 'method' => 'put', 'class'=>'form-horizontal']) !!}
              {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group">
                  {!! FORM::label('name' ,'Name',['class'=>'col-sm-2 control-label']) !!}
                                   <div class="col-sm-5">
                 {!! FORM::text('name' ,$data->name,['class'=>'form-control', 'placeholder'=>'Area of service name']) !!}
                  </div>
                </div>
                <div class="form-group">
                  {!! FORM::label('name' ,'Description',['class'=>'col-sm-2 control-label']) !!}
                   <div class="col-sm-5">
                   {!! FORM::textarea('description' ,$data->description,['class'=>'form-control', 'rows' => 3, 'cols' => 40]) !!}
                  </div>
                </div>
               
                <div class="form-group">
                  {!! FORM::label('parent_id' ,'Parent Node',['class'=>'col-sm-2 control-label']) !!}
                   <div class="col-sm-5">
                   {!! Form::select('parent_id', $aos_sel, $data->parent_id) !!}
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
               
                 @if($type == 'personal')
                <a href="{{route('personal.aos.tree')}}"><button type="button" class="btn btn-default">Back</button></a>
              @else 
                <a href="{{route('aos.tree')}}"><button type="button" class="btn btn-default">Back</button></a>
              @endif
                {!! Form::submit('Submit', ['class'=>'btn btn-info pull-right']) !!}
              </div>
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <!-- /.box-footer -->
              {!! FORM::hidden('_id' ,$data->_id) !!}
              {!! FORM::hidden('parent_id_old' ,$data->parent_id) !!}
            {!! Form::close() !!}
          </div>
         
        </div>
        <!--/.col (right) -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection 
