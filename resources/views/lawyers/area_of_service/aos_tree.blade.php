@extends(Auth::guard('admin')->check() ? 'layouts.admin' : 'layouts.lawyer')

@section('content')
<style>
.dropbtn {
    background-color: #3498DB;
    color: white;
    padding: 16px;
    font-size: 16px;
    border: none;
    cursor: pointer;
}

.dropbtn:hover, .dropbtn:focus {
    background-color: #2980B9;
}

.dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f1f1f1;
    min-width: 160px;
    overflow: auto;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

.dropdown-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
}

.dropdown a:hover {background-color: #ddd;}

.show {display: block;}
</style>

<link rel="stylesheet" href="{{ asset('css/simple-scrollbar.css?123')}}">
<link rel="stylesheet" href="{{ asset('css/Treant.css')}}">
  	
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">	
      <h1>{{ __('Personal notes(s)') }}</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Area of Service</a></li>
        <li class="active">Tree View</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box">
            @if(Auth::guard('admin')->check())
            <div class="box-header" style="float: right">
              <a href="{{route('aos.index')}}">Table View</a> | <a href="{{route('aos.create')}}">Add New Area of Service</a>
            </div>
            @endif
            <div>
             <?php $i = 0 ;?>
             @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
            </div>
            <!-- /.box-header -->
            <div class="box-body">
       			<div class="chart" id="OrganiseChart1" style="min-height: 500px;"></div>


            </div>
           
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<script src="{{ asset('js/raphael.js') }}"></script>
<script src="{{ asset('js/Treant.js') }}"></script>
<script>


var config = {
        container: "#OrganiseChart1",
        rootOrientation:  'WEST', // NORTH || EAST || WEST || SOUTH
        scrollbar: "fancy",
        // levelSeparation: 30,
        siblingSeparation:   20,
        subTeeSeparation:    60,
        
        connectors: {
            type: 'step'
        },
        node: {
            HTMLclass: 'nodeExample1'
        }
    },
    
    root_node = {
        text: {
            name: "YLC",
        },
        HTMLclass : 'dropdown-toggle',
        HTMLid: "ceo",
    	innerHTML : '<a class="container-href" href="javascript:void(0)" onclick="show_div(\'0\')">YLC</a><div id="0" class="dropdown-content">'+
    	<?php if($tree_type == 'personal') {?>'<a href="create_personal">View/Add Child Node</a>' +<?php }?>
          '</div>'
    },
    <?php if(count($aos)> 0 ){
        foreach($aos as $service) { ?>
    node_<?php echo $service->_id;?> = {
		parent: <?php if($service->parent_id==0  || $service->_id== Auth::user()->area_of_service){ echo "root_node";}else{echo "node_".$service->parent_id;};?>,
            text: {
                name: "<?php echo $service->name;?>",
            },
            HTMLclass : 'dropdown-toggle <?php echo $service->_id;?>',
            HTMLid: "node_<?php echo $service->_id;?>",
            innerHTML : '<a class="container-href" href="javascript:void(0)" onclick="show_div(\'<?php echo $service->_id;?>\')"><?php echo $service->name;?></a><div id="<?php echo $service->_id;?>" class="dropdown-content">'+
         <?php if($tree_type == 'personal') {?>' <a href="create_personal/<?php echo $service->_id;?>">Add Child Node</a>'+
            		' <a href="destroy_personal/<?php echo $service->_id;?>" class="confirmation">Delete Node</a>'+
            		' <a href="edit_personal/<?php echo $service->_id;?>">Update Node</a>'+ 
					' <a href="ref_notes_index/<?php echo $service->_id;?>/personal">View/Add Notes</a>'+ <?php } else{ ?>
					' <a href="ref_notes_index/<?php echo $service->_id;?>/standard">View/Add Notes</a>'+ <?php }?>
                  '</div>'
        },
        <?php }
        } ?> 
    

    ALTERNATIVE = [
        config,
        root_node,
        ];
    <?php if(count($aos)> 0){
        foreach($aos as $service) { ?>
        	ALTERNATIVE.push(node_<?php echo $service->_id;?>);	
        <?php }
    } ?>


new Treant( ALTERNATIVE );
    
var  old_div = "";
function show_div(class_name){
	document.getElementById(class_name).classList.toggle("show");	

	if(old_div != ""){
		document.getElementById(old_div).classList.toggle("show");
		
		}
	old_div = class_name;
}


        </script> 
@endsection 
