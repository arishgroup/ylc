    
@extends(Auth::guard('admin')->check() ? 'layouts.admin' : 'layouts.lawyer')

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>{{ __('Book(s)') }}</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Book(s)</a></li>
        <li class="active">Listing</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">


          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
            <div>
                <div><b>Book Title: </b>{!! $parent->name !!}</div>
            </div>
            <div style="float:right"><input id="myInput" type="text" placeholder="Search.."></div>
              <table  class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Sr.</th>
                  <th style='width:75%'>Name</th>
                  <th>Updated Date</th>
                  <th>Action</th>
                  
                </tr>
                </thead>
                <tbody id="tableGrid">
                <?php $i=0;?>
              @if($book->count())
               	@foreach($book as $val)
               	<tr>
                  <td>{!! $i = $i+1 !!}</td>
                  <td><a target='_blank' href="{!! route('show.pdf', [encrypt('/storage/books/'.$val->getBookId->_id.'/book_'.$val->_id.'.'.$val->ext)]) !!}">{!! $val->file_name !!}</a></td>
                  <td>{!! $val->updated_at !!}</td>
                  <td><a target='_blank' href="{!! route('show.pdf', [encrypt('/storage/books/'.$val->getBookId->_id.'/book_'.$val->_id.'.'.$val->ext)]) !!}">View</a></td>
                </tr>
                 @endforeach
                @else
                <tr id="no_record">
                  <td colspan="6">No Record Found!</td>
                </tr>
                @endif   
			  </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection 
