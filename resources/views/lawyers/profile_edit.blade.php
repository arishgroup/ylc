@extends('layouts.backendUser')


@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>{{ __('Lawyer(s) Profile') }}</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('aos.index')}}">Lawyer(s)</a></li>
        <li class="active">Profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info col-md-6">
            <div class="box-header with-border">
              @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::open(['route' => array('lawyers.profile.update',$data['lawyer']->_id), 'method' => 'post', 'class'=>'form-horizontal', 'files'=>'true']) !!}
              {{ csrf_field() }}
              <div class="box-body">
              <fieldset>
                  <legend>Account Information:</legend>
                  <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">{{ __('Profile Image:') }}</label>
                	<div class="col-sm-3">
                     {!! Form::file('profile_img');!!}
                         @if($data['lawyer']->profile_img_ext != "" AND $data['lawyer']->profile_img_ext == 'pdf') 
                      <a  href="{!! route('show.pdf', [encrypt('/storage/lawyers_profile/profile_'.$data['lawyer']->_id.'.'.$data['lawyer']->profile_img_ext )]) !!}" target="_blank" >View File</a>
                      @else
                      	<a  href="{!! asset('storage/lawyers_profile/profile_'.$data['lawyer']->_id.'.'.$data['lawyer']->profile_img_ext)  !!}" target="_blank" >View File</a>
                      @endif
                      @if ($errors->has('profile_img'))
                          <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('profile_img') }}</strong>
                            </span>
                      @endif
                  	</div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">{{ __('First Name:') }}<span class="label_red"> *</span></label>
                    <div class="col-sm-3">
                 {!! FORM::text('first_name' ,$data['lawyer']->first_name,['class'=>'form-control', 'placeholder'=>'Lawyer First Name']) !!}
                        @if ($errors->has('first_name'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('first_name') }}</strong>
                    </span>
                        @endif
                  </div>
                    <label for="name" class="col-sm-2 control-label">{{ __('Last Name:') }}<span class="label_red"> *</span></label>
                  <div class="col-sm-3">
                 {!! FORM::text('last_name' ,$data['lawyer']->last_name,['class'=>'form-control', 'placeholder'=>'Lawyer Last Name']) !!}
                      @if ($errors->has('last_name'))
                          <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('lastname') }}</strong>
                    </span>
                      @endif
                  </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">{{ __('NIC:') }}</label>
                	<div class="col-sm-3">
                     {!! FORM::text('nic' ,$data['lawyer']->nic,['class'=>'form-control', 'placeholder'=>'NIC']) !!}
                    </div>
                    <label for="name" class="col-sm-2 control-label">{{ __('NIC Image:') }}</label>
                	<div class="col-sm-3">
                     {!! Form::file('nic_img');!!}
                         @if($data['lawyer']->nic_ext != "" AND $data['lawyer']->nic_ext == 'pdf') 
                      <a  href="{!! route('show.pdf', [encrypt('/storage/lawyers_profile/nic_'.$data['lawyer']->_id.'.'.$data['lawyer']->nic_ext )]) !!}" target="_blank" >View File</a>
                      @else
                      	<a  href="{!! asset('storage/lawyers_profile/nic_'.$data['lawyer']->_id.'.'.$data['lawyer']->nic_ext)  !!}" target="_blank" >View File</a>
                      @endif
                      @if ($errors->has('nic_img'))
                          <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('nic_img') }}</strong>
                            </span>
                      @endif
                  	</div>
                </div>
                <div class="form-group">
                 {!! FORM::label('name' ,'Mobile',['class'=>'col-sm-2 control-label']) !!}
                  <div class="col-sm-3">
                 {!! FORM::text('mobile' ,$data['lawyer']->mobile,['class'=>'form-control', 'placeholder'=>'Lawyer Mobile']) !!}
                  </div>
                  <label for="name" class="col-sm-2 control-label">{{ __('Email:') }}<span class="label_red"> *</span></label>
                   <div class="col-sm-3">
                   {!! FORM::text('email' ,$data['lawyer']->email,['class'=>'form-control', 'placeholder'=>'Lawyer Email']) !!}
                       @if ($errors->has('email'))
                           <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                           </span>
                       @endif
                  </div>
                </div>
       
              <div class="form-group">
              {!! FORM::label('name' ,'Area of Service',['class'=>'col-sm-2 control-label']) !!}
                   <div class="col-sm-3">
                    
                    {!! Form::select('aos_id', $aos_sel, $data['lawyer']->aos_id,['class'=>'form-control', 'placeholder'=>'Area of service']) !!}
                  </div>
                 {!! FORM::label('name' ,'Location',['class'=>'col-sm-2 control-label']) !!}
                   <div class="col-sm-3">
                    
                    {!! Form::select('location', $data['loc'], $data['lawyer']->location,['class'=>'form-control', 'placeholder'=>'Location']) !!}
                  </div>
                </div>
                 <div class="form-group">
            	 {!! FORM::label('name' ,'Office Address',['class'=>'col-sm-2 control-label']) !!}
                   <div class="col-sm-3">
                   <textarea class="form-control" id="office_address" name="office_address" placeholder="Address">{!! $data['lawyer']->office_address !!}</textarea>
                  </div>
                  {!! FORM::label('name' ,'Home Address',['class'=>'col-sm-2 control-label']) !!}
                   <div class="col-sm-3">
                   <textarea class="form-control" id="home_address" name="home_address" placeholder="Address">{!! $data['lawyer']->home_address !!}</textarea>
                  </div>
                </div>
                <div class="form-group">
                    <label for="inputExperience"  class="col-sm-2 control-label">Experience</label>

                    <div class="col-sm-3">
                      <textarea class="form-control" name="experiance" id="experiance" placeholder="Experience">{!! $data['lawyer']->experiance !!}</textarea>
                    </div>
                    <label for="inputSkills" class="col-sm-2 control-label">Notes</label>

                    <div class="col-sm-3">
                      <textarea class="form-control" name="notes" id="notes" placeholder="Notes">{!! $data['lawyer']->notes !!}</textarea>
                    </div>
                  </div>
               </fieldset>
              @if($data['lawyer']->eduDetail)
              	<?php $cnt_n = 0; ?>
              	@foreach($data['lawyer']->eduDetail as $edu_detail)
			<div class="row_<?php echo $cnt_n;?>">
              <fieldset>
                  <legend>Educational Detail:</legend>
                  <div class="form-group">
              	<input type="hidden" name="edu_id[]" value="{!! $edu_detail->_id !!}" />
              	<input type="hidden" name="file_ext[]" value="{!! $edu_detail->degree_ext !!}" />
                  <label for="name" class="col-sm-2 control-label">{{ __('School/Collage:') }}</label>
                  <div class="col-sm-3">
                 	<input id="collage" type="text" class="form-control" placeholder='Institute Name' name="collage[]" value="{{ old('collage[]', $edu_detail->institute) }}" autofocus>
                  </div>
                  <label for="name" class="col-sm-2 control-label">{{ __('Grade:') }}</label>
                  <div class="col-sm-3">
                 	<input id="grade" type="text" class="form-control" placeholder='Grades' name="grade[]" value="{{ old('grade[]', $edu_detail->grade) }}" autofocus>
                  </div>   
                </div>
                <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">{{ __('From Year:') }}</label>
                  <div class="col-sm-3">
                 	<select name='from_year[]' class='form-control'>
                 		<option value='0'>Select From Year</option>
                 		<?php for($i=date('Y'); $i >=1900 ; $i-- ){
                 		    $selected = '';
                 		    if($i == $edu_detail->from_year){ $selected = 'selected="selected"';}
                 		    ?>
                 			<option value='<?php echo $i;?>'<?php echo $selected?>><?php echo $i;?></option>
                 		<?php } ?>
                 	</select>
                  </div>
                     <label for="name" class="col-sm-2 control-label">{{ __('To Year:') }}</label>
                  <div class="col-sm-3">
					<select name='to_year[]' class='form-control'>
                 		<option value='0'>Select To Year</option>
                 		<?php for($i=date('Y')+7; $i >=1900 ; $i-- ){
                 		     $selected = '';
                 		     if($i == $edu_detail->to_year){ $selected = 'selected="selected"';}
                 		    ?>
                 			
                 			<option value='<?php echo $i;?>'  <?php echo $selected;?>><?php echo $i;?></option>
                 		<?php } ?>
                 	</select>
                  </div>
                </div>
                    <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">{{ __('Degree:') }}</label>
                  <div class="col-sm-3">
                 	<input id="degree" type="text" class="form-control" placeholder='Degree Name' name="degree[]" value="{{ old('degree[]', $edu_detail->degree) }}" autofocus>
                  </div>
                  
                     <label for="name" class="col-sm-2 control-label">{{ __('Degree Image:') }}</label>
                  <div class="col-sm-3">
                 	{!! Form::file('degree_img[]'); !!}
                 	@if($edu_detail->degree_ext != "")
                 		@if($edu_detail->degree_ext == 'pdf') 
                      		<a  href="{!! route('show.pdf', [encrypt('/storage/lawyers_profile/degree/degree_'.$edu_detail->_id.'.'.$edu_detail->degree_ext )]) !!}" target="_blank" >View File</a>
                      	@else
                      		<a  href="{!! asset('storage/lawyers_profile/degree/degree_'.$edu_detail->_id.'.'.$edu_detail->degree_ext)  !!}" target="_blank" >View File</a>
                      	@endif
                    @endif  
                      
                  </div>
                </div>
                <div class="form-group">
                <label for="name" class="col-sm-2 control-label">{{ __('Hobbies:') }}</label>
                  <div class="col-sm-3">
                 	<input id="hobby" type="text" class="form-control" placeholder='Hobbies' name="hobby[]" value="{{ old('hobby[]',$edu_detail->hobbies) }}" autofocus>
                  </div>
                     <label for="name" class="col-sm-2 control-label">{{ __('Description:') }}</label>
                  <div class="col-sm-3">
					<textarea name="edu_desc[]" id="edu_desc" style="width: 100%" cols="10" >{{ old('edu_desc[]',$edu_detail->description) }}</textarea>
                  </div>
                  </div>
                  <div class="form-group">
                  <div class="col-sm-2">
                   </div>
                  	<div class="col-sm-3">
                  	</div>
                   <div class="col-sm-2">
                   </div>
                  	<div class="col-sm-3">
                  	<?php if($cnt_n == 0){ ?>
						<input type="button" class="add-row pull-right" value="Add More">
					<?php } else { ?>
						    <input type="button" class="remove-row pull-right" value="Remove" onClick="remove_row('<?php echo $cnt_n;?>')">
					<?php } $cnt_n = $cnt_n+1;?>
                  	</div>
                </div>
                
                 </fieldset>
                 </div>
                @endforeach
                <?php if($data['lawyer']->eduDetail->count() == ($cnt_n)) {?>
                <div class="new_row" >
                </div>
                <?php } ?>   
             @endif   
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="{{route('lawyers.index')}}"><button type="button" class="btn btn-default">Back</button></a>
                {!! Form::submit('Submit', ['class'=>'btn btn-info pull-right']) !!}
              </div>
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <!-- /.box-footer -->
            {!! Form::close() !!}
          </div>
         
        </div>
        <!--/.col (right) -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <input type="hidden" value="<?php echo $cnt_n;?>" name="row_count" id="row_count" />
<script>
$(document).ready(function(){
    $(".add-row").click(function(){
        var row_count = $('#row_count').val();
        var markup = '<div class="row_'+row_count+'"><fieldset>'+
            			'<legend>Educational Detail:</legend>'+
        '<div class="form-group">'+
        '<label for="name" class="col-sm-2 control-label">School/Collage:</label>'+
        '<div class="col-sm-3">'+
       	'<input id="collage" type="text" class="form-control" placeholder="Institute Name" name="collage[]" autofocus>'+
        '</div>'+
        '<label for="name" class="col-sm-2 control-label">Grade:</label>'+
        '<div class="col-sm-3">'+
       	'<input id="grade" type="text" class="form-control" placeholder="Grades" name="grade[]" autofocus>'+
        '</div>'+
      '</div>'+
      	'<div class="form-group">'+
        '<label for="name" class="col-sm-2 control-label">From Year:</label>'+
        '<div class="col-sm-3">'+
       	'<select name="from_year[]" class="form-control">'+
       		'<option value="0">Select From Year</option>'+
       		<?php for($i=date('Y'); $i >=1900 ; $i-- ){?>
       			'<option value="<?php echo $i;?>"><?php echo $i;?></option>'+
       		<?php } ?>
       	'</select>'+
        '</div>'+
           '<label for="name" class="col-sm-2 control-label">To Year:</label>'+
        	'<div class="col-sm-3">'+
			'<select name="to_year[]" class="form-control">'+
       		'<option value="0">Select To Year</option>'+
       		<?php for($i=date('Y')+7; $i >=1900 ; $i-- ){?>
       			'<option value="<?php echo $i;?>"><?php echo $i;?></option>'+
       		<?php } ?>
       	'</select>'+
        '</div>'+
      '</div>'+
      '<div class="form-group">'+
      '  <label for="name" class="col-sm-2 control-label">Degree:</label>'+
      '<div class="col-sm-3">'+
     	'<input id="degree" type="text" class="form-control" placeholder="Degree Name" name="degree[]" autofocus>'+
     ' </div>'+  
     '<label for="name" class="col-sm-2 control-label">Degree Image:</label>'+
     '<div class="col-sm-3">'+
    	'<input id="degree_img" type="file" class="form-control" placeholder="Degree Name" name="degree_img[]"  autofocus>'+
    	'</div>'+
   		'</div>'+
   		'<div class="form-group">'+
    	   '<label for="name" class="col-sm-2 control-label">Hobbies:</label>'+
           '<div class="col-sm-3">'+
          	'<input id="hobby" type="text" class="form-control" placeholder="Hobbies" name="hobby[]"  autofocus>'+
          	'</div>'+
        '<label for="name" class="col-sm-2 control-label">Description:</label>'+
     '<div class="col-sm-3">'+
		'<textarea name="edu_desc[]" id="edu_desc" style="width: 100%" cols="10" ></textarea>'+
'     </div>'+
'     </div>'+
'<div class="form-group">'+
'    <div class="col-sm-2">'+
'  </div>'+
'  <div class="col-sm-3">'+
'  </div>'+
  '    <div class="col-sm-2">'+
    '  </div>'+
   '  <div class="col-sm-3" style="text-align: right;">'+
		'<input type="button" class="remove-row" value="Remove" onClick="remove_row('+row_count+')">'
    ' </div>'+
  ' </div>'+
	          '</fieldset></div>';
        $(".new_row").append(markup);
        $('#row_count').val(parseInt(row_count)+1);
 
    });
});

function remove_row(row_id){
	$('.row_'+row_id).remove();
}
</script>
@endsection 
