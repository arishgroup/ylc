@extends(Auth::guard('admin')->check() ? 'layouts.admin' : 'layouts.backendUser')


@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>{{ __('Data Entry') }}</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Data Entry</a></li>
        <li class="active">Listing</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box">
            <div class="box-header" style="float: right">
              <a href="{{route('lawyers.datalib')}}"><button id="new_booktype" class="btn btn-primary">Add Content</button></a>
            </div>
            <div>
             @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Sr.</th>
                  <th>Title</th>
                  <th>Area of Service</th>
				  <th>Citation Name</th>	
                  <th>Publish Month</th>
                  <th>Publish Year</th>
                  <th>Tags</th>
                  <th>Action</th>
                  
                </tr>
                </thead>
                <tbody>
                <?php $i = 0 ;
                if($content_lib->count()){
                foreach($content_lib as $val){ ?>
                <tr>
                  <td><?php echo $i = $i+1 ;?></td>
                  <td><?php echo  $val->title; ?></td>
                  <td>{!! $val->area_of_service['name'] !!}</td>
                  <td><?php echo $val->citation_name; ?></td>
                  <td><?php echo $val->publish_month; ?></td>
                  <td><?php echo $val->publish_year; ?></td>
                  <td><?php foreach ($val->getTags as $tags){ ?>
                      <label class="label label-info"><?php echo $tags->title; ?></label>
                  <?php } ?></td>
                  <td><a href="{{ route('lawyers.datalib.edit', $val->_id) }}"><i class="fa fa-edit"></i></a> | <a class="confirmation" href="{{ route('lawyers.datalib.destroy', $val->_id) }}"><i class="fa fa-trash"></i></a></td>
                </tr>
                <?php }
                } else {?>
                
                <tr>
                  <td colspan="8">No Record Found!</td>
                </tr>
                <?php } ?>
                
                </tfoot>
              </table>
            </div>
            <div style="text-align:right; padding-right: 10px;">{{ $content_lib->links() }}</div>    
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection 
