@extends('layouts.backendUser')


@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>{{ __('Lawyer(s) Listing') }}</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Lawyer(s)</a></li>
        <li class="active">View</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box">
            <div class="box-header" style="float: right">
            
              <a href="{{route('lawyers.profile.create')}}"><button id="new_booktype" class="btn btn-primary">Add Lawyer Profile</button></a>
            </div>
            <div>
             @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
            </div>
            <!-- /.box-header -->
            <div class="box-body">
             <div style="float:right"><input id="myInput" type="text" placeholder="Search.."></div>
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Sr.</th>
                  <th>Name</th>
				  <th>Email</th>
				  <th>Area of Service</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody id="tableGrid">
                <?php $i = 0 ;?>
                @if($lawyers->count())
                @foreach($lawyers as $lawyer)
                <tr>
                  <td>{!! $i = $i+1 !!}</td>
                  <td>{!! $lawyer->first_name !!} {!! $lawyer->last_name !!}</td>
                  <td>{!! $lawyer->email !!}</td>
                  <td>@if($lawyer->aos_id){!! $lawyer->aos->name !!} @else {!! '-' !!}@endif</td>
                  <td><a href="{{ route('lawyers.profile.edit', [$lawyer->_id]) }}"><i class="fa fa-edit"></i></a> | <a class="confirmation" href="{{ route('lawyers.profile.destroy', $lawyer->_id) }}"><i class="fa fa-trash"></i></a></td>
                </tr>
                @endforeach
                @else
                <tr>
                  <td colspan="5">No Record Found!</td>
                </tr>
                @endif
                
                </tfoot>
              </table>
            </div>
            <div style="text-align:right; padding-right: 10px;">{{ $lawyers->links() }}</div>    
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection 
