@extends('layouts.admin')


@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>{{ __('Lawyer(s) Registration') }}</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('aos.index')}}">Lawyer(s)</a></li>
        <li class="active">Registration</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info col-md-6">
            <div class="box-header with-border">
              @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::open(['route' => array('lawyers.update', $data['data_collection']->_id), 'method' => 'post', 'class'=>'form-horizontal', 'files'=>'true']) !!}
              {{ csrf_field() }}
              <div class="box-body">
              <fieldset>
                  <legend>Account Information:</legend>
                <div class="form-group">
                  {!! FORM::label('name' ,'First Name',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-3">
                 {!! FORM::text('name' ,$data['data_collection']->name,['class'=>'form-control', 'placeholder'=>'Lawyer First Name']) !!}
                  </div>
                  {!! FORM::label('name' ,'Last Name',['class'=>'col-sm-2 control-label']) !!}
                  <div class="col-sm-3">
                 {!! FORM::text('lastname' ,$data['data_collection']->lastname,['class'=>'form-control', 'placeholder'=>'Lawyer Last Name']) !!}
                  </div>
                </div>
                <div class="form-group">
                	{!! FORM::label('name' ,'Serial Number',['class'=>'col-sm-2 control-label']) !!}
                	<div class="col-sm-3">
                 {!! FORM::text('serial_no' ,$data['data_collection']->serial_no,['class'=>'form-control', 'placeholder'=>'Lawyer Serial Number']) !!}
                  </div>
                  {!! FORM::label('name' ,'Public Serial Number',['class'=>'col-sm-2 control-label']) !!}
                	<div class="col-sm-3">
                 {!! FORM::text('public_serial_no' ,$data['data_collection']->public_serial_no,['class'=>'form-control', 'placeholder'=>'Public Serial Number','readonly']) !!}
                  </div>
                </div>
                <div class="form-group">
                	{!! FORM::label('name' ,'Bar License',['class'=>'col-sm-2 control-label']) !!}
                	<div class="col-sm-3">
                     {!! FORM::text('bar_license_number' ,$data['data_collection']->bar_license_number,['class'=>'form-control', 'placeholder'=>'Lawyer Bar License']) !!}
                    </div>
                      {!! FORM::label('name' ,'High Court License',['class'=>'col-sm-2 control-label']) !!}
                	<div class="col-sm-3">
                     {!! FORM::text('high_court_license_number' ,$data['data_collection']->high_court_license_number,['class'=>'form-control', 'placeholder'=>'Lawyer High Court License']) !!}
                  	</div>
                </div>
                 <div class="form-group">
                     <div class="col-sm-2"></div>
                  <div class="col-sm-3">
                 {!! Form::file('bar_license_number_image');!!}
                  @if($data['data_collection']->bar_license_file != "") 
                  		<a  href="{!! route('show.pdf', [encrypt('/storage/lawyers/bar_license_'.$data['data_collection']->_id.'.'.$data['data_collection']->bar_license_file)]) !!}" target="_blank" >View File</a>
                  	@endif
                  </div>
                  <div class="col-sm-2"></div>
                    <div class="col-sm-3">
                 {!! Form::file('high_court_license_image');!!}
                  @if($data['data_collection']->high_court_file != "") 
                  		<a  href="{!! route('show.pdf', [encrypt('/storage/lawyers/high_court_'.$data['data_collection']->_id.'.'.$data['data_collection']->high_court_file)]) !!}" target="_blank" >View File</a>
                  	@endif
               
                  </div>
               
                 </div>
                <div class="form-group">
                 {!! FORM::label('name' ,'Supreme Court License',['class'=>'col-sm-2 control-label']) !!}
                	<div class="col-sm-3">
                     {!! FORM::text('sup_court_license_number' ,$data['data_collection']->sup_court_license_number,['class'=>'form-control', 'placeholder'=>'Lawyer Supreme Court License']) !!}
                  	</div>
                  {!! FORM::label('name' ,'Email',['class'=>'col-sm-2 control-label']) !!}
                   <div class="col-sm-3">
                   {!! FORM::text('email' ,$data['data_collection']->email,['class'=>'form-control', 'placeholder'=>'Lawyer Email']) !!}
                  </div>
                 
                </div>
                <div class="form-group">
                 <div class="col-sm-2"></div>
                    <div class="col-sm-3">
                 {!! Form::file('sup_court_license_image');!!}
                   @if($data['data_collection']->supreme_court_file != "") 
                  		<a  href="{!! route('show.pdf', [encrypt('/storage/lawyers/supreme_court_'.$data['data_collection']->_id.'.'.$data['data_collection']->supreme_court_file)]) !!}" target="_blank" >View File</a>
                  	@endif
               
                  </div>
               <div class="col-sm-2">
                 </div>
                  <div class="col-sm-3">
                 
                  </div>
                 </div>
                <div class="form-group">
                 {!! FORM::label('name' ,'Mobile',['class'=>'col-sm-2 control-label']) !!}
                  <div class="col-sm-3">
                 {!! FORM::text('mobile' ,$data['data_collection']->mobile,['class'=>'form-control', 'placeholder'=>'Lawyer Mobile']) !!}
                  </div>
                  {!! FORM::label('name' ,'Designation',['class'=>'col-sm-2 control-label']) !!}
                   <div class="col-sm-3">
                    {!! Form::select('designation', $designation,$data['data_collection']->designation,['class'=>'form-control', 'placeholder'=>'Lawyer(s) Designation'] ) !!}
                  </div>
                </div>
       
              <div class="form-group">
              {!! FORM::label('name' ,'Area of Service',['class'=>'col-sm-2 control-label']) !!}
                   <div class="col-sm-3">
                    
                    {!! Form::select('area_of_service', $aos_sel, $data['data_collection']->area_of_service,['class'=>'form-control', 'placeholder'=>'Area of service']) !!}
                  </div>
                  {!! FORM::label('name' ,'Lawyers Role',['class'=>'col-sm-2 control-label']) !!}
                   <div class="col-sm-3">
                    {!! Form::select('role', $role,$data['data_collection']->role,['class'=>'form-control', 'placeholder'=>'Lawyer(s) Role'] ) !!}
                  </div>
                </div>
              </div>
              
               </fieldset>
          
            
                @if($data['edu']->count())  
                <?php $cnt  = 0;?>
                	@foreach($data['edu'] as $education)
                	 <div class='row_<?php echo $cnt;?>'>
                	 <fieldset>
                  <legend>Educational Detail:</legend>
                		<div class="form-group">
                  <label for="name" class="col-sm-2 control-label">{{ __('School/Collage:') }}<span class="label_red"> *</span></label>
                  <div class="col-sm-3">
                 	<input id="collage" type="text" class="form-control{{ $errors->has('collage') ? ' is-invalid' : '' }}" placeholder='Institute Name' name="collage[]" value="{!! $education->institute !!}" autofocus>
                 @if ($errors->has('collage'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('collage') }}</strong>
                    </span>
                @endif
                  </div>
                     <label for="name" class="col-sm-2 control-label">{{ __('Degree:') }}<span class="label_red"> *</span></label>
                  <div class="col-sm-3">
                 	<input id="degree" type="text" class="form-control{{ $errors->has('degree') ? ' is-invalid' : '' }}" placeholder='Degree Name' name="degree[]" value="{!! $education->degree !!}" autofocus>
                 @if ($errors->has('degree'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('degree') }}</strong>
                    </span>
                @endif
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">{{ __('From Year:') }}<span class="label_red"> *</span></label>
                  <div class="col-sm-3">
                 	<select name='from_year[]' class='form-control'>
                 		<option value='0'>Select From Year</option>
                 		<?php for($i=date('Y'); $i >=1900 ; $i-- ){
                 		    ?>
                 			<option <?php if( $education->from_year == $i){ echo 'selected="selected"';} else { echo ''; } ?>  value='<?php echo $i;?>'><?php echo $i;?></option>
                 		<?php } ?>
                 	</select>
                 @if ($errors->has('collage'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('collage') }}</strong>
                    </span>
                @endif
                  </div>
                     <label for="name" class="col-sm-2 control-label">{{ __('To Year:') }}<span class="label_red"> *</span></label>
                  <div class="col-sm-3">
					<select name='to_year[]' class='form-control'>
                 		<option value='0'>Select To Year</option>
                 		<?php for($i=date('Y')+7; $i >=1900 ; $i-- ){?>
                 			<option <?php if( $education->to_year == $i){ echo 'selected="selected"';} else { echo ''; } ?> value='<?php echo $i;?>'><?php echo $i;?></option>
                 		<?php } ?>
                 	</select>
                 @if ($errors->has('degree'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('degree') }}</strong>
                    </span>
                @endif
                  </div>
                </div>
                    <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">{{ __('Grade:') }}<span class="label_red"> *</span></label>
                  <div class="col-sm-3">
                 	<input id="grade" type="text" class="form-control{{ $errors->has('grade') ? ' is-invalid' : '' }}" placeholder='Grades' name="grade[]" value="{!! $education->grade !!}" autofocus>
                 @if ($errors->has('grade'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('grade') }}</strong>
                    </span>
                @endif
                  </div>
                     <label for="name" class="col-sm-2 control-label">{{ __('Hobbies:') }}<span class="label_red"> *</span></label>
                  <div class="col-sm-3">
                 	<input id="hobby" type="text" class="form-control{{ $errors->has('hobby') ? ' is-invalid' : '' }}" placeholder='Degree Name' name="hobby[]" value="{!! $education->hobbies !!}" autofocus>
                 @if ($errors->has('hobby'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('hobby') }}</strong>
                    </span>
                @endif
                  </div>
                </div>
                <div class="form-group">
                  
                     <label for="name" class="col-sm-2 control-label">{{ __('Description:') }}</label>
                  <div class="col-sm-3">
					<textarea name="edu_desc[]" id="edu_desc" style="width: 100%" cols="10" >{!! $education->description !!}</textarea>
                 @if ($errors->has('edu_desc'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('edu_desc') }}</strong>
                    </span>
                @endif
                  </div>
                   <div class="col-sm-2">
                   </div>
                  <div class="col-sm-3">
                  	<?php if($cnt == 0){ ?>
					<input type="button" class="add-row pull-right" value="Add More">
					<?php } else { ?>
					<input type="button" class="remove-row pull-right" value="Remove" onClick="remove_row('<?php echo $cnt;?>')">
					<?php }  ;?>
                  </div>
                </div>
                <?php if($cnt == 0){ ?>
					<div class="new_row" >
                  
                  
                </div>
					<?php }
					$cnt = $cnt+1; ?>
                
                </fieldset></div>
                	@endforeach
                @else
                  <fieldset>
                  <legend>Educational Detail:</legend>
                  <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">{{ __('School/Collage:') }}<span class="label_red"> *</span></label>
                  <div class="col-sm-3">
                 	<input id="collage" type="text" class="form-control{{ $errors->has('collage') ? ' is-invalid' : '' }}" placeholder='Institute Name' name="collage[]" value="{{ old('collage') }}" autofocus>
                 @if ($errors->has('collage'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('collage') }}</strong>
                    </span>
                @endif
                  </div>
                     <label for="name" class="col-sm-2 control-label">{{ __('Degree:') }}<span class="label_red"> *</span></label>
                  <div class="col-sm-3">
                 	<input id="degree" type="text" class="form-control{{ $errors->has('degree') ? ' is-invalid' : '' }}" placeholder='Degree Name' name="degree[]" value="{{ old('degree') }}" autofocus>
                 @if ($errors->has('degree'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('degree') }}</strong>
                    </span>
                @endif
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">{{ __('From Year:') }}<span class="label_red"> *</span></label>
                  <div class="col-sm-3">
                 	<select name='from_year[]' class='form-control'>
                 		<option value='0'>Select From Year</option>
                 		<?php for($i=date('Y'); $i >=1900 ; $i-- ){?>
                 			<option value='<?php echo $i;?>'><?php echo $i;?></option>
                 		<?php } ?>
                 	</select>
                 @if ($errors->has('collage'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('collage') }}</strong>
                    </span>
                @endif
                  </div>
                     <label for="name" class="col-sm-2 control-label">{{ __('To Year:') }}<span class="label_red"> *</span></label>
                  <div class="col-sm-3">
					<select name='to_year[]' class='form-control'>
                 		<option value='0'>Select To Year</option>
                 		<?php for($i=date('Y')+7; $i >=1900 ; $i-- ){?>
                 			<option value='<?php echo $i;?>'><?php echo $i;?></option>
                 		<?php } ?>
                 	</select>
                 @if ($errors->has('degree'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('degree') }}</strong>
                    </span>
                @endif
                  </div>
                </div>
                    <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">{{ __('Grade:') }}<span class="label_red"> *</span></label>
                  <div class="col-sm-3">
                 	<input id="grade" type="text" class="form-control{{ $errors->has('grade') ? ' is-invalid' : '' }}" placeholder='Grades' name="grade[]" value="{{ old('grade') }}" autofocus>
                 @if ($errors->has('grade'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('grade') }}</strong>
                    </span>
                @endif
                  </div>
                     <label for="name" class="col-sm-2 control-label">{{ __('Hobbies:') }}<span class="label_red"> *</span></label>
                  <div class="col-sm-3">
                 	<input id="hobby" type="text" class="form-control{{ $errors->has('hobby') ? ' is-invalid' : '' }}" placeholder='Degree Name' name="hobby[]" value="{{ old('hobby') }}" autofocus>
                 @if ($errors->has('hobby'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('hobby') }}</strong>
                    </span>
                @endif
                  </div>
                </div>
                <div class="form-group">
                  
                     <label for="name" class="col-sm-2 control-label">{{ __('Description:') }}</label>
                  <div class="col-sm-3">
					<textarea name="edu_desc[]" id="edu_desc" style="width: 100%" cols="10" ></textarea>
                 @if ($errors->has('edu_desc'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('edu_desc') }}</strong>
                    </span>
                @endif
                  </div>
                   <div class="col-sm-2">
                   </div>
                  <div class="col-sm-3">
					<input type="button" class="add-row pull-right" value="Add More">
                  </div>
                </div>
                <div class="new_row" >
                  
                  
                </div>
                 </fieldset>
                @endif
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="{{route('lawyers.index')}}"><button type="button" class="btn btn-default">Back</button></a>
                {!! Form::submit('Submit', ['class'=>'btn btn-info pull-right']) !!}
              </div>
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
  			<input type="hidden" value="@if($data['edu']->count()){!! $data['edu']->count() !!}@else{!! '1' !!}@endif" name="row_count" id="row_count" />
              <!-- /.box-footer -->
            {!! Form::close() !!}
          </div>
         
        </div>
        <!--/.col (right) -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<script>
$(document).ready(function(){
    $(".add-row").click(function(){
        var row_count = $('#row_count').val();
        var markup = '<div class="row_'+row_count+'"><fieldset>'+
            			'<legend>Educational Detail:</legend>'+
        '<div class="form-group">'+
        '<label for="name" class="col-sm-2 control-label">School/Collage:</label>'+
        '<div class="col-sm-3">'+
       	'<input id="collage" type="text" class="form-control" placeholder="Institute Name" name="collage[]" autofocus>'+
        '</div>'+
         '  <label for="name" class="col-sm-2 control-label">Degree:</label>'+
        '<div class="col-sm-3">'+
       	'<input id="degree" type="text" class="form-control" placeholder="Degree Name" name="degree[]" autofocus>'+
       ' </div>'+
      '</div>'+
      	'<div class="form-group">'+
        '<label for="name" class="col-sm-2 control-label">From Year:</label>'+
        '<div class="col-sm-3">'+
       	'<select name="from_year[]" class="form-control">'+
       		'<option value="0">Select From Year</option>'+
       		<?php for($i=date('Y'); $i >=1900 ; $i-- ){?>
       			'<option value="<?php echo $i;?>"><?php echo $i;?></option>'+
       		<?php } ?>
       	'</select>'+
        '</div>'+
           '<label for="name" class="col-sm-2 control-label">To Year:</label>'+
        	'<div class="col-sm-3">'+
			'<select name="to_year[]" class="form-control">'+
       		'<option value="0">Select To Year</option>'+
       		<?php for($i=date('Y')+7; $i >=1900 ; $i-- ){?>
       			'<option value="<?php echo $i;?>"><?php echo $i;?></option>'+
       		<?php } ?>
       	'</select>'+
        '</div>'+
      '</div>'+
      '<div class="form-group">'+
      '<label for="name" class="col-sm-2 control-label">Grade:</label>'+
      '<div class="col-sm-3">'+
     	'<input id="grade" type="text" class="form-control" placeholder="Grades" name="grade[]" autofocus>'+
      '</div>'+
        '<label for="name" class="col-sm-2 control-label">Hobbies:</label>'+
      '<div class="col-sm-3">'+
     	'<input id="hobby" type="text" class="form-control" placeholder="Degree Name" name="hobby[]"  autofocus>'+
     	'</div>'+
   		'</div>'+
   		'<div class="form-group">'+
        '<label for="name" class="col-sm-2 control-label">Description:</label>'+
     '<div class="col-sm-3">'+
		'<textarea name="edu_desc[]" id="edu_desc" style="width: 100%" cols="10" ></textarea>'+
'     </div>'+
  '    <div class="col-sm-2">'+
    '  </div>'+
   '  <div class="col-sm-3">'+
		'<input type="button" class="remove-row pull-right" value="Remove" onClick="remove_row('+row_count+')">'
    ' </div>'+
  ' </div>'+
	          '</fieldset><div>';
        $(".new_row").append(markup);
        $('#row_count').val(parseInt(row_count)+1);
        $(".selections").select2();
    });
});

function remove_row(row_id){
	$('.row_'+row_id).remove();
}
</script>
@endsection 
