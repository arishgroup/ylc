@extends(Auth::guard('admin')->check() ? 'layouts.admin' : 'layouts.backendUser')


@section('content')
	<script src="http://demo.itsolutionstuff.com/plugin/jquery.js"></script>
<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>{{ __('Data Entry') }}</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('aos.index')}}">Data Entry</a></li>
        <li class="active">Add</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info col-md-6">
            <div class="box-header with-border">
              @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::open(['route' => 'lawyers.content.upload', 'method' => 'post', 'class'=>'form-horizontal','files'=>'true']) !!}
              {{ csrf_field() }}
              <div class="box-body">
            
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">{{ __('File upload:') }}<span class="label_red"> *</span></label>
                    <div class="col-sm-3">
                 {!! Form::file('image');!!}
                        @if ($errors->has('image'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('image') }}</strong>
                            </span>
                        @endif
                  </div>
                    <label for="name" class="col-sm-2 control-label">{{ __('Area of Service:') }}<span class="label_red"> *</span></label>
                  <div class="col-sm-3">
                 {!! Form::select('area_of_service', $aos,'',['class'=>'form-control', 'placeholder'=>'Select Area of Service'] ) !!}
                      @if ($errors->has('area_of_service'))
                          <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('area_of_service') }}</strong>
                            </span>
                      @endif
                  </div>
                 </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">{{ __('Title:') }}<span class="label_red"> *</span></label>
                    <div class="col-sm-3">
                 {!! FORM::text('title' , '',['class'=>'form-control', 'placeholder'=>'Title']) !!}
                        @if ($errors->has('title'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @endif
                  </div>
                    <label for="name" class="col-sm-2 control-label">{{ __('Court:') }}<span class="label_red"> *</span></label>
                  <div class="col-sm-3">
                 	{!! Form::select('court', $court,'',['class'=>'form-control', 'placeholder'=>'Select Court'] ) !!}
                      @if ($errors->has('court'))
                          <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('court') }}</strong>
                            </span>
                      @endif
                  </div>
                </div>
                <div class="form-group">
                  {!! FORM::label('name' ,'Petitioner',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-3">
                 {!! FORM::text('petitioner' ,'',['class'=>'form-control', 'placeholder'=>'Petitioner']) !!}
                  </div>
                  {!! FORM::label('name' ,'Respondents',['class'=>'col-sm-2 control-label']) !!}
                  <div class="col-sm-3">
                 {!! FORM::text('respondent' ,'',['class'=>'form-control', 'placeholder'=>'Respondents']) !!}
                  </div>
                </div>
                <div class="form-group">
                  {!! FORM::label('name' ,'Appellant',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-3">
                 {!! FORM::text('appellant' ,'',['class'=>'form-control', 'placeholder'=>'Appellant']) !!}
                  </div>
                  {!! FORM::label('name' ,'Defandent',['class'=>'col-sm-2 control-label']) !!}
                  <div class="col-sm-3">
                 {!! FORM::text('defandent' ,'',['class'=>'form-control', 'placeholder'=>'Defandent']) !!}
                  </div>
                </div>
                <div class="form-group">
                  {!! FORM::label('name' ,'Page No.',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-3">
                 {!! FORM::text('page_no' ,'',['class'=>'form-control', 'placeholder'=>'Page No.']) !!}
                  </div>
                  {!! FORM::label('name' ,'Referance',['class'=>'col-sm-2 control-label']) !!}
                  <div class="col-sm-3">
                 {!! FORM::text('referance' ,'',['class'=>'form-control', 'placeholder'=>'Referance']) !!}
                  </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">{{ __('Judge Name:') }}<span class="label_red"> *</span></label>
                    <div class="col-sm-3">
                 
                 <select id="judge_name" name="judge_name[]" class="form-control" multiple></select><br>
                 {!! FORM::text('other_judge' ,'',['class'=>'form-control', 'placeholder'=>'Other Judges']) !!}
                        @if ($errors->has('other_judge'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('other_judge') }}</strong>
                            </span>
                        @endif
                  </div>
                   {!! FORM::label('name' ,'Tags',['class'=>'col-sm-2 control-label']) !!}
                  <div class="col-sm-3">
                 	<select id="tags" name="tags[]" class="form-control" multiple></select>
                 	
                  </div> 
                  
                </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">{{ __('Lawyer In-Favor:') }}<span class="label_red"> *</span></label>
                    <div class="col-sm-3">
                 {!! FORM::text('lawyer_favor' ,'',['class'=>'form-control', 'placeholder'=>'Lawyer In-Favor']) !!}
                        @if ($errors->has('lawyer_favor'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('lawyer_favor') }}</strong>
                            </span>
                        @endif
                  </div>


                    <label for="name" class="col-sm-2 control-label">{{ __('Lawyer Against:') }}<span class="label_red"> *</span></label>
                  <div class="col-sm-3">
                 {!! FORM::text('lawyer_against' ,'',['class'=>'form-control', 'placeholder'=>'Lawyer Name']) !!}
                      @if ($errors->has('lawyer_against'))
                          <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('lawyer_against') }}</strong>
                            </span>
                      @endif
                  </div>
		       </div>
                 <div class="form-group">
                     <label for="name" class="col-sm-2 control-label">{{ __('Citation Name:') }}<span class="label_red"> *</span></label>
                    <div class="col-sm-3">
                 {!! FORM::text('citation_name' ,'',['class'=>'form-control', 'placeholder'=>'Citation Name']) !!}
                        @if ($errors->has('citation_name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('citation_name') }}</strong>
                            </span>
                        @endif
                  </div>
                     <label for="name" class="col-sm-2 control-label">{{ __('City:') }}<span class="label_red"> *</span></label>
                  <div class="col-sm-3">
                 	{!! Form::select('location', $location,'',['class'=>'form-control', 'placeholder'=>'Select Location'] ) !!}
                      @if ($errors->has('location'))
                          <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('location') }}</strong>
                            </span>
                      @endif
                  </div>
               </div>
               <div class="form-group" style="display: none">
                    <label for="name" class="col-sm-2 control-label">{{ __('Location:') }}</label>
                    <div class="col-sm-3">
                    	{!! Form::select('country_id', $country, '',['placeholder' => 'Select Country','class'=>'form-control', 'onChange'=>'GetCountryChange(this.value)', 'id'=>'country_id']) !!}
                    </div>
                    <div class="col-sm-2"></div>
                    <div class="col-sm-5"><div  id='state_div' style='display:none'>{!! Form::select('state_id', array(), '',['placeholder' => 'Select State', 'id'=>'country_state','class'=>'form-control']) !!}
                    </div></div>
                </div>
                
                   <div class="form-group">
                       <label for="name" class="col-sm-2 control-label">{{ __('Category:') }}<span class="label_red"> *</span></label>
                    <div class="col-sm-3">
                 {!! Form::select('category', $category,'',['class'=>'form-control', 'placeholder'=>'Select Category'] ) !!}
                        @if ($errors->has('category'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('category') }}</strong>
                            </span>
                        @endif
                  </div>
                  
                  {!! FORM::label('name' ,'Category Type',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-3">
                 {!! Form::select('category_type', $category_type,'',['class'=>'form-control', 'placeholder'=>'Select Category'] ) !!}
                  </div>
                </div>
                <div class="form-group">
                  {!! FORM::label('name' ,'Publish Month',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-3">
                 {!! FORM::text('publish_month' ,'',['class'=>'form-control', 'placeholder'=>'Publish Month']) !!}
                  </div>
                  {!! FORM::label('name' ,'Publish Year',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-3">
                 {!! FORM::text('publish_year' ,'',['class'=>'form-control', 'placeholder'=>'Pubish Year']) !!}
                  </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">{{ __('Statute(s):') }}<span class="label_red"> *</span></label>
                    <div class="col-sm-3">
                {!! Form::select('statutes', $statutes,'',['class'=>'form-control selections', 'placeholder'=>'Select Statute', 'onChange'=>'GetSectionChange(this, 0, 0)' ] ) !!}
                        @if ($errors->has('statutes'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('statutes') }}</strong>
                            </span>
                        @endif
                  </div>
                    <label for="name" class="col-sm-2 control-label">{{ __('Statute Sections:') }}<span class="label_red"> *</span></label>
                  <div class="col-sm-3">
                 	{!! Form::select('statute_section[]', array(),'',['class'=>'form-control selections', 'placeholder'=>'Select Section', 'id'=>'select_div_0'] ) !!}
                      @if ($errors->has('tatute_section'))
                          <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('tatute_section') }}</strong>
                            </span>
                      @endif
                  </div>
                  <input type="button" class="add-row" value="Add Row">
                </div>
                <div class="new_row" >
                  
                  
                </div>
                
        		<div class="form-group">
                  {!! FORM::label('name' ,'Short Description',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-8">
                    {!! FORM::textarea('short_desc' ,'',['class'=>'form-control', 'rows' => 3, 'cols' => 40]) !!}
					 </div>
                 </div>
        		<div class="form-group">
                  {!! FORM::label('name' ,'Head Notes',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-8">
                    {!! FORM::textarea('head_notes' ,'',['class'=>'form-control', 'rows' => 3, 'cols' => 40]) !!}
					</div>
                 </div>
                <div class="form-group">
                  {!! FORM::label('name' ,'Case Description',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-8">
                    {!! FORM::textarea('article_ckeditor' ,'',['class'=>'form-control', 'rows' => 3, 'cols' => 40]) !!}
					 </div>
                 </div>
                
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="{{route('lawyers.datalib.index')}}"><button type="button" class="btn btn-default">Back</button></a>
                {!! Form::submit('Submit', ['class'=>'btn btn-info pull-right']) !!}
              </div>
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <!-- /.box-footer -->
         	<input type="hidden" value="1" name="row_count" id="row_count" />
            {!! Form::close() !!}
          </div>
         
        </div>
        <!--/.col (right) -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
	<script>
    	CKEDITOR.replace( 'article_ckeditor' );
    	CKEDITOR.replace( 'head_notes' );
    	CKEDITOR.replace( 'short_desc' );
	</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
	<script type="text/javascript">
			$(".selections").select2();

			function GetSectionChange(obj, new_call, cell_id){
				$.ajax({url: "/getSections/"+obj.value+"/"+new_call, success: function(result){
					var row_id = 'select_div_'+cell_id;
					document.getElementById(row_id).innerHTML = result.data;
			    }});
			}
			$(document).ready(function(){
		        $(".add-row").click(function(){
			        var row_count = $('#row_count').val();
			        var markup = '<div class="form-group row_'+row_count+'">'+
				        '<label for="name" class="col-sm-2 control-label"></label>'+
                    '<div class="col-sm-3">'+
	                 '<select class="form-control selections" onchange="GetSectionChange(this, 0, '+row_count+')" name="statutes[]" tabindex="-1" aria-hidden="true">'+
	                 	'<option value="">Select Statute</option>'+
					<?php foreach($statutes as $key => $statute){?>
	                 	'<option value="<?php echo $key;?>"><?php echo $statute?></option>'+
					<?php } ?>
	                 	'</select>'+
	                  '</div>'+
	                  '<label for="name" class="col-sm-2 control-label">Statute Sections</label>'+
	                    '<div class="col-sm-3">'+
	                 		'<select class="form-control selections" id="select_div_'+row_count+'" name="statute_section[]" tabindex="-1" aria-hidden="true">'+
	                 		'<option selected="selected" value="">Select Section</option></select>'+
	                  '</div>'+
	                  '<input type="button" class="remove-row" value="Remove" onClick="remove_row('+row_count+')">'+
		  	          '</div>';
		            $(".new_row").append(markup);
		            $('#row_count').val(parseInt(row_count)+1);
		            $(".selections").select2();
		        });
			});

			function remove_row(row_id){
				$('.row_'+row_id).remove();
			}
		</script>
    <script>
        $('#tags').select2({
            placeholder: "Choose tags...",
            minimumInputLength: 2,
            ajax: {
                url: '/tags/find',
                dataType: 'json',
                data: function (params) {
                    return {
                        q: $.trim(params.term)
                    };
                },
                processResults: function (data) {
                    console.log(data);
                    return {
                        results: data[0]
                    };
                },
                cache: true
            }
        });
    </script>
    <script>
        $('#judge_name').select2({
            placeholder: "Choose Judge...",
            minimumInputLength: 2,
            ajax: {
                url: '/judge/find',
                dataType: 'json',
                data: function (params) {
                    return {
                        q: $.trim(params.term)
                    };
                },
                processResults: function (data) {
                    console.log(data);
                    return {
                        results: data[0]
                    };
                },
                cache: true
            }
        });

        function GetCountryChange(country_code){
    		
			$.ajax({url: "/get_state/"+country_code, success: function(result){
				console.log(result);
				document.getElementById('country_state').innerHTML = result.data;
				document.getElementById('state_div').style.display = 'inline-block';
				document.getElementById('city_div').style.display = 'none';
		    }});
		} 
function GetstateChange(state_code){
	
	$.ajax({url: "/get_city/"+state_code, success: function(result){
		document.getElementById('city_dd').innerHTML = result.data;
		if(result.data != "")
			document.getElementById('city_div').style.display = 'inline-block';
    }});
} 
    </script>
    
@endsection 
