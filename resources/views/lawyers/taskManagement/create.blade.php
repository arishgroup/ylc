@extends(Auth::guard('admin')->check() ? 'layouts.admin' : 'layouts.lawyer')
@section('content')
<?php
$assignee_type= "";
    $assignee_id="";
    $task_name="";
    $task_description="";
    $assigner_start_date="";
    $assigner_end_date="";
    $priority="";
    $current_record="";
?>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>{{ __('Manage Task') }}</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#"><i class="fa fa-tasks"></i> Task Management System</a></li>
                <li class="active"><a href="#"> <i class="fa fa-circle-o"></i>Manage Task</a></li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3><b id="total">0</b></h3>
                            <h4>Total Tasks</h4>
                        </div>
                        <div class="icon">
                            <i class="ion-briefcase"></i>
                        </div>
                        <a data-toggle="modal" data-target="#modal-popup" href="#" onclick="getAllDataTables('-1');" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-xs-6">
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3><b id="pending">0</b></h3>
                            <h4>Pending Tasks</h4>
                        </div>
                        <div class="icon">
                            <i class="ion-arrow-expand"></i>
                        </div>
                        <a data-toggle="modal" data-target="#modal-popup" href="#" onclick="getAllDataTables('0');" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-xs-6">
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3><b id="in_progress">0</b></h3>
                            <h4>In Progress Tasks</h4>
                        </div>
                        <div class="icon">
                            <i class="ion-close-circled"></i>
                        </div>
                        <a data-toggle="modal" data-target="#modal-popup" href="#" onclick="getAllDataTables('1');" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-xs-6">
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3><b id="complete">0</b></h3>
                            <h4>Completed Tasks</h4>
                        </div>
                        <div class="icon">
                            <i class="ion-checkmark-circled"></i>
                        </div>
                        <a data-toggle="modal" data-target="#modal-popup" href="#" onclick="getAllDataTables('2');" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="row">
                <section class="col-lg-6">
                    <div class="box box-info">
                        <div class="margin">
                            @if (Session::has('message'))
                                <div class="box-header with-border">
                                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                                </div>
                            @endif
                            <div class="box-header"></div>
                            <div id="status_msg"></div>
                            <div class="clearfix my-5">

                                <div class="pull-left btn-group">
                                    <button type="button" id="last" onclick="getNavigation('last')" class="btn btn-default">Last </button>
                                    <button type="button" id="prev" onclick="getNavigation('prev')" class="btn btn-default">Previous </button>
                                </div>
                                <div class="pull-right btn-group">
                                    <button type="button" id='next' onclick="getNavigation('next')" class="btn btn-default">Next</button>
                                    <button type="button" id="first" onclick="getNavigation('first')" class="btn btn-default">First</button>
                                </div>
                            </div>
                            {!! Form::open(['route' => 'lawyers.taskManagement.store', 'method' => 'post','id'=>'taskForm' , 'class'=>'form-horizontal','files'=>'true']) !!}
                            {{ csrf_field() }}
                            <div class="form-group row">
                                <div class="col-xs-6">
                                    <h4>{{ __('Teams:') }}<span class="label_red"> *</span></h4>

                                    {!! Form::select('teams',  $teams,"",['class'=>'form-control', 'id'=>'assignee_type', 'placeholder'=>'Select Assignee Type', 'onChange'=>'getAssignee(this.value); getResult();'] ) !!}
                                    @if ($errors->has('assignee_type'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('assignee_type') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-xs-6">
                                    <h4>{{ __('Team Member:') }}<span class="label_red"> *</span></h4>
                                    <div class="input-group">
                                        {!! Form::select('assignee_id', array(),"",['class'=>'form-control', 'id'=>'assignee_id', 'placeholder'=>'Select Assignee' , 'onClick'=>'getResult();'] ) !!}
                                        <span class="input-group-btn">
                                          <button type="button" data-toggle="modal" data-target="#modal-popup" onclick="getAllDataTables('assignee_name')" class="btn btn-primary btn-flat"><i class="fa fa-fw fa-search"></i></button>
                                        </span>
                                    </div>

                                    @if ($errors->has('assignee_id'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('assignee_id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-xs-12">
                                    <h4>{{ __('Task Name:') }}<span class="label_red"> *</span></h4>
                                    <div class="input-group">
                                        {!! FORM::text('task_name' , "" ,['class'=>'form-control', 'id'=>'task_name','placeholder'=>'Task Name','onkeyup'=>'getResult()']) !!}
                                        <span class="input-group-btn">
                                                  <button type="button" data-toggle="modal" data-target="#modal-popup" onclick="getAllDataTables('task_name')" class="btn btn-primary btn-flat"><i class="fa fa-fw fa-search"></i></button>
                                                </span>
                                    </div>
                                    @if ($errors->has('task_name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('task_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-xs-12">
                                    <h4>{{ __('Description:') }}<span class="label_red"> *</span></h4>
                                    <div>
                                        {!! FORM::textarea('task_description' ,"",['class'=>'form-control', 'id'=>'task_description' ,'rows' => 3, 'cols' => 40]) !!}
                                        @if ($errors->has('task_description'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('task_description') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-xs-6">
                                    <h4>{{ __('Start Date:') }}</h4>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class ="form-control" id="start_date" name="assigner_start_date" onclick = 'getResult()' value="">
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <h4>{{ __('End Date:') }}</h4>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        {{--{!! Form::date('deadline', \Carbon\Carbon::now()->format('D/M/Y'), ['class' => 'form-control']) !!}--}}
                                        <input type="text" class ="form-control" id="end_date" name="assigner_end_date" onkeyup = 'getResult()' value="">
                                        @if ($errors->has('assigner_end_date'))
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('assigner_end_date') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-xs-6">
                                    <h4>{{ __('Priority:') }}</h4>
                                    <div>
                                        {{--{!! Form::select('priority',  array('p1','p2'),"",['class'=>'form-control', 'id'=>'priority', 'onkeyup'=>'getResult();'] ) !!}--}}
                                        @if ($errors->has('priority'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('priority') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <h4>{{ __('Attachments:') }}</h4>
                                    <div>
                                        <input type="file" name="attachments[]" multiple />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-xs-12">
                                    <h4>{{ __('Comment:') }}</h4>
                                    <div>
                                        {!! FORM::textarea('comment' ,'',['class'=>'form-control', 'rows' => 3, 'cols' => 40]) !!}
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div>
                                <a href="{{route('taskManagement.index')}}"><button type="button" class="btn btn-default">Back</button></a>
                                <input class="btn btn-primary" type="button" onclick="clearForm()" value="New Task">
                                {!! Form::submit('Submit', ['class'=>'btn btn-primary pull-right']) !!}
                            </div>
                            <div class="box-header"></div>
                            <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" id="assigner_id" name="assigner_id" value="{{ Auth::user()->_id }}">
                            <input type="hidden" id="current_record" name="current_record" value="{!!  $current_record !!}">
                            <!-- /.box-footer -->
                            {!! Form::close() !!}
                        </div>

                            <div class="box box-solid bottom-part">
                                <div class="box-header with-border">
                                    <i class="fa fa-envelope"></i>
                                    <h3 class="box-title">Attachments</h3>
                                </div>
                                <div id="attachments" class="box-body">
                                    <div class="row box-header">
                                        <?php $div_loop=0; ?>
                                        @if(!empty($data->getTaskAttachments[0]['_id']))
                                            @foreach($data->getTaskAttachments as $taskAttachments)
                                                <?php
                                                if($div_loop%4 == 0 && $div_loop!=0){ ?>
                                                    </div><div class='row box-header'> <?php
                                                }   ?>
                                                <div class="col-sm-3">
                                                    @if($taskAttachments->file_extension == "docx" || $taskAttachments->file_extension == "xlsx" || $taskAttachments->file_extension == "pptx")
                                                        <img src='{{ asset("storage/taskmanagement/office.jpg") }}' alt="attachments" height="25" width="25">
                                                        <a target="_blank" href='{{ asset("storage/taskmanagement/$taskAttachments->_id.$taskAttachments->file_extension") }}' >{{$taskAttachments->file_name}}</a>
                                                    @elseif($taskAttachments->file_extension == "pdf")
                                                        <img src='{{ asset("storage/taskmanagement/pdf.jpg") }}' alt="attachments" height="25" width="25">
                                                        <a  href="{!! route('show.pdf', [encrypt('/storage/taskmanagement/'.$taskAttachments->_id.'.'.$taskAttachments->file_extension)]) !!}" target="_blank" >{{$taskAttachments->file_name}}</a>
                                                    @else
                                                        <img src='{{ asset("storage/taskmanagement/$taskAttachments->_id.$taskAttachments->file_extension") }}' alt="attachments" height="25" width="25">
                                                        <a target="_blank" href='{{ asset("storage/taskmanagement/$taskAttachments->_id.$taskAttachments->file_extension") }}' >{{$taskAttachments->file_name}}</a>
                                                    @endif
                                                </div><?php
                                                ++$div_loop; ?>
                                            @endforeach
                                        @else
                                            <div class="col-sm-11">
                                                <div class="panel">
                                                    <div class="panel-heading">
                                                        No File attached Yet
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="box box-solid bottom-part">
                                <div class="box-header with-border">
                                    <i class="fa fa-comments"></i>
                                    <h3 class="box-title">Comments</h3>
                                </div>
                                <section class="box-body">
                                    <div class="row">
                                        <div id="comments" class="col-md-12">
                                            <ul class="timeline">
                                                @if(!empty($data->getTaskComments[0]['comment']))
                                                    @foreach($data->getTaskComments->sortByDesc('created_at') as $taskComment)
                                                        <li>
                                                            <i class="fa fa-comments bg-yellow"></i>
                                                            <div class="timeline-item panel-default">
                                                                <span class="time"><i class="fa fa-clock-o"></i> {{ $taskComment['created_at']->diffForHumans() }}</span>
                                                                <h5 class="panel-heading no-margin"><a href="#">
                                                                        @if($taskComment->user_type =='admin')
                                                                            @if($taskComment->user_id) {!! $taskComment->getTaskAdmin->name  !!} @endif
                                                                        @elseif($taskComment->user_type =='lawyer')
                                                                            @if($taskComment->user_id) {!! $taskComment->getTaskLawyer->name  !!} @endif
                                                                        @elseif($taskComment->user_type =='user')
                                                                            @if($taskComment->user_id) {!! $taskComment->getTaskUser->name  !!} @endif
                                                                        @endif
                                                                    </a> commented on your post</h5>
                                                                <div class="timeline-body">
                                                                    {{ $taskComment['comment'] }}
                                                                </div>
                                                            </div>
                                                        </li>
                                                    @endforeach
                                                @else
                                                    <li>
                                                        <i class="fa fa-comments bg-yellow"></i>
                                                        <div class="timeline-item">
                                                            <div class="panel-heading">
                                                                No Comment Yet
                                                            </div>
                                                        </div>
                                                    </li>
                                                @endif
                                                <li>
                                                    <i class="fa fa-clock-o bg-gray"></i>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </section>
                            </div>

                    </div>
                </section>
                <section class="col-lg-6">
                    <div class="box box-warning">
                        <div class="box-header">
                            <h3 class="box-title">Tasks</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body">
                            <table id="assignee_table" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Task</th>
                                    <th>Description</th>
                                    <th>Deadline</th>
                                    <th>Priority</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Bar Chart</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="chart">
                                <canvas id="barChart" style="height:230px"></canvas>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </section>

    @component('components.popup')
            @slot('model_table')
                model_table
            @endslot
    @endcomponent

    </div>
    <script>
        function getAllDataTables(name) {
            var table ="";
            var task_type = "";
            var assignee_type = "lawyer";
            if(name == "all"){
                table = "#assignee_table";
            }
            if(name == "-1"){
                table = '#model_table';
            }
            if(name == "0" || name == "1" || name == "2"){
                table = '#model_table';
                task_type = name;
            }
            $(table).DataTable({
                destroy: true,
                processing: true,
                serverSide: true,
                "order": [[ 2, "desc" ]],
                ajax: '/getAssigneeDataTables/'+assignee_type+'&'+task_type,
                columns: [
                    { data: 'task_name', name: 'task_name' },
                    { data: 'task_description', name: 'task_description' },
                    { data: 'assigner_end_date', name: 'assigner_end_date' },
                    { data: 'priority', name: 'priority' },
                    { data: 'status', name: 'status' }
                ]
            });
        }
        getAllDataTables("all");


        function getAssigneeTasksCount() {
            var assignee_type = "lawyer";
            $.ajax({url: "/assigneeTasksStatusCount/"+assignee_type, success: function(result){
                    document.getElementById('total').innerHTML = result.data['total'];
                    document.getElementById('pending').innerHTML = result.data['pending'];
                    document.getElementById('in_progress').innerHTML = result.data['in_progress'];
                    document.getElementById('complete').innerHTML = result.data['complete'];
                }});
        }
        getAssigneeTasksCount();


        function changeStatus(){
            var status = document.getElementById("myStatus").value;
            if(status == 1){
                if (confirm("You are about to start the task")) {
                    id = document.getElementById("current_record").value;
                $.ajax({url: "/startTask/"+id, success: function(result){
                    document.getElementById("myStatus").options[0].disabled = true;
                    document.getElementById("myStatus").options[2].disabled = false;
                    document.getElementById('status_msg').innerHTML = "<div class='alert alert-info '>You have started the task </div>";
                    }});
                }
                else{
                    document.getElementById('myStatus').value=0;
                }
            }

            if(status == 2){
                if (confirm("Are you sure you want to end the task")) {
                    id = document.getElementById("current_record").value;
                    $.ajax({
                        url: "/endTask/"+id, success: function (result) {
                            document.getElementById("myStatus").options[0].disabled = true;
                            document.getElementById("myStatus").options[1].disabled = true;
                            document.getElementById('status_msg').innerHTML = "<div class='alert alert-info'>You have ended the task </div>";
                        }
                    });
                }
                else{
                    document.getElementById('myStatus').value=1;
                }
            }

            getAssigneeTasksCount();
        }

        function getNavigation(nav){
            var id=0;
            var assignee_type = "lawyer";
            if(nav == 'next' || nav == 'prev'){
                id = document.getElementById("current_record").value;
            }
            var total_val  = nav+'&'+id+'&'+assignee_type;
            $.ajax({url: "/assigneeDataTablesNavigation/"+total_val,  success: function(result){

                // alert(result.data[0].priority);

                if(result.data[0]){
                        document.getElementById("current_record").value = result.data[0]._id;
                        document.getElementById('task_name').innerHTML = result.data[0].task_name;
                        var priority = result.data[0].priority
                        if(priority == '0'){
                            document.getElementById('priority').innerHTML = "<small class='label label-info'><i class='fa fa-clock-o'></i> Minor </small>";
                        }
                        else if(priority == '1'){
                            document.getElementById('priority').innerHTML = "<small class='label label-primary'><i class='fa fa-clock-o'></i> Low </small>";
                        }
                        else if(priority == '2'){
                            document.getElementById('priority').innerHTML = "<small class='label label-success'><i class='fa fa-clock-o'></i> Normal </small>";
                        }
                        else if(priority == '3'){
                            document.getElementById('priority').innerHTML = "<small class='label label-warning'><i class='fa fa-clock-o'></i> High </small>";
                        }
                        else if(priority == '4'){
                            document.getElementById('priority').innerHTML = "<small class='label label-danger'><i class='fa fa-clock-o'></i> Urgent </small>";
                        }
                        else{
                            document.getElementById('priority').innerHTML = "";
                        }
                        document.getElementById('task_description').innerHTML = result.data[0].task_description;

                        var objSelect_s = document.getElementById("myStatus");
                        setSelectedValue(objSelect_s, result.data[0].status);

                        document.getElementById('end_date').innerHTML = result.data[0].assigner_end_date;

                        if(result.data[1]){
                            document.getElementById('comments').innerHTML = result.data[1];
                        }
                        if(result.data[2]){
                            document.getElementById('attachments').innerHTML = result.data[2];
                        }
                    }
                    else{
                        alert("No record Found");
                    }
                }});
        }

        function setSelectedValue(selectObj, valueToSet) {
            for (var i = 0; i < selectObj.options.length; i++) {

                if (selectObj.options[i].value == valueToSet) {
                    selectObj.options[i].selected = true;
                    return;
                }
            }
        }
    </script>
    @component('components.graph', ['graph'=>$graph])
    @endcomponent

@endsection
