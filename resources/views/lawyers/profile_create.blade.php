@extends('layouts.backendUser')


@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>{{ __('Lawyer(s) Profile') }}</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('aos.index')}}">Lawyer(s)</a></li>
        <li class="active">Profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info col-md-6">
            <div class="box-header with-border">
              @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::open(['route' => 'lawyers.profile.store', 'method' => 'post', 'class'=>'form-horizontal', 'files'=>'true']) !!}
              {{ csrf_field() }}
              <div class="box-body">
              <fieldset>
                  <legend>Account Information:</legend>
                  <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">{{ __('Profile Image:') }}</label>
                	<div class="col-sm-3">
                     {!! Form::file('profile_img');!!}
                      @if ($errors->has('profile_img'))
                          <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('profile_img') }}</strong>
                            </span>
                      @endif
                  	</div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">{{ __('First Name:') }}<span class="label_red"> *</span></label>
                    <div class="col-sm-3">
                 {!! FORM::text('first_name' ,'',['class'=>'form-control', 'placeholder'=>'Lawyer First Name']) !!}
                        @if ($errors->has('first_name'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('first_name') }}</strong>
                    </span>
                        @endif
                  </div>
                    <label for="name" class="col-sm-2 control-label">{{ __('Last Name:') }}<span class="label_red"> *</span></label>
                  <div class="col-sm-3">
                 {!! FORM::text('last_name' ,'',['class'=>'form-control', 'placeholder'=>'Lawyer Last Name']) !!}
                      @if ($errors->has('last_name'))
                          <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('lastname') }}</strong>
                    </span>
                      @endif
                  </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">{{ __('NIC:') }}</label>
                	<div class="col-sm-3">
                     {!! FORM::text('nic' ,'',['class'=>'form-control', 'placeholder'=>'NIC']) !!}
                        @if ($errors->has('nic'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('nic') }}</strong>
                            </span>
                        @endif
                  	</div>
                    <label for="name" class="col-sm-2 control-label">{{ __('NIC Image:') }}</label>
                	<div class="col-sm-3">
                     {!! Form::file('nic_img');!!}
                      @if ($errors->has('nic_img'))
                          <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('nic_img') }}</strong>
                            </span>
                      @endif
                  	</div>
                </div>
                <div class="form-group">
                 {!! FORM::label('name' ,'Mobile',['class'=>'col-sm-2 control-label']) !!}
                  <div class="col-sm-3">
                 {!! FORM::text('mobile' ,'',['class'=>'form-control', 'placeholder'=>'Lawyer Mobile']) !!}
                  </div>
                  <label for="name" class="col-sm-2 control-label">{{ __('Email:') }}<span class="label_red"> *</span></label>
                   <div class="col-sm-3">
                   {!! FORM::text('email' ,'',['class'=>'form-control', 'placeholder'=>'Lawyer Email']) !!}
                       @if ($errors->has('email'))
                           <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                           </span>
                       @endif
                  </div>
                </div>
       
              <div class="form-group">
              {!! FORM::label('name' ,'Area of Service',['class'=>'col-sm-2 control-label']) !!}
                   <div class="col-sm-3">
                    
                    {!! Form::select('aos_id', $aos_sel, $aos_id,['class'=>'form-control', 'placeholder'=>'Area of service']) !!}
                  </div>
                 {!! FORM::label('name' ,'Location',['class'=>'col-sm-2 control-label']) !!}
                   <div class="col-sm-3">
                    
                    {!! Form::select('location', $data['loc'], '',['class'=>'form-control', 'placeholder'=>'Location']) !!}
                  </div>
                </div>
                 <div class="form-group">
            	 {!! FORM::label('name' ,'Office Address',['class'=>'col-sm-2 control-label']) !!}
                   <div class="col-sm-3">
                   <textarea class="form-control" id="office_address" name="office_address" placeholder="Address"></textarea>
                  </div>
                  {!! FORM::label('name' ,'Home Address',['class'=>'col-sm-2 control-label']) !!}
                   <div class="col-sm-3">
                   <textarea class="form-control" id="home_address" name="home_address" placeholder="Address"></textarea>
                  </div>
                </div>
                <div class="form-group">
                    <label for="inputExperience"  class="col-sm-2 control-label">Experience</label>

                    <div class="col-sm-3">
                      <textarea class="form-control" name="experiance" id="experiance" placeholder="Experience"></textarea>
                    </div>
                    <label for="inputSkills" class="col-sm-2 control-label">Notes</label>

                    <div class="col-sm-3">
                      <textarea class="form-control" name="notes" id="notes" placeholder="Notes"></textarea>
                    </div>
                  </div>
               </fieldset>
              <fieldset>
                  <legend>Educational Detail:</legend>
                  <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">{{ __('School/Collage:') }}</label>
                  <div class="col-sm-3">
                 	<input id="collage" type="text" class="form-control" placeholder='Institute Name' name="collage[]" value="{{ old('collage[]') }}" autofocus>
                  </div>
                  <label for="name" class="col-sm-2 control-label">{{ __('Grade:') }}</label>
                  <div class="col-sm-3">
                 	<input id="grade" type="text" class="form-control" placeholder='Grades' name="grade[]" value="{{ old('grade[]') }}" autofocus>
                  </div>   
                </div>
                <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">{{ __('From Year:') }}</label>
                  <div class="col-sm-3">
                 	<select name='from_year[]' class='form-control'>
                 		<option value='0'>Select From Year</option>
                 		<?php for($i=date('Y'); $i >=1900 ; $i-- ){?>
                 			<option value='<?php echo $i;?>'><?php echo $i;?></option>
                 		<?php } ?>
                 	</select>
                  </div>
                     <label for="name" class="col-sm-2 control-label">{{ __('To Year:') }}</label>
                  <div class="col-sm-3">
					<select name='to_year[]' class='form-control'>
                 		<option value='0'>Select To Year</option>
                 		<?php for($i=date('Y')+7; $i >=1900 ; $i-- ){?>
                 			<option value='<?php echo $i;?>'><?php echo $i;?></option>
                 		<?php } ?>
                 	</select>
                  </div>
                </div>
                    <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">{{ __('Degree:') }}</label>
                  <div class="col-sm-3">
                 	<input id="degree" type="text" class="form-control" placeholder='Degree Name' name="degree[]" value="{{ old('degree[]') }}" autofocus>
                  </div>
                  
                     <label for="name" class="col-sm-2 control-label">{{ __('Degree Image:') }}</label>
                  <div class="col-sm-3">
                 	{!! Form::file('degree_img[]');!!}
                  </div>
                </div>
                <div class="form-group">
                <label for="name" class="col-sm-2 control-label">{{ __('Hobbies:') }}</label>
                  <div class="col-sm-3">
                 	<input id="hobby" type="text" class="form-control" placeholder='Hobbies' name="hobby[]" value="{{ old('hobby[]') }}" autofocus>
                  </div>
                     <label for="name" class="col-sm-2 control-label">{{ __('Description:') }}</label>
                  <div class="col-sm-3">
					<textarea name="edu_desc[]" id="edu_desc" style="width: 100%" cols="10" ></textarea>
                  </div>
                  </div>
                  <div class="form-group">
                  <div class="col-sm-2">
                   </div>
                  	<div class="col-sm-3">
                  	</div>
                   <div class="col-sm-2">
                   </div>
                  	<div class="col-sm-3">
						<input type="button" class="add-row pull-right" value="Add More">
                  	</div>
                </div>
                <div class="new_row" >
                </div>
                 </fieldset>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="{{route('lawyer_profile.index')}}"><button type="button" class="btn btn-default">Back</button></a>
                {!! Form::submit('Submit', ['class'=>'btn btn-info pull-right']) !!}
              </div>
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <!-- /.box-footer -->
            {!! Form::close() !!}
          </div>
         
        </div>
        <!--/.col (right) -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <input type="hidden" value="1" name="row_count" id="row_count" />
<script>
$(document).ready(function(){
    $(".add-row").click(function(){
        var row_count = $('#row_count').val();
        var markup = '<div class="row_'+row_count+'"><fieldset>'+
            			'<legend>Educational Detail:</legend>'+
        '<div class="form-group">'+
        '<label for="name" class="col-sm-2 control-label">School/Collage:</label>'+
        '<div class="col-sm-3">'+
       	'<input id="collage" type="text" class="form-control" placeholder="Institute Name" name="collage[]" autofocus>'+
        '</div>'+
        '<label for="name" class="col-sm-2 control-label">Grade:</label>'+
        '<div class="col-sm-3">'+
       	'<input id="grade" type="text" class="form-control" placeholder="Grades" name="grade[]" autofocus>'+
        '</div>'+
      '</div>'+
      	'<div class="form-group">'+
        '<label for="name" class="col-sm-2 control-label">From Year:</label>'+
        '<div class="col-sm-3">'+
       	'<select name="from_year[]" class="form-control">'+
       		'<option value="0">Select From Year</option>'+
       		<?php for($i=date('Y'); $i >=1900 ; $i-- ){?>
       			'<option value="<?php echo $i;?>"><?php echo $i;?></option>'+
       		<?php } ?>
       	'</select>'+
        '</div>'+
           '<label for="name" class="col-sm-2 control-label">To Year:</label>'+
        	'<div class="col-sm-3">'+
			'<select name="to_year[]" class="form-control">'+
       		'<option value="0">Select To Year</option>'+
       		<?php for($i=date('Y')+7; $i >=1900 ; $i-- ){?>
       			'<option value="<?php echo $i;?>"><?php echo $i;?></option>'+
       		<?php } ?>
       	'</select>'+
        '</div>'+
      '</div>'+
      '<div class="form-group">'+
      '  <label for="name" class="col-sm-2 control-label">Degree:</label>'+
      '<div class="col-sm-3">'+
     	'<input id="degree" type="text" class="form-control" placeholder="Degree Name" name="degree[]" autofocus>'+
     ' </div>'+  
     '<label for="name" class="col-sm-2 control-label">Degree Image:</label>'+
     '<div class="col-sm-3">'+
    	'<input id="degree_img" type="file" class="form-control" placeholder="Degree Name" name="degree_img[]"  autofocus>'+
    	'</div>'+
   		'</div>'+
   		'<div class="form-group">'+
    	   '<label for="name" class="col-sm-2 control-label">Hobbies:</label>'+
           '<div class="col-sm-3">'+
          	'<input id="hobby" type="text" class="form-control" placeholder="Hobbies" name="hobby[]"  autofocus>'+
          	'</div>'+
        '<label for="name" class="col-sm-2 control-label">Description:</label>'+
     '<div class="col-sm-3">'+
		'<textarea name="edu_desc[]" id="edu_desc" style="width: 100%" cols="10" ></textarea>'+
'     </div>'+
'     </div>'+
'<div class="form-group">'+
'    <div class="col-sm-2">'+
'  </div>'+
'  <div class="col-sm-3">'+
'  </div>'+
  '    <div class="col-sm-2">'+
    '  </div>'+
   '  <div class="col-sm-3" style="text-align: right;">'+
		'<input type="button" class="remove-row" value="Remove" onClick="remove_row('+row_count+')">'
    ' </div>'+
  ' </div>'+
	          '</fieldset></div>';
        $(".new_row").append(markup);
        $('#row_count').val(parseInt(row_count)+1);
        $(".selections").select2();
    });
});

function remove_row(row_id){
	$('.row_'+row_id).remove();
}
</script></script>
@endsection 
