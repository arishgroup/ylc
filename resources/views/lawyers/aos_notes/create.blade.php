@extends(Auth::guard('admin')->check() ? 'layouts.admin' : 'layouts.lawyer')


@section('content')
<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    @if ($data['type'] == 'personal')
      <h1>{{ __('Personal Area of Service Content(s)') }}</h1>
   @else
   	<h1>{{ __('Company Area of Service Content(s)') }}</h1>
   @endif   
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('aos.tree')}}">Area of Service</a></li>
        <li><a href="{{route('aos.tree')}}">Personal Content</a></li>
        <li class="active">Add</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info col-md-6">
            <div class="box-header with-border">
              @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::open(['route' => 'aos.content.store', 'method' => 'post', 'class'=>'form-horizontal']) !!}
              {{ csrf_field() }}
              <div class="box-body">
                  <div class="box-body col-md-3">
              
              @component('components.side_menu',['record'=>$record])
			@endcomponent
            
              </div>
              <div class="box-body col-md-9">
              <div class="form-group">
                  {!! FORM::label('name' ,'Area of Service',['class'=>'col-sm-2 control-label']) !!}
                                   <div class="col-sm-5">
                 @if($data['type'] == 'personal')
                 	{!! FORM::text('aos' ,$data['name_aos'],['class'=>'form-control', 'placeholder'=>'Title', 'readonly']) !!}
                 	@else
            			{!! FORM::text('aos' ,$data['name_aos'],['class'=>'form-control', 'placeholder'=>'Title', 'readonly']) !!}
            	   @endif 
                  </div>
                </div>
                <div class="form-group">
                  {!! FORM::label('name' ,'Title',['class'=>'col-sm-2 control-label']) !!}
                                   <div class="col-sm-5">
                 {!! FORM::text('title' ,'',['class'=>'form-control', 'placeholder'=>'Title']) !!}
                  </div>
                </div>
                <div class="form-group">
                 <fieldset>
                  <legend>Add Content:</legend>
                  <div class="form-group">
                  {!! FORM::label('name' ,'Heading',['class'=>'col-sm-2 control-label']) !!}
                                   <div class="col-sm-5">
                 {!! FORM::text('content_heading' ,'',['class'=>'form-control', 'placeholder'=>'Heading']) !!}
                  </div>
                </div>
                <div class="form-group">
                  {!! FORM::label('name' ,'Case Description',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-5">
                    <div class="row">
      					<div class="col-sm-12">{!! FORM::textarea('article_ckeditor' ,'',['class'=>'form-control', 'rows' => 3, 'cols' => 40]) !!}</div>
      					<div class="col-sm-12" style="padding-top:10px">{!! Form::submit('Save & Add More', ['class'=>'btn btn-info pull-right']) !!}</div>
    				</div>
                    
					 </div>
					 <div class="col-sm-1">
					 </div>
					 
			     </div>
                 </fieldset>
                </div>
                 </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
               @if($data['type'] == 'personal')
              <a href="{{route('personal.aos.tree')}}"><button type="button" class="btn btn-default">Back</button></a>
              @else 
              <a href="{{route('aos.tree')}}"><button type="button" class="btn btn-default">Back</button></a>
              @endif
                
                <a href="{{route('aos.content.index', [$data['aos'], $data['type']] )}}"><button type="button" class="btn btn-info pull-right">Close</button></a>
              </div>
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <!-- /.box-footer -->
              
              <input type="hidden" value="{!! $data['type'] !!}" id="content_type" name="content_type" />
              <input type="hidden" value="{!! $data['aos'] !!}" id="aos_id" name="aos_id" />	 
              @if(Auth::guard('admin')->check())
              	<input type="hidden" value="{{ Auth::user()->_id }}" id="admin_id" name="admin_id" />
              @else 
              <input type="hidden" value="{{ Auth::user()->_id }}" id="lawyer_id" name="lawyer_id" />
              @endif
            {!! Form::close() !!}
          </div>
         
        </div>
        <!--/.col (right) -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
	<script>
    	CKEDITOR.replace( 'article_ckeditor' );
	</script>
@endsection 
