@extends(Auth::guard('admin')->check() ? 'layouts.admin' : 'layouts.lawyer')


@section('content')
<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
		
		
		<link href="{{ asset('css/custum_app.css') }}" rel="stylesheet">
		
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      @if($data['type'] == 'personal')
      <h1>{{ __('Personal Area of Service Content(s)') }}</h1>
        @else
            			<h1>{{ __('Company Area of Service Content(s)') }}</h1>
            	   @endif
      
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('aos.tree')}}">Area of Service</a></li>
        @if($data['type'] == 'personal')
        <li><a href="{{route('aos.tree')}}">Personal Content</a></li>
        @else
            			<li><a href="{{route('aos.tree')}}">Company Content</a></li>
            	   @endif
        <li class="active">Add</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- right column -->
        <div class="col-md-12">
        <div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
    <p>Some text in the Modal..</p>
  </div>

</div>

          <!-- Horizontal Form -->
          <div class="box box-info col-md-6">
            <div class="box-header with-border">
              @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::open(['route' => array('aos.content.update', $data->_id), 'method' => 'put', 'class'=>'form-horizontal']) !!}
              {{ csrf_field() }}
              <div class="box-body col-md-3">
              
              @component('components.side_menu',['record'=>$record])@endcomponent
            
              </div>
              <div class="box-body col-md-9">
              	
              	<div class="form-group">
                  {!! FORM::label('name' ,'Area of Service',['class'=>'col-sm-2 control-label']) !!}
                                   <div class="col-sm-5">
                 @if($data['type'] == 'personal')
                 	
                 	{!! FORM::text('aos' , $data->aos_personal->name,['class'=>'form-control', 'placeholder'=>'Title', 'readonly']) !!}
                 	@else
            			{!! FORM::text('aos' ,$data->aos_standard->name,['class'=>'form-control', 'placeholder'=>'Title', 'readonly']) !!}
            	   @endif 
                  </div>
                </div>
                <div class="form-group">
                  {!! FORM::label('name' ,'Title',['class'=>'col-sm-2 control-label']) !!}
                                   <div class="col-sm-5">
                 {!! FORM::text('title' ,$data->title,['class'=>'form-control', 'placeholder'=>'Title']) !!}
                  </div>
                </div>
                <div class="form-group">
                 <fieldset>
                  <legend>Add Content:</legend>
                  <div class="form-group">
                  {!! FORM::label('name' ,'Heading',['class'=>'col-sm-2 control-label']) !!}
                                   <div class="col-sm-5">
                 {!! FORM::text('content_heading' ,'',['class'=>'form-control', 'placeholder'=>'Heading','id'=>'content_heading']) !!}
                  </div>
                  @if($data->getContent->count())
                  <div class="col-sm-4">
                  	<a href="#" onClick="showPopup('{!! $data->_id !!}')"><button id="new_booktype" class="btn btn-primary">View All Content</button></a>
                  </div>
                  @endif
                </div>
                <div class="form-group">
                  {!! FORM::label('name' ,'Case Description',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-5">
                    <div class="row">
      					<div class="col-sm-12">{!! FORM::textarea('article_ckeditor' ,'',['class'=>'form-control', 'rows' => 3, 'cols' => 40]) !!}</div>
      					<div class="col-sm-12" style="padding-top:10px">{!! Form::submit('Save & Add More', ['class'=>'btn btn-info pull-right']) !!}  <div style="display:none" id="cancel_btn"><a href="javascript:void(0)" onClick="cancelUpdate()"><button style="margin-right:05px" type="button" class="btn btn-default pull-right">Cancel</button></a></div> </div>
    				</div>
                    
					 </div>
					 <div class="col-sm-4" style="max-height: 345px;overflow-y: scroll;">
					<table id="example1" class="table table-bordered table-striped">
                		
                	<tbody>
                	
					 	@if($data->getContent->count())
					 	@foreach ($data->getContent as $content)
					 	<tr>
                		  	<td width="70%"><a href="{{route('aos.content.view', [$content->_id])}}" target="_blank" >{!! $content->heading !!}</a></td>
                  			<td width="30%"><a href="#" onClick="getData('{!! $content->_id !!}')">Update</a> | <a class="confirmation" href="{{ route('aos.content.destroy',  array($content->_id,$data->type)) }}">Delete</a></td>
                		</tr>
					 	
					 	@endforeach
					 	@endif
					 	              </table>
					 	
					 </div>
			     </div>
                 </fieldset>
                </div>
                 
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="{{route('aos.content.index', [$data->aos_id, $data->type])}}"><button type="button" class="btn btn-default">Back</button></a>
                <a href="{{route('aos.content.index', [$data->aos_id, $data->type] )}}"><button type="button" class="btn btn-info pull-right">Close</button></a>
              </div>
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <!-- /.box-footer -->
              
              <input type="hidden" value="{!! $data->type !!}" id="content_type" name="content_type" />
              <input type="hidden" value="" id="content_id" name="content_id" />
              <input type="hidden" value="{!! $data->aos_id !!}" id="aos_id" name="aos_id" />	 
              <input type="hidden" value="{{ Auth::user()->_id }}" id="lawyer_id" name="lawyer_id" />
            {!! Form::close() !!}
          </div>
         
        </div>
        <!--/.col (right) -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
  <script src="{{ asset('js/jquery.dlmenu.js') }}" ></script>
	<script>
    	CKEDITOR.replace( 'article_ckeditor' );

    	function cancelUpdate(){
    		$('#content_heading').val('');
			$('#content_id').val('');
			CKEDITOR.instances['article_ckeditor'].setData('')
			document.getElementById('cancel_btn').style.display = 'none';
            }
        
    	function getData(id){
    		$.ajax({url: "/aos/get_content/"+id, success: function(result){
				$('#content_heading').val(result['content']['head']);
				$('#content_id').val(id);
				CKEDITOR.instances['article_ckeditor'].setData(result['content']['descr'])
				document.getElementById('cancel_btn').style.display = 'block';
		    }});
		}

    	var modal = document.getElementById('myModal');
 	   
        var span = document.getElementsByClassName("close")[0];
    	// When the user clicks the button, open the modal 
        function showPopup(id){
        	event.preventDefault();
        	var section_id = id;
        	$(".modal-content").html('');
            $.get( "/aos/getContent?id=" + section_id+"&t=<?php echo time();?>", function( data ) {
               
               $(".modal-content").html(data.content);
               
            });
        	
            modal.style.display = "block";
        }
        
        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }
        
        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }

    </script>
  
		
@endsection 
