@extends(Auth::guard('admin')->check() ? 'layouts.admin' : 'layouts.lawyer')

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      @if($data['type'] == 'personal')
      <h1>{{ __('Personal Notes') }}</h1>
       @else
    	<h1>{{ __('Company Notes') }}</h1>
    @endif  
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Notes(s)</a></li>
        <li class="active">View</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
            <div class="box-header" style="float: right">
           
            @if($data['type'] == 'personal')
           		<a href="{{route('aos.content.edit', [$data['content_detail']->getAos->_id])}}"><button id="new_booktype" class="btn btn-primary">Back</button></a>
            @else
           		<a href="{{route('aos.content.edit', [$data['content_detail']->getAos->_id])}}"><button id="new_booktype" class="btn btn-primary">Back</button></a>
            @endif  
          	</div>
            <div class="container-fluid" style="padding-bottom:20px;float:left; width:100%">
              <div class="row" style='padding-bottom:10px'>
              	<div class="col-md-4"><b>Lawyer Name: </b>{!! $data['lawyer']->name." ".$data['lawyer']->lastname !!}</div>
              	<div class="col-md-4"><b>Serial Number: </b>{!! $data['lawyer']->serial_no !!}</div>
              	<div class="col-md-4"><b>Public Serial Number: </b>{!! $data['lawyer']->public_serial_no !!}</div>
              </div>
              <div class="row" style='padding-bottom:10px'>
              	<div class="col-md-4"><b>Email: </b>{!! $data['lawyer']->email !!}</div>
              	<div class="col-md-4"><b>Company Area of Service: </b>@if($data['lawyer']->area_of_service){!! $data['lawyer']->aos->name !!}@endif</div>
              	<div class="col-md-4"><b>Personal Area of Service: </b>{!! $data['aos']->aos_personal->name !!}</div>
              </div>
               <div class="row" style='padding-bottom:10px'>
              	<div class="col-md-4"><b>Head Title: </b>{!! $data['aos']->title !!}</div>
              	
              </div>
              </div>
              <div class="container-fluid" style="padding-bottom:20px;float:left; width:100%">
              <div class="row" style='padding-bottom:10px'>
            <hr>
            </div></div>
             <div class="container-fluid" style="padding-bottom:20px;float:left; width:100%">
             	<div class="row">
             		<div class="col-md-1"><b>Heading:</b></div>
             		<div class="col-md-8">{!! $data['content_detail']->heading !!}</div>
             	</div>
             	<div class="row">
             		<div class="col-md-1"><b>Description:</b></div>
             		<div class="col-md-8">{!! $data['content_detail']->description !!}</div>
             	</div>
             </div>
            </div>
                
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
<script type="text/javascript">
    
    var modal = document.getElementById('myModal');
   
    var span = document.getElementsByClassName("close")[0];

    function showAllContent(aos_id, type){
        
    	var section_id = aos_id;
    	$(".modal-content").html('');
        $.get( "/aos/getFullContent?id=" + section_id+"&t=<?php echo time();?>&type="+type, function( data ) {
           
           $(".modal-content").html(data.content);
           
        });
    	
        modal.style.display = "block";
    }
    
    // When the user clicks the button, open the modal 
    function showPopup(id){
      
    	var section_id = id;
    	$(".modal-content").html('');
        $.get( "/aos/getContent?id=" + section_id+"&t=<?php echo time();?>", function( data ) {
           
           $(".modal-content").html(data.content);
           
        });
    	
        modal.style.display = "block";
    }
    
    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }
    
    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>
@endsection 
