@extends(Auth::guard('admin')->check() ? 'layouts.admin' : 'layouts.lawyer')

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      @if($data['type'] == 'personal')
      <h1>{{ __('Personal Notes') }}</h1>
       @else
    	<h1>{{ __('Company Notes') }}</h1>
    @endif  
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Lawyer(s)</a></li>
        <li class="active">View</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
    <p>Some text in the Modal..</p>
  </div>

</div>

          <div class="box">
            <div style="float:right">
            <div class="box-header" style="float: right">
           
            @if($data['type'] == 'personal')
           		<a href="{{route('personal.aos.tree')}}"><button id="new_booktype" class="btn btn-primary">Back</button></a>
              	<a href="{{route('personal.aos.notes', [$data['aos_id']])}}"><button id="new_booktype" class="btn btn-primary">Add Content(s)</button></a>
            @else
           		<a href="{{route('aos.tree')}}"><button id="new_booktype" class="btn btn-primary">Back</button></a>
            	<a href="{{route('standard.aos.notes', [$data['aos_id']])}}"><button id="new_booktype" class="btn btn-primary">Add Content(s)</button></a>
            @endif  
            
          	@if($data['type'] == 'personal')
          	<a href="javascript:void(0)" onclick="return showAllContent('{!! $data['aos_id'] !!}', 'personal');"><button id="new_booktype" class="btn btn-primary">View All Content</button></a>
          	@else
    	<a href="javascript:void(0)" onclick="return showAllContent('{!! $data['aos_id'] !!}', 'standard');"><button id="new_booktype" class="btn btn-primary">View All Content</button></a>
   			 @endif
   			  
          	</div>
          	</div>
            <div>
             @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
            </div>
            <!-- /.box-header -->
            <div class="box-body">
           	  <div class="float:left; width:100%"><b>@if($data['type'] == 'personal'){!! 'Personal'!!}@else {!! 'Company' !!} @endif  Area of Service: </b>{!! $data['aos_name'] !!}</div>            
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Sr.</th>
                  <th>Title</th>
                  <th>Area of Service</th>
				  <th>Updated Date</th>
                  <th>Action</th>
                  
                </tr>
                </thead>
                <tbody>
                <?php $i = 0 ;?>
                @if($data['aos_content']->count())
                @foreach($data['aos_content'] as $content)
                <tr>
                  <td>{!! $i = $i+1 !!}</td>
                  <td><a href="javascript:void(0)" onClick="showPopup('{!! $content->_id !!}')">{!! $content->title !!}</a></td>
                   @if($data['type'] == 'personal')
                  		<td>{!! $content->aos_personal->name !!}</td>
                   @else
            			<td>{!! $content->aos_standard->name !!}</td>
            	   @endif 
                  <td>{!! $content->updated_at !!}</td>
                  
                  <td><a href="{{ route('aos.content.edit',  $content->_id) }}"><i class="fa fa-edit"></i></a> | <a class="confirmation" href="{{ route('aos.parent.content.destroy', $content->_id) }}"><i class="fa fa-trash"></i></a></td>
                </tr>
                @endforeach
                @else
                <tr>
                  <td colspan="5">No Record Found!</td>
                </tr>
                @endif
                
                </tfoot>
              </table>
            </div>
            <div style="text-align:right; padding-right: 10px;">{{ $data['aos_content']->links() }}</div>    
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
<script type="text/javascript">
    
    var modal = document.getElementById('myModal');
   
    var span = document.getElementsByClassName("close")[0];

    function showAllContent(aos_id, type){
        
    	var section_id = aos_id;
    	$(".modal-content").html('');
        $.get( "/aos/getFullContent?id=" + section_id+"&t=<?php echo time();?>&type="+type, function( data ) {
           
           $(".modal-content").html(data.content);
           
        });
    	
        modal.style.display = "block";
    }
    
    // When the user clicks the button, open the modal 
    function showPopup(id){
      
    	var section_id = id;
    	$(".modal-content").html('');
        $.get( "/aos/getContent?id=" + section_id+"&t=<?php echo time();?>", function( data ) {
           
           $(".modal-content").html(data.content);
           
        });
    	
        modal.style.display = "block";
    }
    
    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }
    
    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>
@endsection 
