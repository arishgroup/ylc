@extends('layouts.admin')


@section('content')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" />
	<script src="http://demo.itsolutionstuff.com/plugin/jquery.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script>
<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>{{ __('Content Library') }}</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('aos.index')}}">Content Library</a></li>
        <li class="active">Registration</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info col-md-6">
            <div class="box-header with-border">
              @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::open(['route' => ['lawyers.datalib.update',$content->_id], 'method' => 'post', 'class'=>'form-horizontal','files'=>'true']) !!}
              {{ csrf_field() }}
              <div class="box-body">
            
                <div class="form-group">
                  {!! FORM::label('name' ,'File upload',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-3">
                 {!! Form::file('image');!!}
                 @if($content->file_ext != "")
                 	<a href="{{ route('lawyers.datalib.download', encrypt($content->_id)) }} ">Download</a>
                 @endif
                  </div>
               
                  {!! FORM::label('name' ,'Area of Service',['class'=>'col-sm-2 control-label']) !!}
                  <div class="col-sm-3">
                 {!! Form::select('area_of_service', $aos,$content->aos,['class'=>'form-control', 'placeholder'=>'Select Area of Service'] ) !!}
                  </div>
                 </div>
                <div class="form-group">
                  {!! FORM::label('name' ,'Title',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-3">
                 {!! FORM::text('title' ,$content->title,['class'=>'form-control', 'placeholder'=>'Title']) !!}
                  </div>
                  {!! FORM::label('name' ,'Court',['class'=>'col-sm-2 control-label']) !!}
                  <div class="col-sm-3">
                 	{!! Form::select('court', $court,$content->court,['class'=>'form-control', 'placeholder'=>'Select Court'] ) !!}
                  </div>
                </div>
                <div class="form-group">
                  {!! FORM::label('name' ,'Appellant',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-3">
                 {!! FORM::text('appellant' ,$content->applent,['class'=>'form-control', 'placeholder'=>'Appellant']) !!}
                  </div>
                  {!! FORM::label('name' ,'Verses',['class'=>'col-sm-2 control-label']) !!}
                  <div class="col-sm-3">
                 {!! FORM::text('verses' ,$content->verses,['class'=>'form-control', 'placeholder'=>'Verses']) !!}
                  </div>
                </div>
                <div class="form-group">
                  {!! FORM::label('name' ,'Page No.',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-3">
                 {!! FORM::text('page_no' ,$content->page_no,['class'=>'form-control', 'placeholder'=>'Page No.']) !!}
                  </div>
                  {!! FORM::label('name' ,'Referance',['class'=>'col-sm-2 control-label']) !!}
                  <div class="col-sm-3">
                 {!! FORM::text('referance' ,$content->reference,['class'=>'form-control', 'placeholder'=>'Referance']) !!}
                  </div>
                </div>
                <div class="form-group">
                  {!! FORM::label('name' ,'Judge Name',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-3">
                 {!! FORM::text('judge_name' ,'',['class'=>'form-control', 'placeholder'=>'Judge Name']) !!}
                  </div>
                  {!! FORM::label('name' ,'Lawyer Name ',['class'=>'col-sm-2 control-label']) !!}
                  <div class="col-sm-3">
                 {!! FORM::text('lawyer_name' ,$content->lawyer_name,['class'=>'form-control', 'placeholder'=>'Lawyer Name']) !!}
                  </div>
                </div>
                 <div class="form-group">
                  {!! FORM::label('name' ,'Citation Name',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-3">
                 {!! FORM::text('citation_name' ,$content->citation_name,['class'=>'form-control', 'placeholder'=>'Citation Name']) !!}
                  </div>
                  {!! FORM::label('name' ,'Journel',['class'=>'col-sm-2 control-label']) !!}
                  <div class="col-sm-3">
                 {!! Form::select('journel', $journel,$content->journel,['class'=>'form-control', 'placeholder'=>'Select Journel'] ) !!}
                  </div>
                  </div>
                   <div class="form-group">
                  {!! FORM::label('name' ,'Category',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-3">
                 {!! Form::select('category', $category,$content->category,['class'=>'form-control', 'placeholder'=>'Select Category'] ) !!}
                  </div>
                  {!! FORM::label('name' ,'Journel Type',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-3">
                 {!! Form::select('jurnel_type', $journel_type,$content->jurnel_type,['class'=>'form-control', 'placeholder'=>'Select Journel Type'] ) !!}
                  </div>
                </div>
                <div class="form-group">
                  {!! FORM::label('name' ,'Publish Month',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-3">
                 {!! FORM::text('publish_month' ,$content->publish_month,['class'=>'form-control', 'placeholder'=>'Publish Month']) !!}
                  </div>
                  {!! FORM::label('name' ,'Publish Year',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-3">
                 {!! FORM::text('publish_year' ,$content->publish_year,['class'=>'form-control', 'placeholder'=>'Pubish Year']) !!}
                  </div>
                </div>
        		<div class="form-group">
        		 {!! FORM::label('name' ,'Tags',['class'=>'col-sm-2 control-label']) !!}
                  <div class="col-sm-3">
                 {!! FORM::text('tags' ,'',['class'=>'form-control', 'placeholder'=>'Tags','data-role'=>'tagsinput']) !!}
                  </div>  
		
			
					@if ($errors->has('tags'))
                		<span class="text-danger">{{ $errors->first('tags') }}</span>
            		@endif
				</div>	
                   <div class="form-group">
                  {!! FORM::label('name' ,'Description',['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-8">
                    {!! FORM::textarea('article_ckeditor' ,$content->description,['class'=>'form-control', 'rows' => 3, 'cols' => 40]) !!}
                 
					 </div>
                 </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="{{route('lawyers.datalib.index')}}"><button type="button" class="btn btn-default">Back</button></a>
                {!! Form::submit('Submit', ['class'=>'btn btn-info pull-right']) !!}
              </div>
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
             
               {!! FORM::hidden('_id' ,$content->_id) !!}
              <!-- /.box-footer -->
         
            {!! Form::close() !!}
          </div>
         
        </div>
        <!--/.col (right) -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
	<script>
    	CKEDITOR.replace( 'article_ckeditor' );
	</script>
@endsection 
