@extends(Auth::guard('admin')->check() ? 'layouts.admin' : 'layouts.lawyer')
@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                News
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#"><i class="fa fa-tasks"></i> News</a></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-12">
                    <!-- small box -->
                    <!-- TO DO List -->
                    <div class="box box-primary">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="box-header with-border">
                                @if (Session::has('message'))
                                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                                @endif
                            </div>
                            <!-- See dist/js/pages/dashboard.js to activate the todoList plugin -->
                            <div class="box-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Sr.</th>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i = 0 ;?>
                                    @if($data->count())
                                        @foreach($data as $news)
                                            <tr>
                                                <td>{!! $i = $i+1 !!}</td>
                                                <td>{!! $news->title !!} @if($news->alert == 1) <small class="pull-right label label-danger"><i class="fa fa-clock-o"></i> Alert </small> @endif</td>
                                                <td>{!! $news->description !!}</td>
                                                <td>
                                                    <a data-toggle="tooltip" data-placement="top" data-original-title="Detail" href="#"><i class="fa fa-book"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="5">No Record Found!</td>
                                        </tr>
                                    @endif

                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /.box -->
                </div>

            </div>
            <!-- /.row -->

        </section>
        <!-- /.content -->
    </div>

@endsection
