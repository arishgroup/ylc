@extends('layouts.admin')

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>{{ __('Lawyer(s) Listing') }}</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Staff</a></li>
        <li><a href="#">Lawyer(s)</a></li>
        <li class="active">View</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box">
            <div class="box-header" style="float: right">
            
              <a href="{{route('lawyers.register', ['aos_id'=>$aos])}}"><button id="new_booktype" class="btn btn-primary">Add New Lawyer</button></a>
            </div>
            <div>
             @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            
            
             @if( $aos_detail )
             <div class="float:left; width:100%"><b>Area of Service: </b>{!! $aos_detail->name !!}</div>
             @endif
             <div id="table_wrap" style="width:100%;float:left;">
              <table id="assignee_table" class="table table-striped table-bordered" >
                  <thead>
                  <tr>
                      <th>Sr.</th>
                      <th>Name</th>
    				  <th>Serial No.</th>	
    				  <th>Public Serial No.</th>	
    				  <th>Email</th>	
                      <th>Area of Service</th>
                      <th>Action</th>
                  </tr>
                  </thead>
                </table>
                </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@stop
@push('scripts')
  <script>
$(function() {
          $('#assignee_table').DataTable({
              destroy: true,
              processing: true,
              serverSide: true,
              ajax: '{!! route("lawyer.datatable") !!}?aos={!! $aos !!}',
              columns: [
                  { data: '_id', name: '_id' },
                  { data: 'name', name: 'name' },
                  { data: 'serial_no', name: 'serial_no' },
                  { data: 'public_serial_no', name: 'public_serial_no' },
                  { data: 'email', name: 'email' },
                  { data: 'area_of_service', name: 'area_of_service' },
                  { data: 'action', name: 'action', orderable: false, searchable: false}
              ]
          }); 
          
});
</script>
@endpush