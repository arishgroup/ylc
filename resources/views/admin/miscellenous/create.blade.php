@extends(Auth::guard('admin')->check() ? 'layouts.admin' : 'layouts.backendUser')


@section('content')
<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>{{ __('Miscllaneous Document(s)') }}</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('role.index')}}">Content Managment System(s)</a></li>
        <li class="active">Add Miscllaneous Document</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info col-md-6">
            <div class="box-header with-border">
              @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::open(['route' => 'miscellaneous.store', 'method' => 'post', 'class'=>'form-horizontal','files'=>'true']) !!}
              {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group">
                  {!! FORM::label('name' ,'File upload',['class'=>'col-sm-2 control-label']) !!}
                                   <div class="col-sm-5">
                 {!! Form::file('image') !!}
                  </div>
                </div>
                <div class="form-group">
                  {!! FORM::label('name' ,'Subject',['class'=>'col-sm-2 control-label']) !!}
                                   <div class="col-sm-5">
                 {!! FORM::text('subject' ,'',['class'=>'form-control', 'placeholder'=>'Subject']) !!}
                  </div>
                </div>
                <div class="form-group">
                  {!! FORM::label('name' ,'Notification No.',['class'=>'col-sm-2 control-label']) !!}
                                   <div class="col-sm-5">
                 {!! FORM::text('notification_no' ,'',['class'=>'form-control', 'placeholder'=>'Notification No.']) !!}
                  </div>
                </div>
                 <div class="form-group">
                  {!! FORM::label('name' ,'Institution',['class'=>'col-sm-2 control-label']) !!}
                                   <div class="col-sm-5">
                 {!! Form::select('institution_id', $ins_list, '',['class'=>'form-control', 'placeholder' => 'Select Institute']) !!}
                  </div>
                </div>
                <div class="form-group">
                  {!! FORM::label('name' ,'Document Type',['class'=>'col-sm-2 control-label']) !!}
                                   <div class="col-sm-5">
                 {!! Form::select('doc_type_id', $doc_type_list, '',['class'=>'form-control', 'placeholder' => 'Select Institute']) !!}
                  </div>
                </div>
                 <div class="form-group">
                {!! FORM::label('name' ,'Tags',['class'=>'col-sm-2 control-label']) !!}
                  <div class="col-sm-5">
                 	<select id="tags" name="tags[]" class="form-control" multiple></select>
                 	
                  </div> 
                  </div>
                 <div class="form-group">
                  {!! FORM::label('name' ,'Year',['class'=>'col-sm-2 control-label']) !!}
                                   <div class="col-sm-5">
                 {!! FORM::text('year' ,'',['class'=>'form-control', 'placeholder'=>'Year']) !!}
                  </div>
                </div>
                 <div class="form-group">
                  {!! FORM::label('name' ,'Month',['class'=>'col-sm-2 control-label']) !!}
                                   <div class="col-sm-5">
                 {!! FORM::text('month' ,'',['class'=>'form-control', 'placeholder'=>'Month']) !!}
                  </div>
                </div>
                
                <div class="form-group">
                  {!! FORM::label('name' ,'Description',['class'=>'col-sm-2 control-label']) !!}
                   <div class="col-sm-5">
                   {!! FORM::textarea('description' ,'',['class'=>'form-control', 'rows' => 3, 'cols' => 40]) !!}
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="{{route('miscellaneous.index')}}"><button type="button" class="btn btn-default">Back</button></a>
                {!! Form::submit('Submit', ['class'=>'btn btn-info pull-right']) !!}
              </div>
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <!-- /.box-footer -->
            {!! Form::close() !!}
          </div>
         
        </div>
        <!--/.col (right) -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
	<script>
    	CKEDITOR.replace( 'description' );
	</script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script>
        $('#tags').select2({
            placeholder: "Choose tags...",
            minimumInputLength: 2,
            ajax: {
                url: '/tags/find',
                dataType: 'json',
                data: function (params) {
                    return {
                        q: $.trim(params.term)
                    };
                },
                processResults: function (data) {
                    console.log(data);
                    return {
                        results: data[0]
                    };
                },
                cache: true
            }
        });
    </script>
@endsection 
