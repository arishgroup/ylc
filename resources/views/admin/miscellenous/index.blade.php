@extends(Auth::guard('admin')->check() ? 'layouts.admin' : 'layouts.backendUser')


@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>{{ __('Miscllaneous Document(s)') }}</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Book(s) For Content(s)</a></li>
        <li class="active">Add</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box">
            <div class="box-header" style="float: right">
              <a href="{{route('miscellaneous.create')}}"><button id="new_booktype" class="btn btn-primary">Add Miscllaneous Document(s)</button></a>
            </div>
            <div>
             @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <div style="float:right"><input id="myInput" type="text" placeholder="Search.."></div>
           
              <table  class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Sr.</th>
                  <th>Subject</th>
                  <th>Notification No.</th>
                  <th>Document Type</th>
                  <th>Institution</th>
                  <th>Tags</th>
                  <th>Updated Date</th>
                  <th>Action</th>
                  
                </tr>
                </thead>
                <tbody id="tableGrid">
                <?php $i=0;?>
              @if($circular_list->count())
               	@foreach($circular_list as $val)
               	<tr>
                  <td>{!! $i = $i+1 !!}</td>
                  <td>{!! $val->subject !!}</td>
                  <td>{!! $val->notification_no !!}</td>
                  <td>@if($val->doc_type_id){!! $val->getDocType->name !!}@endif</td>
                  <td>@if($val->institution_id){!! $val->getInstitute->name !!}@endif</td>
                  <td>@foreach($val->getTags as $tags) <label class="label label-info">{!! $tags->title !!}</label> @endforeach</td>
                  <td>{!! $val->updated_at !!}</td>
                  <td><a href="{{ route('miscellaneous.edit', ['id'=>$val->_id]) }}"><i class="fa fa-edit"></i></a> | <a class="confirmation" href="{{ route('miscellaneous.destroy', $val->_id) }}"><i class="fa fa-trash"></i></a></td>
                </tr>
                 @endforeach
                @else
                <tr id="no_record">
                  <td colspan="8">No Record Found!</td>
                </tr>
                @endif   
			  </table>
			   <div style="text-align:right; padding-right: 10px;">{{ $circular_list->links() }}</div>  
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection 
