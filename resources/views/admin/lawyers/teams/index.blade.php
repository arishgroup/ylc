@extends('layouts.admin')

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>{{ __('Lawyer Team(s)') }}</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Lawyers</a></li>
        <li class="active">Team Listing</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box">
            <div class="box-header" style="float: right">
              <a href="{{route('team.create',['0'])}}"><button id="new_booktype" class="btn btn-primary">Create New Team</button></a>
            </div>
            <div>
             @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered" id="users-table">
        <thead>
            <tr>
                <th>Title</th>
                <th>Area of Service</th>
                <th>Description</th>
                <th>Team Head</th>
                 <th>Other Members</th>
                <!--<th>Description</th> -->
                <th>Created At</th>
                <th>Updated At</th>
            </tr>
        </thead>
    </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    
@stop

@push('scripts')
<script>
$(function() {
    $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('datatables.teams') !!}',
        columns: [
            
            { data: 'title', name: 'title' },
            { data: 'aos', name: 'aos' },
            { data: 'description', name: 'description' },
            { data: 'teamhead', name: 'teamhead' },
            { data: 'member_count', name: 'memcount' },
            { data: 'created_at', name: 'created_at' },
            { data: 'updated_at', name: 'updated_at' }
        ]
    });
});
</script>
@endpush