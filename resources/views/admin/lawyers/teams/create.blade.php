@extends('layouts.admin')


@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>{{ __('Lawyer Team(s)') }}</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('role.index')}}">Lawyers</a></li>
        <li class="active">Teams</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          
          <div class="row">
                    <div class="col-xs-6">

                        <!-- right column -->
                        <div class="box box-info col-md-6">
            <div class="box-header with-border">
              @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
            </div>
            @component('components.top_navigation')
            	@slot('table_model')
            		LawyerTeam
            	@endslot
            @endcomponent
            <div class="box-header"></div>
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::open(['route' => 'lawyer.teams.store', 'method' => 'post', 'class'=>'form-horizontal' ,'files'=>'true']) !!}
              {{ csrf_field() }}
              <div class="box-body">
               <div class="col-sm-12"> 
                    <div class="form-group">
                      <label for="name" class="col-sm-4 control-label">{{ __('Team Tile:') }}<span class="label_red"> *</span></label>
                      <div class="col-sm-7">
                      {!! FORM::text('title' ,'',['class'=>'form-control', 'placeholder'=>'Team Title', 'id'=>'title']) !!}
                      @if ($errors->has('title'))
                          <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('title') }}</strong>
                          </span>
                      @endif
                      </div>
                    </div>
                    <div class="form-group">
                      {!! FORM::label('name' ,'Description',['class'=>'col-sm-4 control-label']) !!}
                       <div class="col-sm-7">
                       {!! FORM::textarea('description' ,'',['class'=>'form-control', 'rows' => 3, 'cols' => 40, 'id'=>'description']) !!}
                      </div>
                    </div>
                     <div class="form-group">
                      {!! FORM::label('name' ,'Area of Service',['class'=>'col-sm-4 control-label']) !!}
                       <div class="col-sm-7">
                       
                       	<select id="aos_id" name="aos_id" onChange="getLawyers(this.value)" class="aos_id" >
                       		<option value="">Select Area Of Service</option>
                       		@if($data['aos'])
                       			@foreach($data['aos'] as $key => $aos)
                       				<option value="{{ $key}}" @if($data['aos_id'] == $key ) selected="selected" @endif>{{ $aos}}</option>
                       			@endforeach
                       		@else 
                       			<option value="0">No Record Found.</option>
                       		@endif
                       	</select>
                      </div>
                    </div>
                    <div class="form-group" id='team_head' style="display: none">
                      {!! FORM::label('name' ,'Team Head',['class'=>'col-sm-4 control-label']) !!}
                       <div class="col-sm-7">
                       	<select id="head_lawyer" name="head_lawyer" onchange="refreshDropdown(this.value)" >
                       		<option value="">Select Team Head</option>
                       		
                       	</select>
                      </div>
                    </div>
                    <div class="form-group" id='other_member' style="display: none">
                      {!! FORM::label('name' ,'Other Team Members',['class'=>'col-sm-4 control-label']) !!}
                       <div class="col-sm-7">
                       <div style="float:left">
                       <select id='MasterSelectBox' name="masterSelectBox" class="form-control" multiple>
                                        	
                                        </select> 
                          </div>
                          <div  style="float:left; padding-left:10px; padding-right:10px">
                          		<button id="btnAdd">></button><br>
								<button id="btnRemove"><</button>
						  </div>
						  <div class="mr-1" style="float:left">
                                	<select id="PairedSelectBox" name="PairedSelectBox[]" multiple></select>
                            	</div>
                      </div>
                    </div>
              	</div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="{{route('lawyer.teams.index')}}"><button type="button" class="btn btn-default">Back</button></a>
                {!! Form::submit('Submit', ['class'=>'btn btn-info pull-right']) !!}
              </div>
              
              </div>
              
              
              
              <input type="hidden" name="_id" id="_id" value="">
              <input type="hidden" name="created_date" id="created_date" value="">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <!-- /.box-footer -->
            {!! Form::close() !!}
          </div>
        			<div class="col-xs-6">
                        <div class="box">
                        	<div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Lawyer Team(s)</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <table id="team_table" class="assignee_table display" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Team Name</th>
                                        <th>Area of Service</th>
                                        <th>Team Head</th>
                                        <th>Team Memders</th>
                                        <th>Total Cases</th>
                                        <th>Create Date</th>
                                    </tr>
                                    </thead>
                                </table>

                            </div>
                            <!-- /.box-body -->
                        </div>
                        </div></div>
                         </div>
              
              
             
        </div>
        <!--/.col (right) -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@stop

@push('scripts')
		<script src="{{ asset('js/pair-select.js') }}"></script>
		<script src="{{ asset('js/pair-select.min.js') }}"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script>
var global_var = "";
function getLawyers(val){
	$.ajax({url: "/admin/get_lawyers/"+val,async: false,success: function(result){
		document.getElementById('team_head').style.display = 'block';
		document.getElementById('head_lawyer').innerHTML = result.data;
		document.getElementById('MasterSelectBox').innerHTML = result.data;
		global_var = result.data;
		$("#MasterSelectBox option[value='0']").remove();
		
		
	//	if(result.data != "")
	//		document.getElementById('city_div').style.display = 'inline-block';
    }});
}

$(document).ready( function(){

 	$('#MasterSelectBox').pairMaster();
    $('#btnAdd').click(function(){
        event.preventDefault()
        $('#MasterSelectBox').addSelected('#PairedSelectBox');
    });
    $('#btnRemove').click(function(){
        event.preventDefault()
        $('#PairedSelectBox').removeSelected('#MasterSelectBox');
    });

   // $('.aos_id').select2();
   
    
});
function refreshDropdown(val){
	document.getElementById('other_member').style.display = 'block';
	document.getElementById('MasterSelectBox').innerHTML = global_var;
	$("#MasterSelectBox option[value='0']").remove();
	$("#MasterSelectBox option[value='"+val+"']").remove();

	$("#PairedSelectBox option").each(function()
	{
	    $("#MasterSelectBox option[value='"+$(this).val()+"']").remove();
	});
	$("#PairedSelectBox option[value='"+val+"']").remove();
}

$('#team_table').DataTable({
    processing: true,
    serverSide: true,
    "order": [5, "desc" ],
    ajax: '{!! route("lawyers.allteams") !!}',
    columns: [
        { data: 'title', name: 'title' },
        { data: 'aos', name: 'aos' },
        { data: 'teamhead', name: 'teamhead' },
        { data: 'member_count', name: 'member_count' },
        { data: 'totalcases', name: 'totalcases' },
        { data: 'updated_at', name: 'updated_at' }
    ]
});
    
<?php if($data['aos_id'] != "0"){ ?>
    getLawyers('<?php echo $data["aos_id"]; ?>');
<?php } ?>
function show_result(rs){
	$('#created_date').val(rs.data['created_at']);
	$('#_id').val(rs.data['_id']);
	$('#title').val(rs.data['title']);
	$('#description').val(rs.data['title']);
	$("#aos_id").val(rs.data['aos_id']);
	getLawyers(rs.data["aos_id"]);
	if(rs.data['team_head'] != ""){
		$("#head_lawyer").val(rs.data['team_head']);
	}

	if(rs.data['team_head'] != ""){
		document.getElementById('other_member').style.display = 'block';
		console.log(rs.data['lawyer_registration_ids']);
		$('#PairedSelectBox').empty();
		if(rs.data['lawyer_registration_ids'] && rs.data['lawyer_registration_ids'] != ""){
    		for(var i = 0; i < rs.data['lawyer_registration_ids'].length; i++){
    			var id_val = rs.data['lawyer_registration_ids'][i];
    			master_val = $("#MasterSelectBox option[value='"+id_val+"']");
    			
    			$("#PairedSelectBox").append("<option value=" + id_val +">"+master_val.text()+"</option>");
    
    			$("#MasterSelectBox option[value='"+id_val+"']").remove();
    			$("#MasterSelectBox option[value='"+rs.data['team_head']+"']").remove();
    			
    		}
		}
	}
}
</script>
@endpush
