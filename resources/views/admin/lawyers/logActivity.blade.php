@extends('layouts.admin')


@section('content')

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>{{ __('Activity Log(s)') }}</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Staff</a></li>
        <li><a href="{{route('lawyers.index')}}">Lawyer(s)</a></li>
        <li class="active">Activity Log</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
          @component('components.lawyer_info',['record'=>$data['userData']])@endcomponent
       
        <div class="col-xs-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              
	<ul class="timeline">
@if($data['logs']->count())
	@php $c_date = ""; @endphp
	@foreach($data['logs'] as $key => $log)
    <!-- timeline time label -->
    	@php $db_create_date = date('d-M-y',strtotime($log->created_at)) @endphp
    	@if($c_date != $db_create_date)
    	@php $c_date = $db_create_date; @endphp
        <li class="time-label">
            <span class="bg-red">
                {!! $db_create_date !!}
            </span>
        </li>
        @endif
    <!-- /.timeline-label -->

    <!-- timeline item -->
    <li>
        <!-- timeline icon -->
        <i class="fa fa fa-circle-o bg-blue"></i>
        <div class="timeline-item">
            <span class="time"><i class="fa fa-clock-o"></i> {!! date('g:i:s A',strtotime($log->created_at)) !!}</span>

            <!-- <h3 class="timeline-header"><a href="#">Support Team</a> ...</h3>  -->

            <div class="timeline-body">
                {{ $log->subject }}
            </div>
		@if(false && $log->log_name!= 'login' )
            <div class="timeline-footer">
                <a class="btn btn-primary btn-xs">view</a>
            </div>
        @endif
        </div>
    </li>
    @endforeach
		@endif
    <!-- END timeline item -->
</ul>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
    <p>Some text in the Modal..</p>
  </div>

</div>

@endsection
