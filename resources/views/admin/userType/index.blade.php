@extends('layouts.admin')


@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>{{ __('User Type(s)') }}</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master Data</a></li>
        <li><a href="#">User Type(s)</a></li>
        <li class="active">Listing</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box">
            <div class="box-header" style="float: right">
              <a href="{{route('utype.create')}}"><button id="new_booktype" class="btn btn-primary">Add User Type(s)</button></a>
            </div>
            <div>
             @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div id="table_wrap" style="width:100%;float:left;">
              <table id="assignee_table" class="table table-striped table-bordered" >
                  <thead>
                  <tr>
                      <th>Sr.</th>
                      <th>Name</th>
                      <th>Updated Date</th>
                      <th>Action</th>
                  </tr>
                  </thead>
                </table>
                </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@stop
@push('scripts')
  <script>
$(function() {
          $('#assignee_table').DataTable({
              destroy: true,
              processing: true,
              serverSide: true,
              ajax: '{!! route("userType.datatable") !!}',
              columns: [
                  { data: '_id', name: '_id' },
                  { data: 'title', name: 'title' },
                  { data: 'updated_at', name: 'updated_at' },
                  { data: 'action', name: 'action', orderable: false, searchable: false}
              ]
          }); 
          
});
</script>
@endpush
