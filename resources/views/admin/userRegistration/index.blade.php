@extends('layouts.admin')


@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>{{ __('User(s)') }}</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">User(s)</a></li>
        <li class="active">Listing</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box">
            <div class="box-header" style="float: right">
              <a href="{{route('user_registration.create', [ $data ])}}"><button id="new_booktype" class="btn btn-primary">Add User(s)</button></a>
            </div>
            <div>
             @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <div id="table_wrap" style="width:100%;float:left;">
             <table id="assignee_table" class="table table-striped table-bordered" >
                  <thead>
                  <tr>
                      <th>Sr.</th>
                      <th>Name</th>
                      <th>User Type</th>
                      <th>Serial Number</th>
                      <th>Email</th>
                      <th>Location</th>
                      <th>Alocated Court</th>
                      <th>Updated Date</th>
                      <th>Action</th>
                  </tr>
                  </thead>
                </table>
                </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
    <p>Some text in the Modal..</p>
  </div>

</div>
@stop
@push('scripts')
<script type="text/javascript">
 
var modal = document.getElementById('myModal');

var span = document.getElementsByClassName("close")[0];
// When the user clicks the button, open the modal 
$('.show_record').click(function (){
  
  	var id = $(this).attr("data-id");

  	var user_id = id;
	$(".modal-content").html('');
    $.get( "/admin/user_registration_show/" + user_id + "?<?php echo time();?>", function( data ) {
       
       $(".modal-content").html(data.content);
       
    });
	
    modal.style.display = "block";
});
// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
    
$(function() {
          $('#assignee_table').DataTable({
              destroy: true,
              processing: true,
              serverSide: true,
              ajax: '{!! route("user.datatable") !!}?type_id={!! $data !!}',
              columns: [
                  { data: '_id', name: '_id' },
                  { data: 'name', name: 'name' },
                  { data: 'user_type', name: 'user_type' },
                  { data: 'serial_no', name: 'serial_no' },
                  { data: 'email', name: 'email' },
                  { data: 'location', name: 'location' },
                  { data: 'alocated_court', name: 'alocated_court' },
                  { data: 'updated_at', name: 'updated_at' },
                  { data: 'action', name: 'action', orderable: false, searchable: false}
              ]
          }); 
          
});
</script>
@endpush
