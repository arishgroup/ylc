@extends('layouts.admin')


@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>{{ __('User(s) Registration') }}</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('user_registration.index',[$data['user_type']])}}">User(s)</a></li>
        <li class="active">Registration</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info col-md-6">
            <div class="box-header with-border">
              @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::open(['route' => 'user_registration.store', 'method' => 'post', 'class'=>'form-horizontal', 'files'=>'true']) !!}
              {{ csrf_field() }}
              <div class="box-body">
              <div class="form-group">
                     <label for="name" class="col-sm-2 control-label">{{ __('Profile Picture:') }}<span class="label_red"> *</span></label>
                  <div class="col-sm-3">
                 {!! Form::file('profile_img');!!}
                  @if ($errors->has('profile_img'))
                      <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('profile_img') }}</strong>
                        </span>
                  @endif
                  </div>
                  </div>
                <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">{{ __('First Name:') }}<span class="label_red"> *</span></label>
                    <div class="col-sm-3">
                 <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder='First Name' name="name" value="{{ old('name') }}" autofocus>
                 @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
                  </div>
                  <label for="name" class="col-sm-2 control-label">{{ __('last Name:') }}<span class="label_red"> *</span></label>
                  <div class="col-sm-3">
                 {!! FORM::text('lastname' ,'',['class'=>'form-control', 'placeholder'=>'Last Name']) !!}
                  @if ($errors->has('lastname'))
                      <span class="invalid-feedback" role="alert">
                     <strong>{{ $errors->first('lastname') }}</strong>
                  </span>
                  @endif
                  </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">{{ __('NIC:') }}<span class="label_red"> *</span></label>
                	<div class="col-sm-3">
                     {!! FORM::text('nic' ,'',['class'=>'form-control', 'placeholder'=>'NIC']) !!}
                        @if ($errors->has('nic'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('nic') }}</strong>
                            </span>
                        @endif
                  	</div>
                    <label for="name" class="col-sm-2 control-label">{{ __('Mobile Number:') }}<span class="label_red"> *</span></label>
                	<div class="col-sm-3">
                     {!! FORM::text('mobile_no' ,'',['class'=>'form-control', 'placeholder'=>'Mobile Number']) !!}
                        @if ($errors->has('mobile_no'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('mobile_no') }}</strong>
                            </span>
                        @endif

                  	</div>
                </div>
                 <div class="form-group">
                     <div class="col-sm-2"></div>
                  <div class="col-sm-3">
                 {!! Form::file('nic_img');!!}
                  @if ($errors->has('nic_img'))
                      <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('nic_img') }}</strong>
                        </span>
                  @endif
                  </div>
                  </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">{{ __('Email:') }}<span class="label_red"> *</span></label>
                   <div class="col-sm-3">
                   {!! FORM::text('email' ,'',['class'=>'form-control', 'placeholder'=>'Email', 'autocomplete'=>"off"]) !!}
                   @if ($errors->has('email'))
                       <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                   @endif
                  </div>
                  <label for="name" class="col-sm-2 control-label">{{ __('User Type:') }}<span class="label_red"> *</span></label>
                   <div class="col-sm-3">
                    {!! Form::select('user_type', $utype,$data['user_type'],['class'=>'form-control', 'placeholder'=>'User Type'] ) !!}
                   @if ($errors->has('user_type'))
                           <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('user_type') }}</strong>
                        </span>
                       @endif
                  </div>
                </div>
                <div class="form-group">
                <label for="name" class="col-sm-2 control-label">{{ __('Password:') }}<span class="label_red"> *</span></label>
                   <div class="col-sm-3">
                   {!! Form::password('password', ['class' => 'form-control']); !!}
                       @if ($errors->has('password'))
                           <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                       @endif
                  </div>
                  <label for="name" class="col-sm-2 control-label">{{ __('Confirm Password:') }}<span class="label_red"> *</span></label>
                   <div class="col-sm-3">
                   {!! Form::password('confirmed', ['class' => 'form-control']); !!}
                       @if ($errors->has('confirmed'))
                           <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('confirmed') }}</strong>
                            </span>
                       @endif
                  </div>
                </div>
                <div class="form-group">
                 {!! FORM::label('name' ,'Alocated Court',['class'=>'col-sm-2 control-label']) !!}
                   <div class="col-sm-3">
                   {!! Form::select('court', $court,'',['class'=>'form-control', 'placeholder'=>'Alocated Court'] ) !!}
                  </div>
                  
                  {!! FORM::label('name' ,'Location',['class'=>'col-sm-2 control-label']) !!}
                   <div class="col-sm-3">
                    {!! Form::select('location', $location,'',['class'=>'form-control', 'placeholder'=>'Location'] ) !!}
                  </div>
                </div>
                <div class="form-group">
                 {!! FORM::label('name' ,'Serial Number',['class'=>'col-sm-2 control-label']) !!}
                   <div class="col-sm-3">
                   {!! FORM::text('serial_no' ,$data['sr_no'],['class'=>'form-control', 'placeholder'=>'Serial Number','readonly']) !!}
                  </div>
                
                 {!! FORM::label('name' ,'Address',['class'=>'col-sm-2 control-label']) !!}
                   <div class="col-sm-3">
                   {!! FORM::textarea('user_address' ,'',['class'=>'form-control', 'rows' => 3, 'cols' => 40]) !!}
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="{{route('user_registration.index',[$data['user_type']])}}"><button type="button" class="btn btn-default">Back</button></a>
                {!! Form::submit('Submit', ['class'=>'btn btn-info pull-right']) !!}
              </div>
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <!-- /.box-footer -->
            {!! Form::close() !!}
          </div>
         
        </div>
        <!--/.col (right) -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection 
