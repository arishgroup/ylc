@extends('layouts.admin')


@section('content')
<style>
.modal-content {
    width: 80% !important;
}
</style>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>{{ __('Activity Log(s)') }}</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">User(s)</a></li>
        <li class="active">Activity Log</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
          @component('components.user_info',['record'=>$data['userData']])@endcomponent
       <div class="col-xs-12">
            <div class="box">
            	<div class="box-body">
                     <div class="container-fluid" style="padding-bottom:20px;float:left; padding-top:20px; width:100%">
                          <div class="row" style='padding-bottom:10px'>
                          <form name="search_content" method="post" action="{{route('search.log')}}" >
                          	<div class="col-md-4"><b>From Date:</b> 
                          		<div class="input-group date">
                                	<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                    <input type="text" class ="form-control" id="start_date" name="start_date" value="{{ $data['rq_data']->start_date}}" >
                                </div>
                            </div>
                          	<div class="col-md-4"><b>To Date:</b>
								<div class="input-group date">
                                	<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                    <input type="text" class ="form-control" id="end_date" name="end_date"  value="{{ $data['rq_data']->end_date }}" >
                                </div>
							</div>
                          	<div class="col-md-4" style="padding-top:19px">
								<div class="input-group date">
                                	<input type="submit" value="Search" class='btn btn-primary' />
                                </div>
							</div>
							<input type="hidden" value="{{$data['userData']->_id}}" name="id" id="id" />
							<input type="hidden" value="user" name="type" id="type" />
							<input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
							</form>
                          </div>
                    </div>
    			</div>
    		</div>
		</div>
        <div class="col-xs-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              
	<ul class="timeline">
@if($data['logs']->count())
	@php $c_date = ""; @endphp
	@foreach($data['logs'] as $key => $log)
    <!-- timeline time label -->
    	@php $db_create_date = date('d-M-y',strtotime($log->created_at)) @endphp
    	@if($c_date != $db_create_date)
    	@php $c_date = $db_create_date; @endphp
        <li class="time-label">
            <span class="bg-red">
                {!! $db_create_date !!}
            </span>
        </li>
        @endif
    <!-- /.timeline-label -->

    <!-- timeline item -->
    <li>
        <!-- timeline icon -->
        <i class="fa fa fa-circle-o bg-blue"></i>
        <div class="timeline-item">
            <span class="time"><i class="fa fa-clock-o"></i> {!! date('g:i:s A',strtotime($log->created_at)) !!}</span>

            <!-- <h3 class="timeline-header"><a href="#">Support Team</a> ...</h3>  -->

            <div class="timeline-body"><b>
                {{ $log->subject }}</b>
            </div>
            @if($log->log_name == 'statute-section')
            @php 
            	$log_details = $log->causer_type::where('_id','=',$log->causer_id)->withTrashed()->get(); 
            	$log_details = $log_details[0]; 
            	//dd( $log_details->bookSection->name );
            	@endphp
            
            <div class="timeline-footer row" style="padding-top:0px">
            	
                <div class="col-xs-3" ><b>Statute</b>: @if($log_details->book_id) {!! $log_details->bookSection->name !!} @else 'N/A' @endif</div>
                <div class="col-xs-3" ><b>Section No</b>: {{ $log_details->section_no }}</div>
                <div class="col-xs-3" ><b>Definition</b>: {{ $log_details->definition }}</div>
                
                <div class="col-xs-12" ><b>Description</b> <a class="btn btn-primary btn-xs" id='myBtn' onClick='show_content("{{ $log->_id }}")';>view description</a></div>
                <div id="{{ $log->_id }}" style="display:none" ><b>Description</b>: {!! $log_details->content_read !!}</div> 
            </div>
           @endif
           @if($log->log_name == 'statute')
            @php 
            	$log_details = $log->causer_type::where('_id','=',$log->causer_id)->withTrashed()->get(); 
            	$log_details = $log_details[0];  
            	@endphp
            
            <div class="timeline-footer row" style="padding-top:0px">
            	
                <div class="col-xs-3" ><b>Statute</b>: {{ $log_details->name }}</div>
                <div class="col-xs-3" ><b>Type</b>: {{ $log_details->bookTypes->name }}</div>
                <div class="col-xs-3" ><b>Location</b>: {{ Config::get('constants.country.'.$log_details->country_id) }} - {{ Config::get('constants.PAK_PROVINCE.'.$log_details->state_id) }}</div>
                <div class="col-xs-3" ><b>Year</b>: {{ $log_details->year }}</div>
            </div>
           @endif
		@if(false && $log->log_name!= 'login' )
            <div class="timeline-footer" style="padding-top:0px">
                <a class="btn btn-primary btn-xs">view</a>
            </div>
        @endif
        </div>
    </li>
    @endforeach
		@endif
    <!-- END timeline item -->
</ul>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
    <div id="content_to_show"></div>
  </div>

</div>
<script>
    function show_content(id){
        document.getElementById('content_to_show').innerHTML = document.getElementById(id).innerHTML;
        $("#myModal").modal();
    }

    $(document).ready(function() {

        $('#start_date').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        });

        $('#end_date').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        });
    });
</script>
@endsection
