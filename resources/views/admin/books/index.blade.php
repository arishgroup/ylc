    
@extends(Auth::guard('admin')->check() ? 'layouts.admin' : 'layouts.lawyer')

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>{{ __('Book(s)') }}</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Book(s)</a></li>
        <li class="active">Listing</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">


          <div class="box">
            <div class="box-header" style="float: right">
              <a href="{{route('book.create')}}"><button id="new_booktype" class="btn btn-primary">Add Book(s)</button></a>
            </div>
            <div>
             @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <div style="float:right"><input id="myInput" type="text" placeholder="Search.."></div>
            <div id="alphabut" class="btn-group btn-group-sm alphabetical_order_btns">
               <?php foreach(range('A','Z') as $v){?>
                     <a href="{{ route('admin.book.index', ['ch'=>$v]) }}"><button class="btn btn-default char"><?php echo $v;?></button></a>
                <?php } ?>
                    </div>
              <table  class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Sr.</th>
                  <th style='width:75%'>Name</th>
                  <th>Updated Date</th>
                  <th>Action</th>
                  
                </tr>
                </thead>
                <tbody id="tableGrid">
                <?php $i=0;?>
              @if($book->count())
               	@foreach($book as $val)
               	<tr>
                  <td>{!! $i = $i+1 !!}</td>
                  <td><a href="{{ route('admin.book.show', ['id'=>$val->_id]) }}">{!! $val->name !!}</a></td>
                  <td>{!! $val->updated_at !!}</td>
                  <td><a href="{{ route('admin.book.show', ['id'=>$val->_id]) }}">View</a> | <a href="{{ route('admin.book.edit', ['id'=>$val->_id]) }}">Update</a></td>
                </tr>
                 @endforeach
                @else
                <tr id="no_record">
                  <td colspan="6">No Record Found!</td>
                </tr>
                @endif   
			  </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection 
