@extends(Auth::guard('admin')->check() ? 'layouts.admin' : 'layouts.backendUser')


@section('content')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>{{ __('Book(s)') }}</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('book.index')}}">Book(s)</a></li>
        <li class="active">update</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info col-md-6">
            <div class="box-header with-border">
              @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::open(['route' => array('admin.book.update',$row_data->_id), 'method' => 'post', 'class'=>'form-horizontal','files'=>'true']) !!}
              {{ csrf_field() }}
              <div class="box-body">
              <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">{{ __('File upload:') }}<span class="label_red"> *</span></label>
                                   <div class="col-sm-3">
                 <input type='file' name='image[]' id='image' data-validation="extension" validate_extension='pdf,docx' data-validation-allowing="pdf, docx" multiple />

                  @if($row_data->file_ext != "") 
                  <a  href="{!! route('show.pdf', [encrypt('/storage/statute/statute_'.$row_data->_id.'.'.$row_data->file_ext)]) !!}" target="_blank" >View File</a>
                  @endif
                   @if ($errors->has('image'))
                       <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('image') }}</strong>
                      </span>
                   @endif

                  </div>
                </div>
              <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">{{ __('Title:') }}<span class="label_red"> *</span></label>
                   <div class="col-sm-5">
                    {!! FORM::text('book_title' , $row_data->name ,['class'=>'form-control', 'placeholder'=>'Title', 'data-validation'=>'length alphanumeric', 'data-validation-length'=>'min4',  'data-validation-allowing'=>'()-_ ' ]) !!}
                       @if ($errors->has('book_title'))
                           <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('book_title') }}</strong>
                      </span>
                       @endif
                  </div>
                </div>
                
                 <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">{{ __('Location:') }}</label>
                    <div class="col-sm-5">
                    	<div class='row'>
                    		<div class="col-sm-3">{!! Form::select('country_id', $country, '',['placeholder' => 'Select Country', 'onChange'=>'GetCountryChange(this.value)', 'id'=>'country_id']) !!}</div>
                    		<div class="col-sm-5" id='state_div' style='display:none'>{!! Form::select('state_id', array(), '',['placeholder' => 'Select State', 'id'=>'country_state']) !!}</div>
                    		<div class="col-sm-4" id='city_div' style='display:none'>{!! Form::select('city_id', array(), '',['placeholder' => 'Select City', 'id'=>'city_dd']) !!}</div>
                    	</div>
                    </div>
                </div>
                <div class="form-group">
                  {!! FORM::label('name' ,'Year',['class'=>'col-sm-2 control-label']) !!}
               <div class="col-sm-5">
                	 {!! FORM::text('book_year' , $row_data->year ,['class'=>'form-control', 'placeholder'=>'Year']) !!}
                  </div>
                </div>
                <div class="form-group">
                  {!! FORM::label('name' ,'Description',['class'=>'col-sm-2 control-label']) !!}
                   <div class="col-sm-5">
                   {!! FORM::textarea('description' ,$row_data->description,['class'=>'form-control', 'rows' => 3, 'cols' => 40]) !!}
                  </div>
                </div>
                <fieldset>
                  <legend>Uploaded Documents:</legend>
                  
                  <div class="form-group">
                  <table  class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Sr.</th>
                  <th style='width:75%'>File name</th>
                  <th>Type</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody id="tableGrid">
                <?php $i=0;?>
              @if($row_data->getAttachments->count())
               	@foreach($row_data->getAttachments as $val)
               	<tr>
                  <td>{!! $i = $i+1 !!}</td>
                  <td>
                  	@if($val->ext == "pdf")
                        <a  href="{!! route('show.pdf', [encrypt('/storage/books/'.$row_data->_id.'/book_'.$val->_id.'.'.$val->ext)]) !!}" target="_blank" >{!! $val->file_name !!}</a>
                    @else
                    <!-- <a target="_blank" href="{!! route('show.word', [encrypt('/storage/books/'.$row_data->_id.'/book_'.$val->_id.'.'.$val->ext)]) !!}" >{!! $val->file_name !!}</a> -->
                        <a target="_blank" href='{{ asset("storage/books/$row_data->_id/book_$val->_id.$val->ext") }}' >{!! $val->file_name !!}</a>
                    @endif
                   </td>
                  <td>{!! $val->updated_at !!}</td>
                  <td><a class="confirmation" href="{{ route('book.attachment.destroy', $val->_id) }}">Delete</a></td>
                </tr>
                 @endforeach
                @else
                <tr id="no_record">
                  <td colspan="6">No Record Found!</td>
                </tr>
                @endif   
			  </table>
                  </div>
                  
                  </fieldset>
                  
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="{{route('admin.book.index')}}"><button type="button" class="btn btn-default">Back</button></a>
                {!! Form::submit('Submit', ['class'=>'btn btn-info pull-right']) !!}
              </div>
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              
              {!! FORM::hidden('_id' ,$row_data->_id) !!}
              <!-- /.box-footer -->
   
            {!! Form::close() !!}
          </div>
         
        </div>
        <!--/.col (right) -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script>
        $('#tags').select2({
            placeholder: "Choose tags...",
            minimumInputLength: 2,
            ajax: {
                url: '/tags/find',
                dataType: 'json',
                data: function (params) {
                    return {
                        q: $.trim(params.term)
                    };
                },
                processResults: function (data) {
                    return {
                        results: data[0]
                    };
                },
                cache: true
            }
        });
  
    
        function GetCountryChange(country_code){
				$.ajax({url: "/get_state/"+country_code,  async: false,success: function(result){
					console.log(result);
					document.getElementById('country_state').innerHTML = result.data;
					document.getElementById('state_div').style.display = 'block';
					//document.getElementById('city_div').style.display = 'none';
			    }});
			} 
    function GetstateChange(state_code){
		$.ajax({url: "/get_city/"+state_code, async: false, success: function(result){
			document.getElementById('city_dd').innerHTML = result.data;
			if(result.data != "")
				document.getElementById('city_div').style.display = 'block';
	    }});
	} 
<?php if($row_data->location_city){ ?>
var objSelect = document.getElementById("country_id");
	setSelectedValue(objSelect, "<?php echo $row_data->location_city;?>");
	GetCountryChange('<?php echo $row_data->location_city;?>');
<?php }?>

<?php if($row_data->state_id){ ?>
var objSelect_n = document.getElementById("country_state");
setSelectedValue(objSelect_n, "<?php echo $row_data->state_id;?>");
//GetstateChange('<?php //echo $row_data->state_id;?>');

<?php }?>



function setSelectedValue(selectObj, valueToSet) {
    for (var i = 0; i < selectObj.options.length; i++) {
        
        if (selectObj.options[i].value == valueToSet) {
            selectObj.options[i].selected = true;
            return;
        }
    }
}

    </script>
@endsection 
