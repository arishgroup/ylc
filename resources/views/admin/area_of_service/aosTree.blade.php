@extends(Auth::guard('admin')->check() ? 'layouts.admin' : 'layouts.lawyer')

@section('content')
<style>
.dropbtn {
    background-color: #3498DB;
    color: white;
    padding: 16px;
    font-size: 16px;
    border: none;
    cursor: pointer;
}

.dropbtn:hover, .dropbtn:focus {
    background-color: #2980B9;
}

.dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f1f1f1;
    min-width: 160px;
    overflow: auto;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

.dropdown-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
}

.dropdown a:hover {background-color: #ddd;}

.show {display: block;}
</style>

<link rel="stylesheet" href="{{ asset('css/simple-scrollbar.css?123')}}">
<link rel="stylesheet" href="{{ asset('css/Treant.css')}}">
  	
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">	
      <h1>{{ __('Area of Service(s)') }}</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Area of Service</a></li>
        <li class="active">Tree View</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box">
            <div class="box-header" style="float: right">
              <a href="{{route('aos.index')}}">Table View</a> | <a href="{{route('aos.create')}}">Add New Area of Service</a>
            </div>
            <div>
             <?php $i = 0 ;?>
             @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
            </div>
            <!-- /.box-header -->
            <div class="box-body">
       			<div class="chart" id="OrganiseChart1"></div>


            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script src="{{ asset('js/raphael.js') }}"></script>
<script src="{{ asset('js/Treant.js') }}"></script>
<script>

var str_admin = '<a href="javascript:void(0)" onclick="show_div(\'0\')">YLC</a><div id="0" class="dropdown-content">'+
'<a href="#contact">Add Reference Materail</a>';


var str_node= '<a href="#contact">Add Reference Materail</a>';

var config = {
        container: "#OrganiseChart1",
        rootOrientation:  'WEST', // NORTH || EAST || WEST || SOUTH
        scrollbar: "fancy",
        // levelSeparation: 30,
        siblingSeparation:   20,
        subTeeSeparation:    60,
        
        connectors: {
            type: 'step'
        },
        node: {
            HTMLclass: 'nodeExample1'
        }
    },
    
    ceo = {
        text: {
            name: "YLC",
        },
        HTMLclass : 'dropdown-toggle',
        HTMLid: "ceo",
    	innerHTML : '<a class="container-href" href="javascript:void(0)" onclick="show_div(\'0\')">YLC</a><div id="0" class="dropdown-content"><a href="create">Add Child Node</a>' +
          '</div>'
    },
    <?php if(count($aos) > 0){
                foreach($aos as $service) { ?>
        
        
    node_<?php echo $service->_id;?> = {
		parent: <?php if($service->parent_id==0){ echo "ceo";} else { echo "node_".$service->parent_id; }?>,
            text: {
                name: "<?php echo $service->name;?>",
            },
            HTMLclass : 'dropdown-toggle <?php echo $service->_id;?>',
            HTMLid: "node_<?php echo $service->_id;?>",
            innerHTML : '<a class="container-href" href="javascript:void(0)" onclick="show_div(\'<?php echo $service->_id;?>\')"><?php echo $service->name;?></a><div id="<?php echo $service->_id;?>" class="dropdown-content">'+
            '<?php if(Auth::guard('admin')->check()){
                echo  ' <a href="create/'.$service->_id.'">Add Child Node</a><a href="edit/'.$service->_id.'">Edit Node</a><a href="destroy/'.$service->_id.'" class="confirmation">Delete Node</a>'.
                    link_to_route("lawyers.index", "Add/View Lawyers",['aos_id'=>$service->_id]) .'<a href="ref_notes_index/'.$service->_id.'/standard">Add/View Company Notes</a>'.link_to_route("team.create", "Add/View Lawyer Teams",['aos_id'=>$service->_id]);
            } else {
                 echo link_to_route("reference.material", "Add Reference Materail",[$service->_id]);
             }?> '+
                  '</div>'
        },
        <?php }
        } ?> 
        
    ALTERNATIVE = [
        config,
        ceo,
        ];
    dontexist = [ ];
    <?php if(count($aos) > 0){
        foreach($aos as $service) { 
			if($service->parent_id != '0') {?>
        	ALTERNATIVE.push(node_<?php echo $service->parent_id;?>);
			<?php } ?>	
    		ALTERNATIVE.push(node_<?php echo $service->_id;?>);	
			
        <?php }?>
   <?php } ?>
  // ALTERNATIVE.push(dontexist);	
console.log(ALTERNATIVE.filter( onlyUnique ));
new Treant( ALTERNATIVE );
    
var  old_div = "";
function show_div(class_name){
	document.getElementById(class_name).classList.toggle("show");	
	if(old_div != ""){
		document.getElementById(old_div).classList.toggle("show");
		
		}
	old_div = class_name;
	
}
function onlyUnique(value, index, self) { 
    return self.indexOf(value) === index;
}

        </script> 
@endsection 
