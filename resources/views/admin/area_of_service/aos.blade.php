@extends('layouts.admin')


@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>{{ __('Area of Service(s)') }}</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Area Of Service(s)</a></li>
        <li class="active">Add</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box">
            <div class="box-header" style="float: right">
              <a href="{{route('aos.tree')}}">Tree View</a> | <a href="{{route('aos.create')}}">Add Root Node</a>
            </div>
            <div>
             @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Sr.</th>
                  <th>Name</th>
                  <th>No. of Childern</th>
                  <th>Description</th>
                  <th>Action</th>
                  
                </tr>
                </thead>
                <tbody>
                <?php $i = 0 ;?>
                @if($aos->count())
                @foreach($aos as $service)
                <tr>
                  <td>{!! $i = $i+1 !!}</td>
                  <td>{!! $service->name !!}</td>
                  <td>{!! $service->parent_id !!}</td>
                  <td>{!! $service->description !!}</td>
                  <td><a href="{{ route('aos.childNode', $service->_id) }}"><i class="fa fa-university"></i></a> | <a href="{{ route('aos.edit', $service->_id) }}"><i class="fa fa-edit"></i></a> | <a class="confirmation" href="{{ route('role.destroy', $service->_id) }}"><i class="fa fa-trash"></i></a></td>
                </tr>
                @endforeach
                @else
                <tr>
                  <td colspan="4">No Record Found!</td>
                </tr>
                @endif
                </tfoot>
              </table>
            </div>
            <div style="text-align:right; padding-right: 10px;">{{ $aos->links() }}</div>    
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<script type="text/javascript">
    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }
</script>
@endsection 
