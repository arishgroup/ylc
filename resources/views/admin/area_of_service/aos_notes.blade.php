@extends(Auth::guard('admin')->check() ? 'layouts.admin' : 'layouts.lawyer')

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      
      <h1>{{ __('Personal Notes') }}</h1>
        
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Lawyer(s)</a></li>
        <li class="active">View</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
    <p>Some text in the Modal..</p>
  </div>

</div>

          <div class="box">
          <div class="box-header" style="float: right">
          	<a href="{{ route('aos.content.lawyer', [$data['lawyer']->_id, 'personal', $data['aos']->aos_personal->_id]) }}"><button id="new_booktype" class="btn btn-primary">Back</button></a>
          	<a href="javascript:void(0)" onclick="return showPopupHead('{!! $data['content_id'] !!}');"><button id="new_booktype" class="btn btn-primary">View All Content</button></a>
          	 
          	</div>
            <div>
             @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <div class="container-fluid" style="padding-bottom:20px;float:left; width:100%">
              <div class="row" style='padding-bottom:10px'>
              	<div class="col-md-4"><b>Lawyer Name: </b>{!! $data['lawyer']->name." ".$data['lawyer']->lastname !!}</div>
              	<div class="col-md-4"><b>Serial Number: </b>{!! $data['lawyer']->serial_no !!}</div>
              	<div class="col-md-4"><b>Public Serial Number: </b>{!! $data['lawyer']->public_serial_no !!}</div>
              </div>
              <div class="row" style='padding-bottom:10px'>
              	<div class="col-md-4"><b>Email: </b>{!! $data['lawyer']->email !!}</div>
              	<div class="col-md-4"><b>Company Area of Service: </b>@if($data['lawyer']->area_of_service){!! $data['lawyer']->aos->name !!}@endif</div>
              	<div class="col-md-4"><b>Personal Area of Service: </b>{!! $data['aos']->aos_personal->name !!}</div>
              </div>
               <div class="row" style='padding-bottom:10px'>
              	<div class="col-md-4"><b>Head Title: </b>{!! $data['aos']->title !!}</div>
              	
              </div>
              </div>
            
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Sr.</th>
                  <th>Title</th>
                  <th>Updated Date</th>
                  
                </tr>
                </thead>
                <tbody>
                <?php $i = 0 ;?>
                @if($data['aos_content']->count())
                @foreach($data['aos_content'] as $content)
                <tr>
                  <td>{!! $i = $i+1 !!}</td>
                  <td><a href="javascript:void(0)" onClick="showPopup('{!! $content->_id !!}')">{!! $content->heading !!}</a></td>
                  <td>{!! $content->updated_at !!}</td>
                </tr>
                @endforeach
                @else
                <tr>
                  <td colspan="5">No Record Found!</td>
                </tr>
                @endif
                
                </tfoot>
              </table>
            </div>
                
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
<script type="text/javascript">
    
    var modal = document.getElementById('myModal');
   
    var span = document.getElementsByClassName("close")[0];

    function showAllContent(lawyer_id, aos_id, type){
        
    	var section_id = aos_id;
    	$(".modal-content").html('');
        $.get( "/aos/getFullContent?id=" + section_id+"&t=<?php echo time();?>&type="+type+"&lawyer="+lawyer_id, function( data ) {
           if(data.content == "")
        	   data.content = "No Record Found.";
    	   
           $(".modal-content").html(data.content);
           
        });
    	
        modal.style.display = "block";
    }
    function showPopupHead(id){
        
    	var section_id = id;
    	$(".modal-content").html('');
        $.get( "/aos/getContent?id=" + section_id+"&t=<?php echo time();?>", function( data ) {
        	if(data.content == "")
          	   data.content = "No Record Found.";
        	   
           $(".modal-content").html(data.content);
           
        });
    	
        modal.style.display = "block";
    }
    
    // When the user clicks the button, open the modal 
    function showPopup(id){
      
    	var section_id = id;
    	$(".modal-content").html('');
        $.get( "/aos/getSingleContent?id=" + section_id+"&t=<?php echo time();?>", function( data ) {
        	if(data.content == "")
          	   data.content = "No Record Found.";
        	   
           $(".modal-content").html(data.content);
           
        });
    	
        modal.style.display = "block";
    }
    
    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }
    
    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>
@endsection 
