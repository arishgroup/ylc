@extends(Auth::guard('admin')->check() ? 'layouts.admin' : 'layouts.backendUser')

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>{{ __('Department(s)') }}</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('department.index')}}">Department(s)</a></li>
        <li class="active">Add</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info col-md-6">
            <div class="box-header with-border">
              @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::open(['route' => array('department.update', $data->_id), 'method' => 'post', 'class'=>'form-horizontal']) !!}
              {{ csrf_field() }}
              <div class="box-body">
              <div class="form-group">
                <label for="name" class="col-sm-2 control-label">{{ __('Department Name:') }}<span class="label_red"> *</span></label>
                   <div class="col-sm-5">
                    {!! FORM::text('dept_name' , $data->name ,['class'=>'form-control', 'placeholder'=>'Department Name', 'data-validation'=>'length alphanumeric', 'data-validation-length'=>'min4',  'data-validation-allowing'=>'-_ ']) !!}
                     @if ($errors->has('dept_name'))
                       <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('dept_name') }}</strong>
                     </span>
                     @endif
                  </div>
                </div>
                <div class="form-group">
                <label for="name" class="col-sm-2 control-label">{{ __('Department Geo-location:') }}<span class="label_red"> *</span></label>
                   <div class="col-sm-5">
                   		<div class="row">
                   			<div class="col-sm-6">
                  			{!! FORM::text('longitude' , $data->longitude  ,['class'=>'form-control', 'placeholder'=>'Longitude', 'data-validation'=>'number', 'data-validation-allowing'=>'float,negative']) !!}
                             @if ($errors->has('longitude'))
                               <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('longitude') }}</strong>
                             </span>
                             @endif
                             </div>
                             <div class="col-sm-6">
                  			{!! FORM::text('latitude' , $data->latitude  ,['class'=>'form-control', 'placeholder'=>'Latitude', 'data-validation'=>'number', 'data-validation-allowing'=>'float,negative']) !!}
                             @if ($errors->has('latitude'))
                               <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('latitude') }}</strong>
                             </span>
                             @endif
                             </div> 		
                   		</div>
                  </div>
                </div>
                <div class="form-group">
                <label for="name" class="col-sm-2 control-label">{{ __('Department Address:') }}</label>
                   <div class="col-sm-5">
                    {!! FORM::textarea('address' ,$data->address,['class'=>'form-control', 'rows' => 3, 'cols' => 40]) !!}
                     @if ($errors->has('address'))
                       <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('address') }}</strong>
                     </span>
                     @endif
                  </div>
                </div>
                <div class="form-group">
                <label for="name" class="col-sm-2 control-label">{{ __('Department Description:') }}</label>
                   <div class="col-sm-5">
                    {!! FORM::textarea('description' ,$data->description,['class'=>'form-control', 'rows' => 3, 'cols' => 40]) !!}
                     @if ($errors->has('description'))
                       <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('description') }}</strong>
                     </span>
                     @endif
                  </div>
                </div>
               </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="{{route('department.index')}}"><button type="button" class="btn btn-default">Back</button></a>
                {!! Form::submit('Submit', ['class'=>'btn btn-info pull-right']) !!}
              </div>
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <!-- /.box-footer -->
   
            {!! Form::close() !!}
          </div>
         
        </div>
        <!--/.col (right) -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection 
