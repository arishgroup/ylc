@extends(Auth::guard('admin')->check() ? 'layouts.admin' : 'layouts.backendUser')

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>{{ __('Department(s)') }}</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Department(s)</a></li>
        <li class="active">Listing</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box">
            <div class="box-header" style="float: right">
              <a href="{{route('department.create')}}"><button id="new_booktype" class="btn btn-primary">Add Department(s)</button></a>
            </div>
            <div>
             @if (Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <div style="float:right"><input id="myInput" type="text" placeholder="Search.."></div>
              <table  class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Sr.</th>
                  <th>Name</th>
                  <th>Longitude</th>
                  <th>Latitude</th>
                  <th>Updated Date</th>
                  <th>Action</th>
                  
                </tr>
                </thead>
                <tbody id="tableGrid">
                <?php $i=0;?>
              @if($dept_list->count())
               	@foreach($dept_list as $val)
               	<tr>
                  <td>{!! $i = $i+1 !!}</td>
                  <td>{!! $val->name !!}</td>
                  <td>{!! $val->longitude !!}</td>
                  <td>{!! $val->latitude !!}</td>
                  <td>{!! $val->updated_at !!}</td>
                  <td><a href="{{ route('department.edit', [$val->_id]) }}"><i class="fa fa-edit"></i></a> | <a class="confirmation" href="{{ route('department.destroy', $val->_id) }}"><i class="fa fa-trash"></i></a></td>
                </tr>
                 @endforeach
                @else
                <tr id="no_record">
                  <td colspan="6">No Record Found!</td>
                </tr>
                @endif   
			  </table>
            </div>
             <div style="text-align:right; padding-right: 10px;">{{ $dept_list->links() }}</div> 
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection 
