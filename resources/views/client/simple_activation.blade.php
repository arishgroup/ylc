@extends('layouts.client')

@section('content')

<div class="container">
<h2 class="entry-title">{{ __('Account Activation - (Family/Personal Account)') }}</h2>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Area of Service:*') }}</b></label>

                            <div class="col-md-4">
                            	<div class="">
                            		<div class="mr-1" style="float:left">
                                        <select id='MasterSelectBox' name="" class="form-control" multiple>
                                        	<option value='1'>Family Law</option>
                                        	<option value='2'>Public Law</option>
                                        	<option value='3'>Criminal Law</option>
                                        </select>
                                     </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                            	<button id="btnAdd">></button><br>
<button id="btnRemove"><</button>
                            </div>

                            <div class="col-md-4">
                            	<div class="mr-1" style="float:left">
                                	<select id="PairedSelectBox" multiple></select>
                            	</div>
                            	<div class="mr-1" style="float:right">
                            		5000 Rupee <br>
                            		<p> will be changed according to the AOS has been set at the backend
                            		
                            	</div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Payment Method:*') }}</b></label>

                            <div class="col-md-4">
                    		    <select id='method' name="" class="form-control">
                                	<option value='1'>Easy Paisa</option>
                                	<option value='2'>Jazzcash</option>
                                	<option value='3'>Bank Transfer</option>
                                </select>
                            </div>
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Payment Type:*') }}</b></label>

                            <div class="col-md-4" >
                            	<select id='payment_method' name="" class="form-control" >
                                	<option value='1'>Mobile Account</option>
                                	<option value='2'>Credit Card</option>
                                	<option value='3'>Online Trasnfer</option>
                                </select>
                            </div>
                        </div>
                        
                           <div class="form-group" id="creditCard" style="padding-bottom:10px; display:none">
                           	<fieldset>
                                <legend>Credit Card:</legend>
                                <div class="col-md-12" >
                                	<div class="col-md-2"></div>
                                	<div class="col-md-2">Credit Number:</div>
                                	<div class="col-md-5"><input type="text" name="cc_no" id="cc_no" style="width:100%" /></div>
                                </div>
                                <div class="col-md-12" >
                                	<div class="col-md-2"></div>
                                	<div class="col-md-2">Expiry Date:</div>
                                	<div class="col-md-7">
                                		<div style="float:left"><select id="month" name="month" >
                                				<option id="0">Select Month</option>
                                				<option id="1">Jan</option>
                                				<option id="2">Feb</option>
                                				<option id="3">March</option>
                                				<option id="4">April</option>
                                			</select> / <select id="year" name="year" >
                                				<option id="0">Select Year</option>
                                				<option id="1">2018</option>
                                				<option id="2">2019</option>
                                				<option id="3">2020</option>
                                				<option id="4">2021</option>
                                				<option id="5">2022</option>
                                			</select></div>
                            		</div>
                                </div>
                                <div class="col-md-12" >
                                	<div class="col-md-2"></div>
                                	<div class="col-md-2">Card Holder:</div>
                                	<div class="col-md-2"><input type="text" name="cc_no" id="cc_no" style="width:100%" /></div>
                                	<div class="col-md-1">CVV:</div>
                                	<div class="col-md-2"><input type="text" name="cc_no" id="cc_no" style="width:100%" /></div>
                                </div>
                            </fieldset>
                           </div>
                           
                           <div class="form-group" id="transaction_script" style="padding-bottom:10px; display:none">
                           	<fieldset>
                                <legend>Transaction Script:</legend>
                                <div class="col-md-12" >
                                	<div class="col-md-2"></div>
                                	<div class="col-md-2">Upload Transaction Recipt:</div>
                                	<div class="col-md-5"><input type='file' /></div>
                                </div>
                                <div class="col-md-12" >
                                	<p>It will take 2-3 working days to verify the transaction to activate the account.<br>
                                		Email will be Delivered on activation.
                                	</p>
                                </div>
                                
                            </fieldset>
                           </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12 offset-md-4" style="text-align: center">
                                <input type="submit" value="{{ __('Activate Account') }}" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@stop
@push('scripts')
<script type="text/javascript">

$(document).ready( function(){

 	$( "#payment_method" ).change(function() {
 	 	  if($(this).val() == 2){
		  	document.getElementById('creditCard').style.display = 'block';
		  	document.getElementById('transaction_script').style.display = 'none';
 	 	  }

 	 	 	if($(this).val() == 3){
 			  	document.getElementById('transaction_script').style.display = 'block';
 			  	document.getElementById('creditCard').style.display = 'none';
 	 	 	  }
		  
 	});
    $('#MasterSelectBox').pairMaster();
    $('#btnAdd').click(function(){
        $('#MasterSelectBox').addSelected('#PairedSelectBox');
        $("#PairedSelectBox option:selected").removeAttr("selected");
        $("#MasterSelectBox option:selected").removeAttr("selected");
        event.preventDefault()
    });
    $('#btnRemove').click(function(){
        $('#PairedSelectBox').removeSelected('#MasterSelectBox');
        $("#MasterSelectBox option:selected").removeAttr("selected");
        $("#PairedSelectBox option:selected").removeAttr("selected");
        event.preventDefault()
    });

});
</script>
@endpush