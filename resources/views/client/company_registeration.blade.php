@extends('layouts.client')

@section('content')

<div class="container">
<h2 class="entry-title">{{ __('Company Account Registeration') }}</h2>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Your ID:*') }}</b></label>

                            <div class="col-md-4">
                            	<div class="">
                            		<div class="mr-1" style="float:left">
                                        <select id='id_type' name="" class="form-control">
                                        	<option value='1'>CUIN</option>
                                        </select>
                                     </div>
                                    <div style="float:left">
                                    	<input id="nic" type="text" placeholder='NIC / BAY FORM' class="form-control{{ $errors->has('nic') ? ' is-invalid' : '' }}" name="nic" value="{{ old('nic') }}" required autofocus>
    								
                                    @if ($errors->has('nic'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('nic') }}</strong>
                                        </span>
                                    @endif
                                    </div>
                                </div>
                            </div>
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('ID - Image:*') }}</b></label>

                            <div class="col-md-4">
                                <input type="file" name="id_img" id="id_img" />
                            </div>
                        </div>
                       <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Incorporation Date:*') }}</b></label>

                            <div class="col-md-4">
                            	<input type="text" name="incorporation_date" id="incorporation_date" placeholder="Incorporation Date" />
                            </div>
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Company - Logo:*') }}</b></label>

                            <div class="col-md-4">
                                <input type="file" name="id_img" id="id_img" />
                            </div>
                        </div>
						<div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Company Registered Name:*') }}</b></label>

                            <div class="col-md-4">
                            	<div class="">
                            		<input id="first_name" type="text" placeholder='Company Name' class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}" required autofocus>
    								
                                    @if ($errors->has('first_name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('first_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Password:*') }}</b></label>

                            <div class="col-md-4">
                            	<input id="password" type="password" placeholder='Password' class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" value="{{ old('password') }}" required autofocus>
                            </div>
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Confirm Password:*') }}</b></label>

                            <div class="col-md-4">
                               <input id="c_password" type="text" placeholder='Confirm Password' class="form-control{{ $errors->has('c_password') ? ' is-invalid' : '' }}" name="c_password" value="{{ old('c_password') }}" required autofocus>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Number of Directors:*') }}</b></label>

                            <div class="col-md-4">
                            	<select id="" name="" >
                            		<option value="0">No. of Director</option>
                            		<option value="0">1</option>
                            		<option value="0" selected="selected">2</option>
                            		<option value="0">3</option>
                            	</select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Directors name:*') }}</b></label>

                            <div class="col-md-4">
                            	<input type="text" name="director_name[]" id="director_name[]" />
                            </div>
                            <div class="col-md-2"></div>

                            <div class="col-md-4">
                            	<input type="text" name="director_name[]" id="director_name[]" />
                            </div>
                        </div>
                        <div class="form-group row" style="padding-bottom: 10px;">
                          <fieldset>
                                <legend>Contact Person:</legend>
                                <div class="form-group row">
                                    <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Name:*') }}</b></label>
    	                            <div class="col-md-4">
                                		<input type="text" name="name" id="name" placeholder="Full Name"/>
                                	</div>
                                	<label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Designation:*') }}</b></label>
    	                            <div class="col-md-4">
        	                        	<input type="text" name="name" id="name" placeholder="Desgination"/>
            	                    </div>
            	                </div>    
            	                <div class="form-group row">
                                    <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Email:*') }}</b></label>
    	                            <div class="col-md-4">
                                		<input type="email" name="name" id="email" placeholder="Email"/>
                                	</div>
                                	<label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Mobile Number:*') }}</b></label>
    	                            <div class="col-md-4">
        	                        	<div style="float:left">
                            				<input id="mobile_number" type="number" placeholder='Mobile Code' class="form-control{{ $errors->has('mobile_code') ? ' is-invalid' : '' }}" name="mobile_code" value="{{ old('mobile_code') }}" required autofocus>
                            			</div>
                            			<div style="float:left">
                            				<input type="submit" value="verify number"/>
                            			</div>
            	                    </div>
            	                </div>  
            	                <div class="form-group row">
            	                	<label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Verify Number:*') }}</b></label>
		                            <div class="col-md-4">
        		                    	<div style="float:left">
                	            			<input id="verify_num" type="text" placeholder='Verify Number' class="form-control{{ $errors->has('verify_num') ? ' is-invalid' : '' }}" name="verify_num" value="{{ old('verify_num') }}" required autofocus>
                    		    		</div>
                            			<div style="float:left">
                            				<input type="submit" value="verify"/>
                            			</div>
                            		</div>
                        		</div>  
        	                  </fieldset>
                             </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Company Email:*') }}</b></label>

                            <div class="col-md-4">
                            	<input type="text" placeholder="Company Email - not on general domain" id="" name="" />
                            </div>
                            
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Confirm Email:*') }}</b></label>
                            <div class="col-md-4">
                            	<input type="text" Placeholder="Confirm Email"/>
                            </div>
                        </div>
                         <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Company Mobile:*') }}</b></label>

                            <div class="col-md-4">
                            	<input id="company_mobile" type="text" placeholder='Company Mobile' class="form-control{{ $errors->has('company_mobile') ? ' is-invalid' : '' }}" name="comapny_mobile" value="{{ old('comapny_mobile') }}" required autofocus>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Province:*') }}</b></label>

                            <div class="col-md-4">
                            	<select id="" name="" >
                            		<option value="0">Province</option>
                            		<option value="1">Punjab</option>
                            		<option value="2">Sindh</option>
                            		<option value="3">Balochistan</option>
                            	</select>
                            </div>
                            
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('City:*') }}</b></label>

                            <div class="col-md-4">
                            	<select id="" name="" >
                            		<option value="0">City</option>
                            		<option value="1">Lahore</option>
                            		<option value="2">Faisalabad</option>
                            		<option value="3">Rawalpindi</option>
                            	</select> Needs to get the Location City Automatically
                            </div>
                        </div>
                        <div class="form-group row">
                       		<label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Address:*') }}</b></label>

                            <div class="col-md-4">
                            	<textarea ></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                       		<label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Company Stamp:*') }}</b></label>

                            <div class="col-md-4">
                                <input type="file" name="id_img" id="id_img" />
                            </div>
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Board Of Resolution:*') }}</b></label>

                            <div class="col-md-4">
                                <input type="file" name="id_img" id="id_img" />
                            </div>
                        </div>
                        
                         <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Google:*') }}</b></label>
                            <div class="col-md-4">
                            	<p>needs to be integrated </p>
                            </div>
                        </div>
                         <div class="form-group row">
                       		<div class="col-md-12">
                       			<input type="checkbox" name="chk-box" id="chk-box" /> I have read and agree to the Terms & condition.
                       		</div>
                        </div>
                        
                        

                        <div class="form-group row mb-0">
                            <div class="col-md-12 offset-md-4" style="text-align: center">
                                <input type="submit" value="{{ __('Register') }}" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@push('scripts')

@endpush