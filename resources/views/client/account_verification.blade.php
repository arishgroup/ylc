@extends('layouts.client')

@section('content')

<div class="container">
<h2 class="entry-title">{{ __('Account Verification') }}</h2>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-8 col-form-label text-md-right"><b>{{ __('2-Copies of Utility Bills :*') }}</b></label>
                        </div>
                        
                        <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Current Month:*') }}</b></label>

                            <div class="col-md-4">
                    		    <input type="file"/>
                            </div>
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Previous Month:*') }}</b></label>

                            <div class="col-md-4" >
                            	<input type="file"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Mobile Number:*') }}</b></label>

                            <div class="col-md-4">
                    		    <input type="text" id="" name="" />
                            </div>
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('NIC (Optional):*') }}</b></label>

                            <div class="col-md-4" >
                            	<input type="text" id="" name="" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Mobile Pin:*') }}</b></label>

                            <div class="col-md-4">
                            	<input type="text" id="" name="" /> <button >Resend Pin</button>
                    		    
                            </div>
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Email:*') }}</b></label>

                            <div class="col-md-4" >
                            	<input type="text" id="" name="" /> <button >Resend Pin</button>
                            </div>
                        </div>
                        <div class="form-group row" style="padding-bottom:130px">
                            
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-12 offset-md-4" style="text-align: center">
                                <input type="submit" value="{{ __('Account Verification') }}" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@stop
@push('scripts')

@endpush