@extends('layouts.client')

@section('content')

<div class="container">
<h2 class="entry-title">{{ __('Family Account Registeration') }}</h2>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Your ID:*') }}</b></label>

                            <div class="col-md-4">
                            	<div class="">
                            		<div class="mr-1" style="float:left">
                                        <select id='id_type' name="" class="form-control">
                                        	<option value='1'>NIC</option>
                                        	<option value='2'>Bay Form</option>
                                        </select>
                                     </div>
                                    <div style="float:left">
                                    	<input id="nic" type="text" placeholder='NIC / BAY FORM' class="form-control{{ $errors->has('nic') ? ' is-invalid' : '' }}" name="nic" value="{{ old('nic') }}" required autofocus>
    								
                                    @if ($errors->has('nic'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('nic') }}</strong>
                                        </span>
                                    @endif
                                    </div>
                                </div>
                            </div>
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('ID - Image:*') }}</b></label>

                            <div class="col-md-4">
                                <input type="file" name="id_img" id="id_img" />
                            </div>
                        </div>
                       <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Family Member:*') }}</b></label>

                            <div class="col-md-4">
                            	<input id="family_member" type="number" placeholder='Family Members' class="form-control{{ $errors->has('family_member') ? ' is-invalid' : '' }}" name="family_member" value="{{ old('family_member') }}" required autofocus>
                            </div>
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Profile - Image:*') }}</b></label>

                            <div class="col-md-4">
                                <input type="file" name="id_img" id="id_img" />
                            </div>
                        </div>
						<div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('First Name:*') }}</b></label>

                            <div class="col-md-4">
                            	<div class="">
                            		<div class="mr-1" style="float:left">
                                        <select id='salutation' name="salutation" class="form-control">
                                        	<option value='1'>Mr.</option>
                                        	<option value='2'>Mrs.</option>
                                        	<option value='3'>Miss.</option>
                                        </select>
                                     </div>
                                    <div style="float:left">
                                    	<input id="first_name" type="text" placeholder='First Name' class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}" required autofocus>
    								
                                    @if ($errors->has('first_name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('first_name') }}</strong>
                                        </span>
                                    @endif
                                    </div>
                                </div>
                            </div>
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Last Name:*') }}</b></label>

                            <div class="col-md-4">
                                <input id="last_name" type="text" placeholder='Last Name' class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name') }}" required autofocus>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Fathers Name:*') }}</b></label>

                            <div class="col-md-4">
                            	<input id="f_name" type="text" placeholder='Fathers Name' class="form-control{{ $errors->has('f_name') ? ' is-invalid' : '' }}" name="f_name" value="{{ old('f_name') }}" required autofocus>
                            </div>
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Mothers Name:*') }}</b></label>

                            <div class="col-md-4">
                               <input id="m_name" type="text" placeholder='Mothers Name' class="form-control{{ $errors->has('m_name') ? ' is-invalid' : '' }}" name="m_name" value="{{ old('m_name') }}" required autofocus>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Email:*') }}</b></label>

                            <div class="col-md-4">
                            	<input id="email" type="email" placeholder='Email' class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                            </div>
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Confirm Email:*') }}</b></label>

                            <div class="col-md-4">
                               <input id="c_email" type="text" placeholder='Confirm Email' class="form-control{{ $errors->has('c_email') ? ' is-invalid' : '' }}" name="c_email" value="{{ old('c_email') }}" required autofocus>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Password:*') }}</b></label>

                            <div class="col-md-4">
                            	<input id="password" type="password" placeholder='Password' class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" value="{{ old('password') }}" required autofocus>
                            </div>
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Confirm Password:*') }}</b></label>

                            <div class="col-md-4">
                               <input id="c_password" type="text" placeholder='Confirm Password' class="form-control{{ $errors->has('c_password') ? ' is-invalid' : '' }}" name="c_password" value="{{ old('c_password') }}" required autofocus>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Mobile Number:*') }}</b></label>

                            <div class="col-md-4">
                            	<div style="float:left">
                            		<input id="mobile_number" type="number" placeholder='Mobile Code' class="form-control{{ $errors->has('mobile_code') ? ' is-invalid' : '' }}" name="mobile_code" value="{{ old('mobile_code') }}" required autofocus>
                            		
                            	</div>
                            	<div style="float:left">
                            		<input type="submit" value="verify number"/>
                            		
                            	</div>
                            </div>
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Verify Number:*') }}</b></label>

                            <div class="col-md-4">
                            	<div style="float:left">
                            		<input id="verify_num" type="text" placeholder='Verify Number' class="form-control{{ $errors->has('verify_num') ? ' is-invalid' : '' }}" name="verify_num" value="{{ old('verify_num') }}" required autofocus>
                            		
                            	</div>
                            	<div style="float:left">
                            		<input type="submit" value="verify"/>
                            		
                            	</div>
                            </div>
                        </div>
                         <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Job Title:*') }}</b></label>

                            <div class="col-md-4">
                            	<select id="" name="" >
                            		<option value="0">Job Title</option>
                            		<option value="1">Doctor</option>
                            		<option value="2">Engineer</option>
                            		<option value="3">Others</option>
                            	</select>
                            </div>
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Company:*') }}</b></label>

                            <div class="col-md-4">
                            	<input id="company" type="text" placeholder='Company Name' class="form-control{{ $errors->has('comapny') ? ' is-invalid' : '' }}" name="comapny" value="{{ old('comapny') }}" required autofocus>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Sex:*') }}</b></label>

                            <div class="col-md-4">
                            	<input type="radio" id="sex" name="sex" value="male"> Male
                            	<input type="radio" id="sex" name="sex" value="female"> Female
                            </div>
                            
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('DOB:*') }}</b></label>

                            <div class="col-md-4">
                            	<input id="dob" type="text" placeholder='DOB' class="form-control{{ $errors->has('DOB') ? ' is-invalid' : '' }}" name="DOB" value="{{ old('DOB') }}" required autofocus>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Province:*') }}</b></label>

                            <div class="col-md-4">
                            	<select id="" name="" >
                            		<option value="0">Province</option>
                            		<option value="1">Punjab</option>
                            		<option value="2">Sindh</option>
                            		<option value="3">Balochistan</option>
                            	</select>
                            </div>
                            
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('City:*') }}</b></label>

                            <div class="col-md-4">
                            	<select id="" name="" >
                            		<option value="0">City</option>
                            		<option value="1">Lahore</option>
                            		<option value="2">Faisalabad</option>
                            		<option value="3">Rawalpindi</option>
                            	</select> Needs to get the Location City Automatically
                            </div>
                        </div>
                         <div class="form-group row">
                       		<label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Address:*') }}</b></label>

                            <div class="col-md-4">
                            	<textarea ></textarea>
                            </div>
                        </div>
                         <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Google:*') }}</b></label>

                            <div class="col-md-4">
                            	<p>needs to be integrated </p>
                            </div>
                            
                            <label for="name" class="col-md-2 col-form-label text-md-right"><b>{{ __('Signature:*') }}</b></label>

                            <div class="col-md-4">
                            	<p>needs to be integrated </p>
                            </div>
                        </div>
                        <div class="form-group row">
                       		<div class="col-md-12">
                       			<input type="checkbox" name="chk-box" id="chk-box" /> I have read and agree to the Terms & condition.
                       		</div>
                        </div>
                        
                        

                        <div class="form-group row mb-0">
                            <div class="col-md-12 offset-md-4" style="text-align: center">
                                <input type="submit" value="{{ __('Register') }}" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@push('scripts')

@endpush