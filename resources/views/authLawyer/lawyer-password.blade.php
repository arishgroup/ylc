@extends('layouts.adminApp')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Lawyer Login') }}</div>

                <div class="card-body">
                    {!! Form::open(['route' => 'lawyer.password.submit', 'method' => 'post', 'class'=>'form-horizontal']) !!}
              			{{ csrf_field() }}

                        <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('Set Password') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" value="{{ old('password') }}" required autofocus>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-sm-4 col-form-label text-md-right">{{ __('Retype Password') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="re_password" value="{{ old('re_password') }}" required autofocus>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            
                        </div>
 						{!! FORM::hidden('email' ,$email) !!}
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Signup') }}
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your ID?') }}
                                </a>
                            </div>
                        </div>
                       {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
