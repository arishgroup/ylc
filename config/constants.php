<?php

    return [
        'menu_links' =>[
            'staff_emp' => [    
                'allEmployees',
                'create_lawyer',
                'lawyers_log',
                'personal_tree',
                'lawyers_update'
                
            ],
            'staff_user' => [
                'user_registration_index',
                'user_registration_create',
                'user_activity_log',
                'user_registration_edit'
            ],
            'masterData_user_type' => [
                'utype_index',
                'utype_create',
                'utype_edit'
            ],
            'masterData_designation' => [
                'designation_list',
                'designation_create',
                'designation_edit'
            ],
            
        ],
        'user_type' => [
            'task_management' =>[
            'user' => 'User',
            'lawyer' => 'Lawyer',
            'client' => 'Client'
            ],

            'news' =>[
                'lawyer' => 'Lawyer',
                'client' => 'Client'
            ]
        ],
        
        'country' =>[
            'us'=>'United States',
            'pk' => 'Pakistan'
        ],
        'US_STATE' => [
            'AL'=>"Alabama",
            'AK'=>"Alaska",
            'AZ'=>"Arizona",
            'AR'=>"Arkansas",
            'CA'=>"California",
            'CO'=>"Colorado",
            'CT'=>"Connecticut",
            'DE'=>"Delaware",
            'DC'=>"District Of Columbia",
            'FL'=>"Florida",
            'GA'=>"Georgia",
            'HI'=>"Hawaii",
            'ID'=>"Idaho",
            'IL'=>"Illinois",
            'IN'=>"Indiana",
            'IA'=>"Iowa",
            'KS'=>"Kansas",
            'KY'=>"Kentucky",
            'LA'=>"Louisiana",
            'ME'=>"Maine",
            'MD'=>"Maryland",
            'MA'=>"Massachusetts",
            'MI'=>"Michigan",
            'MN'=>"Minnesota",
            'MS'=>"Mississippi",
            'MO'=>"Missouri",
            'MT'=>"Montana",
            'NE'=>"Nebraska",
            'NV'=>"Nevada",
            'NH'=>"New Hampshire",
            'NJ'=>"New Jersey",
            'NM'=>"New Mexico",
            'NY'=>"New York",
            'NC'=>"North Carolina",
            'ND'=>"North Dakota",
            'OH'=>"Ohio",
            'OK'=>"Oklahoma",
            'OR'=>"Oregon",
            'PA'=>"Pennsylvania",
            'RI'=>"Rhode Island",
            'SC'=>"South Carolina",
            'SD'=>"South Dakota",
            'TN'=>"Tennessee",
            'TX'=>"Texas",
            'UT'=>"Utah",
            'VT'=>"Vermont",
            'VA'=>"Virginia",
            'WA'=>"Washington",
            'WV'=>"West Virginia",
            'WI'=>"Wisconsin",
            'WY'=>"Wyoming"
        ],
        'PAK_PROVINCE' => [
            'punjab'=> 'Punjab',
            'sindh'=> 'Sindh',
            'balochistan'=> 'Balochistan',
            'nwfp'=> 'Khyber Pakhtunwa',
            'ak'=> 'Azad Kashmir',
            'FATA'=> 'Federal Administrative Tribal Area',
            'gb'=> 'Gilgit-Baltistan',
            'isl' => 'Fedral Capital',
        ],
        'isl' => [
            'isl-1' => 'Islamabad',
        ],
        'punjab' => [
            'punjab-1'=>'Lahore',
            'punjab-2'=>'Faisalabad',
            'punjab-3'=>'Rawalpindi',
             'punjab-4'=>'Multan',
            'punjab-5'=>'Gujranwala',
             'punjab-6'=>'Bahawalpur',
            'punjab-7'=>'Sargodha',
             'punjab-8'=>'Sialkot City',
            'punjab-9'=>'Sheikhupura',
             'punjab-10'=>'Rahimyar Khan',
            'punjab-11'=>'Jhang Sadr',
             'punjab-12'=>'Gujrat',
            'punjab-13'=>'Kasur',
             'punjab-14'=>'Dera Ghazi Khan',
            'punjab-15'=>'Masiwala',
             'punjab-16'=>'Okara',
            'punjab-17'=>'Chiniot',
             'punjab-18'=>'Sadiqabad',
            'punjab-19'=>'Islamabad',
             'punjab-20'=>'Kundian',
            'punjab-21'=>'Pakpattan',
             'punjab-22'=>'Chakwal',
            'punjab-23'=>'Khushab',
             'punjab-24'=>'Jhelum',
            'punjab-25'=>'Mandi Bahauddin',
             'punjab-26'=>'Bhakkar',
            'punjab-27'=>'Toba Tek Singh',
             'punjab-28'=>'Vihari',
            'punjab-29'=>'Lodhran',
            'punjab-30'=>' Muzaffargarh',
            'punjab-31'=>'Jhang City',
             'punjab-32'=>'Sahiwal',
            'punjab-33'=>'Hafizabad',
            'punjab-34'=>' Nankana Sahib',
            'punjab-35'=>'Bahawalnagar',
             'punjab-36'=>'Khanewal',
            'punjab-37'=>'Narowal',
             'punjab-38'=>'Attock City',
            'punjab-39'=>'Rajanpur',
             'punjab-40'=>'Leiah',
            'punjab-41'=>'Mianwali',
            
        ],
        'sindh' => [
            'sindh-1'=>'Karachi',
            'sindh-2'=>'Hyderabad City',
            'sindh-3'=>'Sukkur',
            'sindh-4'=>'Larkana',
            'sindh-5'=>'Mirpur Khas',
            'sindh-6'=>'Shahdad Kot',
            'sindh-7'=>'Nawabshah',
            'sindh-8'=>'Matiari',
            'sindh-9'=>'Thatta',
            'sindh-10'=>'Badin',
            'sindh-11'=>'Tando Allahyar',
            'sindh-12'=>'Sanghar',
            'sindh-13'=>'Ghotki',
            'sindh-14'=>'Kandhkot',
            'sindh-15'=>'Tando Muhammad Khan',
            'sindh-16'=>'Naushahro Firoz',
            'sindh-17'=>'Jacobabad',
            'sindh-18'=>'Umarkot',
            'sindh-19'=>'Jamshoro',
            'sindh-20'=>'Khairpur',
            'sindh-21'=>'Shikarpur',
            'sindh-22'=>'Dadu',
        ],
        'balochistan' => [
            'balochistan-1'=>'Quetta',
            'balochistan-2'=>'Turbat',
            'balochistan-3'=>'Chaman',
            'balochistan-4'=>'Zhob',
            'balochistan-5'=>'Gwadar',
            'balochistan-6'=>'Kohlu',
            'balochistan-7'=>'Khuzdar',
            'balochistan-8'=>'Awaran',
            'balochistan-9'=>'Panjgur',
            'balochistan-10'=>'Mastung',
            'balochistan-11'=>'Kalat',
            'balochistan-12'=>'Gandava',
            'balochistan-13'=>'Dera Allahyar',
            'balochistan-14'=>'Ziarat',
            'balochistan-15'=>'Dera Murad Jamali',
            'balochistan-16'=>'Loralai',
            'balochistan-17'=>'Dera Bugti',
            'balochistan-18'=>'Barkhan',
            'balochistan-19'=>'Uthal',
            'balochistan-20'=>'Killa Abdullah',
            'balochistan-21'=>'Sibi',
            'balochistan-22'=>'Dalbandin',
            'balochistan-23'=>'Musa Khel Baazar',
            'balochistan-24'=>'Kharan',
            'balochistan-25'=>'Qila Saifullah',
            'balochistan-26'=>'Pishin'
        ],
        'nwfp' => [
            'nwfp-1'=>'Serai',
            'nwfp-2'=>'Peshawar',
            'nwfp-3'=>'Abbottabad',
            'nwfp-4'=>'Bannu',
            'nwfp-5'=>'Kohat',
            'nwfp-6'=>'Bardar',
            'nwfp-7'=>'Dera Ismail Khan',
            'nwfp-8'=>'Mehra',
            'nwfp-9'=>'Haripur',
            'nwfp-10'=>'Karak',
            'nwfp-11'=>'Mardan',
            'nwfp-12'=>'Batgram',
            'nwfp-13'=>'Mansehra',
            'nwfp-14'=>'Nowshera',
            'nwfp-15'=>'Charsadda',
            'nwfp-16'=>'Malakand',
            'nwfp-17'=>'Saidu Sharif',
            'nwfp-18'=>'Hangu',
            'nwfp-19'=>'Timargara',
            'nwfp-20'=>'Dasu',
            'nwfp-21'=>'Swabi',
            'nwfp-22'=>'Upper Dir',
            'nwfp-23'=>'Daggar',
            'nwfp-24'=>'Lakki Marwat',
            'nwfp-25'=>'Tank',
            'nwfp-26'=>'Chitral',
            
        ],
        'ak' => [
            'ak-1'=>'Bagh',
            'ak-2'=>'Kotli',
            'ak-3'=>'Athmuqam',
            'ak-4'=>'Rawala Kot',
            'ak-5'=>'New Mirpur',
            
        ],
        'FATA' => [
            'FATA-1'=>'Parachinar',
        ],
        'gb' => [
            'gb-1'=>'Gilgit',
            'gb-2'=>'Eidgah',
            'gb-3'=>'Gakuch',
            'gb-4'=>'Alaabad',
            'gb-5'=>'Chilas',
        ],

        'priority' =>[
            ''=>'Select Priority',
            '0'=>'Minor',
            '1'=> 'Low',
            '2'=>'Normal',
            '3'=> 'High',
            '4'=>'Urgent',
        ],
        'status' =>[
            ''=>'Select Status',
            '0'=>'Pending',
            '1'=> 'In Progress',
            '2'=>'Complete',
        ],
    ];