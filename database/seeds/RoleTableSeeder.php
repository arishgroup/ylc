<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $role =  new Role();
        $role->name = "Research Lawyer";
        $role->Desciprtion = "Description will be provided here";
        $role->save();
        
        $role =  new Role();
        $role->name = "Data Entry";
        $role->Desciprtion = "Only Access to data entry";
        $role->save();
    }
}
