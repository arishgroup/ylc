<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLawyerProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lawyer_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('profile_img_ext');
            $table->string('last_name');
            $table->string('mobile');
            $table->string('nic');
            $table->string('nic_ext');
            $table->string('email');
            $table->string('aos_id');
            $table->string('location');
            $table->longText('office_address');
            $table->longText('home_address');
            $table->longText('experiance');
            $table->longText('notes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lawyer_profiles');
    }
}
