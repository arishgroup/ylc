<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Foundation\Testing\Constraints\SoftDeletedInDatabase;

class CreateLawyerEducationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lawyer_educations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('institute');
            $table->string('degree');
            $table->string('from_year');
            $table->string('to_year');
            $table->string('grade');
            $table->string('hobbies');
            $table->longText('description');
            $table->string('lawyer_id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lawyer_educations');
    }
}
