<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLawyersProfileEducationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lawyers_profile_educations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('institute');
            $table->string('degree');
            $table->string('degree_ext');
            $table->string('from_year');
            $table->string('to_year');
            $table->string('grade');
            $table->string('hobbies');
            $table->longText('description');
            $table->string('lawyer_profile_id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lawyers_profile_educations');
    }
}
