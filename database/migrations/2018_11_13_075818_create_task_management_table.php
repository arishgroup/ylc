<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskManagementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_management', function (Blueprint $table) {
            $table->increments('id');
            $table->string('assignee_type');
            $table->integer('assignee_id');
            $table->string('task_name');
            $table->longText('task_description');
            $table->date('assigner_start_date');
            $table->date('assigner_end_date');
            $table->date('assignee_start_date');
            $table->date('assignee_end_date');
            $table->string('priority');
            $table->string('status');
            $table->integer('assigner_id');
            $table->string('assigner_type');
//            $table->string('assignee_table');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task_management');
    }
}
