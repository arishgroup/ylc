<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentLibrariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_libraries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('court');
            $table->string('applent');
            $table->string('verses');
            $table->string('page_no');
            $table->string('reference');
            $table->string('judge_name');
            $table->string('lawyer_name');
            $table->string('rule');
            $table->string('section');
            $table->string('citation_name');
            $table->string('journel');
            $table->string('category');
            $table->string('link');
            $table->string('file_ext');
            $table->string('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content_libraries');
    }
}
