
var simple_chart_config = {
	chart: {
		container: "#OrganiseChart-simple"
	},
	
	nodeStructure: {
		text: { name: "Parent node" },
		children: [
			{
				text: { name: "First child" },
				link: {
		            href: "www.google.com"
		        },
			},
			{
				text: { name: "Second childs" }
			},
			{
				text: { name: "Third child" }
			}
			,
			{
				text: { name: "Third parent child" }
			}
		]
	}
};

// // // // // // // // // // // // // // // // // // // // // // // // 

var config = {
	container: "#OrganiseChart-simple"
};

var parent_node = {
	text: { name: "Parent node" }
};

var first_child = {
	parent: parent_node,
	text: { name: "First child" }
};

var second_child = {
	parent: parent_node,
	text: { name: "Second child" }
};

var third_child = {
		parent: parent_node,
		text: { name: "Third child" }
	};
var p_one_child = {
		parent: third_child,
		text: { name: "Third P child" }
	};
var simple_chart_config = [
	config, parent_node,
		first_child, second_child,third_child,p_one_child 
];
