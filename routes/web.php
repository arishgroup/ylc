<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test', function () { return view('welcome-test'); });
Route::get('/', function () { return view('welcome'); });

Auth::routes();

Route::get('/register_type', 'Auth\RegisterController@getRegistertype')->name('getRegistertype');
Route::post('/registeration', 'Auth\RegisterController@registertype')->name('registertype');
Route::get('/account_activation', 'Auth\RegisterController@accountActivate')->name('activation');
Route::get('/account_verification', 'Auth\RegisterController@accountVerification')->name('verification');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('logActivity', 'HomeController@logActivity');

Route::post('/users/logout', 'Auth\LoginController@userLogout')->name('user.logout');

Route::get('/pdf_viewer/{enc_path}', 'ShowPdfController@showPdf')->name('show.pdf');
Route::get('/word_viewer/{enc_path}', 'ShowPdfController@showWord')->name('show.word');

Route::get('/getSections/{id}/{select}', 'AjaxController@getSections')->name('getSection');
Route::get('/getCaseLaws/find', 'AjaxController@getCaseLaws');
Route::get('/get_content','AjaxController@getCaseLawContent');
Route::get('/getLawyers','AjaxController@getLawyers');
Route::get('/getClients','AjaxController@getClients');
Route::post('/top_navigation','NavigationAjaxController@top_navigation')->name('top.navigation');



Route::get('/assignerTasksStatus/{id}','AjaxTaskManagementController@assignerTasksStatus');
Route::post('/assigneeTasksStatus','AjaxTaskManagementController@assigneeTasksStatus');
Route::get('/getAssignee/{id}','AjaxTaskManagementController@getAssignee');
Route::get('/getDataTables/{id?}','AjaxTaskManagementController@getDataTables');
Route::get('/getAssigneeDataTables/{id?}','AjaxTaskManagementController@getAssigneeDataTables');
Route::get('/dataTablesNavigation/{id}','AjaxTaskManagementController@dataTablesNavigation');
Route::get('/assigneeTasksStatusCount/{id?}','AjaxTaskManagementController@assigneeTasksStatusCount');
Route::get('/assigneeDataTablesNavigation/{id}','AjaxTaskManagementController@assigneeDataTablesNavigation');
Route::get('/startTask/{id}','AjaxTaskManagementController@startTask');
Route::get('/endTask/{id}','AjaxTaskManagementController@endTask');
Route::post('/assignee_update_tasks/{id?}', 'AjaxTaskManagementController@assignee_update_tasks')->name('ajax.update');

Route::get('/get_state/{code}','AjaxController@getCountryState');
Route::get('/get_city/{code}','AjaxController@getCity');
// Book Controller

Route::prefix('books')->group(function(){
    Route::get('/index','BookController@index')->name('book.index');
    Route::get('/create','BookController@create')->name('book.create');
    Route::post('/store','BookController@store')->name('book.store');
    Route::post('/update','BookController@update')->name('book.update');
    Route::get('/edit/{id}','BookController@edit')->name('book.edit');
    Route::get('/destroy','BookController@destroy')->name('book.destroy');
    Route::get('/destroy_chapter/{id}','BookController@destroyAttachment')->name('book.attachment.destroy');

    Route::get('/lawyer_index','lawyer\BookController@index')->name('lawyer.book.index');
    Route::get('/lawyer_show/{id}','lawyer\BookController@show')->name('lawyer.book.show');

    Route::get('/admin_index','admin\BookController@index')->name('admin.book.index');
    Route::get('/admin_show/{id}','admin\BookController@show')->name('admin.book.show');
    Route::get('/admin_edit/{id}','admin\BookController@edit')->name('admin.book.edit');
    Route::post('/update/{id}','admin\BookController@update')->name('admin.book.update');
});
Route::prefix('lawyer')->group(function(){
    Route::get('/signup', 'LawyersController@showSignupForm')->name('lawyer.signup');
    Route::post('/signup', 'LawyersController@signup')->name('lawyer.signup.submit');

    Route::get('/', 'LawyersController@dashboard')->name('lawyers.dashboard');

    Route::get('/setpassword', 'LawyersController@setPassword')->name('lawyer.setPassword');
    Route::post('/updatepassword', 'LawyersController@updatePassword')->name('lawyer.password.submit');

    Route::get('/login', 'AuthLawyer\LoginController@showLoginForm')->name('lawyer.login');
    Route::post('/login', 'AuthLawyer\LoginController@login')->name('lawyer.login.submit');

    Route::get('/dashboard', 'LawyersController@dashboard')->name('lawyers.dashboard');
    Route::get('/datatableListing', 'LawyersController@datatable')->name('lawyer.datatable');
    Route::GET('/search', 'SearchContentController@index')->name('search.index');
    Route::get('/lawyers_update/{id}', 'LawyerRegistrationController@edit')->name('lawyers.profile');
    Route::post('/logout', 'AuthLawyer\LoginController@userLogout')->name('lawyer.logout');

    Route::get('/view_tasks', 'LawyerTaskManagementController@index')->name('lawyers.taskManagement.index');
    Route::get('/edit_tasks/{id}', 'LawyerTaskManagementController@edit')->name('lawyers.taskManagement.edit');
    Route::get('/create_tasks/{id?}', 'LawyerTaskManagementController@create')->name('lawyers.taskManagement.create');
    Route::POST('/store_tasks/', 'LawyerTaskManagementController@store')->name('lawyers.taskManagement.store');

    Route::get('/endTask/{id}','LawyerTaskManagementController@endTask')->name('lawyers.taskManagement.endTask');

    Route::get('/view_news', 'LawyerNewsController@index')->name('lawyers.news.index');
    Route::get('/show_news/{id}', 'LawyerNewsController@edit')->name('lawyers.news.show');
});

Route::prefix('/data_library')->group(function(){
    Route::get('/', 'DataLibraryController@create')->name('lawyers.datalib');
    Route::get('/destroy/{id}', 'DataLibraryController@destroy')->name('lawyers.datalib.destroy');
    Route::get('/edit/{id}','DataLibraryController@edit')->name('lawyers.datalib.edit');
    Route::put('/update/{id}', 'DataLibraryController@update')->name('datalib.update');
    Route::get('/download/{id}', 'DataLibraryController@getDownload')->name('lawyers.datalib.download');
    Route::get('/index', 'DataLibraryController@index')->name('lawyers.datalib.index');
    Route::POST('/content', 'DataLibraryController@store')->name('lawyers.content.upload');

    Route::get('/task_management_list', 'UsersTaskManagementController@index')->name('users.taskmanagement.index');
    Route::get('/task_management_edit/{id?}', 'UsersTaskManagementController@edit')->name('users.taskmanagement.edit');
});


Route::get('/tags/find', 'TagController@find');
// Add Tags
Route::get('/tags_index', 'TagController@index')->name('tags.index');
Route::get('/tags_create', 'TagController@create')->name('tags.create');
Route::post('/tags_store', 'TagController@store')->name('tags.store');
Route::get('/tags_destroy/{id}', 'TagController@destroy')->name('tags.destroy');
Route::get('/tags_edit/{id}','TagController@edit')->name('tags.edit');
Route::put('/tags_update/{id}','TagController@update')->name('tags.update');

// Add Reference Material
Route::prefix('admin/profile')->group(function(){
    Route::get('/judge_index', 'JudgesProfileController@index')->name('judge_profile.index');
    Route::get('/judge_create', 'JudgesProfileController@create')->name('judge_profile.create');
    Route::post('/judge_store', 'JudgesProfileController@store')->name('judge_profile.store');
    Route::get('/judge_destroy/{id}', 'JudgesProfileController@destroy')->name('judge_profile.destroy');
    Route::get('/judge_edit/{id}','JudgesProfileController@edit')->name('judge_profile.edit');
    Route::put('/judge_update/{id}','JudgesProfileController@update')->name('judge_profile.update');
    Route::get('/judge/find', 'JudgesProfileController@find')->name('judge_profile.find');
    
    
    // Lawyers Profile
    Route::get('/lawyer_profile_index', 'LawyersController@lawyersProfileIndex')->name('lawyer_profile.index');
    Route::get('/lawyer_profile_create', 'LawyersController@lawyersProfileCreate')->name('lawyers.profile.create');
    Route::post('/lawyer_profile_store', 'LawyersController@lawyersProfilestore')->name('lawyers.profile.store');
    Route::get('/lawyer_profile_edit/{id}', 'LawyersController@lawyersProfileEdit')->name('lawyers.profile.edit');
    Route::post('/lawyer_profile_update/{id}', 'LawyersController@lawyersProfileUpdate')->name('lawyers.profile.update');
    Route::get('/lawyer_profile_destroy/{id}', 'LawyersController@lawyersProfileDestroy')->name('lawyers.profile.destroy');
});
// Add Tags


Route::post('/utype_store', 'UserTypeController@store')->name('utype.store');
Route::get('/utype_destroy/{id}', 'UserTypeController@destroy')->name('utype.destroy');
Route::put('/utype_update/{id}','UserTypeController@update')->name('utype.update');

Route::prefix('user')->group(function(){
    Route::get('/login', 'AuthBackEndUser\LoginController@showLoginForm')->name('user.login');
    Route::post('/login', 'AuthBackEndUser\LoginController@login')->name('user.login.submit');
    Route::get('/', 'BackendUserController@index')->name('user.dashboard');
    Route::post('/logout', 'AuthBackEndUser\LoginController@userLogout')->name('user.logout');
});

Route::prefix('admin/master_data')->group(function(){
    Route::get('/utype_index', 'UserTypeController@index')->name('utype.index');
    Route::get('/udatatableListing', 'UserTypeController@datatable')->name('userType.datatable');
    Route::get('/utype_create', 'UserTypeController@create')->name('utype.create');
    Route::get('/utype_edit/{id}','UserTypeController@edit')->name('utype.edit');
    
    
    Route::get('/designation_list','LawyersDesignationController@index')->name('lawyers.designation.index');
    Route::get('/datatableListing', 'LawyersDesignationController@datatable')->name('designation.datatable');
    Route::get('/designation_create','LawyersDesignationController@create')->name('lawyers.designation.create');
    Route::post('/designation_store', 'LawyersDesignationController@store')->name('admin.lawyer.designation.store');
    Route::get('/designation_edit/{id}','LawyersDesignationController@edit')->name('lawyer.designation.edit');
    Route::put('/designation_update/{id}', 'LawyersDesignationController@update')->name('lawyer.designation.update');
    Route::get('/designation_destroy/{id}', 'LawyersDesignationController@destroy')->name('lawyer.designation.destroy');
    
});
Route::prefix('admin/staff')->group(function(){
    Route::get('/allEmployees', 'LawyersController@index')->name('lawyers.index');
    Route::get('/create_lawyer','LawyerRegistrationController@create')->name('lawyers.register');
    Route::post('/store', 'LawyerRegistrationController@store')->name('lawyers.store');
    Route::get('/lawyers_update/{id}', 'LawyerRegistrationController@edit')->name('lawyers.edit');
    Route::post('/lawyers_update_data/{id}', 'LawyerRegistrationController@update')->name('lawyers.update');
    Route::get('/destroy/{id}', 'LawyerRegistrationController@destroy')->name('lawyers.destroy');
    Route::get('/lawyers_log/{id}/{type}', 'LawyersController@activityLog')->name('lawyers.log');
    Route::get('/personal_tree/{id}/{type}','AreaOfServiceController@personalLawyerTreeFromAdmin')->name('lawyer.personal.tree');
   
    // Backend User
    Route::get('/user_registration_index/{type}', 'UserRegistrationController@index')->name('user_registration.index');
    
    Route::get('/user_registration_create/{type}', 'UserRegistrationController@create')->name('user_registration.create');
    Route::get('/user_activity_log/{id}/{type}', 'UserRegistrationController@logActivity')->name('user_registration.log');
    Route::post('/user_registration_search', 'UserRegistrationController@searchLog')->name('search.log');
    Route::get('/user_registration_edit/{id}','UserRegistrationController@edit')->name('user_registration.edit');
});

Route::prefix('admin')->group(function(){
    //Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    //Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');

    Route::get('/login', 'AuthAdmin\LoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'AuthAdmin\LoginController@login')->name('admin.login.submit');

    Route::get('', 'AdminController@index')->name('admin.dashboard');
    Route::get('profile/{id}', 'AdminController@edit')->name('admin.profile');
    Route::post('profile_update/{id}', 'AdminController@update')->name('admin.profile.update');

    Route::post('/logout', 'AuthAdmin\LoginController@userLogout')->name('admin.logout');

    // user registration
    Route::get('/datatableListing', 'UserRegistrationController@datatable')->name('user.datatable');
    Route::post('/user_registration_store', 'UserRegistrationController@store')->name('user_registration.store');
    
    Route::put('/user_registration_update/{id}', 'UserRegistrationController@update')->name('user_registration.update');
    Route::get('/user_registration_destroy/{id}', 'UserRegistrationController@destroy')->name('user_registration.destroy');
    Route::get('/user_registration_show/{id}', 'UserRegistrationController@show')->name('user_registration.show');

    
    // Lawyer Teams
    Route::get('/lawyer_teams', 'admin\LawyerTeamsController@index')->name('lawyer.teams.index');
    Route::get('/team_datatable', 'admin\LawyerTeamsController@fetchRecord')->name('datatables.teams');
    Route::get('/team_create/{aos_id}', 'admin\LawyerTeamsController@create')->name('team.create');
    Route::post('/lawyer_teams_store', 'admin\LawyerTeamsController@store')->name('lawyer.teams.store');
    Route::get('/get_lawyers/{id}', 'AjaxController@get_Lawyers')->name('lawyers.ajax');
    Route::get('/get_all_teams', 'AjaxController@getAllTeams')->name('lawyers.allteams');
    
    
});

// Add Reference Material
Route::get('/ref_material/{id}', 'ReferenceMaterialController@index')->name('reference.material');
Route::get('/ref_material_create/{id}', 'ReferenceMaterialController@create')->name('reference_note.create');
Route::put('/ref_material_store/{id}', 'ReferenceMaterialController@store')->name('reference_note.store');
Route::get('/ref_material_destroy/{id}', 'ReferenceMaterialController@destroy')->name('reference_note.destroy');
Route::get('/ref_material_edit/{id}','ReferenceMaterialController@edit')->name('reference_note.edit');
Route::put('/ref_material_update/{id}','ReferenceMaterialController@update')->name('reference_note.update');

//Circular data
Route::get('/circular', 'CircularController@index')->name('miscellaneous.index');
Route::get('/circular_create', 'CircularController@create')->name('miscellaneous.create');
Route::post('/circular_store', 'CircularController@store')->name('miscellaneous.store');
Route::get('/circular_edit/{id}','CircularController@edit')->name('miscellaneous.edit');
Route::put('/circular_update/{id}', 'CircularController@update')->name('miscellaneous.update');
Route::get('/circular_destroy/{id}', 'CircularController@destroy')->name('miscellaneous.destroy');

// Miscellaneous Document type
Route::get('/doc_type', 'MiscellaneousDocTypeController@index')->name('miscellaneous.document.index');
Route::get('/doc_type_create', 'MiscellaneousDocTypeController@create')->name('miscellaneous.document.create');
Route::post('/doc_type_store', 'MiscellaneousDocTypeController@store')->name('miscellaneous.document.store');
Route::get('/doc_type_edit/{id}','MiscellaneousDocTypeController@edit')->name('miscellaneous.document.edit');
Route::put('/doc_type_update/{id}', 'MiscellaneousDocTypeController@update')->name('miscellaneous.document.update');
Route::get('/doc_type_destroy/{id}', 'MiscellaneousDocTypeController@destroy')->name('miscellaneous.document.destroy');


// Circular Institution
Route::get('/circular_institute', 'CircularInstitutionController@index')->name('circular.institute.index');
Route::get('/circular_institute_create', 'CircularInstitutionController@create')->name('circular.institute.create');
Route::post('/circular_institute_store', 'CircularInstitutionController@store')->name('circular.institute.store');
Route::get('/circular_institute_edit/{id}','CircularInstitutionController@edit')->name('circular.institute.edit');
Route::put('/circular_institute_update/{id}', 'CircularInstitutionController@update')->name('circular.institute.update');
Route::get('/circular_institute_destroy/{id}', 'CircularInstitutionController@destroy')->name('circular.institute.destroy');

//Court Master Data
Route::get('/court_index','CourtController@index')->name('court.index');
Route::get('/court_create','CourtController@create')->name('court.create');
Route::post('/store', 'CourtController@store')->name('court.store');
Route::get('/destroy/{id}', 'CourtController@destroy')->name('court.destroy');
Route::get('/edit','CourtController@edit')->name('court.edit');
Route::post('/court_update', 'CourtController@update')->name('court.update');

// Location
Route::get('/location_index','LocationController@index')->name('location.index');
Route::get('/location_create','LocationController@create')->name('location.create');
Route::post('/location_store', 'LocationController@store')->name('location.store');
Route::get('/location_destroy/{id}', 'LocationController@destroy')->name('location.destroy');
Route::get('/location_edit/{id}','LocationController@edit')->name('location.edit');
Route::put('/location_update/{id}', 'LocationController@update')->name('location.update');

// Department
Route::get('/department_index','DepartmentController@index')->name('department.index');
Route::get('/department_create','DepartmentController@create')->name('department.create');
Route::post('/department_store', 'DepartmentController@store')->name('department.store');
Route::get('/department_destroy/{id}', 'DepartmentController@destroy')->name('department.destroy');
Route::get('/department_edit/{id}','DepartmentController@edit')->name('department.edit');
Route::post('/department_update/{id}', 'DepartmentController@update')->name('department.update');

// Category Type
Route::get('/cat_type_index','CategoryTypeController@index')->name('categoryType.index');
Route::get('/cat_type_create','CategoryTypeController@create')->name('categoryType.create');
Route::post('/cat_type_store', 'CategoryTypeController@store')->name('categoryType.store');
Route::get('/cat_type_destroy/{id}', 'CategoryTypeController@destroy')->name('categoryType.destroy');
Route::get('/cat_type_edit/{id}','CategoryTypeController@edit')->name('categoryType.edit');
Route::put('/cat_type_update/{id}', 'CategoryTypeController@update')->name('categoryType.update');

Route::prefix('admin/role')->group(function(){
    Route::get('/', 'RoleController@index')->name('role.index');
    Route::get('/create', 'RoleController@create')->name('role.create');
    Route::post('/store', 'RoleController@store')->name('role.store');
    Route::get('/edit/{id}','RoleController@edit')->name('role.edit');
    Route::put('/update/{id}', 'RoleController@update')->name('role.update');
    Route::get('/destroy/{id}', 'RoleController@destroy')->name('role.destroy');
});

// Area of service
Route::prefix('aos')->group(function(){
    Route::get('/','AreaOfServiceController@index')->name('aos.index');
    Route::get('/tree','AreaOfServiceController@tree')->name('aos.tree');

    Route::get('/create', 'AreaOfServiceController@create')->name('aos.create');
    Route::post('/store', 'AreaOfServiceController@store')->name('aos.store');
    Route::get('/create/{id}','AreaOfServiceController@childNode')->name('aos.childNode');
    Route::get('/edit/{id}','AreaOfServiceController@edit')->name('aos.edit');
    Route::put('/update/{id}', 'AreaOfServiceController@update')->name('aos.update');
    Route::get('/destroy/{id}', 'AreaOfServiceController@destroy')->name('aos.destroy');

    Route::get('/standard_tree','AreaOfServiceController@lawyerTree')->name('lawyer.aos.tree');
    // Personal Child node
    Route::get('/personal_tree','AreaOfServiceController@personalLawyerTree')->name('personal.aos.tree');
    
    Route::get('/create_personal', 'AreaOfServiceController@createPersonalTree')->name('personal.aos.create');
    Route::get('/create_personal/{id}','AreaOfServiceController@childNodePersonal')->name('personal.aos.childNode');
    Route::post('/store_personal', 'AreaOfServiceController@personalStore')->name('personal.store');
    Route::get('/edit_personal/{id}','AreaOfServiceController@personalEdit')->name('personal.aos.edit');
    Route::put('/update_personal/{id}', 'AreaOfServiceController@personalUpdate')->name('personal.aos.update');
    Route::get('/destroy_personal/{id}', 'AreaOfServiceController@personalDestroy')->name('personal.aos.destroy');

    Route::get('/getContent', 'AreaOfServiceController@getContent')->name('personal.aos.viewContent');
    Route::get('/getSingleContent', 'AreaOfServiceController@getSingleContent')->name('personal.aos.viewSingleContent');

    // Add notes for the area of service
    Route::get('/standard_add_notes/{id}', 'AreaOfServiceController@addStandardNotes')->name('standard.aos.notes');
    Route::get('/persoanl_add_notes/{id}', 'AreaOfServiceController@addPersonalNotes')->name('personal.aos.notes');
    Route::post('/store_content', 'AreaOfServiceController@storeContent')->name('aos.content.store');
    Route::get('/edit_content/{id}','AreaOfServiceController@editContent')->name('aos.content.edit');
    Route::get('/view_content/{id}','AreaOfServiceController@showContent')->name('aos.content.view');
    Route::put('/update_content/{id}', 'AreaOfServiceController@updateContent')->name('aos.content.update');
    Route::get('/ref_notes_index/{id}/{type}','AreaOfServiceController@indexContent')->name('aos.content.index');
    Route::get('/ref_notes_lawyer/{id}/{type}/{aos}','AreaOfServiceController@getAllNotesByLawyerId')->name('aos.content.lawyer');
    Route::get('/ref_notes/{id}/{lawyer_id}','AreaOfServiceController@getAllNotesByHead')->name('aos.content.lawyer.index');

    Route::get('/destroy_content/{id}', 'AreaOfServiceController@destroyContent')->name('aos.content.destroy');

    Route::get('/get_content/{id}','AreaOfServiceController@get_content')->name('aos.content.ajax');
    Route::get('/getFullContent','AreaOfServiceController@getFullContent')->name('aos.content.ajax');
    Route::get('/parent_destroy_content/{id}', 'AreaOfServiceController@destroyTitleContent')->name('aos.parent.content.destroy');
});


Route::prefix('admin/statutes')->group(function(){
    Route::get('/','StatuteController@index')->name('statute.index');
    Route::get('/create','StatuteController@create')->name('statute.create');
    Route::post('/store', 'StatuteController@store')->name('statute.store');
    Route::get('/edit','StatuteController@edit')->name('statute.edit');
    Route::post('/update', 'StatuteController@update')->name('statute.update');
    Route::get('/destroy/{id}', 'StatuteController@destroy')->name('statute.destroy');

    Route::get('/type_index','StatuteTypeController@index')->name('statute.type.index');
    Route::get('/type_create','StatuteTypeController@create')->name('statute.type.create');
    Route::post('/type_store', 'StatuteTypeController@store')->name('statute.type.store');
    Route::get('/type_edit/{id}','StatuteTypeController@edit')->name('statute.type.edit');
    Route::post('/type_update', 'StatuteTypeController@update')->name('statute.type.update');
    Route::get('/type_destroy/{id}', 'StatuteTypeController@destroy')->name('statute.type.destroy');

    // Books section routs
    Route::get('/caseLaw/{id}', 'StatuteSectionsController@caselawWithSection')->name('caselaws');
    Route::get('/section_index','StatuteSectionsController@index')->name('statute.sections.index');
    Route::get('/section_create','StatuteSectionsController@create')->name('statute.sections.create');
    Route::get('/get_content','StatuteSectionsController@get_content')->name('statute.sections.getContent');
    Route::post('/section_store', 'StatuteSectionsController@store')->name('statute.sections.store');
    Route::get('/edit/{id}','StatuteSectionsController@edit')->name('statute.sections.edit');
    Route::post('/section_update', 'StatuteSectionsController@update')->name('statute.section.update');
    Route::get('/section_destroy/{id}', 'StatuteSectionsController@destroy')->name('statute.sections.destroy');
});


Route::prefix('news')->group(function(){
    Route::get('/','NewsController@index')->name('news.index');
    Route::get('/create','NewsController@create')->name('news.create');
    Route::post('/store', 'NewsController@store')->name('news.store');
    Route::get('/edit/{id}','NewsController@edit')->name('news.edit');
    Route::post('/update/{id}', 'NewsController@update')->name('news.update');
    Route::get('/destroy/{id}', 'NewsController@destroy')->name('news.destroy');
    Route::get('/getGroup/{id}','NewsController@getGroup')->name('news.getGroup');
});
Route::prefix('taskManagement')->group(function(){
    Route::get('/','TaskManagementController@index')->name('taskManagement.index');
    Route::get('/create/{id?}','TaskManagementController@create')->name('taskManagement.create');
    Route::post('/store','TaskManagementController@store')->name('taskManagement.store');
    Route::get('/edit/{id}','TaskManagementController@edit')->name('taskManagement.edit');
    Route::post('/update/{id}', 'TaskManagementController@update')->name('taskManagement.update');
    Route::get('/destroy/{id}','TaskManagementController@destroy')->name('taskManagement.destroy');

    Route::post('/update_after_assignee_start/{id}','TaskManagementController@update_after_assignee_start')->name('taskManagement.update_after_assignee_start');

});

Route::get('/test','HomeController@test')->name('test');